# Shop Migration



## Version 4.0.0

>	#### Attention: {.alert .alert-danger}
>
>	Please execute all upgrade wizards in install tool!

- Sizes will be converted to sizes-relation-records
- Colors will be converted to colors-relation-records
- If you have overridden the invoice PDF template, it's necessary to sync them with the recent one, in order to ensure to have proper prices in there.



### Shipping cost

The shipping costs are migrated into weight-based shipping costs. This requires every product has a weight.







### Migration of email templates

The email templates must be migrated to HTML Fluid-Mails.

- Override the email template paths:

```php
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['layoutRootPaths'][1676624511] = 'EXT:your_extension/Resources/Private/Templates/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][1676624511] = 'EXT:your_extension/Resources/Private/Templates/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['partialRootPaths'][1676624511] = 'EXT:your_extension/Resources/Private/Partials/Email/';
```

- Divide the templates into two. One for html and the other for plain text. For example:

From `customerConfirmation.html`:
```html
<div xmlns="http://www.w3.org/1999/xhtml" lang="en"
	 xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	 xmlns:shop="http://typo3.org/ns/CodingMs/Shop/ViewHelpers">
	<f:layout name="Email"/>
	<f:section name="Subject">Shop Product OnInvoice [Templates/Email/Checkout/OnInvoice/CustomerConfirmation.html]</f:section>
	<f:section name="BodyHtml">
		<p style="{paragraphStyles}">
			Dear {basketOrder.name}, thanks for your order.
		</p>
		<f:variable name="type" value="Order" />
		<f:render partial="Email/OrderSummary" arguments="{_all}"/>
	</f:section>

	<f:section name="Body">
		Dear {basketOrder.name}, thanks for your order.
		<f:variable name="type" value="Order" />
		<f:render partial="Email/OrderSummaryPlain" arguments="{_all}"/>
	</f:section>
</div>
```

To: `customerConfirmation.html` and `customerConfirmation.txt`:
```html
<div xmlns="http://www.w3.org/1999/xhtml" lang="en"
	 xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	 xmlns:shop="http://typo3.org/ns/CodingMs/Shop/ViewHelpers">
	<f:layout name="SystemEmail"/>
	<f:section name="Main">
		<p>
			Dear {basketOrder.name}, thanks for your order.
		</p>
		<f:variable name="type" value="Order" />
		<f:render partial="OrderSummary" arguments="{_all}"/>
	</f:section>
</div>
```
```text
Dear {basketOrder.name}, thanks for your order.
<f:variable name="type" value="Order" />
<f:render partial="OrderSummaryPlain" arguments="{_all}"/>
```

>	#### Attention: {.alert .alert-danger}
>
>	If you've overridden the Checkout Service, you have to add constants `CUSTOMER_CONFIRMATION_EMAIL_TEMPLATE` and `ORDER_EMAIL_TEMPLATE` to your service class.


#### Clean up

In order to clean your system up, follow the next steps:

```typo3_typoscript
# Remove TypoScript constant
themes.configuration.extension.shop.email.contentType = …
themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation = …
themes.configuration.extension.shop.email.templates.onInvoice.order = …
themes.configuration.extension.shop.email.templates.prePayment.customerConfirmation = …
themes.configuration.extension.shop.email.templates.prePayment.order = …
themes.configuration.extension.shop.email.templates.request.customerConfirmation = …
themes.configuration.extension.shop.email.templates.request.request = …

themes.configuration.extension.shop.email.templates.payPal.customerConfirmation = …
themes.configuration.extension.shop.email.templates.payPal.order = …
themes.configuration.extension.shop.email.templates.payPalPlus.customerConfirmation = …
themes.configuration.extension.shop.email.templates.payPalPlus.order = …
themes.configuration.extension.shop.email.templates.klarna.customerConfirmation = …
themes.configuration.extension.shop.email.templates.klarna.order = …
themes.configuration.extension.shop.email.templates.stripe.customerConfirmation = …
themes.configuration.extension.shop.email.templates.stripe.order = …
```

```typo3_typoscript
# Remove TypoScript setup
plugin.tx_shop.settings {
	email {
        contentType = {$themes.configuration.extension.shop.email.contentType}
	}
    orderOptions {
        onInvoice.email.customerConfirmation.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        onInvoice.email.order.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        prePayment.email.customerConfirmation.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        prePayment.email.order.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        request.email.customerConfirmation.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        request.email.request.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        payPal.email.customerConfirmation.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        payPal.email.order.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        payPalPlus.email.customerConfirmation.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        payPalPlus.email.order.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        klarna.email.customerConfirmation.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        klarna.email.order.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        stripe.email.customerConfirmation.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
        stripe.email.order.template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
	}
}
```

>   #### Attention: {.alert .alert-danger}
>
>   PHP 7.4 or higher required!

*	If you're using own Fluid-Templates, you might to add `readonly="{fieldData.readonly}"` attributes on each Checkout.html form field. There are some more changes in this Template, please compare and check if they are important for you.



## Version 3.3.0

* **IMPORTANT:** You need to execute the _Migrate datetime to integer_ install tool upgrade wizard in order to migrate some internal date information.

  **What it does in detail:**

  Converts the `order_date`, `paid_date` and `send_date` in `tx_shop_domain_model_basketorder` from type DATETIME to TIMESTAMP in database.


* **OPTIONAL:** You might need to execute the _Attach frontend user to basket order_ install tool upgrade wizard in order to assign frontend user (identified by email) to existing basket orders.

  **What it does in detail:**

  Fetch all basket orders and search for frontend user with the same email. If a match is found, the frontend user will be inserted as an owner in the basket order.


* **IMPORTANT:** You need to execute the _Migrate tax values of products to new relations with tax records_ install tool upgrade wizard in order to migrate your tax rates into tax-rate-records.

  **What it does in detail:**

  Fetch all products and identify the tax value in product record. Afterwards for each unique tax value a tax record is created in the same container as the product, and assigned to the product record.


* **IMPORTANT:** You need to execute the _Migrate net gross prices_ install tool upgrade wizard in order to migrate your prices information into the new rudimentary format.

  **What it does in detail:**

  Fetch all products and identify if the products-container/page-tree is configured as `b2b` or `b2c`. This is usually defined by TypoScript constant `themes.configuration.extension.shop.basket.displayType = b2c`. This information will be transferred into the `price_type` field in the product. Depending on this value, the only remaining `price` field (the `price_with_tax` field will be removed) will be filled with price with or without taxes.

  **New behaviour:**

  Since this version of the Shop, the TypoScript constant `themes.configuration.extension.shop.basket.displayType = b2c` is only for choosing the display type of the prices in the list view, detail view and basket. Each product owns now a `price_type` which defines if the giving price is _net_ or _gross_ - there's no need to have net+gross prices in the database.

* **IMPORTANT for Stripe users:** After switching from Charge API to Payment Intent API in Stripe checkout the changes in `EXT:shop_pro/Resources/Templates/BasketOrder/List.html` template have to be respected
* If you're using own Fluid-Templates, you need to add a `<f:flashMessages />` in the `EXT:shop/Resources/Templates/Product/List.html` in order to display important messages.



## Version 3.2.0

* To select a product image for the preview in the product list, you have to upload an image and check the `Show in preview` checkbox. The first image is no longer automatically displayed as a preview image.



## Version 3.0.0

* If you're using own Fluid-Templates, you might need to remove the parameter `noCacheHash="1"`.



## Version 2.10.0

*   We have move the PayPal-Plus JavaScript file from TypoScript into CheckoutOrder.html Fluid-Template, because otherwise it will be always loaded. If you've overridden this Template and you're using PayPal-Plus, you need to add the required script-Tag:
    ```xml
    <f:if condition="{configuration.type} == 'payPalPlus'">
        <script src="https://www.paypalobjects.com/webstatic/ppplus/ppplus.min.js" type="text/javascript"></script>
    </f:if>
    ```
*   Additionally we've added some Fluid lines in `BasketOrder/ConfirmOrder` template, in order to provide Klarna payment.



## Version 2.0.0

>   #### Attention: {.alert .alert-danger}
>
>   PHP 7.1 or higher required!


### Migration steps

*   Remove Meta-Tag and Title ViewHelper in Fluid-Templates.
*   Products now have record types, which have optionally downloads or questions. After updating you have set the required record type within your products.
*   Price and offer_rebate_value is changed from varchar to int (in cent) - this values needs to be converted.
*   Tax is changed to new percent TCA/form engine field - this value might needs to be converted.
*   Some TypoScript identifier were renamed:
    *   Old: `settings.show.pageUid` -> New: `settings.detail.defaultPid`
    *   ATTENTION: In case of your details-links won't work, try to replace your plugin content-element with a new one! It might be, that the Flex form still contain an old setting for `settings.detail.defaultPid`!
*   The partial `Product/AddToBasket` is moved to `Product/Buttons/AddToBasket` - you need to modify your f:render usages.
*   TypoScript filter setting `filter.concatenateMultipleTagsBy` has been renamed to `filter.tag.concatenate`
