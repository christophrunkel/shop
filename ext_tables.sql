#
# Table structure for table 'tx_shop_domain_model_basket'
#
CREATE TABLE tx_shop_domain_model_basket (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	basket_items int(11) unsigned DEFAULT '0' NOT NULL,
	price int(11) DEFAULT '0' NOT NULL,
	data text,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_basketitem'
#
CREATE TABLE tx_shop_domain_model_basketitem (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	quantity int(11) DEFAULT '0' NOT NULL,
	product int(11) unsigned DEFAULT '0',
	price int(11) DEFAULT '0' NOT NULL,
	tax int(11) DEFAULT '0' NOT NULL,
	tax_calculation varchar(255) DEFAULT '' NOT NULL,
	price_with_tax int(11) DEFAULT '0' NOT NULL,
	basket int(11) unsigned DEFAULT '0',
	custom_information text,
	custom_information_type varchar(255) DEFAULT '' NOT NULL,
	custom_information_label varchar(255) DEFAULT '' NOT NULL,
	custom_information_required tinyint(1) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_basketorder'
#
CREATE TABLE tx_shop_domain_model_basketorder (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	information varchar(255) DEFAULT '' NOT NULL,
	status varchar(255) DEFAULT '' NOT NULL,
	processed smallint(5) DEFAULT 0 NOT NULL,
	type varchar(255) DEFAULT '' NOT NULL,
	name varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	vat_id varchar(255) DEFAULT '' NOT NULL,
	vat_zone varchar(255) DEFAULT '' NOT NULL,
	vatable smallint(5) DEFAULT 0 NOT NULL,

	price int(11) DEFAULT '0' NOT NULL,
	tax int(11) DEFAULT '0' NOT NULL,
	tax_information varchar(255) DEFAULT '' NOT NULL,
	price_with_tax int(11) DEFAULT '0' NOT NULL,

	ordered tinyint(4) unsigned DEFAULT '0' NOT NULL,
	order_date int(11) DEFAULT '0' NOT NULL,

	paid tinyint(4) unsigned DEFAULT '0' NOT NULL,
	paid_date int(11) DEFAULT '0' NOT NULL,
	paid_price int(11) DEFAULT '0' NOT NULL,

	send tinyint(4) unsigned DEFAULT '0' NOT NULL,
	send_date int(11) DEFAULT '0' NOT NULL,

	invoice_number int(11) DEFAULT '0' NOT NULL,
	time_for_payment int(11) DEFAULT '0' NOT NULL,

	email_copy text,
	params text,
	callback_parameter text,
	basket int(11) unsigned DEFAULT '0' NOT NULL,

	frontend_user int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_product'
#
CREATE TABLE tx_shop_domain_model_product (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	information varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	slug varchar(255) DEFAULT '' NOT NULL,
	subtitle varchar(255) DEFAULT '' NOT NULL,
	product_no varchar(255) DEFAULT '' NOT NULL,
	product_type int(11) unsigned DEFAULT '0' NOT NULL,
	price int(11) DEFAULT '0' NOT NULL,
	price_type varchar(255) DEFAULT '' NOT NULL,
	tax int(11) DEFAULT '0' NOT NULL,
	graduated_prices int(11) DEFAULT '0' NOT NULL,
	discount int(11) DEFAULT '0' NOT NULL,
	teaser text,
	description text,
	description_file int(11) DEFAULT '0' NOT NULL,
	pdf_description text DEFAULT '' NOT NULL,
	digital_product int(4) unsigned DEFAULT 0 NOT NULL,
	unit varchar(255) DEFAULT '' NOT NULL,
	unit_factor int(11) DEFAULT '0' NOT NULL,
	www varchar(255) DEFAULT '' NOT NULL,
	in_stock tinyint(1) unsigned DEFAULT '0' NOT NULL,
	stock_amount int(11) DEFAULT '0' NOT NULL,
	weight varchar(255) DEFAULT '' NOT NULL,
	bulky tinyint(4) unsigned DEFAULT '0' NOT NULL,
	offer tinyint(4) unsigned DEFAULT '0' NOT NULL,
	offer_rebate_value int(11) DEFAULT '0' NOT NULL,
	highlight tinyint(4) unsigned DEFAULT '0' NOT NULL,
	first_in_list tinyint(4) unsigned DEFAULT '0' NOT NULL,
	color varchar(255) DEFAULT '' NOT NULL,
	size varchar(255) DEFAULT '' NOT NULL,
	views int(11) DEFAULT '0' NOT NULL,
	views_day int(11) DEFAULT '0' NOT NULL,
	views_week int(11) DEFAULT '0' NOT NULL,
	views_month int(11) DEFAULT '0' NOT NULL,
	views_year int(11) DEFAULT '0' NOT NULL,
	views_last_update int(11) DEFAULT '0' NOT NULL,
	#
	# Tab: SEO
	canonical_link varchar(255) DEFAULT '' NOT NULL,
	html_title varchar(255) DEFAULT '' NOT NULL,
	meta_abstract varchar(255) DEFAULT '' NOT NULL,
	meta_description varchar(255) DEFAULT '' NOT NULL,
	meta_keywords varchar(255) DEFAULT '' NOT NULL,
	#
	parent int(11) unsigned DEFAULT '0' NOT NULL,
	variants int(11) unsigned DEFAULT '0' NOT NULL,
	#
	#
	attribute1 text,
	attribute2 text,
	attribute3 text,
	attribute4 text,
	attribute5 text,
	attribute6 text,
	attribute7 text,
	attribute8 text,
	#
	# Tab: relations
	categories int(11) unsigned DEFAULT '0' NOT NULL,
	tags int(11) unsigned DEFAULT '0' NOT NULL,
	related_products int(11) unsigned DEFAULT '0' NOT NULL,
	file_collections int(11) unsigned DEFAULT '0' NOT NULL,
	files int(11) unsigned DEFAULT '0' NOT NULL,
	question_categories int(11) unsigned DEFAULT '0' NOT NULL,
	attach_frontend_user_groups text,
	#
	# Tab: images
	images int(11) unsigned DEFAULT '0' NOT NULL,
	feature_icons int(11) unsigned DEFAULT '0' NOT NULL,
	other_images int(11) unsigned DEFAULT '0' NOT NULL,
	#
	# Tab: videos
	videos int(11) unsigned DEFAULT '0' NOT NULL,
	#
	# Tab: attributes
	attributes int(11) unsigned DEFAULT '0' NOT NULL,
	#
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_producttype'
#
CREATE TABLE tx_shop_domain_model_producttype (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	filter varchar(255) DEFAULT '' NOT NULL,
	notice varchar(255) DEFAULT '' NOT NULL,
	image int(11) unsigned DEFAULT '0' NOT NULL,
	import_id varchar(255) DEFAULT '' NOT NULL,

	title_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	title_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	slug_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	slug_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	subtitle_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	subtitle_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	product_no_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	product_no_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	product_type_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	product_type_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	teaser_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	teaser_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	digital_product_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	digital_product_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	description_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	description_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	pdf_description_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	pdf_description_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	description_file_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	description_file_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	www_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	www_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	weight_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	weight_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	bulky_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	bulky_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	highlight_enabled tinyint(4) unsigned DEFAULT '0' NOT NULL,
	highlight_enabled_variant tinyint(4) unsigned DEFAULT '0' NOT NULL,
	first_in_list_enabled tinyint(4) unsigned DEFAULT '0' NOT NULL,
	first_in_list_enabled_variant tinyint(4) unsigned DEFAULT '0' NOT NULL,
	color_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	color_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	size_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	size_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	#
	# Tab: SEO
	canonical_link_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	canonical_link_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	html_title_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	html_title_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	meta_abstract_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	meta_abstract_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	meta_description_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	meta_description_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	meta_keywords_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	meta_keywords_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	#
	# Tab: prices
	price_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	price_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	price_type_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	price_type_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	tax_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	tax_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	offer_enabled tinyint(4) unsigned DEFAULT '0' NOT NULL,
	offer_enabled_variant tinyint(4) unsigned DEFAULT '0' NOT NULL,
	offer_rebate_value_enabled tinyint(4) unsigned DEFAULT '0' NOT NULL,
	offer_rebate_value_enabled_variant tinyint(4) unsigned DEFAULT '0' NOT NULL,
	stock_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	stock_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	unit_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	unit_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	discount_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	discount_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	graduated_prices_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	graduated_prices_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	#
	# Tab: relations
	categories_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	categories_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	tags_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	tags_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	related_products_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	related_products_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	file_collections_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	file_collections_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	files_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	files_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	question_categories_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	question_categories_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attach_frontend_user_groups_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attach_frontend_user_groups_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	#
	# Tab: images
	images_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	images_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	feature_icons_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	feature_icons_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	other_images_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	other_images_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	#
	# Tab: videos
	videos_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	videos_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	#
	# Tab: attributes
	attributes_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attributes_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	#
	# Tab: custom
	#
	# Custom information during the ordering process
	custom_information_information varchar(255) DEFAULT '' NOT NULL,
	custom_information_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	custom_information_required tinyint(1) unsigned DEFAULT '0' NOT NULL,
	custom_information_label varchar(255) DEFAULT '' NOT NULL,
	custom_information_type varchar(255) DEFAULT '' NOT NULL,
	#
	# Custom product attributes
	attribute1_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute1_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute1_label varchar(255) DEFAULT '' NOT NULL,
	attribute1_description text,
	attribute1_type varchar(255) DEFAULT '' NOT NULL,
	attribute2_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute2_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute2_label varchar(255) DEFAULT '' NOT NULL,
	attribute2_description text,
	attribute2_type varchar(255) DEFAULT '' NOT NULL,
	attribute3_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute3_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute3_label varchar(255) DEFAULT '' NOT NULL,
	attribute3_description text,
	attribute3_type varchar(255) DEFAULT '' NOT NULL,
	attribute4_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute4_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute4_label varchar(255) DEFAULT '' NOT NULL,
	attribute4_description text,
	attribute4_type varchar(255) DEFAULT '' NOT NULL,
	attribute5_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute5_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute5_label varchar(255) DEFAULT '' NOT NULL,
	attribute5_description text,
	attribute5_type varchar(255) DEFAULT '' NOT NULL,
	attribute6_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute6_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute6_label varchar(255) DEFAULT '' NOT NULL,
	attribute6_description text,
	attribute6_type varchar(255) DEFAULT '' NOT NULL,
	attribute7_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute7_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute7_label varchar(255) DEFAULT '' NOT NULL,
	attribute7_description text,
	attribute7_type varchar(255) DEFAULT '' NOT NULL,
	attribute8_enabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute8_enabled_variant tinyint(1) unsigned DEFAULT '0' NOT NULL,
	attribute8_label varchar(255) DEFAULT '' NOT NULL,
	attribute8_description text,
	attribute8_type varchar(255) DEFAULT '' NOT NULL,
	#
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_product_product_mm'
#
CREATE TABLE tx_shop_product_product_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_shop_domain_model_productcategory'
#
CREATE TABLE tx_shop_domain_model_productcategory (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	image int(11) unsigned DEFAULT '0' NOT NULL,
	import_id varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_product_productcategory_mm'
#
CREATE TABLE tx_shop_product_productcategory_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_shop_domain_model_producttag'
#
CREATE TABLE tx_shop_domain_model_producttag (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description varchar(255) DEFAULT '' NOT NULL,
	variant varchar(255) DEFAULT '' NOT NULL,
	category varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_producttagcategory'
#
CREATE TABLE tx_shop_domain_model_producttagcategory (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description text,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_productattribute'
#
CREATE TABLE tx_shop_domain_model_productattribute (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title text,
	value text,
	category int(11) DEFAULT '0' NOT NULL,
	product int(11) DEFAULT '0' NOT NULL,
	import_id varchar(255) DEFAULT '' NOT NULL,

	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_productattributecategory'
#
CREATE TABLE tx_shop_domain_model_productattributecategory (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	template varchar(255) DEFAULT 'List' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state TEXT DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_productgraduatedprice'
#
CREATE TABLE tx_shop_domain_model_productgraduatedprice (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	quantity int(11) DEFAULT '0' NOT NULL,
	price int(11) DEFAULT '0' NOT NULL,
	product int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state TEXT DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_productshippingcost'
#
CREATE TABLE tx_shop_domain_model_productshippingcost (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	label varchar(255) DEFAULT '' NOT NULL,
	label_public varchar(255) DEFAULT '' NOT NULL,
	label_bulky_public varchar(255) DEFAULT '' NOT NULL,
	weight int(11) DEFAULT '0' NOT NULL,
	bulky int(11) DEFAULT '0' NOT NULL,
	price int(11) DEFAULT '0' NOT NULL,
	overlay int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state TEXT DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_productdiscount'
#
CREATE TABLE tx_shop_domain_model_productdiscount (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	label varchar(255) DEFAULT '' NOT NULL,
	label_public varchar(255) DEFAULT '' NOT NULL,
	type varchar(255) DEFAULT '' NOT NULL,
	order_option varchar(255) DEFAULT '' NOT NULL,
	value_fixed int(11) DEFAULT '0' NOT NULL,
	value_percent int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state TEXT DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_productcolor'
#
CREATE TABLE tx_shop_domain_model_productcolor (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	import_id varchar(255) DEFAULT '' NOT NULL,
	hex varchar(7) DEFAULT '' NOT NULL,
	image int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state TEXT DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_domain_model_productsize'
#
CREATE TABLE tx_shop_domain_model_productsize (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	import_id varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state TEXT DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_shop_product_productgraduatedprice_mm'
#
CREATE TABLE tx_shop_product_graduatedprice_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_shop_product_producttag_mm'
#
CREATE TABLE tx_shop_product_producttag_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_shop_product_productfilecollection_mm'
#
CREATE TABLE tx_shop_product_productfilecollection_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_shop_product_productquestioncategories_mm'
#
CREATE TABLE tx_shop_product_productquestioncategories_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'sys_file_reference'
#
CREATE TABLE sys_file_reference (
	showinpreview tinyint(4) DEFAULT '0' NOT NULL,
	attach_to_email tinyint(4) DEFAULT '0' NOT NULL,
	filename varchar(255) DEFAULT '' NOT NULL
);

#
# Table structure for table 'tx_shop_domain_model_producttax'
#
CREATE TABLE tx_shop_domain_model_producttax
(
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	value int(11) DEFAULT '0' NOT NULL,
	stripe_id varchar(255) DEFAULT '' NOT NULL,
	overlay int(11) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid)

) AUTO_INCREMENT = 10000;

#
# Table structure for table 'tx_shop_domain_model_producttaxcountryoverlay'
#
CREATE TABLE tx_shop_domain_model_producttaxcountryoverlay
(
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	country_code varchar(255) DEFAULT '' NOT NULL,
	value int(11) DEFAULT '0' NOT NULL,
	default_tax int(11) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid)

);

#
# Table structure for table 'tx_shop_domain_model_productshippingcostcountryoverlay'
#
CREATE TABLE tx_shop_domain_model_productshippingcostcountryoverlay
(
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	country_code varchar(255) DEFAULT '' NOT NULL,
	price int(11) DEFAULT '0' NOT NULL,
	bulky int(11) DEFAULT '0' NOT NULL,
	default_shipping_cost int(11) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid)

);

#
# Table structure for table 'fe_users'
#
CREATE TABLE fe_users
(
	tx_shop_stripe_id varchar(255) DEFAULT '' NOT NULL,

	tx_shop_order_group int(11) unsigned DEFAULT '0' NOT NULL,
	tx_shop_order_group_orders_visible smallint(5) DEFAULT 0 NOT NULL,
);

#
# Table structure for table 'tx_shop_domain_model_productclickcount'
#
CREATE TABLE tx_shop_domain_model_productclickcount (
	product int(11) DEFAULT '0' NOT NULL,
	click_timestamp int(11) DEFAULT '0' NOT NULL
);
