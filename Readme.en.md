# Shop Extension for TYPO3

This extension is an extensive shop for TYPO3 for realizing Webshops, Online-Shops or Payment-Subscriptions (comparable with tt_products, Quick-Shop or Aimeos).


**Features:**

*   Quicksearch for homepage or sidebar
*   Search filter and custom list views
*   Teaser element for homepage or sidebar
*   HTML or Plain text email support
*   Minimum order value configurable
*   Product types and variants can be individually configured
*   Group products using categories (Multiple assignment possible)
*   Group products using tags and tag categories (Multiple assignment possible)
*   Graduated prices for products
*   Various shipping costs per product, based on weight and delivery country
*   Highlight or push products to top of a list by checkbox
*   Custom product attributes (categorizable) which extends the product compare sheet
*   B2B and B2C preconfigured
*   Simple stock logic with automated reduction
*   Checkout as request, on-invoice or pre-payment
*   Easily customizable checkout form incl. divergent optional delivery address
*   Statistics about product views (je letzter Tag, Monat, Jahr und insgesamt)
*   Documents in products possible (static selection, by category, by folder)
*   FAQ in products via EXT:questions possible
*   Automatic gross/net price calculation by predefined tax rates
*   Tax rates can be configured differently depending on delivery country

**Pro-Features:**

*   Isotope support for list view
*   Frontend-User plugin for viewing own basket orders in frontend
*   Backend Dashboard widgets for top clicked products, recent basket orders and top selling products
*   Backend module for managing products and orders
*   Backend module for inspecting statistics on visitors and purchases
*   Attachment of frontend user groups on successful purchase possible
*   Automatic attachment of product documents to the purchase confirmation email possible. E.g. to provide digital purchases.
*   PDF creation for product sheets (Data sheet, Invoice, Delivery note)
*   Digital products without quantity etc. (for example for ebooks or similar)
*   Customer information and instructions on the shopping cart products in order to be able to customize products.
*   PayPal checkout integration
*   PayPal Plus checkout integration
*   Klarna checkout integration (pay_now, Klarna Payments)
*   Stripe checkout integration (credit card)
*   Stripe subscription system for portals with recurring payments
*   Bookmarks feature for products
*   Compare feature for bookmarked products
*   Automatic invoice and delivery note generation
*   Resend order mails including invoice PDF for backend user
*   Manual order creation in backend module
*   Attach delivery note to admin or customer mail
*   Attach order xml to admin email or store as xml file in file system
*   UPS API connection possible (EXT:ups_api required)
*   Overview of orders in the front end also possible across users (users grouped by customer/company, for example)
*   Setting up a corporate fashing shop possible
*   Custom configurable additional attributes in products, based on product types/variants (including typing, own labels in FE+BE, etc.)

If you need some additional or custom feature - get in contact!


**Links:**

*   [Shop Documentation](https://www.coding.ms/documentation/typo3-shop "Shop Documentation")
*   [Shop Bug-Tracker](https://gitlab.com/codingms/typo3-shop/shop/-/issues "Shop Bug-Tracker")
*   [Shop Repository](https://gitlab.com/codingms/typo3-shop/shop "Shop Repository")
*   [TYPO3 Shop Productdetails](https://www.coding.ms/typo3-extensions/typo3-shop/ "TYPO3 Shop Productdetails")
*   [TYPO3 Shop Documentation](https://www.coding.ms/documentation/typo3-shop/ "TYPO3 Shop Documentation")
*   [TYPO3 Shop Demo](https://shop-12.typo3-demos.de/ "TYPO3 Shop Demo")
*   [Reference: Niedersachsen Gin](https://www.niedersachsen-gin.de/ "Reference: Niedersachsen Gin")
*   [Reference: coding.ms by InnoCoding GmbH](https://www.coding.ms/de/typo3-extensions/ "Reference: coding.ms by InnoCoding GmbH")
