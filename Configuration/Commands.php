<?php

return [
    'shop:testmail' => [
        'class' => \CodingMs\Shop\Command\TestmailCommand::class
    ],
    'shop:updateViewCount' => [
        'class' => \CodingMs\Shop\Command\UpdateViewCountCommand::class
    ],
];
