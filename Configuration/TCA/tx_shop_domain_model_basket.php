<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_basket';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'crdate',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => '',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-shop-basket']
    ],
    'types' => [
        '1' => ['showitem' => 'basket_items, data'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\AdditionalTca\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\AdditionalTca\Tca\Configuration::full('endtime'),
        'basket_items' => [
            'exclude' => 0,
            'label' => $lll . '.basket_items',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_shop_domain_model_basketitem',
                'foreign_field' => 'basket',
                'minitems' => 1,
                'maxitems' => 999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'both',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                    'enabledControls' => [
                        'info' => false,
                        'new' => false,
                        'dragdrop' => false,
                        'sort' => false,
                        'hide' => false,
                        'delete' => false
                    ],
                ],
            ],
        ],
        'data' => [
            'exclude' => 0,
            'label' => $lll . '.data',
            'config' => \CodingMs\Shop\Tca\Configuration::get('textareaLarge'),
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
