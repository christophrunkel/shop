<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_productcategory';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'label_alt' => 'description',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'default_sortby' => 'sorting',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'title,description,image,import_id',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-shop-productcategory'],
        'accessUtility' => \CodingMs\ShopPro\Utility\AccessUtility::class
    ],
    'types' => [
        '1' => ['showitem' => 'title,import_id,description,image,--div--;LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_productcategory.tab_language,sys_language_uid,--palette--,l10n_parent,l10n_diffsource,--div--;LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_productcategory.tab_access,hidden,--palette--;;1'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'crdate' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'tstamp' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'cruser_id' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'import_id' => [
            'exclude' => 1,
            'label' => $lll . '.import_id',
            'description' => $lll . '.import_id.description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', false, true),
        ],
        'description' => [
            'exclude' => 0,
            'label' => $lll . '.description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('rte'),
        ],
        'image' => [
            'exclude' => 0,
            'label' => $lll . '.image',
            'config' => \CodingMs\Shop\Tca\Configuration::get('imageSingleAltTitle'),
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
