<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_productshippingcostcountryoverlay';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'hideTable' => true,
        'label' => 'country_code',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'delete' => 'deleted',
        'searchFields' => 'country_code,value',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-shop-tax'
        ],
    ],
    'types' => [
        '1' => ['showitem' => '--palette--;;country_code_price_bulky'],
    ],
    'palettes' => [
        'country_code_price_bulky' => [
            'showitem' => 'country_code,price,bulky',
        ],
    ],
    'columns' => [
        'country_code' => [
            'exclude' => 0,
            'label' => $lll . '.country_code',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'static_countries',
                'allowNonIdValues' => true,
                'foreign_table_where' => 'ORDER BY static_countries.cn_short_en',
                'itemsProcFunc' => 'SJBR\\StaticInfoTables\\Hook\\Backend\\Form\\FormDataProvider\\TcaSelectItemsProcessor->translateCountriesSelector',
                'itemsProcFunc_config' => [
                    'indexField' => 'cn_iso_2',
                ],
                'eval' => 'trim',
            ],
        ],
        'price' => [
            'exclude' => 0,
            'label' => $lll . '.price',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency', true),
        ],
        'bulky' => [
            'exclude' => 0,
            'label' => $lll . '.bulky',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency', true),
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
