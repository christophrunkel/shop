<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_basketorder';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'type' => 'type',
        'label' => 'name',
        'label_alt' => 'email',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => '',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-shop-basketorder'],
        'accessUtility' => \CodingMs\ShopPro\Utility\AccessUtility::class
    ],
    'types' => [
        '1' => ['showitem' => '
                information,
                --palette--;;status_type,
                --palette--;' . $lll . '.palette_buyer;buyer,
                --palette--;' . $lll . '.palette_price_tax;price_tax,
                --palette--;;ordered_information,
                --palette--;;paid_paid_date_paid_price,
                --palette--;;send_send_date,
                processed,
                frontend_user,
            --div--;' . $lll . '.tab_basket,
                basket,
            --div--;' . $lll . '.tab_email_copy,
                email_copy,
            --div--;' . $lll . '.tab_params,
                params,
            --div--;' . $lll . '.tab_callback,
                callback_parameter
            '
        ],
        'request' => ['showitem' => '
                --palette--;;status_type,
                --palette--;' . $lll . '.palette_buyer;buyer,
                processed,
            --div--;' . $lll . '.tab_basket,
                basket,
            --div--;' . $lll . '.tab_email_copy,
                email_copy,
            --div--;' . $lll . '.tab_params,
                params,
            --div--;' . $lll . '.tab_callback,
                callback_parameter
            '
        ],
        'onInvoice' => ['showitem' => '
                --palette--;;status_type,
                --palette--;' . $lll . '.palette_buyer;buyer,
                --palette--;' . $lll . '.palette_price_tax;price_tax,
                --palette--;;ordered_information,
                --palette--;;paid_paid_date_paid_price,
                --palette--;;send_send_date,
                processed,
            --div--;' . $lll . '.tab_basket,
                basket,
            --div--;' . $lll . '.tab_email_copy,
                email_copy,
            --div--;' . $lll . '.tab_params,
                params,
            --div--;' . $lll . '.tab_callback,
                callback_parameter
            '
        ],
        'payPal' => ['showitem' => '
                --palette--;;status_type,
                --palette--;' . $lll . '.palette_buyer;buyer,
                --palette--;' . $lll . '.palette_price_tax;price_tax,
                --palette--;;ordered_information,
                --palette--;;paid_paid_date_paid_price,
                --palette--;;send_send_date,
                processed,
            --div--;' . $lll . '.tab_basket,
                basket,
            --div--;' . $lll . '.tab_email_copy,
                email_copy,
            --div--;' . $lll . '.tab_params,
                params,
            --div--;' . $lll . '.tab_callback,
                callback_parameter
            '
        ],
        'payPalPlus' => ['showitem' => '
                --palette--;;status_type,
                --palette--;' . $lll . '.palette_buyer;buyer,
                --palette--;' . $lll . '.palette_price_tax;price_tax,
                --palette--;;ordered_information,
                --palette--;;paid_paid_date_paid_price,
                --palette--;;send_send_date,
                processed,
            --div--;' . $lll . '.tab_basket,
                basket,
            --div--;' . $lll . '.tab_email_copy,
                email_copy,
            --div--;' . $lll . '.tab_params,
                params,
            --div--;' . $lll . '.tab_callback,
                callback_parameter
            '
        ],
        'klarna' => ['showitem' => '
                --palette--;;status_type,
                --palette--;' . $lll . '.palette_buyer;buyer,
                --palette--;' . $lll . '.palette_price_tax;price_tax,
                --palette--;;ordered_information,
                --palette--;;paid_paid_date_paid_price,
                --palette--;;send_send_date,
                processed,
            --div--;' . $lll . '.tab_basket,
                basket,
            --div--;' . $lll . '.tab_email_copy,
                email_copy,
            --div--;' . $lll . '.tab_params,
                params,
            --div--;' . $lll . '.tab_callback,
                callback_parameter
            '
        ],
        'stripe' => ['showitem' => '
                --palette--;;status_type,
                --palette--;' . $lll . '.palette_buyer;buyer,
                --palette--;' . $lll . '.palette_price_tax;price_tax,
                --palette--;;ordered_information,
                --palette--;;paid_paid_date_paid_price,
                --palette--;;send_send_date,
                processed,
            --div--;' . $lll . '.tab_basket,
                basket,
            --div--;' . $lll . '.tab_email_copy,
                email_copy,
            --div--;' . $lll . '.tab_params,
                params,
            --div--;' . $lll . '.tab_callback,
                callback_parameter
            '
        ],
        'prePayment' => ['showitem' => '
                --palette--;;status_type,
                --palette--;' . $lll . '.palette_buyer;buyer,
                --palette--;' . $lll . '.palette_price_tax;price_tax,
                --palette--;;ordered_information,
                --palette--;;paid_paid_date_paid_price,
                --palette--;;send_send_date,
                processed,
            --div--;' . $lll . '.tab_basket,
                basket,
            --div--;' . $lll . '.tab_email_copy,
                email_copy,
            --div--;' . $lll . '.tab_params,
                params,
            --div--;' . $lll . '.tab_callback,
                callback_parameter
            '
        ],
        'manual' => ['showitem' => '
                --palette--;;status_type,
                --palette--;' . $lll . '.palette_buyer;buyer,
                --palette--;' . $lll . '.palette_price_tax;price_tax,
                --palette--;;ordered_information,
                --palette--;;paid_paid_date_paid_price,
                --palette--;;send_send_date,
                processed,
            --div--;' . $lll . '.tab_basket,
                basket,
            --div--;' . $lll . '.tab_params,
                params
            ',
            'columnsOverrides' => [
                'params' => [
                    'config' => [
                        'type' => 'user',
                        'renderType' => 'BasketOrderDataForm'
                    ]
                ],
                'basket' => [
                    'config' => [
                        'overrideChildTca' => [
                            'types' => [
                                '1' => ['showitem' => 'basket_items']
                            ],
                            'columns' => [
                                'basket_items' => [
                                    'config' => [
                                        'readOnly' => false,
                                        'appearance' => [
                                            'levelLinksPosition' => 'both',
                                            'enabledControls' => [
                                                'info' => true,
                                                'new' => true,
                                                'dragdrop' => true,
                                                'sort' => true,
                                                'hide' => true,
                                                'delete' => true
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                    ]
                ]
            ]
        ],
    ],
    'palettes' => [
        'status_type' => ['showitem' => 'status, type', 'canNotCollapse' => 1],
        'price_tax' => ['showitem' => 'price, tax, price_with_tax, --linebreak--, tax_information, --linebreak--, vatable, --linebreak--, vat_id, vat_zone', 'canNotCollapse' => 1],
        'buyer' => ['showitem' => 'name, email, --linebreak--, frontend_user', 'canNotCollapse' => 1],
        'ordered_information' => ['showitem' => 'ordered, order_date, --linebreak--, invoice_number, time_for_payment', 'canNotCollapse' => 1],
        'paid_paid_date_paid_price' => ['showitem' => 'paid, paid_date, --linebreak--, paid_price', 'canNotCollapse' => 1],
        'send_send_date' => ['showitem' => 'send, send_date,', 'canNotCollapse' => 1],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', '', $extKey),
        'sys_language_uid' => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\AdditionalTca\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\AdditionalTca\Tca\Configuration::full('endtime'),
        'tstamp' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'cruser_id' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'status' => [
            'exclude' => 0,
            'label' => $lll . '.status',
            'config' => \CodingMs\Shop\Tca\Configuration::get('select', true, false, '', [
                ['label' => $lll . '.prepared', 'value' => 'prepared'], // checkout:all -> during checkout process
                ['label' => $lll . '.ordered', 'value' => 'ordered'], // checkout:onInvoice -> successfully ordered
                ['label' => $lll . '.paid', 'value' => 'paid'], // checkout:payPal -> IPN was valid
                ['label' => $lll . '.canceled', 'value' => 'canceled'], // checkout:all -> Something went wrong or was aborted
                ['label' => $lll . '.requested', 'value' => 'requested'], // checkout:request -> successfully requested
            ]),
        ],
        'type' => [
            'exclude' => 0,
            'label' => $lll . '.type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('select', true, false, '', [
                ['label' => $lll . '.manual', 'value' => 'manual'],
                ['label' => $lll . '.request', 'value' => 'request'],
                ['label' => $lll . '.oninvoice', 'value' => 'onInvoice'],
                ['label' => $lll . '.prepayment', 'value' => 'prePayment'],
                ['label' => $lll . '.paypal', 'value' => 'payPal'],
                ['label' => $lll . '.paypalplus', 'value' => 'payPalPlus'],
                ['label' => $lll . '.klarna', 'value' => 'klarna'],
                ['label' => $lll . '.stripe', 'value' => 'stripe'],
            ]),
        ],
        'invoice_number' => [
            'exclude' => 0,
            'label' => $lll . '.invoice_number',
            'config' => \CodingMs\Shop\Tca\Configuration::get('int'),
        ],
        'time_for_payment' => [
            'exclude' => 0,
            'label' => $lll . '.time_for_payment',
            'config' => \CodingMs\Shop\Tca\Configuration::get('int'),
        ],
        'processed' => [
            'exclude' => 0,
            'label' => $lll . '.processed',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.processed_label'),
        ],
        'frontend_user' => [
            'exclude' => 0,
            'label' => $lll . '.frontend_user',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('frontendUserSingle'),
        ],
        'crdate' => [
            'exclude' => 0,
            'label' => $lll . '.crdate',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('dateTime', false, true, '', ['dbType' => 'timestamp']),
        ],
        'tax_information' => [
            'exclude' => 0,
            'label' => '',
            'config' => [
                'type' => 'none',
                'renderType' => 'TaxInformation'
            ]
        ],
        'email' => [
            'exclude' => 0,
            'label' => $lll . '.email',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'name' => [
            'exclude' => 0,
            'label' => $lll . '.name',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'vat_id' => [
            'exclude' => 0,
            'label' => $lll . '.vat_id',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'vat_zone' => [
            'exclude' => 0,
            'label' => $lll . '.vat_zone',
            'config' => \CodingMs\Shop\Tca\Configuration::get('select', true, false, '', [
                ['label' => 'Inland', 'value' => 'inland'],
                ['label' => 'European Union', 'value' => 'europeanUnion'],
                ['label' => 'Third Country', 'value' => 'thirdCountry'],
            ]),
        ],
        'vatable' => [
            'exclude' => 0,
            'label' => $lll . '.vatable',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.vatable_label'),
        ],
        'price' => [
            'exclude' => 0,
            'label' => $lll . '.price',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency', false, true),
        ],
        'tax' => [
            'exclude' => 0,
            'label' => $lll . '.tax',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency', false, true),
        ],
        'price_with_tax' => [
            'exclude' => 0,
            'label' => $lll . '.price_with_tax',
            'description' => $lll . '.price_with_tax_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency', false, true),
        ],
        'ordered' => [
            'exclude' => 0,
            'label' => $lll . '.ordered',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.ordered_label'),
            'displayCond' => 'FIELD:type:!=:request'
        ],
        'order_date' => [
            'exclude' => 0,
            'label' => $lll . '.order_date',
            'config' => \CodingMs\Shop\Tca\Configuration::get('dateTime'),
            'displayCond' => 'FIELD:type:!=:request'
        ],
        'paid' => [
            'exclude' => 0,
            'label' => $lll . '.paid',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.paid_label'),
            'displayCond' => 'FIELD:type:!=:request'
        ],
        'paid_date' => [
            'exclude' => 0,
            'label' => $lll . '.paid_date',
            'config' => \CodingMs\Shop\Tca\Configuration::get('dateTime'),
            'displayCond' => 'FIELD:type:!=:request'
        ],
        'paid_price' => [
            'exclude' => 0,
            'label' => $lll . '.paid_price',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency'),
            'displayCond' => 'FIELD:type:!=:request'
        ],
        'send' => [
            'exclude' => 0,
            'label' => $lll . '.send',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.send_label'),
            'displayCond' => 'FIELD:type:!=:request'
        ],
        'send_date' => [
            'exclude' => 0,
            'label' => $lll . '.send_date',
            'config' => \CodingMs\Shop\Tca\Configuration::get('dateTime'),
            'displayCond' => 'FIELD:type:!=:request'
        ],
        'email_copy' => [
            'exclude' => 0,
            'label' => $lll . '.email_copy',
            'config' => \CodingMs\Shop\Tca\Configuration::get('textareaLarge'),
        ],
        'params' => [
            'exclude' => 0,
            'label' => $lll . '.params',
            'config' => \CodingMs\Shop\Tca\Configuration::get('textareaLarge'),
        ],
        'callback_parameter' => [
            'exclude' => 0,
            'label' => $lll . '.callback_parameter',
            'config' => \CodingMs\Shop\Tca\Configuration::get('textareaLarge'),
            'displayCond' => [
                'OR' => [
                    'FIELD:type:=:payPal',
                    'FIELD:type:=:payPalPlus'
                ]
            ]
        ],
        'basket' => [
            'exclude' => 0,
            'label' => $lll . '.basket',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_shop_domain_model_basket',
                'minitems' => 1,
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'both',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                    'enabledControls' => [
                        'info' => false,
                        'new' => false,
                        'dragdrop' => false,
                        'sort' => false,
                        'hide' => false,
                        'delete' => false
                    ],
                ],
                'overrideChildTca' => [
                    'columns' => [
                        'basket_items' => [
                            'config' => [
                                'readOnly' => true,
                                'appearance' => [
                                    'levelLinksPosition' => 'both',
                                    'enabledControls' => [
                                        'info' => true,
                                        'new' => false,
                                        'dragdrop' => false,
                                        'sort' => false,
                                        'hide' => false,
                                        'delete' => false
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
            ],
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
    $return['columns']['status']['config']['items'] = [
        [$lll . '.prepared', 'prepared'], // checkout:all -> during checkout process
        [$lll . '.ordered', 'ordered'], // checkout:onInvoice -> successfully ordered
        [$lll . '.paid', 'paid'], // checkout:payPal -> IPN was valid
        [$lll . '.canceled', 'canceled'], // checkout:all -> Something went wrong or was aborted
        [$lll . '.requested', 'requested'], // checkout:request -> successfully requested
    ];
    $return['columns']['type']['config']['items'] = [
        [$lll . '.manual', 'manual'],
        [$lll . '.request', 'request'],
        [$lll . '.oninvoice', 'onInvoice'],
        [$lll . '.prepayment', 'prePayment'],
        [$lll . '.paypal', 'payPal'],
        [$lll . '.paypalplus', 'payPalPlus'],
        [$lll . '.klarna', 'klarna'],
        [$lll . '.stripe', 'stripe'],
    ];
    $return['columns']['vat_zone']['config']['items'] = [
        ['Inland', 'inland'],
        ['European Union', 'europeanUnion'],
        ['Third Country', 'thirdCountry'],
    ];
}
return $return;
