<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

/**
 * Add extra field showinpreview and some special news controls to sys_file_reference record
 */
$newSysFileReferenceColumns = [
    'showinpreview' => [
        'exclude' => true,
        'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.showinpreview',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
    'attach_to_email' => [
        'exclude' => true,
        'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.attach_to_email',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
    'filename' => [
        'exclude' => true,
        'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.filename',
        'description' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.filename_description',
        'config' => [
            'type' => 'input',
            'eval' => 'trim',
            'default' => '',
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference', $newSysFileReferenceColumns);

// add special news palette
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('sys_file_reference', 'shopProductPalette', 'showinpreview');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('sys_file_reference', 'shopProductFilePalette', 'filename, attach_to_email');

// Palette with only title and alternative text
$GLOBALS['TCA']['sys_file_reference']['palettes']['titleAlternativePalette']['showitem'] = 'title,alternative';
