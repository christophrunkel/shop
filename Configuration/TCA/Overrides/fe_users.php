<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'fe_users';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$additionalColumn = [
    'tx_shop_stripe_id' => [
        'exclude' => 0,
        'label' => $lll . '.stripe_id',
        'description' => $lll . '.stripe_id_description',
        'config' => \CodingMs\Shop\Tca\Configuration::get('string', false, true),
    ],
    'tx_shop_order_group' => [
        'exclude' => 0,
        'label' => $lll . '.order_group',
        'description' => $lll . '.order_group_description',
        'config' => \CodingMs\Shop\Tca\Configuration::get('orderGroupSingle'),
    ],
    'tx_shop_order_group_orders_visible' => [
        'exclude' => 0,
        'label' => $lll . '.order_group_orders_visible',
        'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.group_orders_visible_enabled_label', ['default' => 0]),
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'fe_users',
    $additionalColumn
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    'tx_shop_stripe_id',
    '',
    'after:tx_modules_vat_number'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    'tx_shop_order_group',
    '',
    'after:tx_shop_stripe_id'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    'tx_shop_order_group_orders_visible',
    '',
    'after:tx_shop_order_group'
);
