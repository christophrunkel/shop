<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
// Static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'shop',
    'Configuration/TypoScript',
    'Shop'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'shop',
    'Configuration/TypoScript/Stylesheet',
    'Shop - Default stylesheets'
);
