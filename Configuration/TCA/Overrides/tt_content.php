<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
// Register selectable plugins
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'shop',
    'Products',
    'Shop productlist and singleview'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'shop',
    'Basket',
    'Shop basket'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'shop',
    'BasketOrder',
    'Shop checkout & order by mail'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'shop',
    'BasketButton',
    'Shop basket button'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'shop',
    'QuickSearch',
    'Shop quick search'
);
//
// Include flex forms
$flexForms = ['Products'];
foreach ($flexForms as $pluginName) {
    $extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase('shop');
    $pluginSignature = strtolower($extensionName) . '_' . strtolower($pluginName);
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
    $flexForm = 'FILE:EXT:shop/Configuration/FlexForms/' . $pluginName . '.xml';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, $flexForm);
}
