<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_basketitem';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'quantity',
        'label_alt' => 'product',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => '',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-shop-basketitem']
    ],
    'types' => [
        '1' => ['showitem' => '
            --palette--;;quantity_product,
            --palette--;' . $lll . '.palette_price_tax;price_tax,
            --palette--;;custom_information
        '],
    ],
    'palettes' => [
        'quantity_product' => ['showitem' => 'quantity, product', 'canNotCollapse' => 1],
        'price_tax' => ['showitem' => 'price, tax, price_with_tax', 'canNotCollapse' => 1],
        'custom_information' => ['showitem' => 'custom_information, custom_information_required, --linebreak--, custom_information_type, custom_information_label'],
    ],
    'columns' => [
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\AdditionalTca\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\AdditionalTca\Tca\Configuration::full('endtime'),
        'quantity' => [
            'exclude' => 0,
            'label' => $lll . '.quantity',
            'config' => \CodingMs\Shop\Tca\Configuration::get('int'),
        ],
        'price' => [
            'exclude' => 0,
            'label' => $lll . '.price',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency'),
        ],
        'tax' => [
            'exclude' => 0,
            'label' => $lll . '.tax',
            'config' => \CodingMs\Shop\Tca\Configuration::get('percent'),
        ],
        'price_with_tax' => [
            'exclude' => 0,
            'label' => $lll . '.price_with_tax',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency'),
        ],
        'product' => [
            'exclude' => 0,
            'label' => $lll . '.product',
            'config' => [
                'type' => 'group',
                'allowed' => 'tx_shop_domain_model_product',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1
            ],
        ],
        'custom_information' => [
            'exclude' => 0,
            'label' => $lll . '.custom_information',
            'description' => $lll . '.custom_information_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('textareaSmall'),
        ],
        'custom_information_type' => [
            'exclude' => 0,
            'label' => $lll . '.custom_information_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'custom_information_label' => [
            'exclude' => 0,
            'label' => $lll . '.custom_information_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'custom_information_required' => [
            'exclude' => 0,
            'label' => $lll . '.custom_information_required',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox'),
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
    $return['columns']['product']['config']['internal_type'] = 'db';
}
return $return;
