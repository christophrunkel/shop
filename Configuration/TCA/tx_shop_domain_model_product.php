<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_product';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'label_alt' => 'parent',
        'label_alt_force' => true,
        'label_userFunc' => CodingMs\Shop\Tca\LabelService::class . '->getProductLabel',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,subtitle,product_no,price,description,unit,unit_factor,www,in_stock,tax,color,weight,size,attribute1,attribute2,attribute3,attribute4,',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-shop-product'],
        'accessUtility' => \CodingMs\ShopPro\Utility\AccessUtility::class
    ],
    'types' => [
        '0' => [
            'showitem' => '
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_general') . ',
                information,
                slug,
                --palette--;' . $lll . '.palette_title_product_no;title_product_no_product_type,
                --palette--;' . $lll . '.palette_subtitle_highlight_first_in_list;subtitle_highlight_first_in_list,
                digital_product,
                teaser,
                description,
                description_file,
                pdf_description,
                www,
                --palette--;;color_size_weight_bulky,
                --palette--;' . $lll . '.palette_views;views,
            --div--;' . $lll . '.tab_variants,
                variants,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_seo') . ',
                canonical_link,
                html_title,
                meta_abstract,
                meta_description,
                meta_keywords,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_prices') . ',
                --palette--;' . $lll . '.palette_price_tax;price_tax,
                --palette--;' . $lll . '.palette_offer_offer_rebate_value;offer_offer_rebate_value,
                --palette--;' . $lll . '.palette_in_stock_stock_amount;in_stock_stock_amount,
                --palette--;' . $lll . '.palette_unit_unit_factor;unit_factor_unit,
                discount,
                graduated_prices,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_relations') . ',
                categories,
                tags,
                related_products,
                attach_frontend_user_groups,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_files') . ',
                files,
                file_collections,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_images') . ',
                images,
                feature_icons,
                other_images,
            --div--;' . $lll . '.tab_videos,
                videos,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label($lll . '.tab_attributes') . ',
                attribute1,
                attribute2,
                attribute3,
                attribute4,
                attribute5,
                attribute6,
                attribute7,
                attribute8,
                attributes,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_language') . ',
                sys_language_uid;;;;1-1-1,
                l10n_parent,
                l10n_diffsource,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_access') . ',
                hidden;;1,
                starttime,
                endtime
            '
        ],
        // attribute1, attribute2, attribute3, attribute4, attribute5, attribute6, attribute7, attribute8,
    ],
    'palettes' => [
        'offer_offer_rebate_value' => ['showitem' => 'offer, offer_rebate_value', 'canNotCollapse' => 1],
        'unit_factor_unit' => ['showitem' => 'unit_factor, unit', 'canNotCollapse' => 1],
        'price_tax' => ['showitem' => 'price_type, price, tax', 'canNotCollapse' => 1],
        'title_product_no_product_type' => ['showitem' => 'title, product_no, product_type', 'canNotCollapse' => 1],
        'subtitle_highlight_first_in_list' => ['showitem' => 'subtitle, highlight, first_in_list', 'canNotCollapse' => 1],
        'color_size_weight_bulky' => ['showitem' => 'color, size, --linebreak--, weight, bulky', 'canNotCollapse' => 1],
        'in_stock_stock_amount' => ['showitem' => 'in_stock, stock_amount', 'canNotCollapse' => 1],
        'views' => ['showitem' => 'views, --linebreak--, views_day, views_week, --linebreak--, views_month, views_year, --linebreak--, views_last_update'],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', '', $extKey),
        'sys_language_uid' => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'sorting' => [
            'exclude' => 0,
            'label' => $lll . '.sorting',
            'config' => \CodingMs\Shop\Tca\Configuration::get('int', false, true),
        ],
        'crdate' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'tstamp' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'cruser_id' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'slug' => [
            'exclude' => 0,
            'label' => $lll . '.slug',
            'config' => \CodingMs\Shop\Tca\Configuration::get('slug'),
        ],
        'subtitle' => [
            'exclude' => 0,
            'label' => $lll . '.subtitle',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'product_no' => [
            'exclude' => 0,
            'label' => $lll . '.product_no',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'product_type' => [
            'exclude' => 0,
            'label' => $lll . '.product_type',
            'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_shop_domain_model_producttype',
                'foreign_table_where' => 'AND tx_shop_domain_model_producttype.pid=###CURRENT_PID### ORDER BY title',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
            ],
        ],
        'price_type' => [
            'exclude' => 0,
            'label' => $lll . '.price_type',
            'description' => $lll . '.price_type_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['label' => $lll . '.price_type_net', 'value' => 'net'],
                    ['label' => $lll . '.price_type_gross', 'value' => 'gross'],
                ],
            ],
        ],
        'price' => [
            'exclude' => 0,
            'label' => $lll . '.price',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency'),
        ],
        'tax' => [
            'exclude' => 0,
            'label' => $lll . '.tax',
            'config' => \CodingMs\Shop\Tca\Configuration::get('taxSingle', true),
        ],
        'discount' => [
            'exclude' => 0,
            'label' => $lll . '.discount',
            'description' => $lll . '.discount_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('discountSingle'),
        ],
        'teaser' => [
            'exclude' => 0,
            'label' => $lll . '.teaser',
            'config' => \CodingMs\Shop\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'description' => [
            'exclude' => 0,
            'label' => $lll . '.description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'description_file' => [
            'exclude' => 0,
            'label' => $lll . '.description_file',
            'config' => \CodingMs\Shop\Tca\Configuration::get('fileSingle'),
        ],
        'pdf_description' => [
            'exclude' => 0,
            'label' => $lll . '.pdf_description',
            'description' => $lll . '.pdf_description_description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => 1,
                'richtextConfiguration' => 'default',
                'default' => '',
            ],
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'digital_product' => [
            'exclude' => 0,
            'label' => $lll . '.digital_product',
            'description' => $lll . '.digital_product_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get(
                'checkbox',
                false,
                false,
                $lll . '.digital_product_label'
            ),
        ],
        'unit' => [
            'exclude' => 0,
            'label' => $lll . '.unit',
            'description' => $lll . '.unit_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'unit_factor' => [
            'exclude' => 0,
            'label' => $lll . '.unit_factor',
            'description' => $lll . '.unit_factor_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('int'),
        ],
        'www' => [
            'exclude' => 0,
            'label' => $lll . '.www',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'highlight' => [
            'exclude' => 0,
            'label' => $lll . '.highlight',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.highlight_label'),
        ],
        'first_in_list' => [
            'exclude' => 0,
            'label' => $lll . '.first_in_list',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.first_in_list_label'),
        ],
        'in_stock' => [
            'exclude' => 0,
            'label' => $lll . '.in_stock',
            'displayCond' => 'FIELD:digital_product:REQ:false',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.in_stock_label'),
        ],
        'stock_amount' => [
            'exclude' => 0,
            'label' => $lll . '.stock_amount',
            'displayCond' => 'FIELD:digital_product:REQ:false',
            'config' => \CodingMs\Shop\Tca\Configuration::get('int'),
        ],
        'offer' => [
            'exclude' => 0,
            'label' => $lll . '.offer',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.offer_label'),
        ],
        'offer_rebate_value' => [
            'exclude' => 0,
            'label' => $lll . '.offer_rebate_value',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency'),
        ],
        'color' => [
            'exclude' => 0,
            'label' => $lll . '.color',
            'config' => \CodingMs\Shop\Tca\Configuration::get('productColorSingle', true),
        ],
        'weight' => [
            'exclude' => 0,
            'label' => $lll . '.weight',
            'config' => \CodingMs\Shop\Tca\Configuration::get('weight'),
        ],
        'bulky' => [
            'exclude' => 0,
            'label' => $lll . '.bulky',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.bulky_label'),
        ],
        'size' => [
            'exclude' => 0,
            'label' => $lll . '.size',
            'config' => \CodingMs\Shop\Tca\Configuration::get('productSizeSingle', true),
        ],

        'canonical_link' => [
            'exclude' => 0,
            'label' => $lll . '.canonical_link',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get(
                'link',
                false,
                false,
                $lll . '.canonical_link',
                [
                    'title' => $lll . '.canonical_link',
                    'blindLinkFields' => 'class,target,title',
                    'blindLinkOptions' => 'mail,folder,file,telephone,shop_product,url,news_news'
                ]
            ),
        ],
        'html_title' => [
            'exclude' => 0,
            'label' => $lll . '.html_title',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'meta_abstract' => [
            'exclude' => 0,
            'label' => $lll . '.meta_abstract',
            'config' => \CodingMs\Shop\Tca\Configuration::get('textareaSmall'),
        ],
        'meta_description' => [
            'exclude' => 0,
            'label' => $lll . '.meta_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('textareaSmall'),
        ],
        'meta_keywords' => [
            'exclude' => 0,
            'label' => $lll . '.meta_keywords',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],

        'attribute1' => [
            'exclude' => 0,
            'label' => $lll . '.attribute1',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute2' => [
            'exclude' => 0,
            'label' => $lll . '.attribute2',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute3' => [
            'exclude' => 0,
            'label' => $lll . '.attribute3',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute4' => [
            'exclude' => 0,
            'label' => $lll . '.attribute4',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute5' => [
            'exclude' => 0,
            'label' => $lll . '.attribute5',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute6' => [
            'exclude' => 0,
            'label' => $lll . '.attribute6',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute7' => [
            'exclude' => 0,
            'label' => $lll . '.attribute7',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute8' => [
            'exclude' => 0,
            'label' => $lll . '.attribute8',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'categories' => [
            'exclude' => 0,
            'label' => $lll . '.categories',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_shop_domain_model_productcategory',
                'foreign_table_where' => ' AND tx_shop_domain_model_productcategory.pid = ###CURRENT_PID### AND tx_shop_domain_model_productcategory.sys_language_uid <= 0 ORDER BY tx_shop_domain_model_productcategory.title',
                'MM' => 'tx_shop_product_productcategory_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => [
                            'table' => 'tx_shop_domain_model_productcategory',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'tags' => [
            'exclude' => 0,
            'label' => $lll . '.tags',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_shop_domain_model_producttag',
                'foreign_table_where' => ' AND tx_shop_domain_model_producttag.pid = ###CURRENT_PID### AND tx_shop_domain_model_producttag.sys_language_uid <= 0 ORDER BY tx_shop_domain_model_producttag.title',
                'MM' => 'tx_shop_product_producttag_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => [
                            'table' => 'tx_shop_domain_model_producttag',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'related_products' => [
            'exclude' => 0,
            'label' => $lll . '.related_products',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_shop_domain_model_product',
                'foreign_table_where' => ' AND tx_shop_domain_model_product.uid != ###THIS_UID### AND tx_shop_domain_model_product.sys_language_uid <= 0 ORDER BY tx_shop_domain_model_product.title ',
                'MM' => 'tx_shop_product_product_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => [
                            'table' => 'tx_shop_domain_model_product',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'parent' => [
            'exclude' => 0,
            'label' => $lll . '.parent',
            'config' => [
                'type' => 'group',
                'allowed' => 'tx_shop_domain_model_product',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 0,
            ],
        ],
        'variants' => [
            'exclude' => 0,
            'label' => $lll . '.variants',
            'displayCond' => [
                'AND' => [
                    'FIELD:parent:REQ:false',
                    'REC:NEW:false',
                ],
            ],
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_shop_domain_model_product',
                'foreign_field' => 'parent',
                'foreign_sortby' => 'sorting',
                'foreign_table_where' => 'tx_shop_domain_model_product.pid=###CURRENT_PID###',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 0,
                    'levelLinksPosition' => 'both',
                    'showSynchronizationLink' => 1,
                    'showAllLocalizationLink' => 1,
                    'useSortable' => 1,
                    'showPossibleLocalizationRecords' => 0,
                    'enabledControls' => [
                        'info' => 1,
                        'new' => 1,
                        'dragdrop' => 1,
                        'sort' => 1,
                        'hide' => 1,
                        'delete' => 1,
                        'localize' => 1,
                    ],
                    'behaviour' => [
                        'localizationMode' => 'select',
                        'localizeChildrenAtParentLocalization' => true,
                    ],
                ],
            ],
        ],
        'attributes' => [
            'exclude' => 0,
            'label' => $lll . '.attributes',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_shop_domain_model_productattribute',
                'foreign_field' => 'product',
                'foreign_sortby' => 'sorting',
                'foreign_table_where' => 'tx_shop_domain_model_productattribute.pid=###CURRENT_PID###',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 0,
                    'levelLinksPosition' => 'both',
                    'showSynchronizationLink' => 1,
                    'showAllLocalizationLink' => 1,
                    'useSortable' => 1,
                    'showPossibleLocalizationRecords' => 0,
                    'enabledControls' => [
                        'info' => 1,
                        'new' => 1,
                        'dragdrop' => 1,
                        'sort' => 1,
                        'hide' => 1,
                        'delete' => 1,
                        'localize' => 1,
                    ],
                    'behaviour' => [
                        'localizationMode' => 'select',
                        'localizeChildrenAtParentLocalization' => true,
                    ],
                ],
            ],
        ],
        'graduated_prices' => [
            'exclude' => 0,
            'label' => $lll . '.graduated_prices',
            'description' => $lll . '.graduated_prices_description',
            'displayCond' => 'FIELD:digital_product:REQ:false',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_shop_domain_model_productgraduatedprice',
                'foreign_field' => 'product',
                // Don't sort by quantity, because otherwise the quantity field is in irre missing!
                'foreign_sortby' => 'sorting',
                'foreign_table_where' => 'tx_shop_domain_model_productgraduatedprice.pid=###CURRENT_PID###',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 0,
                    'levelLinksPosition' => 'bottom',
                    'showSynchronizationLink' => 1,
                    'showAllLocalizationLink' => 1,
                    'useSortable' => 1,
                    'showPossibleLocalizationRecords' => 0,
                    'enabledControls' => [
                        'info' => 1,
                        'new' => 1,
                        'dragdrop' => 1,
                        'sort' => 1,
                        'hide' => 1,
                        'delete' => 1,
                        'localize' => 1,
                    ],
                    'behaviour' => [
                        'localizationMode' => 'select',
                        'localizeChildrenAtParentLocalization' => true,
                    ],
                ],
            ],
        ],
        'images' => [
            'exclude' => 0,
            'label' => $lll . '.images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'images',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => $lll . '.images_add',
                        'showPossibleLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'images',
                        'tablenames' => 'tx_shop_domain_model_product',
                    ],
                    // custom configuration for displaying fields in the overlay/reference table
                    // to use the newsPalette and imageoverlayPalette instead of the basicoverlayPalette
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;shopProductPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;shopProductPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                        ],
                        'columns' => [
                            'crop' => [
                                'config' => [
                                    'cropVariants' => [
                                        'desktop' => [
                                            'disabled' => true,
                                        ],
                                        'teaser-item' => [
                                            'title' => $lll . '.images_crop_teaser_item',
                                            'allowedAspectRatios' => [
                                                '4:3' => [
                                                    'title' => '4:3',
                                                    'value' => 4 / 3,
                                                ],
                                                '1:1' => [
                                                    'title' => '1:1',
                                                    'value' => 1 / 1,
                                                ],
                                            ],
                                        ],
                                        'list-item' => [
                                            'title' => $lll . '.images_crop_list_item',
                                            'allowedAspectRatios' => [
                                                '4:3' => [
                                                    'title' => '4:3',
                                                    'value' => 4 / 3,
                                                ],
                                                '1:1' => [
                                                    'title' => '1:1',
                                                    'value' => 1 / 1,
                                                ],
                                            ],
                                        ],
                                        'detail-view' => [
                                            'title' => $lll . '.images_crop_detail_view',
                                            'allowedAspectRatios' => [
                                                '16:9' => [
                                                    'title' => '16:9',
                                                    'value' => 16 / 9,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
            )
        ],
        'feature_icons' => [
            'exclude' => 0,
            'label' => $lll . '.feature_icons',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'feature_icons',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => $lll . '.feature_icons_add',
                        'showPossibleLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'feature_icons',
                        'tablenames' => 'tx_shop_domain_model_product',
                    ],
                    // custom configuration for displaying fields in the overlay/reference table
                    // to use the newsPalette and imageoverlayPalette instead of the basicoverlayPalette
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;shopProductPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;shopProductPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                        ]
                    ]
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
            )
        ],
        'other_images' => [
            'exclude' => 0,
            'label' => $lll . '.other_images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('other_images', [
                // Use the imageo verlayPalette instead of the basic overlayPalette
                'overrideChildTca' => [
                    'types' => [
                        '0' => [
                            'showitem' => '
                                --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                                --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                        ],
                    ],
                ],
            ])
        ],
        'files' => [
            'exclude' => 0,
            'label' => $lll . '.files',
            'description' => $lll . '.files_description',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'files',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => $lll . '.files_add',
                        'showPossibleLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'files',
                        'tablenames' => 'tx_shop_domain_model_product',
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                    --palette--;;shopProductFilePalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                                    --palette--;;shopProductFilePalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;;shopProductFilePalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                                    --palette--;;shopProductFilePalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                                    --palette--;;shopProductFilePalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                                    --palette--;;shopProductFilePalette,
                                    --palette--;;filePalette'
                            ],
                        ]
                    ]
                ]
            )
        ],
        'file_collections' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_product.file_collections',
            'description' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_product.file_collections_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'sys_file_collection',
                'MM' => 'tx_shop_product_productfilecollection_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => [
                            'table' => 'sys_file_collection',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'videos' => [
            'exclude' => false,
            'label' => $lll . '.videos',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'videos',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => $lll . '.add_video',
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                                --palette--;;titleAlternativePalette,
                                --palette--;;filePalette'
                            ],
                        ],
                    ],
                    'maxitems' => 20,
                ],
                'mp4'
            ),
        ],
        'attach_frontend_user_groups' => [
            'exclude' => true,
            'label' => $lll . '.attach_frontend_user_groups',
            'description' => $lll . '.attach_frontend_user_groups_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'fe_groups',
                'size' => 6,
                'minitems' => 0,
            ],
        ],
        'views' => [
            'exclude' => true,
            'label' => $lll . '.views',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_day' => [
            'exclude' => true,
            'label' => $lll . '.views_day',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_week' => [
            'exclude' => true,
            'label' => $lll . '.views_week',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_month' => [
            'exclude' => true,
            'label' => $lll . '.views_month',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_year' => [
            'exclude' => true,
            'label' => $lll . '.views_year',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'views_last_update' => [
            'exclude' => true,
            'label' => $lll . '.views_last_update',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('dateTime', false, true, '', ['dbType' => 'timestamp']),
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
    $return['columns']['price_type']['config']['items'] = [
        [$lll . '.price_type_net', 'net'],
        [$lll . '.price_type_gross', 'gross'],
    ];
    $return['columns']['parent']['config']['internal_type'] = 'db';
    $return['columns']['images']['config']['foreign_match_fields']['table_local'] = 'sys_file';
    $return['columns']['feature_icons']['config']['foreign_match_fields']['table_local'] = 'sys_file';
    $return['columns']['files']['config']['foreign_match_fields']['table_local'] = 'sys_file';
}
return $return;
