<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_productattribute';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        //
        // ACHTUNG:
        // Do not use a relation value in label/title!!!
        // Performance will absolutely break down!
        //
        //'label_alt' => 'title',
        //'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'default_sortby' => 'sorting',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'title,',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-shop-productattribute'],
        'accessUtility' => \CodingMs\ShopPro\Utility\AccessUtility::class
    ],
    'types' => [
        '1' => ['showitem' => '--palette--;LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_productattribute.palette_title_value_category;title_value_category,--div--;LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_productattribute.tab_language,sys_language_uid,--palette--,l10n_parent,l10n_diffsource,--div--;LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_productattribute.tab_access,hidden,--palette--;;1'],
    ],
    'palettes' => [
        'title_value_category' => ['showitem' => 'title, value, category', 'canNotCollapse' => 1],
    ],
    'columns' => [
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'value' => [
            'exclude' => 0,
            'label' => $lll . '.value',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'category' => [
            'exclude' => 0,
            'label' => $lll . '.category',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeCategorySingle', true),
        ],
        'product' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
