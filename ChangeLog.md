# Shop Change-Log

## 2024-04-18 Release of version 4.1.3

*	[BUGFIX] Fix related products on product variants



## 2024-04-11 Release of version 4.1.2

*	[BUGFIX] Fix creating invoice numbers in a multi-language environment
*	[TASK] Optimize error handline for success page redirect
*	[BUGFIX] Fix accessing undefined array key in backend list



## 2024-03-25 Release of version 4.1.1

*	[BUGFIX] Fix reading custom-information in AJAX requests



## 2024-03-21 Release of version 4.1.0

*	[FEATURE] Add customer information and instructions on the shopping cart products in order to be able to customize products.
*	[BUGFIX] Don't throw an error, if no maximum order value for a payment type is setted
*	[TASK] Set an empty default value for cc of basket order admin mails
*	[BUGFIX] Manipulate inputs for fixing submit search/reset
*	[FEATURE] Introduce minimum and maximum order value based on payment type
*	[TASK] Optimize button margins in frontend
*	[TASK] Optimize product filter logic in frontend when no tag-multiple-checkbox is chosen
*	[TASK] Add TypoScript option for hiding color and size in order mail
*	[BUGFIX] Prevent double click on submit buttons, for example the checkout confirm page



## 2024-02-09 Release of version 4.0.6

*	[BUGFIX] Prevent issues on non available cc mail configuration



## 2024-02-07 Release of version 4.0.5

*	[TASK] Assume configured sorting of color/size on frontend variant filters



## 2024-02-06 Release of version 4.0.4

*	[TASK] Add translations, color and size in invoice, delivery-note and emails
*	[TASK] Name fe_user.title into possible frontend user data mapping
*	[TASK] Name fe_user.name into possible frontend user data mapping
*	[BUGFIX] Fix accessing non existing user name array index in abstract checkout
*	[BUGFIX] Fix usage of disabled products in invoices



## 2024-01-26 Release of version 4.0.3

*	[BUGFIX] Fix usage of question categories in products
*	[TASK] Add missing documentation link



## 2024-01-17 Release of version 4.0.2

*	[BUGFIX] Fix usage of product variants and missing base product properties
*	[TASK] Preparations for admin order list plugin in pro-version



## 2024-01-15 Release of version 4.0.1

*	[TASK] Optimize shipping costs including the documentation about shipping costs logic
*	[TASK] Refactoring of order mails and add missing shipping costs in summary
*	[BUGFIX] Fix shipping cost usage with basket order weight 0
*	[BUGFIX] Fix graduated prices calculation on gross-price products
*	[TASK] Change delivery note creation possible from is-send to is-paid



## 2024-01-11 Release of version 4.0.0

*	[BUGFIX] Fix product list filter settings/caching
*	[FEATURE] Add reset button for list search
*	[TASK] Migration for bootstrap 5
*	[BUGFIX] Remove try-catch from middleware because this sucks exception details
*	[TASK] Add backend pdf routes, closes #141
*	[TASK] Add frontent templates for displaying custom attributes
*	[TASK] Optimize version conditions in PHP code
*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10
*	[TASK] Add RTE as custom attribute type, see #135
*	[TASK] Cleanup, closes #120
*	[TASK] Add missing translations, see #130
*	[TASK] Add product type filter to BE product overview, see #133
*	[TASK] Add tag category filter to BE tags module, see #132
*	[TASK] Add formatted weight and bulky information in product variant view in Backend
*	[BUGFIX] Fix usage of og_image array key in product detail view
*	[TASK] Refactor finish order by using events for attachments and generated files
*	[BUGFIX] Fix inconsistent usage of sendMail in checkout services
*	[FEATURE] Add delivery note to admin|customer mail
*	[TASK] Updates for Overview of orders in the front end feature
*	[TASK] Add CC for order mails
*	[TASK] Add repository query constraints for backend color and size filters
*	[TASK] Sync language files
*	[TASK] Optimize icons in TCA
*	[FEATURE] Introduce shipping cost based on weight and delivery country
*	[BUGFIX] Fix migrate product size and color wizard if existing string value, see #114
*	[TASK] Add migration wizard for product type
*	[TASK] Add graduated prices sort by quantity
*	[BUGFIX] Fix migrate net gross prices wizard if database field not exists
*	[TASK] Add import ids to product size, color and type
*	[FEATURE] Add color and size backend modules and upgrade wizard, see #110
*	[BUGFIX] Fix product color amount model helper
*	[TASK] Add backend alert in product overview if no type, color or tag records, see #109
*	[TASK] Add some Product model helper getter for color, size and price
*	[FEATURE] Implement color and size as reusable objects
*	[TASK] Optimize TCA and display type only on new products
*	[TASK] Add image in product type record
*	[TASK] Add custom labels and types for attributes, see #106
*	[TASK] Add missing getters and setters for manage product variants #104
*	[BUGFIX] Fix taxes summary in basket view
*	[WIP] Add product type module and product variant summary view
*	[FEATURE] Add tax country overlays, see #103
*	[TASK] Add data-count attribute to basket button badge see #101
*	[FEATURE] Add videos for products, see #100
*	[TASK] Add documentation translation and configuration, see #99
*	[TASK] Update stripe customer on profile update
*	[TASK] Update product repository for digital filter, see #91
*	[FEATURE] Add digital products
*	[TASK] Optimize labels in product TCA, see #83
*	[TASK] Remove email compare for logged in users
*	[TASK] Switch from Charge API to Payment Intent API
*	[TASK] Make basket items editable only for manual orders
*	[BUGFIX] Remove secret key from basket order parameters
*	[TASK] Add order detail page
*	[TASK] Add price and date to order list
*	[TASK] Add invoice and delivery note downloads to order plugin
*	[TASK] Show error message in order plugin if not logged in
*	[TASK] Add search word filter repository changes to basket order repository, see #25
*	[BUGFIX] Add product getter and setter to product attribute model
*	[TASK] Remove ComputeProductPricesCommand and TaxCalculation, because it is no longer required
*	[!!!][TASK] Refactor price handling in products and graduated price - see Migration.md!
*	[TASK] Migrate TCA and other configuration using Rector
*	[TASK] Migrate ext_tables to TCA/Overrides
*	[TASK] Connect Frontend user with Stripe customer
*	[TASK] Migrate CSH into TCA description
*	[TASK] Add type filter for Basket Order
*	[FEATURE] Attach frontend user groups on successful purchase
*	[TASK] Migrate from downloadmanager into regular file collections
*	[TASK] Add migration wizard for connecting basket orders with frontend users
*	[BUGFIX] Fix admin mail for request checkout
*	[TASK] Optimize code style using cs-fixer
*	[TASK] Optimize code by using phpstan
*	[FEATURE] Use frontend user in checkout, if one is logged-in
*	[TASK] Optimize basketorder backend module filter, see shop_pro #58
*	[BUGFIX] Remove required from TCA slug field
*	[FEATURE] Add gender to the basket checkout fields
*	[TASK] Move the `deliveryTime` in BasketFooterRow above the `additionalPrices` in order to display it even without `additionalPrices`
*	[BUGFIX] Fix request/invoice button label in basket overview
*	[TASK] Add TCA definition for `accessUtility`
*	[TASK] Remove product record types completely
*	[TASK] Add repository methods for shop_pro modules
*	[BUGFIX] Fix issues for PHP 8.0+
*	[BUGFIX] Fix issue in TypoScript service based on guidelines
*	[TASK] Extend user and basket order tables for stripe
*	[TASK] Add manage tax view
*	[TASK] Add tax relation to product records
*	[TASK] Add widget for top clicked products
*	[FEATURE] Add click counter for products



## 2023-11-01  Release of version 3.2.9

*	[BUGFIX] Fix some PHP warnings



## 2023-09-07  Release of version 3.2.8

*	[TASK] Add some more logging for checkouts



## 2023-09-05  Release of version 3.2.7

*	[BUGFIX] Provide upgrade wizard for date field type modification



## 2023-08-14  Release of version 3.2.6

*	[BUGFIX] Show zero tax in basket item row if order is not vatable
*	[BUGFIX] Fix basket initialization



## 2023-07-14  Release of version 3.2.5

*	[BUGFIX] Fix tax calculation with multiple products in basket
*	[BUGFIX] Fix some undefined array index usages



## 2023-03-13  Release of version 3.2.4

*	[BUGFIX] Fix discount value type casting



## 2023-02-27  Release of version 3.2.3

*	[BUGFIX] Fix invalid meta data nesting in documentation



## 2023-02-27  Release of version 3.2.2

*	[BUGFIX] Fix invalid page link in documentation



## 2022-12-29  Release of version 3.2.1

*	[BUGFIX] Fix request checkout and checkout button label
*	[BUGFIX] Migrate TypoScript conditions in tsconfig
*	[BUGFIX] Fix base domain model properties mapping



## 2022-08-18  Release of version 3.2.0

*	[FEATURE] Menu processor
*	[TASK] Filter tags and categories by selected storage in product plugin
*	[TASK] Make preview image selectable
*	[TASK] Make fields price, tax and price_with_tax readonly in basket order



## 2022-08-16  Release of version 3.1.0

*	[FEATURE] Add select fields to the checkout form
*	[BUGFIX] Revert tca changes fot date fields in basket order
*	[BUGFIX] Fix doubled and incomplete og:image tags



## 2022-08-15  Release of version 3.0.0

*	[BUGFIX] Fix localization parent in TCA
*	[BUGFIX] Fix datetime evaluation for datetime fields in basket order
*	[TASK] Remove objectmanager usage for image-service
*	[BUGFIX] Do not use typed property for importId
*	[TASK] Remove typo3 version checks
*	[TASK] Remove calls to ObjectManager object
*	[TASK] Override database log writer
*	[TASK] Remove beforeProductDetailsRendering and beforeProductListRendering slots
*	[TASK] Adjust event listener documentation
*	[TASK] Remove afterPayPalCallback signal
*	[TASK] Remove showRecordFieldList from TCA example
*	[TASK] Move extension icon into public resources
*	[TASK] Migration for TYPO3 11 and PHP 8
*	[TASK] Preparations for TYPO3 11 and PHP 8
*	[TASK] Remove objectmanager usage for image-service



## 2022-12-29  Release of version 2.16.3

*	[BUGFIX] Fix request checkout and checkout button label
*	[BUGFIX] Fix TypoScript constant naming



## 2022-09-09  Release of version 2.16.2

*	[BUGFIX] Migrate TypoScript condition
*	[BUGFIX] Fix base domain model properties mapping



## 2022-08-18  Release of version 2.16.1

*	[TASK] Add menu processor documentation



## 2022-08-17  Release of version 2.16.0

*	[FEATURE] Menu processor
*	[TASK] Filter tags and categories by selected storage in product plugin



## 2022-08-16  Release of version 2.15.0

*	[FEATURE] Add select fields to the checkout form


## 2022-07-25  Release of version 2.14.3

*	[BUGFIX] Fix datetime evaluation for datetime fields in basket order
*	[TASK] Remove objectmanager usage for image-service
*	[BUGFIX] Do not use typed property for importId
*	[TASK] Make indexer hook for EXT:ke_search compatible with ke_search >= 4.0.0
*	[TASK] Add field "import_id" for product attributes



## 2022-03-29  Release of version 2.14.2

*	[TASK] Add configuration documentation translations
*	[TASK] Add documentation for signal slots
*	[TASK] Add field "import_id" for product categories



## 2022-03-02  Release of version 2.14.1

*	[BUGFIX] Disable some default constants



## 2022-02-25  Release of version 2.14.0

*	[TASK] Normalize configuration settings
*	[TASK] Compare and adjust translations for TypoScript constants
*	[FEATURE] Email attachment files in product record
*	[BUGFIX] Fix documentation configuration
*	[TASK] Add documentations configuration



## 2021-11-24  Release of version 2.13.0

*	[FEATURE] Add Discount objects for products.



## 2021-11-02  Release of version 2.12.1

*	[TASK] Adjustment of the PayPal checkout documentation.



## 2021-09-15  Release of version 2.12.0

*	[FEATURE] Add Stripe credit card as payment provider.



*	[TASK] Corrected Typoscript constants changes
*	[TASK] Defined Typoscript constants - moved out of setup


## 2021-09-06  Release of version 2.11.0

*	[FEATURE] Add checkout option for requiring the buyer to enter his email address twice



## 2021-08-27  Release of version 2.10.3

*	[TASK] Add transaction number to order summary template



## 2021-07-15  Release of version 2.10.2

*	[TASK] Add simple phone number verification



## 2021-07-09  Release of version 2.10.1

*	[TASK] Optimize error handling for Klarna payment



## 2021-07-05  Release of version 2.10.0

*	[FEATURE] Add Klarna payment
*	[TASK] Fix typo in english translations
*	[TASK] !!! Move PayPal-Plus JavaScript file from TypoScript into ConfirmOrder.html - see information in Migration.md
*	[TASK] Extend documentation for EXT:shop_import



## 2021-06-09  Release of version 2.9.1

*	[TASK] Add documentation for EXT:shop_import



## 2021-05-31  Release of version 2.9.0

*	[TASK] Adding information for PayPal Checkout in Documentation
*	[BUGFIX] Fix broken image path



## 2021-05-27  Release of version 2.8.9

*	[TASK] Enhance Documentation for Command/ComputeProductPrices
*	[TASK] Enhance Documentation for PayPalPlus



## 2021-05-26  Release of version 2.8.8

*	[BUGFIX] Set correct pathes to images in documentation



## 2021-05-26  Release of version 2.8.7

*	[BUGFIX] Set correct pathes to images and links in documentation



## 2021-05-26  Release of version 2.8.6

*	[TASK] Restructure Documentation
*	[TASK] Add more information to the documentation
*	[TASK] Fix typo in documentation



## 2021-05-15  Release of version 2.8.5

*	[TASK] Add more extension information



## 2021-05-15  Release of version 2.8.4

*	[TASK] Add free TYPO3 extension repository release



## 2021-05-06  Release of version 2.8.3

*	[BUGFIX] PDF invoice file name



## 2021-04-30  Release of version 2.8.2

*	[TASK] Add getter for product pdf description
*	[TASK] Add tax id in pdf invoice for inland b2b



## 2021-04-16  Release of version 2.8.1

*	[TASK] Split mail attachment configuration for admin and customer emails
*	[BUGFIX] Fix mail attachment for TYPO3 9



## 2021-04-06  Release of version 2.8.0

*	[FEATURE] Add a canonical link in products and add a sitemap.xml data provider
*	[BUGFIX] Fix fetching related downloadmanager file collections



## 2021-02-25  Release of version 2.7.6

*	[TASK] Add 'processed' filter to basket order backend module



## 2021-02-15  Release of version 2.7.5

*	[TASK] Add documentation EN translation files
*	[TASK] Documentation correction and translation to german
*	[TASK] Remove beforeProductDetailsRendering and beforeProductListRendering signals



## 2021-01-19  Release of version 2.7.4

*	[TASK] Labels for ups shipping feature
*	[BUGFIX] Fix reading vat id if it's not in use in frontend



## 2021-01-08  Release of version 2.7.3

*	[BUGFIX] Double display of personal data in confirm order View
*	[TASK] ComputeProductPrices command Documentation
*	[TASK] Remove callback creation from PayPal checkout documentation



## 2021-01-08  Release of version 2.7.2

*	[TASK] Pass selected country as language to PayPal Plus



## 2021-01-07  Release of version 2.7.1

*	[TASK] Added loading indication to PayPal Plus payment wall



## 2021-01-06  Release of version 2.7.0

*	[FEATURE] Price calculation command



## 2021-01-06  Release of version 2.6.1

*	[BUGFIX] Fix Isotope tag filter



## 2021-01-05  Release of version 2.6.0

*	[FEATURE] PayPal Plus checkout



## 2020-12-17  Release of version 2.5.3

*	[BUGFIX] Fix update and information row
*	[TASK] Normalize some translation values
*	[TASK] Add testmail command



## 2020-12-09  Release of version 2.5.2

*	[BUGFIX] Fix ExtensionUtility for TYPO3 10



## 2020-12-09  Release of version 2.5.1

*	[BUGFIX] Reset basket service on recalculate basket



## 2020-12-09  Release of version 2.5.0

*	[BUGFIX] Fix vatable identification for inland basket orders
*	[TASK] Use a short label for articlenumber in checkout table
*	[FEATURE] Add additional checkout row for shipping information



## 2020-12-09  Release of version 2.4.0

*	[TASK] Remove all vat information from checkout confirmation when order isn't vatable
*	[FEATURE] Add a vat notice in basket confirmation, basket order in backend and in invoice PDF
*	[FEATURE] Add Vatable, VAT-ID and VAT-Zone in basket order - this is required to identify the vatable behaviour
*	[FEATURE] Add fields in checkout: VAT-ID, housenumber and country
*	[FEATURE] Add country select field including configuration for default settings
*	[FEATURE] Add support for different taxes in a single order
*	[TASK] Add product no basket view and optimize columns
*	[TASK] Add unit/factor information and subtile in basket view
*	[TASK] Optimize and extend translations and label
*	[BUGFIX] Fix types in TypoScript service and domain model



## 2020-12-04  Release of version 2.3.3

*	[TASK] Optimize default Stylesheets
*	[TASK] Optimize error messages when basket order initialization failed
*	[BUGFIX] Fix wrong value sign in negative shipping costs in order confirmation view
*	[BUGFIX] Fix wrong PayPal price value for payment
*	[BUGFIX] Fix wrong type definition in basket model and add comments about tax values
*	[BUGFIX] Fix setting wrong price value when updating the basket order on re-confirm
*	[BUGFIX] Change tax field in basket order from percent into currency value



## 2020-12-02  Release of version 2.3.2

*	[TASK] Working on documentation
*	[TASK] Optimize tax formatting in basket view
*	[TASK] Optimize basket order fields in backend views by TCA
*	[TASK] Optimize shipping costs and shipping costs documentation
*	[BUGFIX] Fix B2C basket calculations with graduated prices



## 2020-12-01  Release of version 2.3.1

*	[TASK] Invoice attachment
*	[TASK] Add missing getter/setting in ProductGraduatedPrice model
*	[BUGFIX] Add types and lazy loading for creation user in Base model



## 2020-11-30  Release of version 2.3.0

*	[TASK] Add Backend order creation documentation
*	[BUGFIX] Add error handling for data container outside of webroot
*	[BUGFIX] Fix return types of new date fields in basket order
*	[FEATURE] Extension basket order and add invoice creation
*	[TASK] Add tax and price with tax to basket order and basket items
*	[TASK] Add caching to TypoScript service
*	[TASK] Remove tax from basket item row in b2b mode
*	[TASK] Add gross/netto feature to graduated prices
*	[FEATURE] Backend order creation
*	[TASK] Add extbase mapping for TYPO3 10
*	[TASK] Signal slot in PayPal callback
*	[TASK] Optimize labels and information about taxes and b2b/b2c mode
*	[BUGFIX] Fix reading Setup TypoScript configuration in Backend context
*	[BUGFIX] Fix versions in ext_emconf.php
*	[TASK] Round float values in basket processing
*	[FEATURE] A checkbox to indicate which orders have already been processed
*	[BUGFIX] findAllForBackendList method in BasketOrderRepository



## 2020-11-16  Release of version 2.2.3

*	[TASK] Route enhancer documentation
*	[TASK] Moved PayPal checkout service to shop_pro
*	[TASK] Optimize debugging and logging



## 2020-11-03  Release of version 2.2.2

*	[BUGFIX] Generate paypal return links after generation of a new basket order object
*	[BUGFIX] Redirect to error page after failed checkout
*	[BUGFIX] Redirect to success page after successful checkout



## 2020-11-03  Release of version 2.2.1

*	[BUGFIX] Fix JavaScript method definition bug in older browsers



## 2020-11-03  Release of version 2.2.0

*	[FEATURE] Delivery address
*	[TASK] Update PayPal checkout documentation



## 2020-11-02  Release of version 2.1.5

*	[TASK] Add documemtation files
*	[TASK] Set max php version to 7.4.99



## 2020-10-16  Release of version 2.1.4

*	[BUGFIX] Information row in shop flexform



## 2020-10-11  Release of version 2.1.3

*	[TASK] Add extra tags in composer.json



## 2020-10-11  Release of version 2.1.2

*	[BUGFIX] Change database type for description_file to integer
*	[TASK] Add filter for selections in product TCA



## 2020-10-11  Release of version 2.1.1

*	[BUGFIX] Fix usage of description files in products



## 2020-09-15  Release of version 2.1.0

*	[FEATURE] Database logging
*	[FEATURE] Setting the gross/net price depending on the store type
*	[BUGFIX] Compatibility with TYPO3 9 (TYPO3\CMS\Core\Mail\MailMessage)
*	[TASK] Add support for html emails
*	[TASK] Add constants for email templates



## 2020-09-15  Release of version 2.0.0

*	[TASK] Add filter isotope endpoint to Default Layout
*	[BUGFIX] Send requests only once per changed tag category to json api (isotope filter)
*	[TASK] Save/load isotope filter in/from the session
*	[TASK] Display current filter status in the tag/category select box
*	[BUGFIX] Add tag uid instead of tag object to session in ProductController
*	[BUGFIX] Fixed initial value for category filter selector
*	[TASK] Added render type to select types in the flex form
*	[TASK] Dont allow the user to put more products in the basket than there are available
*	[TASK] EXT:questions and EXT:downloadmanager support
*	[TASK] Redirect user back to basket if ordered quantity is greater than stock amount
*	[BUGFIX] Check if the ProductCategory image is disabled
*	[BUGFIX] Removed usage of shop:html in partial SingleView
*	[BUGFIX] Check if the serialized data in SessionHandler is null
*	[BUGFIX] Using MailMessage::text() instead of MailMessage::setBody.
*	[TASK] Added type hints and PHPDoc strings
*	[TASK] Fixed legal notice
*	[TASK] Strict types in all php files
*	[BUGFIX] Show prices without taxes in b2b shops
*	[BUGFIX] Add minimumOrderValueReached to output json in JsonApiController
*	[BUGFIX] Casting $settings['storagePid'] to int before setting pid of a basket item in BasketService
*	[TASK] Remove debug service
*	[TASK] Add csv export for products and product tags
*	[TASK] Migrate annotations to Typo3 10
*	[TASK] Use TYPO3Fluid instead of TYPO3\CMS namespace for viewhelpers
*	[TASK] Use AdditionalTca
*	[TASK] Added Configuration/Extbase/Persistence/Classes.php
*	[TASK] Migrated KeSearchIndexerConfigurationHook from TYPO3_DB to doctrine-dbal
*	[TASK] Move Currency and Percent form elements to EXT:additional_tca
*	[TASK] Remove translated relations from product selections in TCA
*	[TASK] Remove "NOT NULL" for text fields in ext_tables.sql
*	[TASK] Add JavaScript history back method with refresh
*	[FEATURE] Add street, postal code and city as predefined field in checkout form
*	[FEATURE] Add shipping costs as relation configurable in backend
*	[FEATURE] Add minimum order value
*	[FEATURE] Add open graph meta data
*	[FEATURE] Add graduated prices for products
*	[FEATURE] Add pre payment order checkout
*	[FEATURE] Add search word fields by TypoScript setting
*	[TASK] Rename TypoScript file extensions to .typoscript
*	[FEATURE] Add extension configuration for enable/disable product list caching
*	[TASK] Optimize error handing in finish checkout service
*	[BUGFIX] Fix locallang xliff file errors
*	[BUGFIX] Fix cdata wrapping in locallang file
*	[FEATURE] Add a slot for manipulating product details data before rendering
*	[TASK] Translate locallang**.xlf files (2 csh files)
*	[TASK] Add target language in xliff files
*	[BUGFIX] Fix error view for BasketOrder controller
*	[FEATURE] Add a slot for manipulating product list data before rendering
*	[BUGFIX] Fix error handling in checkout
*	[TASK] Translate locallang**.xlf files
*	[TASK] Performance and caching optimization



## 2020-01-27  Release of version 1.6.0

*	[FEATURE] Finalize stock logic.
*	[TASK] Prevent buy a larger amount than products on stock
*	[TASK] Add translation keys
*	[TASK] Open checkbox confirmation links in checkout process in new browser tab
*	[TASK] Change attributes fields to text SQL fields
*	[TASK] Add description file getter and setter
*	[FEATURE] Add stock logic
*	[BUGFIX] Fix Enter key behavior on item quantity change in basket
*	[TASK] Add german translation for item quantity change in basket
*	[FEATURE] Add tax information per item to basket view



## 2019-12-11  Release of version 1.5.1

*	[BUGFIX] Fix UTF-8 issue in PayPal callback
*	[BUGFIX] Fix Fluid mail template closing tag
*	[BUGFIX] Fix basketOrder not available in mail templates
*	[TASK] Example output in mail templates
*	[BUGFIX] Fix unclosed section



## 2019-12-09  Release of version 1.5.0

*	[BUGFIX] Fix email content types
*	[BUGFIX] Fix UTF-8 issue in PayPal callback
*	[FEATURE] Setup page uid to link at privacy and terms pages in checkout
*	[FEATURE] Add switch to change email type (text/plain,text/html)
*	[TASK] Remove unused math View Helper
*	[TASK] Add some migration hints in Migration.md
*	[BUGFIX] Show correct total price in basket
*	[TASK] Show prices in checkout according to basket display type
*	[BUGFIX] Show correct price with additional costs in order confirmation
*	[TASK] Add constant for setting basket display type
*	[BUGFIX] Adjust tax calculation
*	[BUGFIX] Fix MySQL and Model data types for prices and set paid price in PayPay order finishing
*	[TASK] Add documentation for PayPal checkout testing and debugging
*	[BUGFIX] Update quantity correctly (issue #2)



## 2019-11-18  Release of version 1.4.0

*	[BUGFIX] Fix items not removed from basket
*	[BUGFIX] Fix list pre-filtering for product tags
*	[BUGFIX] Fix Fluid template links for detail page
*	[BUGFIX] Fix FlexForm/Detail view page uid overriding
*	[TASK] Optimizing slug field TCA for TYPO3 9.5
*	[BUGFIX] Fix FlexForm translation key for basket button.
*	[FEATURE] Refactoring of checkout process including PayPal checkout.
*	[FEATURE] Implement possibility to search for multiple tags in a single request.



## 2019-07-18  Release of version 1.3.0

*	[TASK] Added search word facility to product repository for backend overview.
*	[TASK] Add getter/setter for stock amount in product model.
*	[TASK] Add getter/setter for slug and record type in product model.
*	[TASK] Add new content element wizards.
*	[TASK] Add mapping for extbase model and restrict backend list access.
*	[BUGFIX] Fix usage of no-image.png.
*	[BUGFIX] Fix tag pre filtering.



## 2019-03-06  Release of version 1.2.0

*	[FEATURE] Adding record type for products (default, downloads, questions, ...)
*	[TASK] TCA refactoring.
*	[TASK] JavaScript refactoring.
*	[TASK] Adding form engine field for currency and percent.
*	[FEATURE] Adding flexform selection, so that products can be displayed immediately without navigating through the list view.
*	[TASK] Sort related products in backend by title.
*	[TASK] Lazy loading for product attributes.
*	[FEATURE] Adding repository methods for backend module.
*	[FEATURE] Adding Metatags and titletag for detail view.
*	[TASK] Moving products language and access in own tab in TCA.
*	[BUGFIX] Fixing SQL table definition - image default value.
*	[BUGFIX] Fixing typo in controller usage.
*	[TASK] Excluding chash by default.
*	[TASK] Removing exclude fields from FlexForm.



## 2017-12-04  Release of version 1.1.2

*	[BUGFIX] Fixing feature icons in Product model



## 2017-11-22  Release of version 1.1.0

*	[BUGFIX] Fixing attribute category handling
*	[TASK] Adding product model getter/setter
*	[BUGFIX] Fixing product image TCA for different TYPO3 versions
*	[FEATURE] Adding showinpreview checkbox in product images (similar to tx_news). Product model supports now `product.imagesNonPreview` and `product.imagesPreviewOnly`.
*	[FEATURE] Adding showinpreview checkbox in product featureIcons (similar to tx_news). Product model supports now `product.featureIconsNonPreview` and `product.featureIconsPreviewOnly`.
*	[FEATURE] Adding a math subtract ViewHelper
*	[FEATURE] Adding linkhandler configuration
*	[FEATURE] Adding product image links
*	[TASK] Adding translations
*	[FEATURE] Adding ke_search indexer for products
*	[TASK] Adding tooltips for related products tags
*	[FEATURE] Adding a bookmark and compare feature
*	[TASK] Extending and optimizing a bookmark and compare feature
*	[TASK] Remove exclude fields from configuration
*	[FEATURE] Extending product attribute categories with a template selection, so that each category can be displayed differently
*	[FEATURE] Finalize bookmarks feature
*	[BUGFIX] Fixing extension loaded ViewHelper
*	[TASK] Migrate Session-Handler
*	[TASK] TCA migration for 7.6.x
*	[FEATURE] Product get an offer checkbox and an offer-value-field
*	[BUGFIX] Refactoring the variant logic and JavaScript library
*	[BUGFIX] Update basket item quantity fixed
*	[FEATURE] Adding fixed additional costs, like shipping or packing
*	[TASK] Moving a getBasketObject method in a new BasketService
*	[TASK] Adding Teaser-Text field
