<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
// Displays products
//
// Enable caching for products list
// In case of JavaScript filtering (displaying all products immediately!) a caching is recommended!
// In case of HTML+Filter-form a caching won't work, because it would cache filtered list results!
$listPluginCache = (bool)\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
)->get('shop', 'listPluginCache');
$productsCacheableActions = $listPluginCache ? '' : 'list';

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Shop',
    'Products',
    [\CodingMs\Shop\Controller\ProductController::class => 'list, show'],
    [\CodingMs\Shop\Controller\ProductController::class => $productsCacheableActions]
);
//
// Basket functionality
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Shop',
    'Basket',
    [\CodingMs\Shop\Controller\BasketController::class => 'show, removeFromBasket'],
    [\CodingMs\Shop\Controller\BasketController::class => 'show, removeFromBasket']
);
//
// Basket order
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Shop',
    'BasketOrder',
    [\CodingMs\Shop\Controller\BasketOrderController::class => 'checkout, confirmOrder, order, success, error, cancel, callback, payPalPlusCallback, payPalPlusSuccessRedirect, stripeReturnRedirect, stripeCallback'],
    [\CodingMs\Shop\Controller\BasketOrderController::class => 'checkout, confirmOrder, order, success, error, cancel, callback, payPalPlusCallback, payPalPlusSuccessRedirect, stripeReturnRedirect, stripeCallback']
);
//
// Displays the Basket-Button and process the adding of products
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Shop',
    'BasketButton',
    [\CodingMs\Shop\Controller\BasketController::class => 'showBasketButton'],
    [\CodingMs\Shop\Controller\BasketController::class => 'showBasketButton']
);
//
// Displays the quick search
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Shop',
    'QuickSearch',
    [\CodingMs\Shop\Controller\ProductController::class => 'showQuickSearch'],
    [\CodingMs\Shop\Controller\ProductController::class => '']
);
//
// JSON API for JavaScrip actions
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Shop',
    'JsonApi',
    [\CodingMs\Shop\Controller\JsonApiController::class => 'bookmark, filter, updateBasketItem, addBasketItem'],
    [\CodingMs\Shop\Controller\JsonApiController::class => 'bookmark, filter, updateBasketItem, addBasketItem']
);
//
// register svg icons: identifier and filename
$iconsSvg = [
    'contains-products' => 'Resources/Public/Icons/Extension.svg',
    'apps-pagetree-folder-contains-products' => 'Resources/Public/Icons/Extension.svg',
    'mimetypes-x-content-shop-basket' => 'Resources/Public/Icons/iconmonstr-shopping-cart-2.svg',
    'mimetypes-x-content-shop-basketitem' => 'Resources/Public/Icons/iconmonstr-shopping-cart-24.svg',
    'mimetypes-x-content-shop-basketorder' => 'Resources/Public/Icons/iconmonstr-shopping-cart-23.svg',
    'mimetypes-x-content-shop-product' => 'Resources/Public/Icons/iconmonstr-shipping-box-3.svg',
    'mimetypes-x-content-shop-producttype' => 'Resources/Public/Icons/iconmonstr-sitemap-6.svg',
    'mimetypes-x-content-shop-productcategory' => 'Resources/Public/Icons/iconmonstr-link-2.svg',
    'mimetypes-x-content-shop-quicksearch' => 'Resources/Public/Icons/iconmonstr-magnifier-2.svg',
    'mimetypes-x-content-shop-producttag' => 'Resources/Public/Icons/iconmonstr-tag-4.svg',
    'mimetypes-x-content-shop-productattribute' => 'Resources/Public/Icons/iconmonstr-task-1.svg',
    'mimetypes-x-content-shop-productattributecategory' => 'Resources/Public/Icons/iconmonstr-task-1.svg',
    'mimetypes-x-content-shop-productgraduatedprice' => 'Resources/Public/Icons/iconmonstr-tag-23.svg',
    'mimetypes-x-content-shop-productshippingcost' => 'Resources/Public/Icons/iconmonstr-tag-21.svg',
    'mimetypes-x-content-shop-productdiscount' => 'Resources/Public/Icons/iconmonstr-tag-21.svg',
    'mimetypes-x-content-shop-productcolor' => 'Resources/Public/Icons/iconmonstr-paintbrush-7.svg',
    'mimetypes-x-content-shop-productsize' => 'Resources/Public/Icons/iconmonstr-ruler-22.svg',
    'mimetypes-x-content-shop-tax' => 'Resources/Public/Icons/iconmonstr-coin-8.svg',
];
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
foreach ($iconsSvg as $identifier => $path) {
    $iconRegistry->registerIcon(
        $identifier,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:shop/' . $path]
    );
}
//
// Backend configuration
$pageTypoScript = '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:shop/Configuration/PageTS/tsconfig.typoscript">';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($pageTypoScript);
//
// KE Search indexer
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('ke_search')) {
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['ke_search']['registerIndexerConfiguration'][] = 'CodingMs\\Shop\\Hooks\\KeSearchIndexerConfigurationHook';
    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['ke_search']['customIndexer'][] = 'CodingMs\\Shop\\Hooks\\KeSearchIndexerConfigurationHook';
}
//
// Register language aware flex form handling in FormEngine
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1607437245] = [
    'nodeName' => 'TaxInformation',
    'priority' => 40,
    'class' => \CodingMs\Shop\Form\Container\TaxInformation::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1606493969] = [
    'nodeName' => 'BasketOrderDataForm',
    'priority' => 40,
    'class' => \CodingMs\Shop\Form\Container\BasketOrderDataForm::class,
];
//
// Manipulate TCA
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['tcaDatabaseRecord'][\CodingMs\Shop\Backend\FormDataProvider\DatabaseEditRow::class] = [
    'depends' => [
        \TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseEditRow::class,
    ]
];
//
// logging
$extConf = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('shop');
$loglevel = !empty($extConf['loglevel']) ? $extConf['loglevel'] : 'ERROR';
$loglevel = strtolower($loglevel);

$GLOBALS['TYPO3_CONF_VARS']['LOG']['CodingMs']['Shop']['writerConfiguration'] = [
    $loglevel => [
        \CodingMs\Shop\Log\Writer\DatabaseWriter::class => []
    ]
];

// update wizards
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['shop_migrateDatetimeToIntegerWizard'] = \CodingMs\Shop\Updates\MigrateDatetimeToIntegerWizard::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['shop_migrateTaxValuesToRelationsWizard'] = \CodingMs\Shop\Updates\MigrateTaxValuesToRelationsWizard::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['shop_migrateNetGrossPricesWizard'] = \CodingMs\Shop\Updates\MigrateNetGrossPricesWizard::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['shop_attachFrontendUserToBasketOrderWizard'] = \CodingMs\Shop\Updates\AttachFrontendUserToBasketOrderWizard::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['shop_migrateProductTypeWizard'] = \CodingMs\Shop\Updates\MigrateProductTypeWizard::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['shop_migrateProductSizeWizard'] = \CodingMs\Shop\Updates\MigrateProductSizeWizard::class;
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['shop_migrateProductColorWizard'] = \CodingMs\Shop\Updates\MigrateProductColorWizard::class;
//
// Override Mail template paths
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][1675956615] = 'EXT:shop/Resources/Private/Templates/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['partialRootPaths'][1675956615] = 'EXT:shop/Resources/Private/Partials/Email/';
