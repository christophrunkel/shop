# Shop Erweiterung für TYPO3

Diese Erweiterung ist eine umfangreiche Shop-Erweiterung für TYPO3 zur Umsetzung von Webshops, Online-Shops oder Abo-Systemen (vergleicbar mit tt_products, Quick-Shop oder Aimeos).


**Features:**

*   Schnellsuche für Startseite oder Seitenleiste
*   Suchfilter und benutzerdefinierte Listenansichten
*   Teaser-Element für Startseite oder Seitenleiste
*   HTML- oder Plain-Text E-Mails
*   Mindestbestellwert konfigurierbar
*   Produkt-Typen und Varianten individuell konfigurierbar
*   Produkte nach Kategorien gruppierbar (Mehrfachzuordnung möglich)
*   Gruppieren von Produkten anhand von Tags und Tag-Kategorien (Mehrfachzuordnung möglich)
*   Staffelpreise für Produkte
*   Unterschiedliche Versandkosten pro Produkt, basierend auf Gewicht und Lieferland
*   Produkte hervorheben oder in der Liste ganz oben anzeigen via Checkbox
*   Benutzerdefinierte Produktattribute (kategorisierbar), welche die Produkt-Vergleichsansicht erweitern
*   B2B und B2C vorkonfiguriert
*   Einfache Lagerlogik mit automatisierter Reduzierung
*   Kaufabwicklung auf Anfrage, auf Rechnung oder Vorkasse
*   Einfach anpassbares Checkout-Formular inkl. abweichender optionaler Lieferadresse
*   Statistik über den Produktaufruf (je letzter Tag, Monat, Jahr und insgesamt)
*   Dokumente in Produkten möglich (statische Auswahl, per Kategorie, per Verzeichnis)
*   FAQ in Produkten via EXT:questions möglich
*   Automatische Brutto-/Nettopreisberechnung mit vordefinierten Steuersätzen
*   Steuersätze können je Lieferland abweichend konfiguriert werden
*   Frei konfigurierbare zusätzliche Attribute im Product, basierend auf dem Produkt-Typen/Varianten (inkl. Typisierung, eigene Label in FE+BE, etc.)

**Pro-Features:**

*   Isotope-Support für Listenansicht
*   Frontend-Benutzer können Ihre Bestellungen im Frontend einsehen
*   Backend Dashboard-Widgets für die Top-Clicked Produkte, neuste Bestellungen und Top-Selling Produkte
*   Backendmodul zur Verwaltung von Produkten und Bestellungen
*   Backend-Modul zur Einsicht von Statistiken zu Besuchern und Käufen
*   Zuweisung von Frontend-Benutzergruppen bei erfolgreichem Kauf möglich
*   Automatisches Anhängen von Produkt-Dokumente an die Kauf-Bestätigungsmail möglich. Bspw. um digitale Käufe bereitzustellen.
*   PDF-Generator für Produkt-Dokumente (Datenblatt, Rechnung, Lieferschein)
*   Digitale Produkte ohne Menge usw. (bspw. für E-Books o.ä.)
*   Kunden Informationen und Hinweise an den Warenkorb-Produkten, um Produkte customizen zu können.
*   PayPal Bestellvorgang
*   PayPal Plus Bestellvorgang
*   Klarna Bestellvorgang (pay_now Klarna Payments)
*   Stripe Bestellvorgang (Kreditkarte)
*   Stripe Abo-System für Portale mit wiederkehrenden Zahlungen
*   Merkzettel-Funktion für Produkte
*   Vergleichsfunktion für Merkzettel-Produkte
*   Automatische Rechnungs- und Lieferschein-Generierung
*   Erneutes Absenden von Bestellmails inkl. Rechnungs-PDF für Backend-Benutzer
*   Manuelle Erstellung von Bestellungen im Backendmodul
*   UPS-API-Verbindung möglich (EXT:ups_api erforderlich)
*   Lieferschein an Admin- oder Kundenmail anhängen
*   Bestell-XML an die Admin-E-Mail anhängen oder als XML-Datei im Dateisystem ablegen
*   Übersicht der Bestellungen im Frontend auch Benutzer-Übergreifend möglich (Benutzer bspw. gruppiert nach Kunde/Firma)
*   Aufbau eines Corporate Fashion Shops möglich

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Shop Dokumentation](https://www.coding.ms/documentation/typo3-shop "Shop Dokumentation")
*   [Shop Bug-Tracker](https://gitlab.com/codingms/typo3-shop/shop/-/issues "Shop Bug-Tracker")
*   [Shop Repository](https://gitlab.com/codingms/typo3-shop/shop "Shop Repository")
*   [TYPO3 Shop Produktdetails](https://www.coding.ms/typo3-extensions/typo3-shop/ "TYPO3 Shop Produktdetails")
*   [TYPO3 Shop Dokumentation](https://www.coding.ms/de/dokumentation/typo3-shop/ "TYPO3 Shop Dokumentation")
*   [TYPO3 Shop Demo](https://shop-12.typo3-demos.de/ "TYPO3 Shop Demo")
*   [Referenz: Niedersachsen Gin](https://www.niedersachsen-gin.de/ "Referenz: Niedersachsen Gin")
*   [Referenz: coding.ms by InnoCoding GmbH](https://www.coding.ms/de/typo3-extensions/ "Referenz: coding.ms by InnoCoding GmbH")
