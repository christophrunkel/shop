<?php

declare(strict_types=1);

namespace CodingMs\Shop\Command;

use CodingMs\Shop\Service\Checkout\OnInvoiceCheckoutService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TestmailCommand extends Command
{
    protected function configure(): void
    {
        $this->setDescription('Sends a testmail using the mail function in the CheckoutService.');
        $this->addArgument('to', InputArgument::REQUIRED, 'Receiver email address.');
        $this->addArgument('from', InputArgument::OPTIONAL, 'Sender email address.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int error code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $to = $input->getArgument('to');
        $from = $input->getArgument('from') ?? $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'];

        /** @var OnInvoiceCheckoutService $onInvoiceCheckoutService */
        $onInvoiceCheckoutService = GeneralUtility::makeInstance(OnInvoiceCheckoutService::class);
        $onInvoiceCheckoutService->sendMail(
            [$from => $from],
            [$to => $to],
            '[SHOP] Testmail',
            'This test mail has been sent by the shop:testmail command',
            'text/html'
        );

        $io->writeln('Sending mail from ' . $from . ' to ' . $to);
        return Command::SUCCESS;
    }
}
