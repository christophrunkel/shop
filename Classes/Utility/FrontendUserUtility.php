<?php

namespace CodingMs\Shop\Utility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\FrontendUser;
use CodingMs\Shop\Domain\Repository\FrontendUserRepository;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\UserAspect;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Utility for frontend users
 */
class FrontendUserUtility
{
    protected FrontendUserRepository $frontendUserRepository;

    public function __construct(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    /**
     * Returns the current logged in frontend user
     * @return FrontendUser|null
     */
    public function getCurrentFrontendUser(): ?FrontendUser
    {
        /** @var Context $context */
        $context = GeneralUtility::makeInstance(Context::class);
        $frontendUser = null;
        if ($context->getPropertyFromAspect('frontend.user', 'isLoggedIn')) {
            /** @var UserAspect $frontendUserAspect */
            $frontendUserAspect = $context->getAspect('frontend.user');
            /** @var FrontendUser $frontendUser */
            $frontendUser = $this->frontendUserRepository->findByUid((int)$frontendUserAspect->get('id'));
        }
        return $frontendUser;
    }
}
