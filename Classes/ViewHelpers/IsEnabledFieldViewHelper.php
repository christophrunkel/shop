<?php

declare(strict_types=1);

namespace CodingMs\Shop\ViewHelpers;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Closure;
use CodingMs\Shop\Domain\Model\Product;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\ViewHelperInterface as CompilableInterface;

/**
 * Class IsEnabledFieldViewHelper
 * @noinspection PhpUnused
 */
class IsEnabledFieldViewHelper extends AbstractViewHelper implements CompilableInterface
{
    use CompileWithRenderStatic;

    public function initializeArguments(): void
    {
        $this->registerArgument('product', 'object', 'The current product', true);
        $this->registerArgument('attribute', 'string', 'Name of the attribute', true);
    }

    /**
     * @param array $arguments
     * @param Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return bool
     */
    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $isEnabled = false;
        $attribute = $arguments['attribute'];
        $product = $arguments['product'];
        if ($product instanceof Product) {
            $enabledFields = $product->getEnabledFields();
            $isEnabled = in_array($attribute, $enabledFields);
        }
        return $isEnabled;
    }
}
