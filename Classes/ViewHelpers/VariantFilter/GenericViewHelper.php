<?php

declare(strict_types=1);

namespace CodingMs\Shop\ViewHelpers\VariantFilter;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Closure;
use CodingMs\Shop\Domain\Model\Product;
use CodingMs\Shop\Domain\Repository\ProductRepository;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\ViewHelperInterface as CompilableInterface;

/**
 * Class GenericViewHelper
 * @noinspection PhpUnused
 */
class GenericViewHelper extends AbstractViewHelper implements CompilableInterface
{
    use CompileWithRenderStatic;

    protected ProductRepository $productRepository;

    /**
     * @param ProductRepository $productRepository
     */
    public function __construct(
        ProductRepository $productRepository
    )
    {
        $this->productRepository = $productRepository;
    }

    public function initializeArguments(): void
    {
        $this->registerArgument('product', 'int', 'Uid of the current product', true);
        $this->registerArgument('field', 'string', 'Field for the filter: color, size, title', true);
        $this->registerArgument('sorting', 'string', 'Optional: none, asc, desc', false, 'asc');
        $this->registerArgument('equalSize', 'string', 'Optional: Product must have this size for linking', false, '');
        $this->registerArgument('equalColor', 'string', 'Optional: Product must have this color for linking', false, '');
    }

    /**
     * @param array $arguments
     * @param Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return mixed
     */
    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $filter = [];
        $field = $arguments['field'];
        $sorting = $arguments['sorting'];
        $productUid = (int)$arguments['product'];
        $equalSize = trim($arguments['equalSize']);
        $equalColor = trim($arguments['equalColor']);
        $productRepository = GeneralUtility::makeInstance(ProductRepository::class);
        //
        // read configured sorting from extension configuration
        $sortingField = 'title';
        $backendConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('shop', 'sorting');
        if (isset($backendConfiguration['orderingFor' . ucfirst($field)])) {
            if ($backendConfiguration['orderingFor' . ucfirst($field)] === 'sorting') {
                $sortingField = 'sorting';
            }
        }
        /** @var Product $product */
        $product = $productRepository->findByIdentifier($productUid);
        if ($product instanceof Product) {
            if ($equalSize === '' && $equalColor === '') {
                $filter = self::populateEntry($product, $productUid, $field, $filter, $sortingField);
            } elseif ($equalSize === $product->getSize()->getTitle() && $equalColor === '') {
                $filter = self::populateEntry($product, $productUid, $field, $filter, $sortingField);
            } elseif ($equalSize === '' && $equalColor === $product->getColor()->getTitle()) {
                $filter = self::populateEntry($product, $productUid, $field, $filter, $sortingField);
            } elseif ($equalSize === $product->getSize()->getTitle() && $equalColor === $product->getColor()->getTitle()) {
                $filter = self::populateEntry($product, $productUid, $field, $filter, $sortingField);
            }
            foreach ($product->getVariants() as $variant) {
                if ($equalSize === '' && $equalColor === '') {
                    $filter = self::populateEntry($variant, $productUid, $field, $filter, $sortingField);
                } elseif ($equalSize === $variant->getSize()->getTitle() && $equalColor === '') {
                    $filter = self::populateEntry($variant, $productUid, $field, $filter, $sortingField);
                } elseif ($equalSize === '' && $equalColor === $variant->getColor()->getTitle()) {
                    $filter = self::populateEntry($variant, $productUid, $field, $filter, $sortingField);
                } elseif ($equalSize === $variant->getSize()->getTitle() && $equalColor === $variant->getColor()->getTitle()) {
                    $filter = self::populateEntry($variant, $productUid, $field, $filter, $sortingField);
                }
            }
        }
        //
        // Sorting
        switch ($sorting) {
            case 'asc':
                ksort($filter);
                break;
            case 'desc':
                krsort($filter);
                break;
            default:
        }
        return $filter;
    }

    /**
     * @param Product $product
     * @param int $productUid
     * @param string $field
     * @param array<mixed> $filter
     * @param string $sortingField
     * @return array<mixed>
     */
    protected static function populateEntry(Product $product, int $productUid, string $field, array $filter, string $sortingField = 'title'): array
    {
        $fieldTitle = self::getPropertyTitle($product, $field);
        $fieldIndex = $fieldTitle;
        if ($sortingField === 'sorting') {
            $fieldIndex = self::getPropertySorting($product, $field);
        }
        if (!isset($filter[$fieldIndex])) {
            $object = $product->_getProperty($field);
            if (!is_object($object)) {
                $object = null;
            }
            $filter[$fieldIndex] = [
                'label' => $fieldTitle,
                'product' => $product,
                'object' => $object,
                'active' => ($product->getUid() === $productUid),
            ];
        } elseif (!$filter[$fieldIndex]['active']) {
            $filter[$fieldIndex]['active'] = ($product->getUid() === $productUid);
        }
        return $filter;
    }

    protected static function getPropertyTitle(Product $product, string $field): string
    {
        $productField = $product->_getProperty($field);
        if (is_object($productField)) {
            $title = $productField->getTitle() ?? 'getTitle not found on object';
        } else {
            $title = $productField;
        }
        return $title;
    }

    protected static function getPropertySorting(Product $product, string $field): int
    {
        $sorting = 0;
        $productField = $product->_getProperty($field);
        if (is_object($productField)) {
            $sorting = $productField->getSorting() ?? 0;
        }
        return $sorting;
    }
}
