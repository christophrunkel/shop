<?php

namespace CodingMs\Shop\Log\Writer;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\LogRecord;
use TYPO3\CMS\Core\Log\Writer\AbstractWriter;
use TYPO3\CMS\Core\Log\Writer\WriterInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Log writer that writes the log records into a database table.
 */
class DatabaseWriter extends AbstractWriter
{
    /**
     * Table to write the log records to.
     *
     * @var string
     */
    protected $logTable = 'sys_log';

    /**
     * Set name of database log table
     *
     * @param string $tableName Database table name
     * @return AbstractWriter
     */
    public function setLogTable($tableName)
    {
        $this->logTable = $tableName;
        return $this;
    }

    /**
     * Get name of database log table
     *
     * @return string Database table name
     */
    public function getLogTable()
    {
        return $this->logTable;
    }

    /**
     * Writes the log record
     *
     * @param LogRecord $record Log record
     * @return WriterInterface $this
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function writeLog(LogRecord $record): WriterInterface
    {
        try {
            // Avoid database writes until boot has been completed.
            if (!GeneralUtility::getContainer()->get('boot.state')->complete) {
                return $this;
            }
        } catch (\LogicException $e) {
            // LogicException will be thrown if the container isn't available yet.
            return $this;
        }

        $data = '';
        $context = $record->getData();
        if (!empty($context)) {
            // Fold an exception into the message, and string-ify it into context so it can be jsonified.
            if (isset($context['exception']) && $context['exception'] instanceof \Throwable) {
                $context['exception'] = (string)$context['exception'];
            }
            $data = '- ' . json_encode($context);
        }

        $fieldValues = [
            'tstamp' => time(),
            'request_id' => $record->getRequestId(),
            'time_micro' => $record->getCreated(),
            'component' => $record->getComponent(),
            'level' => $record->getLevel(),
            'details' => $record->getMessage(),
            'data' => $data,
            'type' => 0,
            'action' => 0,
            'IP' => $_SERVER['REMOTE_ADDR'],
        ];
        if (LogLevel::normalizeLevel($record->getLevel()) <= 3) {
            $fieldValues['error'] = 2;
        }

        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable($this->logTable)
            ->insert($this->logTable, $fieldValues);

        return $this;
    }
}
