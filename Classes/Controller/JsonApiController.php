<?php

declare(strict_types=1);

namespace CodingMs\Shop\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Product;
use CodingMs\Shop\Domain\Repository\ProductCategoryRepository;
use CodingMs\Shop\Domain\Repository\ProductRepository;
use CodingMs\Shop\Domain\Repository\ProductTagCategoryRepository;
use CodingMs\Shop\Domain\Repository\ProductTagRepository;
use CodingMs\Shop\Domain\Session\SessionHandler;
use CodingMs\Shop\Service\BasketService;
use CodingMs\Shop\Service\TypoScriptService;
use CodingMs\Shop\Utility\ToolsUtility;
use Exception;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * JsonApiController
 * @noinspection PhpUnused
 */
class JsonApiController extends BaseController
{
    /**
     * @var array<string, mixed>
     */
    protected array $json = ['messages' => []];

    protected BasketService $basketService;

    public function __construct(
        SessionHandler $sessionHandler,
        ProductRepository $productRepository,
        ProductTagRepository $productTagRepository,
        ProductTagCategoryRepository $productTagCategoryRepository,
        ProductCategoryRepository $productCategoryRepository,
        BasketService $basketService
    ) {
        parent::__construct(
            $sessionHandler,
            $productRepository,
            $productTagRepository,
            $productTagCategoryRepository,
            $productCategoryRepository
        );
        $this->basketService = $basketService;
    }

    /**
     * Remind filter settings
     *
     * @return ResponseInterface
     * @throws NoSuchArgumentException
     */
    public function filterAction(): ResponseInterface
    {
        // Get content data
        $pid = (int)$GLOBALS['TSFE']->id;
        $tags = [];
        $tagCategories = [];
        if ($this->request->hasArgument('remind')) {
            $this->json['remind'] = ($this->request->getArgument('remind') === 'true');
            $this->json['isotope'] = '0';
            if ($this->request->hasArgument('isotope')) {
                if (is_string($isotope = $this->request->getArgument('isotope'))) {
                    $this->json['isotope'] = $isotope;
                }
                $isotopeFilterOr = GeneralUtility::trimExplode(',', $this->json['isotope'], true);
                if (is_array($isotopeFilterOr) && count($isotopeFilterOr)) {
                    foreach ($isotopeFilterOr as $filterOr) {
                        $isotopeFilterAnd = GeneralUtility::trimExplode('.', $filterOr, true);
                        if (is_array($isotopeFilterAnd) && count($isotopeFilterAnd)) {
                            foreach ($isotopeFilterAnd as $filterAnd) {
                                $filter = '.' . trim($filterAnd);
                                if (substr($filter, 0, 13) === '.product-tag-') {
                                    $tagUid = (int)substr($filter, 13);
                                    if ($tagUid > 0) {
                                        $tags[$tagUid] = $tagUid;
                                    }
                                }
                                if (substr($filter, 0, 22) === '.product-tag-category-') {
                                    $tagCategoryUid = (int)substr($filter, 22);
                                    if ($tagCategoryUid > 0) {
                                        $tagCategories[$tagCategoryUid] = $tagCategoryUid;
                                    }
                                }
                            }
                        }
                    }
                    $tags = array_unique($tags);
                    $tagCategories = array_unique($tagCategories);
                    // Store session data
                    $this->session = $this->sessionHandler->restoreFromSession();
                    $this->session[$pid]['filter']['isotope'] = $this->json['isotope'];
                    $this->session[$pid]['filter']['tag']['values'] = $tags;
                    $this->session[$pid]['filter']['tagCategory']['values'] = $tagCategories;
                    $this->sessionHandler->writeToSession($this->session);
                }
            }
        }
        // Return
        $this->json[$pid]['filter']['tag']['values'] = $tags;
        $this->json[$pid]['filter']['tagCategory']['values'] = $tagCategories;
        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write((string)json_encode($this->json));
        return $response;
    }

    /**
     * Update a product in basket
     *
     * @return ResponseInterface
     * @throws NoSuchArgumentException
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function updateBasketItemAction(): ResponseInterface
    {
        //
        // Initialize only?
        $initialize = false;
        if ($this->request->hasArgument('initialize')) {
            $initialize = true;
        }
        //
        // Is editable only?
        $editable = false;
        if ($this->request->hasArgument('editable')) {
            $editable = (int)$this->request->getArgument('editable');
        }
        //
        // Validate quantity
        $quantity = 0;
        if ($this->request->hasArgument('quantity')) {
            $quantity = (int)$this->request->getArgument('quantity');
        }
        //
        // Validate product
        $productUid = 0;
        if ($this->request->hasArgument('product')) {
            $productUid = (int)$this->request->getArgument('product');
        }
        //
        // Restore basket data
        $this->session = $this->sessionHandler->restoreFromSession();
        $basket = $this->session['basket'];
        if ($editable) {
            // Unset checkout type in edit mode!
            $this->session['checkout']['type'] = '';
        }
        //
        // Ensure the product exists
        if (!$initialize) {
            $product = $this->productRepository->findByUid($productUid);
            if ($product instanceof Product) {
                $product->fillFieldsFromParent();
                if (!isset($basket[$productUid]) || is_int($basket[$productUid])) {
                    // "is_int"-condition is for backward compatibility for old basket sessions
                    $basket[$productUid] = [
                        'quantity' => 0,
                        'customInformation' => '',
                    ];
                }
                //
                // Validate custom information
                if ($this->request->hasArgument('customInformation') && ($customInformation = $this->request->getArgument('customInformation')) !== '') {
                    if ($this->settings['pro']) {
                        $basket[$productUid]['customInformation'] = $customInformation;
                        // Write back into session
                        $this->session['basket'] = $basket;
                        $this->sessionHandler->writeToSession($this->session);
                        $this->json['messages']['ok'] = $this->translate('tx_shop_message.info_basket_item_custom_information_changed');
                        $this->json['status'] = 'success';
                    } else {
                        $this->json['messages']['danger'] = 'Custom information on basket items requires pro version!';
                        $this->json['status'] = 'error';
                    }
                }
                else if (!$product->isDigitalProduct() && $product->getInStock() && $product->getStockAmount() < $quantity) {
                    //
                    // Change product amount in basket
                    $quantity = $product->getStockAmount();
                    $basket[$productUid]['quantity'] = (int)$quantity;
                    $this->session['basket'] = $basket;
                    $this->sessionHandler->writeToSession($this->session);
                    $this->json['messages']['danger'] = $this->translate(
                        'tx_shop_message.error_basket_item_amount_not_available',
                        [
                            0 => $quantity,
                            1 => $product->getTitle()
                        ]
                    );
                    $this->json['status'] = 'error';
                } else {
                    $basket[$productUid]['quantity'] = $quantity;
                    // Write back into session
                    $this->session['basket'] = $basket;
                    $this->sessionHandler->writeToSession($this->session);
                    $this->json['messages']['ok'] = $this->translate('tx_shop_message.info_basket_item_quantity_changed', [$quantity]);
                    $this->json['status'] = 'success';
                }
            } else {
                $this->json['messages']['danger'] = $this->translate('tx_shop_message.error_product_not_found');
                $this->json['status'] = 'error';
            }
            //
            $this->json['product'] = $productUid;
            $this->json['quantity'] = $quantity;
        } else {
            $this->json['status'] = 'success';
        }
        //
        // In normal basket view, all prices are vatable!
        $vatable = true;
        if (isset($this->session['checkout']['vatable']) && !$editable) {
            $vatable = (bool)$this->session['checkout']['vatable'];
        }
        $basketObject = $this->basketService->getBasketObject(
            $basket,
            $this->settings,
            (string)$this->session['checkout']['type'],
            $vatable,
            $this->session['checkout']['vatOverlayCountryCode'] ?? null
        );
        if (trim($this->settings['debug']) === '1') {
            $this->json['basket'] = $basketObject->toArray();
            $this->json['session'] = $this->session;
            $this->json['settings'] = $this->settings;
            $this->json['initialize'] = $initialize;
            $this->json['editable'] = $editable;
        }
        $this->json['minimumOrderValueReached'] = $basketObject->getIsMinimumOrderValueReached();
        $this->json['orderValue'] = $basketObject->getFinalPrice();
        //
        // Render basket content
        $partialFileName = 'Basket/BasketContent.html';
        $configuration = TypoScriptService::getTypoScript((int)$this->settings['basketOrder']['checkoutPid']);
        $partialRootPath = ToolsUtility::getTemplatePath($configuration['plugin']['tx_shop']['view'], 'partial', $partialFileName);
        /** @var StandaloneView $listItemView */
        $listItemView = GeneralUtility::makeInstance(StandaloneView::class);
        if ((int)VersionNumberUtility::getCurrentTypo3Version() >= 12) {
            $listItemView->setRequest($this->request);
            $listItemView->getTemplatePaths()->fillDefaultsByPackageName('shop');
        } else {
            $listItemView->setControllerContext($this->controllerContext);
        }
        $listItemView->setTemplatePathAndFilename($partialRootPath . $partialFileName);
        $listItemView->assign('settings', $this->settings);
        $listItemView->assign('basket', $basketObject);
        $listItemView->assign('editable', $editable);
        $listItemView->assign('vatable', $vatable);
        if (!$editable) {
            // Don't use vat data and information in editable basket view!
            $listItemView->assign('vatId', $this->session['checkout']['vatId']);
            $listItemView->assign('vatType', $this->session['checkout']['vatType']);
            $listItemView->assign('hasVatId', $this->session['checkout']['hasVatId']);
            $listItemView->assign('vatZone', $this->session['checkout']['vatZone']);
            $listItemView->assign('vatNotice', $this->session['checkout']['vatNotice']);
        }
        $this->json['html'] = base64_encode($listItemView->render());
        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write((string)json_encode($this->json));
        return $response;
    }

    /**
     * @return ResponseInterface
     * @throws NoSuchArgumentException
     * @noinspection PhpUnused
     */
    public function addBasketItemAction(): ResponseInterface
    {
        //
        // Quantity
        $quantity = 0;
        if ($this->request->hasArgument('quantity')) {
            $quantity = (int)$this->request->getArgument('quantity');
        }
        //
        // Product
        $productUid = 0;
        if ($this->request->hasArgument('product')) {
            $productUid = (int)$this->request->getArgument('product');
        }
        //
        // Restore basket data
        $this->session = $this->sessionHandler->restoreFromSession();
        $basket = $this->session['basket'];
        //
        // Ensure the product exists
        $product = $this->productRepository->findByUid($productUid);
        if ($product instanceof Product) {
            $product->fillFieldsFromParent();
            if (!isset($basket[$productUid]) || is_int($basket[$productUid])) {
                // "is_int"-condition is for backward compatibility for old basket sessions
                $basket[$productUid] = [
                    'quantity' => 0,
                    'customInformation' => '',
                ];
            }
            //
            // Custom information
            if ($this->request->hasArgument('customInformation') && ($customInformation = $this->request->getArgument('customInformation')) !== '') {
                if($this->settings['pro']) {
                    $basket[$productUid]['customInformation'] = $customInformation;
                } else {
                    $this->json['messages']['danger'] = 'Custom information on basket items requires pro version!';
                    $this->json['status'] = 'error';
                }
            }
            if ($basket[$productUid]['quantity'] > 0) {
                $updated_quantity = $quantity + (int)$basket[$productUid]['quantity'];
            } else {
                $updated_quantity = $quantity;
            }
            if ($product->isDigitalProduct()) {
                $updated_quantity = 1;
            }
            if (!$product->isDigitalProduct() && $product->getInStock() && $product->getStockAmount() < $updated_quantity) {
                //
                // Change product amount in basket
                $quantity = $product->getStockAmount();
                $basket[$productUid]['quantity'] = (int)$quantity;
                $this->session['basket'] = $basket;
                $this->sessionHandler->writeToSession($this->session);
                $this->json['messages']['danger'] = $this->translate(
                    'tx_shop_message.error_basket_item_amount_not_available',
                    [
                        0 => $quantity,
                        1 => $product->getTitle()
                    ]
                );
                $this->json['status'] = 'error';
            } else {
                $basket[$productUid]['quantity'] = $updated_quantity;
                // Write back into session
                $this->session['basket'] = $basket;
                $this->sessionHandler->writeToSession($this->session);
                $this->json['messages']['ok'] = $this->translate(
                    'tx_shop_message.ok_basket_item_added',
                    [0 => $quantity]
                );
                $this->json['status'] = 'success';
            }
        } else {
            $this->json['messages']['danger'] = $this->translate('tx_shop_message.error_product_not_found');
            $this->json['status'] = 'error';
        }
        //
        $this->json['product'] = $productUid;
        $this->json['quantity'] = $quantity;
        $basketObject = $this->basketService->getBasketObject($basket, $this->settings);
        $this->json['basketItemQuantity'] = $basketObject->getBasketItemCount();
        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write((string)json_encode($this->json));
        return $response;
    }

    /**
     * Handle bookmark actions
     *
     * remind: Reminds an additional product. This value can be true or false
     * clear: Clear all reminded product bookmarks
     *
     * @return ResponseInterface
     * @throws NoSuchArgumentException
     * @noinspection PhpUnused
     */
    public function bookmarkAction(): ResponseInterface
    {
        // Restore session data
        $this->session = $this->sessionHandler->restoreFromSession();
        // Be sure the bookmarks structure is available
        if (!isset($this->session['bookmarks'])) {
            $this->session['bookmarks'] = [
                'items' => []
            ];
        }
        // Remind or forget
        if ($this->request->hasArgument('remind')) {
            $this->json['remind'] = ($this->request->getArgument('remind') === 'true');
            $this->json['product'] = 0;
            if ($this->request->hasArgument('product')) {
                $this->json['product'] = (int)$this->request->getArgument('product');
            }
            if ($this->json['remind']) {
                // First, check if max items are reached
                $maxItems = (int)($this->settings['bookmarks']['maxItems'] ?? 3);
                $currentCount = (int)($this->session['bookmarks']['count'] ?? 0);
                if ($currentCount >= $maxItems) {
                    $this->json['messages'] = [
                        'danger' => $this->translate('tx_shop_message.error_bookmark_max_items_reached')
                    ];
                } else {
                    $this->session['bookmarks']['items'][$this->json['product']] = $this->json['product'];
                    $this->json['messages'] = [
                        'info' => $this->translate('tx_shop_message.ok_bookmark_reminded')
                    ];
                }
            } else {
                if (isset($this->session['bookmarks']['items'][$this->json['product']])) {
                    unset($this->session['bookmarks']['items'][$this->json['product']]);
                    $this->json['messages'] = [
                        'info' => $this->translate('tx_shop_message.ok_bookmark_forgotten')
                    ];
                }
            }
        } elseif ($this->request->hasArgument('clear')) {
            $this->json['clear'] = ($this->request->getArgument('clear') === 'true');
            if ($this->json['clear']) {
                $this->session['bookmarks']['items'] = [];
                $this->json['messages'] = [
                    'info' => $this->translate('tx_shop_message.ok_bookmarks_cleared')
                ];
            }
        }
        $this->session['bookmarks']['count'] = count($this->session['bookmarks']['items']);
        $this->json['bookmarks'] = $this->session['bookmarks'];
        $this->sessionHandler->writeToSession($this->session);
        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write((string)json_encode($this->json));
        return $response;
    }
}
