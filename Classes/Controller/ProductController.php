<?php

declare(strict_types=1);

namespace CodingMs\Shop\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\FileReference;
use CodingMs\Shop\Domain\Model\Product;
use CodingMs\Shop\Domain\Model\ProductCategory;
use CodingMs\Shop\Domain\Model\ProductTag;
use CodingMs\Shop\Domain\Model\ProductTagCategory;
use CodingMs\Shop\PageTitle\PageTitleProvider;
use CodingMs\ShopPro\Service\PdfService;
use Exception;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotException;
use TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotReturnException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Product controller
 */
class ProductController extends BaseController
{
    /**
     * Selected Category-Object, if there is a selected one
     * @var ?ProductCategory
     */
    protected ?ProductCategory $selectedCategory = null;

    /**
     * Selected Tag-Object, if there is a selected one
     * @var ?ProductTag
     */
    protected ?ProductTag $selectedTag = null;

    /**
     * List view
     *
     * @throws InvalidQueryException
     * @throws InvalidSlotException
     * @throws InvalidSlotReturnException
     * @throws NoSuchArgumentException
     * @throws StopActionException
     */
    public function listAction(): ResponseInterface
    {
        // Prepare this action
        $this->prepareAction();
        //
        // Show a product immediately?
        if ((int)$this->settings['showProduct'] > 0) {
            $arguments = ['product' => $this->settings['showProduct']];
            return (new ForwardResponse('show'))->withArguments($arguments);
        }
        // Get products
        $filter = $this->settings['filter'];
        if (isset($this->settings['list']['layout']) && $this->settings['list']['layout'] == 'isotope') {
            // When Isotope is in use
            // Don't filter by repository
            $filter['category']['values'] = [];
            $filter['tag']['values'] = [];
            $filter['tagCategory']['values'] = [];
        }
        $products = $this->productRepository->findAllByFilter($filter);
        //
        $assignedValues = [
            'products' => $products,
            'content' => $this->content,
            'settings' => $this->settings,
            'session' => $this->session
        ];
        //
        $this->view->assignMultiple($assignedValues);
        return $this->htmlResponse();
    }

    /**
     * @throws InvalidQueryException
     * @throws NoSuchArgumentException
     * @throws Exception
     */
    protected function prepareAction(): void
    {
        // Restore session data
        $this->restoreSession();
        // Static template available?
        // Debug extension?
        if (!isset($this->settings['debug'])) {
            $this->addFlashMessage(
                'Please include the static template!',
                $this->translate('tx_shop_message.error_title'),
                AbstractMessage::ERROR
            );
        }
        $this->settings['debug'] = (boolean)$this->settings['debug'];
        $this->settings['filter']['word']['fields'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['word']['fields'] ?? '',
            true
        );
        //
        // Extract item elements
        $this->settings['list']['item']['elements'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['list']['item']['elements'] ?? '',
            true
        );
        //
        // Extract item elements
        $this->settings['detail']['fields'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['detail']['fields'] ?? '',
            true
        );
        //
        // Check if there are some invalid/outdated products
        $invalidProducts = $this->productRepository->findByPriceType('');
        if (count($invalidProducts)) {
            $messageTitle = 'Invalid price type!';
            $message = 'Try to run shop product migration wizard or ensure having selected a valid price type in products.';
            $invalidProductUids = [];
            foreach ($invalidProducts as $invalidProduct) {
                $invalidProductUids[] = $invalidProduct->getUid();
            }
            $message .= ' [product uids: ' . implode(', ', $invalidProductUids) . ']';
            $this->addFlashMessage($message, $messageTitle, AbstractMessage::ERROR);
            /** @var LogManager $logManager */
            $logManager = GeneralUtility::makeInstance(LogManager::class);
            $logger = $logManager->getLogger(__CLASS__);
            $logger->log(LogLevel::ERROR, $messageTitle . ' ' . $message);
            unset($message, $messageTitle);
        }
        //
        // Use filter fields
        $this->settings['filter']['fields'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['fields'] ?? '',
            true
        );
        //
        // Get content data
        $this->content = $this->configurationManager->getContentObject()->data ?? [];
        $this->view->assign('content', $this->content);
        $pid = $this->content['pid'];
        //
        // Validate search word
        if ($this->request->hasArgument('word')
            && is_string($word = $this->request->getArgument('word'))) {
            $this->session[$pid]['filter']['word']['value'] = trim($word);
        }
        if (!isset($this->session[$pid]['filter']['word'])) {
            $this->session[$pid]['filter']['word'] = ['value' => ''];
        }
        $this->settings['filter']['word']['value'] = $this->session[$pid]['filter']['word']['value'];
        //
        // Products categories
        //
        // settings.filter.category.allowed:
        // Allowed categories are defined by FlexForm or by available tags in storage
        // This is an array of tag uids
        //
        // settings.filter.category.options:
        // Collected categories which are passed to the select box/checkboxes
        // Finally these are the allowed tags.
        // This is an ObjectStorage of Category Model
        //
        // settings.filter.category.values:
        // Array with selected Tag Model
        //
        // Preparation
        $this->settings['filter']['category']['allowed'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['category']['values'] ?? '',
            true
        );
        // All possible/available categories for selection
        $this->settings['filter']['category']['options'] = $this->productCategoryRepository->findAllOrderedByTitle(
            $this->settings['filter']['category']['allowed']
        );
        // Ensure that categories values array is defined
        if (!isset($this->session[$pid]['filter']['category']['values'])) {
            $this->session[$pid]['filter']['category']['values'] = [];
        }
        // First: Is there are a passed category
        if ($this->request->hasArgument('category')) {
            $categoryUid = (int)$this->request->getArgument('category');
            // But the category must be allowed
            if (count($this->settings['filter']['category']['options']) > 0) {
                $this->session[$pid]['filter']['category']['values'] = [];
                /** @var ProductCategory $option */
                foreach ($this->settings['filter']['category']['options'] as $option) {
                    if ($option->getUid() === $categoryUid) {
                        $this->session[$pid]['filter']['category']['values'] = [
                            $categoryUid => $categoryUid
                        ];
                        $this->settings['filter']['category']['value'] = $option;
                    }
                }
            }
        }
        //
        // When there were no category passed by parameter,
        // try to restore from session.
        //
        // Difference between settings and session:
        // -> session contains only uids
        // -> settings the real model objects
        // -> session is additionally divided by page
        elseif (count($this->session[$pid]['filter']['category']['values']) > 0) {
            $this->settings['filter']['category']['values'] = [];
            foreach ($this->session[$pid]['filter']['category']['values'] as $categoryUid) {
                /** @var ProductCategory $category */
                $category = $this->productCategoryRepository->findByIdentifier($categoryUid);
                // But the tag must be allowed
                if (count($this->settings['filter']['category']['options']) > 0) {
                    /** @var ProductCategory $option */
                    foreach ($this->settings['filter']['category']['options'] as $option) {
                        if ($option->getUid() === $category->getUid()) {
                            $this->settings['filter']['category']['value'] = $category;
                        }
                    }
                }
            }
        }
        //
        // Products Tag
        //
        // settings.filter.tag.allowed:
        // Allowed tags are defined by FlexForm or by available tags in storage
        // This is an array of tag uids
        //
        // settings.filter.tag.options:
        // Collected tags which are passed to the select box/checkboxes
        // Finally these are the allowed tags.
        // This is an ObjectStorage of Tag Model
        //
        // settings.filter.tag.values:
        // Array with selected Tag Model
        //
        // Preparation
        if (!isset($this->session[$pid]['filter']['tag'])) {
            $this->session[$pid]['filter']['tag'] = [
                'values' => []
            ];
        }
        //
        // First of all, transfer possible selected tags from FlexForm into allowed
        $this->settings['filter']['tag']['allowed'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['tag']['values'] ?? '',
            true
        );
        // Then, find all tags depending on allowed tags.
        // This might be all available tags in storage (in case of no tags preselected in FlexForm),
        // or exactly the same as selected in FlexForm
        $this->settings['filter']['tag']['options'] = $this->productTagRepository->findAllOrderedByTitle(
            $this->settings['filter']['tag']['allowed']
        );
        //
        // First: Is there are a passed tag
        if ($this->request->hasArgument('tag')) {
            $tagArray = $this->request->getArgument('tag');
            if (!is_array($tagArray)) {
                $tagArray = [(int)$tagArray];
            }
            $tagArray = array_map('intval', $tagArray);
            $this->session[$pid]['filter']['tag']['values'] = [];
            foreach ($tagArray as $tagUid) {
                // But the tag must be allowed
                if (count($this->settings['filter']['tag']['options']) > 0 && $tagUid > 0) {
                    /** @var ProductTag $option */
                    foreach ($this->settings['filter']['tag']['options'] as $option) {
                        if ($option->getUid() === $tagUid) {
                            $this->session[$pid]['filter']['tag']['values'][$tagUid] = $tagUid;
                        }
                    }
                }
            }
            $this->settings['filter']['tag']['values'] = $this->session[$pid]['filter']['tag']['values'];
        } elseif ($this->request->hasArgument('submit')) {
            // Do nothing, because there might simple no tags has been selected
        }
        //
        // When there were no tag passed by parameter,
        // try to restore from session.
        //
        // Difference between settings and session:
        // -> session contains only uids
        // -> settings the real model objects
        // -> session is additionally divided by page
        elseif (count($this->session[$pid]['filter']['tag']['values']) > 0) {
            $this->settings['filter']['tag']['values'] = [];
            foreach ($this->session[$pid]['filter']['tag']['values'] as $tagUid) {
                $tag = $this->productTagRepository->findByIdentifier($tagUid);
                // But the tag must be allowed
                if ($tag instanceof ProductTag && count($this->settings['filter']['tag']['options']) > 0) {
                    /** @var ProductTag $option */
                    foreach ($this->settings['filter']['tag']['options'] as $option) {
                        if ($option->getUid() === $tag->getUid()) {
                            $this->settings['filter']['tag']['values'][$tag->getUid()] = $tag;
                        }
                    }
                }
            }
        }
        //
        // Product Tag Category
        //
        // settings.filter.tagCategory.allowed:
        // Allowed tag categories are defined by FlexForm or by available tag categories in storage
        // This is an array of tag uids
        //
        // settings.filter.tag.options:
        // Collected tag categories which are passed to the select box/checkboxes
        // Finally these are the allowed tags.
        // This is an ObjectStorage of Tag Model
        //
        // settings.filter.tag.values:
        // Array with selected Tag Model
        //
        // Preparation
        if (!isset($this->session[$pid]['filter']['tag'])) {
            $this->session[$pid]['filter']['tag'] = [
                'values' => []
            ];
        }
        //
        // First of all, transfer possible selected tags from FlexForm into allowed
        $this->settings['filter']['tagCategory']['allowed'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['filter']['tagCategory']['values'] ?? '',
            true
        );
        // Then, find all tag categories depending on allowed tag categories.
        // This might be all available tag categories in storage (in case of no tag categories preselected in FlexForm),
        // or exactly the same as selected in FlexForm
        $this->settings['filter']['tagCategory']['options'] = $this->productTagCategoryRepository->findAllOrderedByTitle(
            $this->settings['filter']['tagCategory']['allowed']
        );
        // Ensure that categories values array is defined
        if (!isset($this->session[$pid]['filter']['tagCategory']['values'])) {
            $this->session[$pid]['filter']['tagCategory']['values'] = [];
        }
        //
        // First: Is there are a passed tag category
        if ($this->request->hasArgument('tagCategory')) {
            $tagCategoryUid = (int)$this->request->getArgument('tagCategory');
            // But the tag category must be allowed
            if (count($this->settings['filter']['tagCategory']['options']) > 0) {
                /** @var ProductTagCategory $option */
                foreach ($this->settings['filter']['tagCategory']['options'] as $option) {
                    if ($option->getUid() === $tagCategoryUid) {
                        $this->session[$pid]['filter']['tagCategory']['values'] = [
                            $tagCategoryUid => $tagCategoryUid
                        ];
                    }
                }
            }
        }
        //
        // When there were no tag category passed by parameter,
        // try to restore from session.
        //
        // Difference between settings and session:
        // -> session contains only uids
        // -> settings the real model objects
        // -> session is additionally divided by page
        elseif (count($this->session[$pid]['filter']['tagCategory']['values']) > 0) {
            $this->settings['filter']['tagCategory']['values'] = [];
            foreach ($this->session[$pid]['filter']['tagCategory']['values'] as $tagCategoryUid) {
                $tagCategory = $this->productTagRepository->findByIdentifier($tagCategoryUid);
                // But the tag category must be allowed
                if ($tagCategory instanceof ProductTag
                    && count($this->settings['filter']['tagCategory']['options']) > 0) {
                    /** @var ProductTagCategory $option */
                    foreach ($this->settings['filter']['tagCategory']['options'] as $option) {
                        if ($option->getUid() === $tagCategory->getUid()) {
                            $this->settings['filter']['tagCategory']['values'][$tagCategory->getUid()] = $tagCategory;
                        }
                    }
                }
            }
        }

        // How to sort the result
        $this->settings['list']['sortBy'] = '';
        $this->settings['list']['sortFields'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['list']['sortFields'] ?? '',
            true
        );
        if (!empty($this->settings['list']['sortFields'])) {
            $sortFields = [];
            foreach ($this->settings['list']['sortFields'] as $sortField) {
                $langKey = GeneralUtility::camelCaseToLowerCaseUnderscored($sortField);
                $sortFields[$sortField] = LocalizationUtility::translate('tx_shop_label.sort_by_' . $langKey, 'shop');
                $this->settings['list']['sortBy'] = ($this->settings['list']['sortBy'] == '') ? $sortField : $this->settings['list']['sortBy'];
            }
            $this->settings['list']['sortFields'] = $sortFields;
        }
        // Current sorting type
        if ($this->request->hasArgument('sortBy')) {
            $tempSortBy = $this->request->getArgument('sortBy');
            if (in_array($tempSortBy, $this->settings['list']['sortFields'])) {
                $this->settings['list']['sortBy'] = $tempSortBy;
            }
        }
        //
        // Sort order (asc or desc)
        $this->settings['list']['sortOrderOptions'] = [];
        $this->settings['list']['sortOrderOptions']['asc'] = LocalizationUtility::translate('tx_shop_label.asc', 'shop');
        $this->settings['list']['sortOrderOptions']['desc'] = LocalizationUtility::translate('tx_shop_label.desc', 'shop');
        if ($this->request->hasArgument('sortOrder')) {
            $this->settings['list']['sortOrder'] = $this->request->getArgument('sortOrder');
        } elseif (isset($this->session[$pid]['list']['sortOrder'])) {
            $this->settings['list']['sortOrder'] = $this->session[$pid]['list']['sortOrder'];
        }
        // Validate sort order
        if (!isset($this->settings['list']['sortOrder']) || !array_key_exists($this->settings['list']['sortOrder'], $this->settings['list']['sortOrderOptions'])) {
            $this->settings['list']['sortOrder'] = 'asc';
        }
        //
        // Pass some more settings to the filter
        $this->settings['filter']['sortBy'] = $this->settings['list']['sortBy'];
        $this->settings['filter']['sortOrder'] = $this->settings['list']['sortOrder'];
        $this->settings['filter']['showHighlightedOnly'] = $this->settings['list']['showHighlightedOnly'];
        $this->settings['filter']['listLimit'] = (int)$this->settings['list']['limit'];
        $this->settings['filter']['listOffset'] = (int)$this->settings['list']['offset'];
        //
        // Override detail page uid by flexform value, if it's set!
        $detailPageUid = (int)$this->settings['detail']['pageUid'];
        if ($detailPageUid > 0) {
            $this->settings['detail']['defaultPid'] = $detailPageUid;
        }
        unset($this->settings['detail']['pageUid']);
        //
        // Reset filter settings
        if ($this->request->hasArgument('reset')) {
            $this->session[$pid]['filter']['word']['value'] = '';
            $this->session[$pid]['filter']['tag']['values'] = [];
            $this->session[$pid]['filter']['tagCategory']['values'] = [];
            $this->session[$pid]['filter']['category']['values'] = [];
            $this->settings['filter']['word']['value'] = '';
            $this->settings['filter']['tag']['values'] = [];
            $this->settings['filter']['tagCategory']['values'] = [];
            $this->settings['filter']['category']['values'] = [];
        }
        //
        // Write back to session
        $this->sessionHandler->writeToSession($this->session);
    }

    /**
     * Product Single-View
     *
     * @throws NoSuchArgumentException
     * @throws Exception
     */
    public function showAction(): ResponseInterface
    {
        // Prepare this action
        $this->prepareAction();
        /**
         * @todo wenn die liste via category etc. eingegrenzt wurde, duerfen auch nur die Produkte angezeigt werden
         * welche in dieser Gruppe enthalten sind!!!
         */
        //
        // Fetch product object
        $product = null;
        $productUid = 0;
        if ($this->request->hasArgument('product')) {
            $productUid = (int)$this->request->getArgument('product');
            if ($productUid > 0) {
                $product = $this->productRepository->findByIdentifier($productUid);
                $product->fillFieldsFromParent();
            }
        }
        //
        // Redirect when there is no product object
        if (!($product instanceof Product)) {
            //
            // Check if we need to redirect to a special page
            if ($this->settings['detail']['notFoundPid'] > 0) {
                /** @var UriBuilder $uriBuilder */
                $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
                $uri = $uriBuilder->reset()
                    ->setTargetPageUid((int)$this->settings['detail']['notFoundPid'])
                    ->setArguments([
                        'tx_shop_products' => [
                            'action' => 'show',
                            'controller' => 'Product',
                            'product' => $productUid,
                        ]
                    ])->setCreateAbsoluteUri(true)
                    ->build();
                header('Location: ' . $uri);
                exit;
            }
        } else {
            //
            // Build PDF
            if ($this->request->hasArgument('createPdf')) {
                $createPdf = (int)$this->request->getArgument('createPdf');
                if ($createPdf > 0) {
                    if ($this->settings['pro']) {
                        $pdfService = GeneralUtility::makeInstance(PdfService::class);
                        if ($pdfService instanceof PdfService) {
                            $pdfService->createProductPdf($product, $this->settings);
                        }
                    } else {
                        throw new Exception(
                            (string)LocalizationUtility::translate(
                                'tx_shop_message.error_pro_version_required',
                                'shop'
                            )
                        );
                    }
                }
            }
            //
            // Process meta information
            $this->injectMetaInformation($product, $this->settings['siteName']);
        }
        //
        // Is there a search word?!
        $searchWord = '';
        if ($this->request->hasArgument('searchWord')) {
            $searchWord = $this->request->getArgument('searchWord');
        }
        $this->restoreSession();
        $this->view->assign('searchWord', $searchWord);
        $this->view->assign('product', $product);
        $this->view->assign('content', $this->content);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('session', $this->session);
        return $this->htmlResponse();
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTypoScriptFrontendController(): TypoScriptFrontendController
    {
        return $GLOBALS['TSFE'];
    }

    /**
     * @param Product $product
     * @param string $siteName
     */
    protected function injectMetaInformation(Product $product, string $siteName = ''): void
    {
        //
        // Insert canonical tag depending on product canonical link
        if ($product->getCanonicalLink() !== '') {
            $canonicalLink = $product->getCanonicalLink();
            if (substr($canonicalLink, 0, strlen('t3://page?uid=')) === 't3://page?uid=') {
                $pageId = (int)str_replace('t3://page?uid=', '', $canonicalLink);
                /** @var UriBuilder $uriBuilder */
                $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
                $canonicalLink = $uriBuilder->reset()
                    ->setTargetPageUid($pageId)
                    ->setArguments([
                        'tx_shop_products' => [
                            'action' => 'show',
                            'controller' => 'Product',
                            'product' => $product->getUid(),
                        ]
                    ])
                    ->setCreateAbsoluteUri(true)
                    ->build();
            }
            $typoScriptFrontendController = $this->getTypoScriptFrontendController();
            $typoScriptFrontendController->page['canonical_link'] = $canonicalLink;
        }
        //
        /** @var MetaTagManagerRegistry $metaTagManagerRegistry */
        $metaTagManagerRegistry = GeneralUtility::makeInstance(MetaTagManagerRegistry::class);

        $metaTagManagerDescription = $metaTagManagerRegistry->getManagerForProperty('description');
        $metaTagManagerDescription->addProperty('description', $product->getMetaDescription());

        $metaTagManagerAbstract = $metaTagManagerRegistry->getManagerForProperty('abstract');
        $metaTagManagerAbstract->addProperty('abstract', $product->getMetaAbstract());

        $metaTagManagerKeywords = $metaTagManagerRegistry->getManagerForProperty('keywords');
        $metaTagManagerKeywords->addProperty('keywords', $product->getMetaKeywords());

        $metaTagManagerOgTitle = $metaTagManagerRegistry->getManagerForProperty('og:title');
        $metaTagManagerOgTitle->addProperty('og:title', $product->getHtmlTitle());

        $metaTagManagerOgDescription = $metaTagManagerRegistry->getManagerForProperty('og:description');
        $metaTagManagerOgDescription->addProperty('og:description', $product->getMetaDescription());

        $metaTagManagerOgType = $metaTagManagerRegistry->getManagerForProperty('og:type');
        $metaTagManagerOgType->addProperty('og:type', 'WebPage');

        $metaTagManagerOgSiteName = $metaTagManagerRegistry->getManagerForProperty('og:site_name');
        $metaTagManagerOgSiteName->addProperty('og:site_name', $siteName);

        $requestUrl = GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL');
        if (is_string($requestUrl)) {
            $metaTagManagerOgUrl = $metaTagManagerRegistry->getManagerForProperty('og:url');
            $metaTagManagerOgUrl->addProperty('og:url', $requestUrl);
        }
        //
        // Preview image
        $previewImages = $product->getImagesPreviewOnly();
        if (count($previewImages) > 0) {
            /** @var FileReference $previewImage */
            $previewImage = $previewImages[0];
            /** @var ImageService $imageService */
            $imageService = GeneralUtility::makeInstance(ImageService::class);
            $image = $imageService->getImage($previewImage->getLink(), $previewImage, true);
            //
            // Cropping
            if ($image->hasProperty('crop') && $image->getProperty('crop')) {
                $cropString = $image->getProperty('crop');
            }
            $cropVariantCollection = CropVariantCollection::create((string)($cropString ?? ''));
            $cropVariant = 'default';
            $cropArea = $cropVariantCollection->getCropArea($cropVariant);
            $processingInstructions = [
                'width' => 600,
                'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
            ];
            //
            $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
            $imageUrl = $imageService->getImageUri($processedImage, true);
            $width = $processedImage->getProperty('width');
            $height = $processedImage->getProperty('height');
            //
            $metaTagManagerOgImage = $metaTagManagerRegistry->getManagerForProperty('og:image');
            $metaTagManagerOgImage->addProperty('og:image', $imageUrl);
            $metaTagManagerOgImageUrl = $metaTagManagerRegistry->getManagerForProperty('og:image:url');
            $metaTagManagerOgImageUrl->addProperty('og:image:url', $imageUrl);

            $metaTagManagerOgImageWidth = $metaTagManagerRegistry->getManagerForProperty('og:image:width');
            $metaTagManagerOgImageWidth->addProperty('og:image:width', (string)$width);
            $metaTagManagerOgImageHeight = $metaTagManagerRegistry->getManagerForProperty('og:image:height');
            $metaTagManagerOgImageHeight->addProperty('og:image:height', (string)$height);
        }
        //
        /** @var PageTitleProvider $seoTitlePageTitleProvider */
        $seoTitlePageTitleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
        $seoTitlePageTitleProvider->setTitle($product->getHtmlTitle());
    }

    /**
     * Display product quick search
     */
    public function showQuickSearchAction(): ResponseInterface
    {
        // Do nothing, only show search form!
        return $this->htmlResponse();
    }
}
