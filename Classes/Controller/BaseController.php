<?php

declare(strict_types=1);

namespace CodingMs\Shop\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Repository\ProductCategoryRepository;
use CodingMs\Shop\Domain\Repository\ProductRepository;
use CodingMs\Shop\Domain\Repository\ProductTagCategoryRepository;
use CodingMs\Shop\Domain\Repository\ProductTagRepository;
use CodingMs\Shop\Domain\Session\SessionHandler;
use CodingMs\Shop\Service\TypoScriptService;
use Exception;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Base for shop controller
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class BaseController extends ActionController
{
    protected ProductRepository $productRepository;
    protected ProductTagRepository $productTagRepository;
    protected ProductTagCategoryRepository $productTagCategoryRepository;
    protected ProductCategoryRepository $productCategoryRepository;
    protected SessionHandler $sessionHandler;

    /**
     * @var array<string, mixed>
     */
    protected array $session = [];

    /**
     * @var array<string, mixed>
     */
    protected array $content = [];

    public function __construct(
        SessionHandler $sessionHandler,
        ProductRepository $productRepository,
        ProductTagRepository $productTagRepository,
        ProductTagCategoryRepository $productTagCategoryRepository,
        ProductCategoryRepository $productCategoryRepository
    ) {
        $this->sessionHandler = $sessionHandler;
        $this->productRepository = $productRepository;
        $this->productTagRepository = $productTagRepository;
        $this->productTagCategoryRepository = $productTagCategoryRepository;
        $this->productCategoryRepository = $productCategoryRepository;
    }

    /**
     * @throws SiteNotFoundException
     */
    public function initializeAction(): void
    {
        //
        // Always work on settings from base extension!
        // Controller in Pro-Extension will inherit this class
        $configuration = TypoScriptService::getTypoScript((int)$GLOBALS['TSFE']->id);
        $this->settings = array_replace_recursive(
            $this->settings,
            $configuration['plugin']['tx_shop']['settings']
        );
        //
        // Pro available and activated?
        $pro = isset($this->settings['pro']) ? (bool)$this->settings['pro'] : false;
        $this->settings['pro'] = ($pro && ExtensionManagementUtility::isLoaded('shop_pro'));
    }

    /**
     * Restore session data and initialize bookmarks objects
     *
     * @throws Exception
     */
    protected function restoreSession(): void
    {
        $this->session = $this->sessionHandler->restoreFromSession();
    }

    /**
     * Prepare, because initialize action don't have the FlashMessageQueue
     *
     * @throws Exception
     */
    protected function prepareAction(): void
    {
        // Extract bookmarks compare exclude fields
        $this->settings['bookmarks']['compare']['fields']['exclude'] = GeneralUtility::trimExplode(
            ',',
            $this->settings['bookmarks']['compare']['fields']['exclude'],
            true
        );
        // Restore session data
        $this->restoreSession();
        // Static template available?
        // Debug extension?
        if (!isset($this->settings['debug'])) {
            $messageBody = 'Please include the static template!';
            $messageTitle = $this->translate('tx_shop_message.error_title');
            $this->addFlashMessage($messageBody, $messageTitle, AbstractMessage::ERROR);
        }
        $this->settings['debug'] = (boolean)$this->settings['debug'];
        //
        // Get content data
        $this->content = $this->configurationManager->getContentObject()->data ?? [];
        $this->view->assign('content', $this->content);
    }

    /**
     * @param string $key
     * @param array<int, int|string> $arguments
     * @return string
     */
    protected function translate(string $key, array $arguments = []): string
    {
        return (string)LocalizationUtility::translate($key, 'Shop', $arguments);
    }
}
