<?php

namespace CodingMs\Shop\FormEngine\ItemsProcFunc;

use CodingMs\Shop\Service\TypoScriptService;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Product
{
    /**
     * @param array $params
     * @throws SiteNotFoundException
     * @throws DBALException
     * @throws Exception
     */
    public function filterByStorage(array &$params): void
    {
        $pid = $params['flexParentDatabaseRow']['pid'];
        $typoScript = TypoScriptService::getTypoScript($pid);
        $storage = (int)$typoScript['plugin']['tx_shop']['persistence']['storagePid'] ?? 0;
        $pages = GeneralUtility::trimExplode(',', $params['flexParentDatabaseRow']['pages'], true);
        $pages = array_map('intval', $pages);
        $table = $params['config']['foreign_table'];
        /** @var ConnectionPool $connectionPool*/
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable($table);
        $queryBuilder
            ->select('uid', 'pid')
            ->from($table);
        /** @var \Doctrine\DBAL\Result $result*/
        $result = $queryBuilder->execute();
        $records = $result->fetchAllAssociative();

        if (!empty($pages)) {
            $filter = function ($record) use ($pages) {
                return in_array($record['pid'], $pages);
            };
        } else {
            $filter = function ($record) use ($storage) {
                return $record['pid'] === $storage;
            };
        }
        $filteredRecords = array_filter($records, $filter);
        $filteredUids = array_map(function ($filteredRecord) {return $filteredRecord['uid'];}, $filteredRecords);
        $params['items'] = array_filter($params['items'], function ($item) use ($filteredUids) {return in_array($item[1], $filteredUids);});
    }
}
