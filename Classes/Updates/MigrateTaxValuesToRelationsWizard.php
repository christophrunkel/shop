<?php

declare(strict_types=1);

namespace CodingMs\Shop\Updates;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class MigrateTaxValuesToRelationsWizard implements UpgradeWizardInterface
{
    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'shop_migrateTaxValuesToRelationsWizard';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Migrate tax values of products to new relations with tax records';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Will migrate old tax values of products to relations. Distinct tax values will be collected and created as records in the new tax table.';
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception|DBALException
     */
    public function executeUpdate(): bool
    {
        $oldTaxValues = $this->getOldTaxValuesUsed();
        if ($oldTaxValues) {
            $existingTaxRecords = $this->getExistingTaxRecords();
            foreach ($oldTaxValues as $key => $oldTaxValue) {
                foreach ($existingTaxRecords as $existingTagRecord) {
                    if ($existingTagRecord['value'] === $oldTaxValue['tax'] && $existingTagRecord['pid'] === $oldTaxValue['pid']) {
                        unset($oldTaxValues[$key]);
                    }
                }
            }

            // if there are still values in the array we need to create new records
            foreach ($oldTaxValues as $oldTaxValue) {
                /** @var Connection $connection */
                $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_shop_domain_model_producttax');
                $connection->insert(
                    'tx_shop_domain_model_producttax',
                    [
                        'pid' => (int)$oldTaxValue['pid'],
                        'value' => (int)$oldTaxValue['tax'],
                        'title' => number_format((int)$oldTaxValue['tax'] / 100, 2, ',') . ' %',
                    ],
                    [
                        Connection::PARAM_INT,
                        Connection::PARAM_INT,
                        Connection::PARAM_STR,
                    ]
                );
            }

            $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_shop_domain_model_product');
            $sql = '
                UPDATE tx_shop_domain_model_product
                LEFT OUTER JOIN tx_shop_domain_model_producttax
                ON tx_shop_domain_model_product.tax = tx_shop_domain_model_producttax.value
                    AND tx_shop_domain_model_product.pid = tx_shop_domain_model_producttax.pid
                SET tx_shop_domain_model_product.tax = tx_shop_domain_model_producttax.uid
                WHERE tx_shop_domain_model_producttax.value IS NOT NULL
            ';
            $query = $connection->prepare($sql);
            $query->executeStatement();

            return true;
        }
        return false;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool Whether an update is required (TRUE) or not (FALSE)
     * @throws Exception
     * @throws DBALException
     */
    public function updateNecessary(): bool
    {
        if ($this->getOldTaxValuesUsed()) {
            return true;
        }
        return false;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [];
    }

    /**
     * Returns an array of tax values lower than 10000
     *
     * 10000 is the starting autoincrement value of the tax table
     * to make sure that no referenced uid is considered to be a tax value
     *
     * @return array
     * @throws DBALException
     * @throws Exception
     */
    protected function getOldTaxValuesUsed(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_shop_domain_model_product');
        return $queryBuilder
            ->select('pid', 'tax')
            ->from('tx_shop_domain_model_product')
            ->where(
                $queryBuilder->expr()->lt('tax', 10000),
            )
            ->groupBy('pid', 'tax')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    /**
     * Returns an array of existing tax records
     *
     * @return array
     * @throws DBALException
     * @throws Exception
     */
    protected function getExistingTaxRecords(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_shop_domain_model_producttax');
        return $queryBuilder
            ->select('*')
            ->from('tx_shop_domain_model_producttax')
            ->executeQuery()
            ->fetchAllAssociative();
    }
}
