<?php

declare(strict_types=1);

namespace CodingMs\Shop\Updates;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class MigrateProductColorWizard implements UpgradeWizardInterface
{
    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'shop_migrateProductColorWizard';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Attaches color to products';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Searches for products with no product color or string and creates new or sets to existing product color';
    }

    /**
     * Execute the update - update colors set to 0 or '' then update colors set to strings
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     * @throws DBALException
     * @throws Exception
     */
    public function executeUpdate(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_product');
        //
        // Find pids of records where color set to '' or 0
        $result = $queryBuilder
            ->select('pid')
            ->from('tx_shop_domain_model_product')
            ->orWhere($queryBuilder->expr()->eq(
                'color',
                $queryBuilder->createNamedParameter('', Connection::PARAM_STR)
            ))
            ->orWhere($queryBuilder->expr()->eq(
                'color',
                $queryBuilder->createNamedParameter(0, Connection::PARAM_STR)
            ))
            ->groupBy('pid')
            ->executeQuery();
        //
        // Get each pid in turn
        while ($row = $result->fetchAssociative()) {
            $pid = $row['pid'];
            //
            // Get default color or newly created default color uid
            $newProductColorUid = $this->getProductColor($pid);
            //
            // Attach color to products with that pid
            $productQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_domain_model_product');
            $productQueryBuilder->update('tx_shop_domain_model_product')
                ->where($productQueryBuilder->expr()->eq(
                    'color',
                    $productQueryBuilder->createNamedParameter('', Connection::PARAM_STR)
                ))
                ->orWhere($productQueryBuilder->expr()->eq(
                    'color',
                    $productQueryBuilder->createNamedParameter(0, Connection::PARAM_STR)
                ))
                ->andWhere($productQueryBuilder->expr()->eq(
                    'pid',
                    $productQueryBuilder->createNamedParameter($pid, Connection::PARAM_INT)
                ))
                ->set('color', $newProductColorUid)
                ->execute();
        }
        //
        // Find pids of records where color is a non-numeric string
        $result = $queryBuilder
            ->select('pid', 'color')
            ->from('tx_shop_domain_model_product')
            ->where(
                $queryBuilder->expr()->comparison(
                    $queryBuilder->quoteIdentifier('color'),
                    'REGEXP',
                    $queryBuilder->createNamedParameter('[[:alpha:]]')
                )
            )
            ->groupBy('pid', 'color')
            ->executeQuery();
        //
        // Get each pid and color in turn
        while ($row = $result->fetchAssociative()) {
            $pid = $row['pid'];
            $color = $row['color'];
            //
            // Create new color record
            $newProductColorUid = $this->setProductColor($pid, $color);
            //
            // Attach color to products
            $productQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_domain_model_product');
            $productQueryBuilder->update('tx_shop_domain_model_product')
                ->where($productQueryBuilder->expr()->eq(
                    'color',
                    $productQueryBuilder->createNamedParameter($color, Connection::PARAM_STR)
                ))
                ->andWhere($productQueryBuilder->expr()->eq(
                    'pid',
                    $productQueryBuilder->createNamedParameter($pid, Connection::PARAM_INT)
                ))
                ->set('color', $newProductColorUid)
                ->execute();
        }
        return true;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool
     * @throws DBALException
     */
    public function updateNecessary(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_product');
        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from('tx_shop_domain_model_product')
            ->orWhere($queryBuilder->expr()->eq(
                'color',
                $queryBuilder->createNamedParameter('', Connection::PARAM_STR)
            ))
            ->orWhere($queryBuilder->expr()->eq(
                'color',
                $queryBuilder->createNamedParameter(0, Connection::PARAM_STR)
            ))
            ->orWhere(
                $queryBuilder->expr()->comparison(
                    $queryBuilder->quoteIdentifier('color'),
                    'REGEXP',
                    $queryBuilder->createNamedParameter('[[:alpha:]]')
                )
            )
            ->execute();
        return $numberOfEntries > 0;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [];
    }

    /**
     * @param int $pid
     * @return array
     */
    public function getProductColorDefaultValues(int $pid): array
    {
        $fieldValues = [];
        $fieldValues['title'] = 'default';
        $fieldValues['hex'] = '#ff0000';
        $fieldValues['pid'] = $pid;
        return $fieldValues;
    }

    /**
     * @param int $pid
     * @return int
     * @throws DBALException
     * @throws Exception
     */
    public function getProductColor(int $pid): int
    {
        // Get product color uid or create new one if doesn't exist
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_productcolor');
        $productColorUid = $queryBuilder
            ->select('uid')
            ->from('tx_shop_domain_model_productcolor')
            ->where(
                $queryBuilder->expr()->eq('pid', $pid)
            )
            ->execute()
            ->fetchOne();
        if (!$productColorUid) {
            $fieldValues = $this->getProductColorDefaultValues($pid);
            $productColorQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_domain_model_productcolor');
            $productColorQueryBuilder->insert('tx_shop_domain_model_productcolor')->values($fieldValues)->execute();
            $productColorUid = (int)$productColorQueryBuilder->getConnection()->lastInsertId();
        }
        return $productColorUid;
    }

    /**
     * @param int $pid
     * @param string $color
     * @return int
     * @throws DBALException
     */
    public function setProductColor(int $pid, string $color): int
    {
        $fieldValues = [];
        $fieldValues['title'] = $color;
        $fieldValues['hex'] = '#ff0000';
        $fieldValues['pid'] = $pid;
        $productColorQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_productcolor');
        $productColorQueryBuilder->insert('tx_shop_domain_model_productcolor')->values($fieldValues)->execute();
        return (int)$productColorQueryBuilder->getConnection()->lastInsertId();
    }
}
