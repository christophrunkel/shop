<?php

declare(strict_types=1);

namespace CodingMs\Shop\Updates;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Result;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class AttachFrontendUserToBasketOrderWizard implements UpgradeWizardInterface
{
    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'shop_attachFrontendUserToBasketOrderWizard';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Attach frontend user to basket order';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Searches for frontend user which belongs to basket order and connect them to each other';
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function executeUpdate(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_basketorder');
        $result = $queryBuilder
            ->select('*')
            ->from('tx_shop_domain_model_basketorder')
            ->where(
                $queryBuilder->expr()->eq('frontend_user', 0)
            )
            ->executeQuery();
        while ($row = $result->fetchAssociative()) {
            $queryBuilder->update('tx_shop_domain_model_basketorder')
                ->set('frontend_user', $this->getFrontendUserUidByEmail($row['email']), false)
                ->where(
                    $queryBuilder->expr()->eq('uid', $row['uid'])
                );
            $queryBuilder->execute();
        }
        return true;
    }

    /**
     * Store fetched frontend user in order to reduce amount of performed queries
     * @var array
     */
    protected array $frontendUser = [];

    protected function getFrontendUserUidByEmail(string $email): int
    {
        if (isset($this->frontendUser[$email])) {
            return $this->frontendUser[$email]['uid'] ?? 0;
        }
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('fe_users');
        $result = $queryBuilder
            ->select('*')
            ->from('fe_users')
            ->where(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->eq('username', $queryBuilder->createNamedParameter($email)),
                    $queryBuilder->expr()->eq('email', $queryBuilder->createNamedParameter($email))
                )
            )
            ->executeQuery();
        if ($result instanceof Result) {
            if (is_array($frontendUser = $result->fetchAssociative())) {
                $this->frontendUser[$email] = $frontendUser;
                return $this->frontendUser[$email]['uid'] ?? 0;
            }
        }
        return 0;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool Whether an update is required (TRUE) or not (FALSE)
     * @throws Exception
     * @throws DBALException
     */
    public function updateNecessary(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_basketorder');
        $result = $queryBuilder
            ->count('uid')
            ->from('tx_shop_domain_model_basketorder')
            ->where(
                $queryBuilder->expr()->eq('frontend_user', 0)
            )
            ->executeQuery();
        return (bool)($result->fetchOne() ?? 0);
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [];
    }
}
