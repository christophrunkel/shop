<?php

declare(strict_types=1);

namespace CodingMs\Shop\Updates;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class MigrateProductTypeWizard implements UpgradeWizardInterface
{
    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'shop_migrateProductTypeWizard';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Attaches product type to products';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Searches for products with no product type and sets to existing product type or creates and sets a default product type';
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     * @throws DBALException
     * @throws Exception
     */
    public function executeUpdate(): bool
    {
        // Get pids for products with missing product type
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_product');
        $result = $queryBuilder
            ->select('pid')
            ->from('tx_shop_domain_model_product')
            ->where(
                $queryBuilder->expr()->eq('product_type', 0)
            )
            ->groupBy('pid')
            ->executeQuery();
        //
        // Get each pid in turn
        while ($row = $result->fetchAssociative()) {
            $pid = $row['pid'];
            //
            // Get product type uid if one already exists for pid
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_domain_model_producttype');
            $productTypeUid = $queryBuilder
                ->select('uid')
                ->from('tx_shop_domain_model_producttype')
                ->where(
                    $queryBuilder->expr()->eq('pid', $pid)
                )
                ->execute()
                ->fetchOne();
            //
            // Create a product type for pid if none exists
            if (!$productTypeUid) {
                $fieldValues = $this->getProductTypeDefaultValues($pid);
                $productTypeQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                    ->getQueryBuilderForTable('tx_shop_domain_model_producttype');
                $productTypeQueryBuilder->insert('tx_shop_domain_model_producttype')->values($fieldValues)->execute();
                $productTypeUid = (int)$productTypeQueryBuilder->getConnection()->lastInsertId();
            }
            //
            // Attach product type to products with that pid
            $productQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_domain_model_product');
            $productQueryBuilder->update('tx_shop_domain_model_product')
                ->where($productQueryBuilder->expr()->eq(
                    'product_type',
                    $productQueryBuilder->createNamedParameter(0, Connection::PARAM_INT)
                ))
                ->andWhere($productQueryBuilder->expr()->eq(
                    'pid',
                    $productQueryBuilder->createNamedParameter($pid, Connection::PARAM_INT)
                ))
                ->set('product_type', $productTypeUid)
                ->execute();
        }
        return true;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool Whether an update is required (TRUE) or not (FALSE)
     * @throws Exception
     * @throws DBALException
     */
    public function updateNecessary(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_product');
        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from('tx_shop_domain_model_product')
            ->where(
                $queryBuilder->expr()->eq('product_type', 0)
            )
        ->execute()
        ->fetchOne();
        return $numberOfEntries > 0;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [];
    }

    /**
     * @param int $pid
     * @return array
     */
    public function getProductTypeDefaultValues(int $pid): array
    {
        $fieldValues = [];
        $fieldValues['title'] = 'default';
        $fieldValues['pid'] = $pid;
        foreach ($GLOBALS['TCA']['tx_shop_domain_model_producttype']['columns'] as $key => $value) {
            if (str_ends_with($key, '_enabled')) {
                $fieldValues[$key] = 1;
            }
        }
        return $fieldValues;
    }
}
