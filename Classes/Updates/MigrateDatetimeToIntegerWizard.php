<?php

declare(strict_types=1);

namespace CodingMs\Shop\Updates;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class MigrateDatetimeToIntegerWizard implements UpgradeWizardInterface
{
    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'shop_migrateDatetimeToIntegerWizard';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Migrate datetime to integer';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Will migrate old datetime values of basket order fields from datetime to integer. Has to be run before trying to update database fields!';
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function executeUpdate(): bool
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_shop_domain_model_basketorder');
        $sql = [
            'UPDATE tx_shop_domain_model_basketorder SET order_date = 0 WHERE order_date IS NULL;',
            'UPDATE tx_shop_domain_model_basketorder SET paid_date = 0 WHERE paid_date IS NULL;',
            'UPDATE tx_shop_domain_model_basketorder SET send_date = 0 WHERE send_date IS NULL;',

            "ALTER TABLE tx_shop_domain_model_basketorder MODIFY COLUMN order_date varchar(32) DEFAULT '0' NOT NULL;",
            "ALTER TABLE tx_shop_domain_model_basketorder MODIFY COLUMN paid_date varchar(32) DEFAULT '0' NOT NULL;",
            "ALTER TABLE tx_shop_domain_model_basketorder MODIFY COLUMN send_date varchar(32) DEFAULT '0' NOT NULL;",

            "UPDATE tx_shop_domain_model_basketorder SET order_date = UNIX_TIMESTAMP(order_date) WHERE order_date != '0000-00-00 00:00:00';",
            "UPDATE tx_shop_domain_model_basketorder SET order_date = '0' WHERE order_date = '0000-00-00 00:00:00';",
            "UPDATE tx_shop_domain_model_basketorder SET paid_date = UNIX_TIMESTAMP(paid_date) WHERE paid_date != '0000-00-00 00:00:00';",
            "UPDATE tx_shop_domain_model_basketorder SET paid_date = '0' WHERE paid_date = '0000-00-00 00:00:00';",
            "UPDATE tx_shop_domain_model_basketorder SET send_date = UNIX_TIMESTAMP(send_date) WHERE send_date != '0000-00-00 00:00:00';",
            "UPDATE tx_shop_domain_model_basketorder SET send_date = '0' WHERE send_date = '0000-00-00 00:00:00';",

            "ALTER TABLE tx_shop_domain_model_basketorder MODIFY COLUMN order_date int(11) DEFAULT '0' NOT NULL;",
            "ALTER TABLE tx_shop_domain_model_basketorder MODIFY COLUMN paid_date int(11) DEFAULT '0' NOT NULL;",
            "ALTER TABLE tx_shop_domain_model_basketorder MODIFY COLUMN send_date int(11) DEFAULT '0' NOT NULL;",

        ];

        foreach ($sql as $queryLine) {
            $query = $connection->prepare($queryLine);
            $query->executeStatement();
        }
        return true;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool Whether an update is required (TRUE) or not (FALSE)
     * @throws Exception
     * @throws DBALException
     */
    public function updateNecessary(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_shop_domain_model_basketorder');
        $result = $queryBuilder
                ->select('order_date', 'paid_date', 'send_date')
                ->from('tx_shop_domain_model_basketorder')
                ->where(
                    $queryBuilder->expr()->orX(
                        $queryBuilder->expr()->isNotNull('order_date'),
                        $queryBuilder->expr()->isNotNull('paid_date'),
                        $queryBuilder->expr()->isNotNull('send_date'),
                    )
                )
                ->executeQuery();
        while ($row = $result->fetchAssociative()) {
            if (
                MathUtility::canBeInterpretedAsInteger($row['order_date'])
                && MathUtility::canBeInterpretedAsInteger($row['paid_date'])
                && MathUtility::canBeInterpretedAsInteger($row['send_date'])
            ) {
                continue;
            }
            return true;
        }
        return false;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [];
    }
}
