<?php

declare(strict_types=1);

namespace CodingMs\Shop\Updates;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class MigrateProductSizeWizard implements UpgradeWizardInterface
{
    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'shop_migrateProductSizeWizard';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Attaches size to products';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Searches for products with no product size or string and creates new or sets to existing product size';
    }

    /**
     * Execute the update - update sizes set to 0 or '' then update sizes set to strings
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     * @throws DBALException
     * @throws Exception
     */
    public function executeUpdate(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_product');
        //
        // Find pids of records where size set to '' or 0
        $result = $queryBuilder
            ->select('pid')
            ->from('tx_shop_domain_model_product')
            ->orWhere($queryBuilder->expr()->eq(
                'size',
                $queryBuilder->createNamedParameter('', Connection::PARAM_STR)
            ))
            ->orWhere($queryBuilder->expr()->eq(
                'size',
                $queryBuilder->createNamedParameter(0, Connection::PARAM_STR)
            ))
            ->groupBy('pid')
            ->executeQuery();
        //
        // Get each pid in turn
        while ($row = $result->fetchAssociative()) {
            $pid = $row['pid'];
            //
            // Get default size or newly created default size uid
            $newProductSizeUid = $this->getProductSize($pid);
            //
            // Attach size to products
            $productQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_domain_model_product');
            $productQueryBuilder->update('tx_shop_domain_model_product')
                ->where($productQueryBuilder->expr()->eq(
                    'size',
                    $productQueryBuilder->createNamedParameter('', Connection::PARAM_STR)
                ))
                ->orWhere($productQueryBuilder->expr()->eq(
                    'size',
                    $productQueryBuilder->createNamedParameter(0, Connection::PARAM_STR)
                ))
                ->andWhere($productQueryBuilder->expr()->eq(
                    'pid',
                    $productQueryBuilder->createNamedParameter($pid, Connection::PARAM_INT)
                ))
                ->set('size', $newProductSizeUid)
                ->execute();
        }
        //
        // Find pids of records where size is a non-numeric string
        $result = $queryBuilder
            ->select('pid', 'size')
            ->from('tx_shop_domain_model_product')
            ->where(
                $queryBuilder->expr()->comparison(
                    $queryBuilder->quoteIdentifier('size'),
                    'REGEXP',
                    $queryBuilder->createNamedParameter('[[:alpha:]]')
                )
            )
            ->groupBy('pid', 'size')
            ->executeQuery();
        //
        // Get each pid and size in turn
        while ($row = $result->fetchAssociative()) {
            $pid = $row['pid'];
            $size = $row['size'];
            // Create new size record
            $newProductSizeUid = $this->setProductSize($pid, $size);
            //
            // Attach size to products
            $productQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_domain_model_product');
            $productQueryBuilder->update('tx_shop_domain_model_product')
                ->where($productQueryBuilder->expr()->eq(
                    'size',
                    $productQueryBuilder->createNamedParameter($size, Connection::PARAM_STR)
                ))
                ->andWhere($productQueryBuilder->expr()->eq(
                    'pid',
                    $productQueryBuilder->createNamedParameter($pid, Connection::PARAM_INT)
                ))
                ->set('size', $newProductSizeUid)
                ->execute();
        }
        return true;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool
     * @throws DBALException
     */
    public function updateNecessary(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_product');
        $numberOfEntries = $queryBuilder
            ->count('uid')
            ->from('tx_shop_domain_model_product')
            ->where($queryBuilder->expr()->eq(
                'size',
                $queryBuilder->createNamedParameter('', Connection::PARAM_STR)
            ))
            ->orWhere($queryBuilder->expr()->eq(
                'size',
                $queryBuilder->createNamedParameter(0, Connection::PARAM_STR)
            ))
            ->orWhere(
                $queryBuilder->expr()->comparison(
                    $queryBuilder->quoteIdentifier('size'),
                    'REGEXP',
                    $queryBuilder->createNamedParameter('[[:alpha:]]')
                )
            )
            ->execute();
        return $numberOfEntries > 0;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [];
    }

    /**
     * @param int $pid
     * @return array
     */
    public function getProductSizeDefaultValues(int $pid): array
    {
        $fieldValues = [];
        $fieldValues['title'] = 'default';
        $fieldValues['pid'] = $pid;
        return $fieldValues;
    }

    /**
     * @param int $pid
     * @return int
     * @throws DBALException
     * @throws Exception
     */
    public function getProductSize(int $pid): int
    {
        // Get product size uid or create new one if doesn't exist
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_productsize');
        $productSizeUid = $queryBuilder
            ->select('uid')
            ->from('tx_shop_domain_model_productsize')
            ->where(
                $queryBuilder->expr()->eq('pid', $pid)
            )
            ->execute()
            ->fetchOne();
        if (!$productSizeUid) {
            $fieldValues = $this->getProductSizeDefaultValues($pid);
            $productSizeQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_domain_model_productsize');
            $productSizeQueryBuilder->insert('tx_shop_domain_model_productsize')->values($fieldValues)->execute();
            $productSizeUid = (int)$productSizeQueryBuilder->getConnection()->lastInsertId();
        }
        return $productSizeUid;
    }

    /**
     * @param int $pid
     * @param string $size
     * @return int
     * @throws DBALException
     */
    public function setProductSize(int $pid, string $size): int
    {
        $fieldValues = [];
        $fieldValues['title'] = $size;
        $fieldValues['pid'] = $pid;
        $productSizeQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_productsize');
        $productSizeQueryBuilder->insert('tx_shop_domain_model_productsize')->values($fieldValues)->execute();
        return (int)$productSizeQueryBuilder->getConnection()->lastInsertId();
    }

}
