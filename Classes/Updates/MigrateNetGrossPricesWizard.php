<?php

declare(strict_types=1);

namespace CodingMs\Shop\Updates;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Service\TypoScriptService;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Exception\InvalidFieldNameException;
use Exception as BaseException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class MigrateNetGrossPricesWizard implements UpgradeWizardInterface
{
    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'shop_migrateNetGrossPricesWizard';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Migrate net/gross prices';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Will migrate net/gross prices into a single price field with type field.';
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception|DBALException
     */
    public function executeUpdate(): bool
    {
        $typoScripts = [];
        $oldPrices = $this->getOldPriceUsed();
        if ($oldPrices) {
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            foreach ($oldPrices as $oldPrice) {
                $sql = 'UPDATE tx_shop_domain_model_product ';
                $connection = $connectionPool->getConnectionForTable('tx_shop_domain_model_product');
                if (!isset($typoScripts[$oldPrice['pid']])) {
                    try {
                        $typoScripts[$oldPrice['pid']] = TypoScriptService::getTypoScript($oldPrice['pid']);
                    } catch (SiteNotFoundException $exception) {
                        throw new BaseException($exception->getMessage() . ' - ensure all product container are inside website roots!');
                    }
                }
                $displayType = $typoScripts[$oldPrice['pid']]['plugin']['tx_shop']['settings']['basket']['displayType'];
                if ($displayType === 'b2c') {
                    $sql .= 'SET price_type = "gross", price = ' . (int)$oldPrice['price_with_tax'] . ', price_with_tax = 0 ';
                    $this->transferGraduatedPriceGross((int)$oldPrice['uid']);
                }
                if ($displayType === 'b2b') {
                    $sql .= 'SET price_type = "net", price_with_tax = 0 ';
                }
                $sql .= 'WHERE uid = ' . (int)$oldPrice['uid'];
                $query = $connection->prepare($sql);
                $query->executeStatement();
            }
            return true;
        }
        return false;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool Whether an update is required (TRUE) or not (FALSE)
     * @throws Exception
     * @throws DBALException
     */
    public function updateNecessary(): bool
    {
        if ($this->getOldPriceUsed()) {
            return true;
        }
        return false;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [];
    }

    /**
     * Returns an array of products with out-dated price information
     *
     * @return array
     * @throws DBALException
     * @throws Exception
     */
    protected function getOldPriceUsed(): array
    {
        try {
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_shop_domain_model_product');
            return $queryBuilder
                ->select('pid', 'uid', 'title', 'price_type', 'price', 'price_with_tax')
                ->from('tx_shop_domain_model_product')
                ->where(
                    $queryBuilder->expr()->eq('price_type', '""')
                )
                ->executeQuery()
                ->fetchAllAssociative();
        } catch (InvalidFieldNameException) {
            return [];
        }
    }

    /**
     * Transfer the gross into net price of graduated prices of a product
     *
     * @param int $productUid
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    protected function transferGraduatedPriceGross(int $productUid): void
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $connection = $connectionPool->getConnectionForTable('tx_shop_domain_model_productgraduatedprice');
        $sql = 'UPDATE tx_shop_domain_model_productgraduatedprice ';
        $sql .= 'SET price =price_with_tax ';
        $sql .= 'WHERE uid = ' . $productUid;
        $query = $connection->prepare($sql);
        $query->executeStatement();
    }
}
