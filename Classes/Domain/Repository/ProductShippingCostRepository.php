<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\ProductShippingCost;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ProductShippingCostRepository extends Repository
{
    /**
     * @var array<string, string> Default order is by start descending
     */
    protected $defaultOrderings = [
        'weight' => QueryInterface::ORDER_ASCENDING
    ];

    /**
     * @param array $filter
     * @param bool $count
     * @return array|QueryResultInterface|int
     */
    public function findAllForBackendList(array $filter = [], bool $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] != '' && is_string($filter['sortingField'])) {
                $sortingField = $filter['sortingField'];
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([$sortingField => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings([$sortingField => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }

    /**
     * @param int $weight
     * @return ProductShippingCost|null
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findClosestByWeight(int $weight=0): ?ProductShippingCost
    {
        //
        // Persisted records
        // starting from 0 kg -> 0,5 €
        // starting from 1 kg -> 1 €
        // starting from 2 kg -> 2 €
        // starting from 5 kg -> 5 €
        // starting from 10 kg -> 10 €
        //
        // Select * FROM psc WHERE weight >= $weight ORDER weight ASC
        //
        // Example -> $weight: 1,5 kg
        // Result: 2, 5, 10
        //
        // Example -> $weight: 4,5 kg
        // Result: 5, 10
        $query = $this->createQuery();
        $result = $query->matching($query->greaterThanOrEqual('weight', $weight))
            ->setOrderings(['weight' => QueryInterface::ORDER_ASCENDING])
            ->setLimit(1)
            ->execute();
        /** @var ?ProductShippingCost $productShippingCost*/
        $productShippingCost = $result->getFirst();
        return $productShippingCost;
    }
}
