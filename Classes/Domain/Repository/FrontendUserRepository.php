<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\FrontendUser;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @noinspection PhpUnused
 */
class FrontendUserRepository extends \CodingMs\Modules\Domain\Repository\FrontendUserRepository
{
    /**
     * @param string $stripeId
     * @return FrontendUser|null
     */
    public function findOneByStripeId(string $stripeId): ?FrontendUser
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $result = $query->matching($query->equals('stripeId', $stripeId))->setLimit(1)->execute();
        /** @var ?FrontendUser $feUser*/
        $feUser = $result->getFirst();
        return $feUser;
    }

    /**
     * @param int $uid
     * @return FrontendUser|null
     */
    public function findOneByUid(int $uid): ?FrontendUser
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $result = $query->matching($query->equals('uid', $uid))->setLimit(1)->execute();
        /** @var ?FrontendUser $feUser*/
        $feUser = $result->getFirst();
        return $feUser;
    }
}
