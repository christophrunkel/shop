<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute1Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute2Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute3Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute4Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute5Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute6Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute7Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute8Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\DescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\HiddenTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\HtmlTitleTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaAbstractTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaDescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaKeywordsTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\SlugTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\SortingTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TeaserTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TitleTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\ViewsTrait;
use CodingMs\Questions\Domain\Model\QuestionCategory;
use CodingMs\Questions\Domain\Repository\QuestionCategoryRepository;
use CodingMs\Shop\Domain\Model\Traits\AttachFrontendUserGroupsTrait;
use CodingMs\Shop\Domain\Model\Traits\Product\GraduatedPricesTrait;
use CodingMs\Shop\Domain\Model\Traits\Product\PricesTrait;
use CodingMs\Shop\Domain\Model\Traits\Product\RelatedProductsTrait;
use CodingMs\Shop\Domain\Model\Traits\Product\TagsTrait;
use CodingMs\Shop\Domain\Model\Traits\Product\TaxTrait;
use CodingMs\Shop\Domain\Model\Traits\ProductAttributesTrait;
use CodingMs\Shop\Domain\Model\Traits\ProductDiscountTrait;
use CodingMs\Shop\Domain\Model\Traits\ProductFileCollectionsTrait;
use Doctrine\DBAL\Result;
use Exception;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Shop product
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Product extends Base
{
    public const TABLE = 'tx_shop_domain_model_product';
    public const CUSTOM_ATTRIBUTE_FIELDS = ['attribute1', 'attribute2', 'attribute3', 'attribute4', 'attribute5', 'attribute6', 'attribute7', 'attribute8'];
    public const IMAGE_FIELDS = ['images', 'feature_icons', 'other_images'];
    public const RELATION_FIELDS = ['categories', 'tags', 'related_products', 'file_collections', 'files', 'question_categories', 'attach_frontend_user_groups'];
    public const SEO_FIELDS = ['canonical_link', 'html_title', 'meta_abstract', 'meta_description', 'meta_keywords'];

    use TitleTrait;
    use TeaserTrait;
    use DescriptionTrait;
    use HiddenTrait;
    use SortingTrait;
    use PricesTrait;
    use TaxTrait;

    use HtmlTitleTrait;
    use MetaAbstractTrait;
    use MetaDescriptionTrait;
    use MetaKeywordsTrait;
    use SlugTrait;
    use ViewsTrait;

    use TagsTrait;
    use GraduatedPricesTrait;
    use RelatedProductsTrait;
    use ProductDiscountTrait;
    use ProductAttributesTrait;
    use ProductFileCollectionsTrait;
    use AttachFrontendUserGroupsTrait;

    use Attribute1Trait;
    use Attribute2Trait;
    use Attribute3Trait;
    use Attribute4Trait;
    use Attribute5Trait;
    use Attribute6Trait;
    use Attribute7Trait;
    use Attribute8Trait;

    protected string $subtitle = '';
    protected string $productNo = '';
    protected string $pdfDescription = '';

    protected ?FileReference $descriptionFile = null;
    protected ?Product $parent = null;
    protected string $unit = '';
    protected int $unitFactor = 0;
    protected string $www = '';
    protected bool $inStock = false;
    protected bool $highlight = false;
    protected bool $firstInList = false;
    protected bool $digitalProduct = false;
    protected ?ProductType $productType = null;

    protected bool $offer = false;
    protected int $offerRebateValue = 0;
    protected int $stockAmount = 0;
    protected ?ProductColor $color = null;
    protected int $weight = 0;
    protected bool $bulky = false;
    protected ?ProductSize $size = null;
    protected string $canonicalLink = '';
    protected array $questionCategories = [];

    /**
     * @var string This field is only for the basket
     */
    protected string $customInformation = '';

    public function getCustomInformation(): string
    {
        return $this->customInformation;
    }

    public function setCustomInformation(string $customInformation): void
    {
        $this->customInformation = $customInformation;
    }







    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Shop\Domain\Model\ProductCategory>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $categories;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $images;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $featureIcons;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $otherImages;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $files;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $videos;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<Product>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $variants;

    public function __construct()
    {
        $this->categories = new ObjectStorage();
        $this->tags = new ObjectStorage();
        $this->relatedProducts = new ObjectStorage();
        $this->attachFrontendUserGroups = new ObjectStorage();
        $this->images = new ObjectStorage();
        $this->files = new ObjectStorage();
        $this->featureIcons = new ObjectStorage();
        $this->otherImages = new ObjectStorage();
        $this->videos = new ObjectStorage();
        $this->variants = new ObjectStorage();
        $this->attributes = new ObjectStorage();
        $this->graduatedPrices = new ObjectStorage();
    }

    protected function fillFieldFromParent(string $propertyName)
    {
        if ($this->isVariant()) {
            $productType = $this->parent->getProductType();
            $isEnabledMethod = 'is' . ucfirst($propertyName) . 'Enabled';
            // Field MUST be enabled in product-type
            if ($productType->$isEnabledMethod()) {
                $isVariantMethod = 'is' . ucfirst($propertyName) . 'EnabledVariant';
                // But field MUST NOT be enabled in product-type variant
                if (!$productType->$isVariantMethod()) {
                    if ($propertyName == 'tax') {
                        $getter = 'get' . ucfirst($propertyName) . 'Object';
                    } elseif ($propertyName == 'weight') {
                        // Get bulky as well, because it's combined with weight
                        $this->setBulky($this->parent->getBulky());
                        $getter = 'get' . ucfirst($propertyName);
                    } else {
                        $getter = 'get' . ucfirst($propertyName);
                    }
                    $setter = 'set' . ucfirst($propertyName);
                    try {
                        $this->$setter($this->parent->$getter());
                    } catch (Exception $exception) {
                        die($exception->getMessage());
                    }
                }
            }
        }
    }

    public function fillFieldsFromParent(): void
    {
        foreach ($this->getEnabledFields() as $field) {
            $this->fillFieldFromParent($field);
        }
    }

    public function getFillFieldsFromParent(): void
    {
        $this->fillFieldsFromParent();
    }

    /**
     * @return string $subtitle
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string $productNo
     */
    public function getProductNo(): string
    {
        return $this->productNo;
    }

    /**
     * @param string $productNo
     */
    public function setProductNo(string $productNo): void
    {
        $this->productNo = $productNo;
    }

    /**
     * @return string
     */
    public function getPdfDescription(): string
    {
        return $this->pdfDescription;
    }

    /**
     * @param string $pdfDescription
     */
    public function setPdfDescription(string $pdfDescription): void
    {
        $this->pdfDescription = $pdfDescription;
    }

    /**
     * @return ?FileReference $descriptionFile
     */
    public function getDescriptionFile(): ?FileReference
    {
        return $this->descriptionFile;
    }

    /**
     * @return string $descriptionFileContent
     */
    public function getDescriptionFileContent(): string
    {
        $content = '';
        $file = $this->getDescriptionFile();
        if ($file instanceof FileReference) {
            $content = $file->getOriginalResource()->getContents();
        }
        return $content;
    }

    /**
     * Sets the description file
     *
     * @param ?FileReference $descriptionFile
     */
    public function setDescriptionFile(?FileReference $descriptionFile): void
    {
        $this->descriptionFile = $descriptionFile;
    }

    /**
     * @return int
     */
    public function getStockAmount(): int
    {
        return $this->stockAmount;
    }

    /**
     * @param int $stockAmount
     */
    public function setStockAmount(int $stockAmount): void
    {
        $this->stockAmount = $stockAmount;
    }

    /**
     * @return bool
     */
    public function getIsBuyable(): bool
    {
        return $this->isDigitalProduct() or $this->getStockAmount() > 0 or !$this->isInStock();
    }

    /**
     * @return string $unit
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit(string $unit): void
    {
        $this->unit = $unit;
    }

    public function getUnitFactor(): int
    {
        return $this->unitFactor;
    }

    public function setUnitFactor(int $unitFactor): void
    {
        $this->unitFactor = $unitFactor;
    }

    /**
     * @return string $www
     */
    public function getWww(): string
    {
        return $this->www;
    }

    /**
     * @param string $www
     */
    public function setWww(string $www): void
    {
        $this->www = $www;
    }

    /**
     * @return bool $inStock
     */
    public function getInStock(): bool
    {
        return $this->inStock;
    }

    /**
     * @param bool $inStock
     */
    public function setInStock(bool $inStock): void
    {
        $this->inStock = $inStock;
    }

    /**
     * @return bool
     */
    public function isInStock(): bool
    {
        return $this->getInStock();
    }

    public function getHighlight(): bool
    {
        return $this->highlight;
    }

    public function getHighlightForBackend(): string
    {
        return (string)LocalizationUtility::translate(
            'tx_shop_label.' . ($this->highlight ? 'yes' : 'no'),
            'Shop'
        );
    }

    public function setHighlight(bool $highlight): void
    {
        $this->highlight = $highlight;
    }

    public function isHighlight(): bool
    {
        return $this->highlight;
    }

    public function getFirstInList(): bool
    {
        return $this->firstInList;
    }

    public function getFirstInListForBackend(): string
    {
        return (string)LocalizationUtility::translate(
            'tx_shop_label.' . ($this->firstInList ? 'yes' : 'no'),
            'Shop'
        );
    }

    public function setFirstInList(bool $firstInList): void
    {
        $this->firstInList = $firstInList;
    }

    public function isFirstInList(): bool
    {
        return $this->firstInList;
    }

    public function getDigitalProduct(): bool
    {
        return $this->digitalProduct;
    }

    public function getDigitalProductForBackend(): string
    {
        return (string)LocalizationUtility::translate(
            'tx_shop_label.' . ($this->digitalProduct ? 'yes' : 'no'),
            'Shop'
        );
    }

    public function setDigitalProduct(bool $digitalProduct): void
    {
        $this->digitalProduct = $digitalProduct;
    }

    public function isDigitalProduct(): bool
    {
        return $this->digitalProduct;
    }

    public function getOffer(): bool
    {
        return $this->offer;
    }

    public function setOffer(bool $offer): void
    {
        $this->offer = $offer;
    }

    public function isOffer(): bool
    {
        return $this->offer;
    }

    /**
     * @return int
     */
    public function getOfferRebateValue(): int
    {
        return $this->offerRebateValue;
    }

    /**
     * @return float
     */
    public function getOfferRebateValueAsFloat(): float
    {
        return round($this->offerRebateValue / 100, 2);
    }

    /**
     * @param int $offerRebateValue
     */
    public function setOfferRebateValue(int $offerRebateValue): void
    {
        $this->offerRebateValue = $offerRebateValue;
    }

    public function getColor(): ?ProductColor
    {
        return $this->color;
    }

    public function getColorForBackend(): string
    {
        $color = '';
        $colorObject = $this->getColor();
        if ($colorObject instanceof ProductColor) {
            $hex = $colorObject->getHex();
            $colorCube = '<i style="background-color: ' . $hex . '; width: 16px; height: 16px; display: block"></i>';
            $colorCube = '<span style="border: 1px solid black; padding: 1px; background-color: #fff; display: inline-block" title="' . $hex . '">' . $colorCube . '</span>';
            $color = $colorCube . ' ' . $colorObject->getTitle() . ', ' . $colorObject->getImportId() . ', ' . $hex;
        }
        return $color;
    }

    public function getColorCountOverVariants(): int
    {
        $colors = [
            $this->getColor()->getUid() => $this->getColor()->getUid(),
        ];
        foreach ($this->getVariants() as $variant) {
            $colors[$variant->getColor()->getUid()] = $variant->getColor()->getUid();
        }
        return count($colors);
    }

    public function setColor(ProductColor $color): void
    {
        $this->color = $color;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getWeightAsFloat(): float
    {
        return round($this->getWeight() / 100, 2);
    }

    public function getWeightAsString(): string
    {
        return number_format($this->getWeightAsFloat(), 2, ',', '.');
    }

    public function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }

    public function getBulky(): bool
    {
        return $this->bulky;
    }

    public function isBulky(): bool
    {
        return $this->bulky;
    }

    public function setBulky(bool $bulky): void
    {
        $this->bulky = $bulky;
    }

    public function getSize(): ?ProductSize
    {
        return $this->size;
    }

    public function getSizeForBackend(): string
    {
        $size = '';
        $sizeObject = $this->getSize();
        if ($sizeObject instanceof ProductSize) {
            $size = $sizeObject->getTitle() . ', ' . $sizeObject->getImportId();
        }
        return $size;
    }

    public function getSizeCountOverVariants(): int
    {
        $sizes = [
            $this->getSize()->getUid() => $this->getSize()->getUid(),
        ];
        foreach ($this->getVariants() as $variant) {
            $sizes[$variant->getSize()->getUid()] = $variant->getSize()->getUid();
        }
        return count($sizes);
    }

    public function setSize(ProductSize $size): void
    {
        $this->size = $size;
    }

    public function getCanonicalLink(): string
    {
        return $this->canonicalLink;
    }

    public function setCanonicalLink(string $canonicalLink): void
    {
        $this->canonicalLink = $canonicalLink;
    }

    /**
     * Adds a ProductCategory
     *
     * @param \CodingMs\Shop\Domain\Model\ProductCategory $category
     */
    public function addCategory(ProductCategory $category): void
    {
        $this->categories->attach($category);
    }

    /**
     * Removes a ProductCategory
     *
     * @param \CodingMs\Shop\Domain\Model\ProductCategory $categoryToRemove The ProductCategory to be removed
     */
    public function removeCategory(ProductCategory $categoryToRemove): void
    {
        $this->categories->detach($categoryToRemove);
    }

    /**
     * Returns the categories
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Shop\Domain\Model\ProductCategory> $categories
     */
    public function getCategories(): ObjectStorage
    {
        return $this->categories;
    }

    /**
     * Returns the categories as a String
     *
     * @return string
     */
    public function getCategoriesString(): string
    {
        $this->fillFieldFromParent('categories');
        $categoriesArray = [];
        $categories = $this->getCategories();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                /** @var \CodingMs\Shop\Domain\Model\ProductCategory $category */
                $categoriesArray[] = $category->getTitle();
            }
        }
        return implode(', ', $categoriesArray);
    }

    /**
     * @return string
     */
    public function getCategoriesForBackend(): string
    {
        $html = [];
        $categories = $this->getCategories();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $html[$category->getTitle()] = $category->getTitle();
            }
        }
        ksort($html);
        return implode('<br />', $html);
    }

    /**
     * Returns the categories as a String
     *
     * @return string
     */
    public function getCategoriesHtmlString(): string
    {
        $this->fillFieldFromParent('categories');
        $categoriesArray = [];
        $categories = $this->getCategories();
        if (!empty($categories)) {
            /** @var \CodingMs\Shop\Domain\Model\ProductCategory $category */
            foreach ($categories as $category) {
                // Build label
                $label = '<label class="productCategory_' . $category->getUid() . '">' . $category->getTitle() . '</label>';
                $image = '';
                $fileReference = $category->getImage();
                $publicUrl = '';
                if ($fileReference instanceof \TYPO3\CMS\Extbase\Domain\Model\FileReference) {
                    $file = $fileReference->getOriginalResource();
                    $publicUrl = $file->getPublicUrl();
                    // Build image
                    $image = '<img class="productCategoryImage productCategoryImage_' . $category->getUid() . '" src="' . $publicUrl . '" title="' . $category->getTitle() . '" alt="' . $category->getTitle() . '">';
                }
                $categoriesArray[] = $label . $image;
            }
        }
        return implode(', ', $categoriesArray);
    }

    /**
     * Returns the categories as a String
     *
     * @return string
     */
    public function getCssClasses()
    {
        $classesArray = [];
        $classesArray[] = 'product-' . $this->getUid();
        // Get all categories
        $categories = $this->getCategories();
        if (!empty($categories)) {
            /** @var \CodingMs\Shop\Domain\Model\ProductCategory $category */
            foreach ($categories as $category) {
                $classesArray[] = 'product-category-' . $category->getUid();
            }
        }
        // Get all tags
        $tags = $this->getTags();
        if (!empty($tags)) {
            /** @var ProductTag $tag */
            foreach ($tags as $tag) {
                $classesArray[] = 'product-tag-' . $tag->getUid();
            }
        }
        // Is first in list?
        if ($this->isFirstInList()) {
            $classesArray[] = 'product-first-in-list';
        }
        // Is in stock?
        if ($this->isInStock()) {
            $classesArray[] = 'product-in-stock';
        }
        // is highlighted?
        if ($this->isHighlight()) {
            $classesArray[] = 'product-highlight';
        }
        // Is offer?
        if ($this->isOffer()) {
            $classesArray[] = 'product-offer';
        }
        // Implode and return
        return implode(' ', $classesArray);
    }

    /**
     * Sets the categories
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Shop\Domain\Model\ProductCategory> $categories
     */
    public function setCategories(ObjectStorage $categories): void
    {
        $this->categories = $categories;
    }

    /**
     * Returns the graduated prices as a String
     *
     * @return string
     */
    public function getGraduatedPricesForBackend(): string
    {
        $html = [];
        $graduatedPrices = $this->getGraduatedPrices();
        if (isset($graduatedPrices)) {
            /** @var \CodingMs\Shop\Domain\Model\ProductGraduatedPrice $graduatedPrice */
            foreach ($graduatedPrices as $graduatedPrice) {
                $html[$graduatedPrice->getQuantity()] = LocalizationUtility::translate('tx_shop_label.graduated_price_title', 'Shop', [$graduatedPrice->getQuantity(), number_format($graduatedPrice->getPriceAsFloat(), 2, ',', '.') . ' €']);
            }
            ksort($html);
        }
        return implode('<br />', $html);
    }

    /**
     * Returns the question categories as a String
     *
     * @return string
     */
    public function getQuestionCategoriesForBackend(): string
    {
        $html = [];
        $questionCategories = $this->getQuestionCategories();
        if (isset($questionCategories)) {
            /** @var QuestionCategory $questionCategory */
            foreach ($questionCategories as $questionCategory) {
                $html[] = $questionCategory->getTitle();
            }
        }
        return implode('<br />', $html);
    }

    /**
     * Returns the frontend user groups as a String
     *
     * @return string
     */
    public function getAttachFrontendUserGroupsForBackend(): string
    {
        $html = [];
        $frontendUserGroups = $this->getAttachFrontendUserGroups();
        if (isset($frontendUserGroups)) {
            /** @var \CodingMs\Modules\Domain\Model\FrontendUserGroup $frontendUserGroup */
            foreach ($frontendUserGroups as $frontendUserGroup) {
                $html[] = $frontendUserGroup->getTitle();
            }
        }
        return implode('<br />', $html);
    }

    /**
     * Returns the filenames as a String
     *
     * @return string
     */
    public function getFilesForBackend(): string
    {
        $html = [];
        $files = $this->getFiles();
        /** @var FileReference $file */
        foreach ($files as $file) {
            $html[] = $file->getFilename();
        }
        return implode('<br />', $html);
    }

    public function addImage(FileReference $image): void
    {
        $this->images->attach($image);
    }

    public function removeImage(FileReference $imageToRemove): void
    {
        $this->images->detach($imageToRemove);
    }

    /**
     * Returns all images
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<FileReference> $images
     */
    public function getImages(): ObjectStorage
    {
        /** @phpstan-ignore-next-line */
        if ($this->images instanceof LazyLoadingProxy) {
            $this->images->_loadRealInstance();
        }
        return $this->images;
    }

    public function getImagesForBackend()
    {
        $images = [];
        foreach ($this->getImages() as $image) {
            $file = $image->getOriginalResource();
            $html = '<img class="img-fluid" style="border: 1px dashed #999; margin-bottom: 5px; padding: 1px;" src="' . $file->getPublicUrl() . '" title="' . $image->getTitle() . '" alt="' . $image->getTitle() . '"><br />';
            $html .= LocalizationUtility::translate('tx_shop_label.title', 'Shop') . ': ' . $file->getTitle() . '<br />';
            $html .= LocalizationUtility::translate('tx_shop_label.alternative', 'Shop') . ': ' . $file->getAlternative() . '<br />';
            $html .= LocalizationUtility::translate('tx_shop_label.description', 'Shop') . ': ' . $file->getDescription() . '<br />';
            $images[] = $html;
        }
        return implode('<br />', $images);
    }

    public function getVideosForBackend()
    {
        $videos = [];
        foreach ($this->getVideos() as $video) {
            $file = $video->getOriginalResource();
            $html = '<img class="img-fluid" style="border: 1px dashed #999; margin-bottom: 5px; padding: 1px;" src="' . $file->getPublicUrl() . '" title="' . $video->getTitle() . '" alt="' . $video->getTitle() . '"><br />';
            $html .= LocalizationUtility::translate('tx_shop_label.title', 'Shop') . ': ' . $file->getTitle() . '<br />';
            $html .= LocalizationUtility::translate('tx_shop_label.alternative', 'Shop') . ': ' . $file->getAlternative() . '<br />';
            $html .= LocalizationUtility::translate('tx_shop_label.description', 'Shop') . ': ' . $file->getDescription() . '<br />';
            $videos[] = $html;
        }
        return implode('<br />', $videos);
    }

    /**
     * Returns all images marked as showinpreview
     * @return array $images
     */
    public function getImagesPreviewOnly(): array
    {
        $images = [];
        if ($this->getImages()->count()) {
            /** @var FileReference $mediaItem */
            foreach ($this->getImages() as $mediaItem) {
                if ($mediaItem->getOriginalResource()->getProperty('showinpreview')) {
                    $images[] = $mediaItem;
                }
            }
        }
        return $images;
    }

    /**
     * Returns all images non-marked as showinpreview
     * @return array $images
     */
    public function getImagesNonPreview(): array
    {
        $images = [];
        if ($this->getImages()->count()) {
            /** @var FileReference $mediaItem */
            foreach ($this->getImages() as $mediaItem) {
                if (!$mediaItem->getOriginalResource()->getProperty('showinpreview')) {
                    $images[] = $mediaItem;
                }
            }
        }
        return $images;
    }

    /**
     * Returns the first images
     * @return FileReference|null $image
     */
    public function getFirstImage(): ?FileReference
    {
        $image = null;
        /** @var FileReference $tempImage */
        foreach ($this->getImages() as $tempImage) {
            $image = $tempImage;
            break;
        }
        return $image;
    }

    /**
     * Sets the images
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<FileReference> $images
     */
    public function setImages(ObjectStorage $images): void
    {
        $this->images = $images;
    }

    public function addFeatureIcon(FileReference $featureIcon): void
    {
        $this->featureIcons->attach($featureIcon);
    }

    public function removeFeatureIcon(FileReference $featureIcon): void
    {
        $this->featureIcons->detach($featureIcon);
    }

    /**
     * Returns the feature icon
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<FileReference> $featureIcons
     */
    public function getFeatureIcons()
    {
        return $this->featureIcons;
    }

    public function getFeatureIconsForBackend()
    {
        $images = [];
        foreach ($this->getFeatureIcons() as $image) {
            $file = $image->getOriginalResource();
            $html = '<img class="img-fluid" style="border: 1px dashed #999; margin-bottom: 5px; padding: 1px;" src="' . $file->getPublicUrl() . '" title="' . $file->getTitle() . '" alt="' . $file->getTitle() . '"><br />';
            $html .= LocalizationUtility::translate('tx_shop_label.title', 'Shop') . ': ' . $file->getTitle() . '<br />';
            $html .= LocalizationUtility::translate('tx_shop_label.alternative', 'Shop') . ': ' . $file->getAlternative() . '<br />';
            $html .= LocalizationUtility::translate('tx_shop_label.description', 'Shop') . ': ' . $file->getDescription() . '<br />';
            $images[] = $html;
        }
        return implode('<br />', $images);
    }

    /**
     * Returns all images marked as showinpreview
     * @return array $images
     */
    public function getFeatureIconsPreviewOnly(): array
    {
        $images = [];
        if ($this->getFeatureIcons()->count()) {
            /** @var FileReference $mediaItem */
            foreach ($this->getFeatureIcons() as $mediaItem) {
                if ($mediaItem->getOriginalResource()->getProperty('showinpreview')) {
                    $images[] = $mediaItem;
                }
            }
        }
        return $images;
    }

    /**
     * Returns all images non-marked as showinpreview
     * @return array $images
     * @noinspection PhpUnused
     */
    public function getFeatureIconsNonPreview(): array
    {
        $images = [];
        if ($this->getFeatureIcons()->count()) {
            /** @var FileReference $mediaItem */
            foreach ($this->getFeatureIcons() as $mediaItem) {
                if (!$mediaItem->getOriginalResource()->getProperty('showinpreview')) {
                    $images[] = $mediaItem;
                }
            }
        }
        return $images;
    }

    /**
     * Sets the feature icon
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<FileReference> $featureIcons
     */
    public function setFeatureIcons(ObjectStorage $featureIcons): void
    {
        $this->featureIcons = $featureIcons;
    }

    /**
     * Adds a feature icon
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $otherImage
     */
    public function addOtherImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $otherImage): void
    {
        $this->otherImages->attach($otherImage);
    }

    /**
     * Removes a feature icon
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $otherImage The feature icon to be removed
     */
    public function removeOtherImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $otherImage): void
    {
        $this->otherImages->detach($otherImage);
    }

    /**
     * Returns the feature icon
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $otherImages
     */
    public function getOtherImages()
    {
        return $this->otherImages;
    }

    public function getOtherImagesForBackend()
    {
        $images = [];
        /** @var \TYPO3\CMS\Extbase\Domain\Model\FileReference $image */
        foreach ($this->getOtherImages() as $image) {
            $file = $image->getOriginalResource();
            $html = '<img class="img-fluid" style="border: 1px dashed #999; margin-bottom: 5px; padding: 1px;" src="' . $file->getPublicUrl() . '" title="' . $file->getTitle() . '" alt="' . $file->getTitle() . '"><br />';
            $html .= LocalizationUtility::translate('tx_shop_label.title', 'Shop') . ': ' . $file->getTitle() . '<br />';
            $html .= LocalizationUtility::translate('tx_shop_label.alternative', 'Shop') . ': ' . $file->getAlternative() . '<br />';
            $html .= LocalizationUtility::translate('tx_shop_label.description', 'Shop') . ': ' . $file->getDescription() . '<br />';
            $images[] = $html;
        }
        return implode('<br />', $images);
    }

    /**
     * Sets the feature icon
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $otherImages
     */
    public function setOtherImages(ObjectStorage $otherImages): void
    {
        $this->otherImages = $otherImages;
    }

    /**
     * Returns the question categories
     */
    public function getQuestionCategories(): array
    {
        if (ExtensionManagementUtility::isLoaded('questions') && empty($this->questionCategories)) {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_product_productquestioncategories_mm');
            $result = $queryBuilder
                ->select('uid_foreign')
                ->from('tx_shop_product_productquestioncategories_mm')
                ->where(
                    $queryBuilder->expr()->eq('uid_local', $queryBuilder->createNamedParameter($this->getUid()))
                )->execute();

            $uidList = [];
            if ($result instanceof Result) {
                foreach ($result->fetchAllAssociative() as $uid) {
                    array_push($uidList, $uid['uid_foreign']);
                }
                if (!is_array($uidList) || count($uidList) === 0) {
                    return [];
                }
                /** @var \CodingMs\Questions\Domain\Repository\QuestionCategoryRepository $questionCategoryRepository */
                $questionCategoryRepository = GeneralUtility::makeInstance(QuestionCategoryRepository::class);
                $questionTypeQuery = $questionCategoryRepository->createQuery();
                $questionTypeQuery->getQuerySettings()->setRespectStoragePage(false);
                $questionTypeQuery->matching($questionTypeQuery->in('uid', $uidList));
                $this->questionCategories = $questionTypeQuery->execute()->toArray();
            }
        }
        return $this->questionCategories;
    }

    /**
     * @param array<mixed> $questionCategories
     * @return void
     */
    public function setQuestionCategories(array $questionCategories): void
    {
        $this->questionCategories = $questionCategories;
    }

    /**
     * @param object $object
     * @param string $method
     * @return string
     * @throws Exception
     */
    protected function checkMethod(object $object, string $method): string
    {
        $checkMethod = 'is' . ucfirst($method);
        if (method_exists($object, $checkMethod)) {
            return $checkMethod;
        }
        $checkMethod = 'get' . ucfirst($method);
        if (method_exists($object, $checkMethod)) {
            return $checkMethod;
        }
        throw new Exception($checkMethod . ' not found on ' . get_class($this) . '!');
    }

    /**
     * @param array $list
     * @return array
     */
    public function toCsvArray(array $list = []): array
    {
        $array = [];
        foreach ($list['fields'] as $key => $options) {
            // Try to catch non-existing class methods
            try {
                if ($options['hideInExport'] == 1) {
                    continue;
                }
                $array[$key] = '';
                $keyParts = explode('.', $key);
                if (count($keyParts) == 2) {
                    $firstMethod = $this->checkMethod($this, $keyParts[0]);
                    $object = $this->$firstMethod();
                    // Is a lazy object?!
                    if ($object instanceof LazyLoadingProxy) {
                        $object = $object->_loadRealInstance();
                    }
                    $secondMethod = $this->checkMethod($object, $keyParts[1]);
                    $array[$key] = $this->$firstMethod()->$secondMethod();
                } elseif (count($keyParts) == 1) {
                    $firstMethod = $this->checkMethod($this, $keyParts[0]);
                    $array[$key] = $this->$firstMethod();
                }
                // Format value
                if ($options['format'] == 'Product/Tags') {
                    $tags = [];
                    /** @var ProductTag $tag */
                    foreach ($this->getTags() as $tag) {
                        $tags[] = $tag->getTitle();
                    }
                    $array[$key] = implode(', ', $tags);
                } elseif ($options['format'] == 'Product/Categories') {
                    $categories = [];
                    /** @var Category $category */
                    foreach ($this->getCategories() as $category) {
                        $categories[] = $category->getTitle();
                    }
                    $array[$key] = implode(', ', $categories);
                } elseif ($options['format'] == 'Boolean') {
                    $array[$key] = $array[$key] ?
                        LocalizationUtility::translate('tx_shop_label.yes', 'ShopPro') :
                        LocalizationUtility::translate('tx_shop_label.no', 'ShopPro');
                } else {
                    $array[$key] = strip_tags($array[$key]);
                }
            } // Non-existing class methods found?!
            catch (Exception $e) {
                // Write the exception message into the cell
                $message = 'Error in settings.' . $list['id'] . '.fields.' . str_replace('.', '_', $key) . "\n";
                $array[$key] = $message . $e->getMessage();
            }
        }
        return $array;
    }

    /**
     * @return ObjectStorage<FileReference>
     */
    public function getFiles(): ObjectStorage
    {
        return $this->files;
    }

    /**
     * @param ObjectStorage<FileReference> $files
     */
    public function setFiles(ObjectStorage $files): void
    {
        $this->files = $files;
    }

    /**
     * @return ObjectStorage
     */
    public function getVideos(): ObjectStorage
    {
        return $this->videos;
    }

    /**
     * @param ObjectStorage $videos
     */
    public function setVideos(ObjectStorage $videos): void
    {
        $this->videos = $videos;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<Product> $variants
     */
    public function getVariants()
    {
        if ($this->isVariant()) {
            $this->variants->attach($this->parent);
            foreach ($this->parent->getVariants() as $variant) {
                if ($variant->getUid() !== $this->getUid()) {
                    $this->variants->attach($variant);
                }
            }
        }
        return $this->variants;
    }

    public function getLowestPriceOverVariants(): int
    {
        $prices = [
            $this->getPrice() => $this->getPrice(),
        ];
        foreach ($this->getVariants() as $variant) {
            $prices[$variant->getPrice()] = $variant->getPrice();
        }
        ksort($prices);
        return $prices[array_key_first($prices)];
    }

    public function getLowestPriceOverVariantsAsFloat(): float
    {
        return round($this->getLowestPriceOverVariants() / 100, 2);
    }

    public function getLowestPriceOverVariantsAsString(): string
    {
        return number_format($this->getLowestPriceOverVariantsAsFloat(), 2, ',', '.');
    }

    public function getParent(): ?Product
    {
        return $this->parent;
    }

    /**
     * @return array $variants
     */
    public function getVariantsSummarizedData(): array
    {
        //
        // Collect products and variants
        $products = [$this];
        foreach ($this->getVariants() as $variant) {
            $variant->fillFieldsFromParent();
            array_push($products, $variant);
        }
        //
        // Build rows
        $variantsSummary = [];
        if (count($this->variants) > 0 && !empty($this->getProductType())) {
            $variantsSummary = [];
            //
            // Get properties based on product type
            foreach ($this->getEnabledFields() as $enabledField) {
                if ($enabledField === 'productType') {
                    continue;
                }
                if (self::isSeoField(GeneralUtility::camelCaseToLowerCaseUnderscored($enabledField))) {
                    if (!isset($variantsSummary['seo_section'])) {
                        $variantsSummary['seo_section'] = [
                            'label' => LocalizationUtility::translate('tx_shop_label.seo', 'Shop'),
                            'section' => true,
                            'colspan' => count($products) + 1,
                        ];
                    }
                }
                if (self::isRelationsField(GeneralUtility::camelCaseToLowerCaseUnderscored($enabledField))) {
                    if (!isset($variantsSummary['relation_section'])) {
                        $variantsSummary['relation_section'] = [
                            'label' => LocalizationUtility::translate('tx_shop_label.relations', 'Shop'),
                            'section' => true,
                            'colspan' => count($products) + 1,
                        ];
                    }
                }
                if (self::isImagesField(GeneralUtility::camelCaseToLowerCaseUnderscored($enabledField))) {
                    if (!isset($variantsSummary['images_section'])) {
                        $variantsSummary['images_section'] = [
                            'label' => LocalizationUtility::translate('tx_shop_label.images', 'Shop'),
                            'section' => true,
                            'colspan' => count($products) + 1,
                        ];
                    }
                }
                if (self::isVideosField(GeneralUtility::camelCaseToLowerCaseUnderscored($enabledField))) {
                    if (!isset($variantsSummary['videos_section'])) {
                        $variantsSummary['videos_section'] = [
                            'label' => LocalizationUtility::translate('tx_shop_label.videos', 'Shop'),
                            'section' => true,
                            'colspan' => count($products) + 1,
                        ];
                    }
                }
                if (self::isAttributesField(GeneralUtility::camelCaseToLowerCaseUnderscored($enabledField))) {
                    if (!isset($variantsSummary['attributes_section'])) {
                        $variantsSummary['attributes_section'] = [
                            'label' => LocalizationUtility::translate('tx_shop_label.attributes', 'Shop'),
                            'section' => true,
                            'colspan' => count($products) + 1,
                        ];
                    }
                }
                if (self::isCustomField(GeneralUtility::camelCaseToLowerCaseUnderscored($enabledField))) {
                    if (!isset($variantsSummary['custom_section'])) {
                        $variantsSummary['custom_section'] = [
                            'label' => LocalizationUtility::translate('tx_shop_label.custom', 'Shop'),
                            'section' => true,
                            'colspan' => count($products) + 1,
                        ];
                    }
                }
                $variantsSummary[$enabledField] = [
                    'label' => LocalizationUtility::translate(
                        'tx_shop_label.' . GeneralUtility::camelCaseToLowerCaseUnderscored($enabledField),
                        'Shop'
                    ),
                    'items' => [],
                    'variant' => $this->isEnabledVariantField($enabledField),
                    'html' => false,
                    'section' => false,
                ];
            }
            foreach ($products as $product) {
                $uid = $product->getUid();
                foreach ($variantsSummary as $key => $value) {
                    //
                    // Don't progress section rows
                    if (str_ends_with($key, '_section')) {
                        continue;
                    }
                    //
                    $getterMethod = 'get' . ucfirst($key);
                    if ($getterMethod === 'getSize') {
                        $getterMethod = 'getSizeForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getColor') {
                        $getterMethod = 'getColorForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getCategories') {
                        $getterMethod = 'getCategoriesForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getTags') {
                        $getterMethod = 'getTagsForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getAttributes') {
                        $getterMethod = 'getAttributesForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getImages') {
                        $getterMethod = 'getImagesForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getVideos') {
                        $getterMethod = 'getVideosForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getFeatureIcons') {
                        $getterMethod = 'getFeatureIconsForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getOtherImages') {
                        $getterMethod = 'getOtherImagesForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getRelatedProducts') {
                        $getterMethod = 'getRelatedProductsForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getGraduatedPrices') {
                        $getterMethod = 'getGraduatedPricesForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getFiles') {
                        $getterMethod = 'getFilesForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getAttachFrontendUserGroups') {
                        $getterMethod = 'getAttachFrontendUserGroupsForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    if ($getterMethod === 'getQuestionCategories') {
                        $getterMethod = 'getQuestionCategoriesForBackend';
                        $variantsSummary[$key]['html'] = true;
                    }
                    //
                    // Booleans
                    if ($getterMethod === 'getHighlight') {
                        $getterMethod = 'getHighlightForBackend';
                    }
                    if ($getterMethod === 'getFirstInList') {
                        $getterMethod = 'getFirstInListForBackend';
                    }
                    if ($getterMethod === 'getDigitalProduct') {
                        $getterMethod = 'getDigitalProductForBackend';
                    }
                    //
                    // RTE
                    if (in_array($getterMethod, ['getTeaser', 'getDescription', 'getPdfDescription'])) {
                        $variantsSummary[$key]['html'] = true;
                    }
                    //
                    // Currency
                    if (in_array($getterMethod, ['getPrice'])) {
                        $getterMethod .= 'AsString';
                        $append = ' €';
                    }
                    //
                    // Percent
                    if (in_array($getterMethod, ['getTax'])) {
                        $getterMethod .= 'AsString';
                        $append = ' %';
                    }
                    //
                    // Kg
                    if (in_array($getterMethod, ['getWeight'])) {
                        $getterMethod .= 'AsString';
                        $append = ' kg';
                        if ($product->getBulky()) {
                            $append = $append . ', ' . LocalizationUtility::translate('tx_shop_label.bulky', 'Shop');
                        }
                    }
                    if (isset($append)) {
                        $variantsSummary[$key]['items'][$uid] = $product->$getterMethod() . $append;
                        unset($append);
                    } else {
                        $variantsSummary[$key]['items'][$uid] = $product->$getterMethod();
                    }
                }
            }
        }
        return $variantsSummary;
    }

    /**
     * Fields which can be enabled/disabled by product type
     * @return array<int, string>
     */
    public function getEnabledFields(): array
    {
        $enabledFields = [];
        foreach ($this->getProductType()->_getProperties() as $fieldName => $enabled) {
            if (str_ends_with($fieldName, 'Enabled') && $enabled) {
                $enabledFields[] = str_replace('Enabled', '', $fieldName);
            }
        }
        return $enabledFields;
    }

    public function isEnabledField(string $field): bool
    {
        $enabledVariantFields = $this->getEnabledFields();
        $enabledVariantFields[] = 'title';
        $enabledVariantFields[] = 'subtitle';
        $enabledVariantFields[] = 'productNo';
        $enabledVariantFields[] = 'slug';
        return in_array($field, $enabledVariantFields);
    }

    /**
     * Fields which can be enabled/disabled by product type
     * @return array<int, string>
     */
    public function getEnabledVariantFields(): array
    {
        $enabledFields = [];
        foreach ($this->getProductType()->_getProperties() as $fieldName => $enabled) {
            if (str_ends_with($fieldName, 'EnabledVariant') && $enabled) {
                $enabledFields[] = str_replace('EnabledVariant', '', $fieldName);
            }
        }
        return $enabledFields;
    }

    public function isEnabledVariantField(string $field): bool
    {
        $enabledVariantFields = $this->getEnabledVariantFields();
        $enabledVariantFields[] = 'title';
        $enabledVariantFields[] = 'subtitle';
        $enabledVariantFields[] = 'productNo';
        $enabledVariantFields[] = 'slug';
        return in_array($field, $enabledVariantFields);
    }

    protected function isVariant(): bool
    {
        return $this->parent instanceof Product;
    }

    /**
     * @return ProductType|null
     */
    public function getProductType(): ?ProductType
    {
        //
        // If there is a parent product, use the type of them
        if ($this->isVariant()) {
            $productType = $this->parent->getProductType();
        } else {
            $productType = $this->productType;
        }
        if (!($productType instanceof ProductType)) {
            throw new Exception(
                'Product ' . $this->getTitle() . ' (' . $this->getUid() . ') requires a valid product type! '
                . 'Try to run shop product migration wizard or ensure having selected a valid product type in products.'
            );
        }
        return $productType;
    }

    /**
     * @param ProductType|null $productType
     */
    public function setProductType(?ProductType $productType): void
    {
        $this->productType = $productType;
    }

    public static function isSeoField(string $field): bool
    {
        return in_array($field, self::SEO_FIELDS);
    }

    public static function isRelationsField(string $field): bool
    {
        return in_array($field, self::RELATION_FIELDS);
    }

    public static function isImagesField(string $field): bool
    {
        return in_array($field, self::IMAGE_FIELDS);
    }

    public static function isVideosField(string $field): bool
    {
        return in_array($field, ['videos']);
    }

    public static function isAttributesField(string $field): bool
    {
        return in_array($field, ['attributes']);
    }

    public static function isCustomField(string $field): bool
    {
        return in_array($field, self::CUSTOM_ATTRIBUTE_FIELDS);
    }

    /*`
     * @return array<mixed>
     */
    public function getCustomAttribute(string $attribute): array
    {
        $attributeSettings = [];
        if ($this->isEnabledField($attribute) && $this->isCustomField($attribute)) {
            $productType = $this->getProductType();
            $attributeValueGetter = 'get' . ucfirst($attribute);
            $attributeLabelGetter = 'get' . ucfirst($attribute) . 'Label';
            $attributeTypeGetter = 'get' . ucfirst($attribute) . 'Type';
            $attributeDescriptionGetter = 'get' . ucfirst($attribute) . 'Description';
            $attributeRequired = false;
            $attributeType = $productType->$attributeTypeGetter();
            if (str_ends_with($attributeType, '_required')) {
                $attributeRequired = true;
                $attributeType = str_replace('_required', '', $attributeType);
            }
            if ($attributeType === 'checkbox') {
                $attributeType = 'bool';
            }
            if ($attributeType === 'rte') {
                $attributeType = 'html';
            }
            $attributeSettings = [
                'attribute' => $attribute,
                'label' => $productType->$attributeLabelGetter(),
                'type' => $attributeType,
                'required' => $attributeRequired,
                'description' => $productType->$attributeDescriptionGetter(),
                'filled' => false,
            ];
            if ($attributeType === 'bool') {
                $attributeSettings['value'] = (bool)$this->$attributeValueGetter();
                //if ($attributeSettings['value']) {
                $attributeSettings['filled'] = true;
                //}
            } elseif ($attributeType === 'int') {
                $attributeSettings['value'] = (int)$this->$attributeValueGetter();
                if ($attributeSettings['value'] > 0) {
                    $attributeSettings['filled'] = true;
                }
            } else {
                $attributeSettings['value'] = $this->$attributeValueGetter();
                if (trim($attributeSettings['value']) !== '') {
                    $attributeSettings['filled'] = true;
                }
            }
        }
        return $attributeSettings;
    }

    /**
     * @return string
     * @throws \TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException
     */
    public function getBackendPdfLink(): string
    {
        if ((int)VersionNumberUtility::getCurrentTypo3Version() >= 12) {
            $parameter = [
                'id' => $this->pid,
                'M' => 'shoppro_shoppro',
                'controller' => 'Backend',
                'product' => $this->uid,
            ];
        } else {
            $parameter = [
                'id' => $this->pid,
                'M' => 'web_ShopProShop',
                'tx_shoppro_web_shopproshop' => [
                    'controller' => 'Backend',
                    'product' => $this->uid,
                ]
            ];
        }
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        return (string)$uriBuilder->buildUriFromRoute('shop_create_product_pdf', $parameter);
    }

}
