<?php

namespace CodingMs\Shop\Domain\Model\Traits\Product;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\ProductTax;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;

/**
 * Product tax is a tex-relation-record, but getTax returns only the tax-value
 * in order to stay backward compatible!
 */
trait TaxTrait
{
    /**
     * !!! ATTENTION !!!
     * We don't set a type on the property,
     * because otherwise we get a conflict with lazy loading,
     * because union-types are not supported yet!
     *
     * @var ProductTax
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $tax;

    public function getTax(): int
    {
        if (!($this->tax instanceof ProductTax)) {
            $this->getTaxObject();
        }
        return $this->tax->getValue();
    }

    public function getTaxObject(): ProductTax
    {
        /** @phpstan-ignore-next-line */
        if ($this->tax instanceof LazyLoadingProxy) {
            $this->tax->_loadRealInstance();
        }
        if (is_null($this->tax)) {
            throw new Exception('Product [' . $this->getUid() . ' | ' . $this->getTitle() . '] has no tax rate defined!');
        }
        return $this->tax;
    }

    public function getTaxAsFloat(): float
    {
        return round($this->getTax() / 100, 2);
    }

    public function getTaxAsString(): string
    {
        return number_format($this->getTaxAsFloat(), 2, ',', '.');
    }

    /**
     * @param string|null $countryCode
     * @return int
     */
    public function getTaxForCountry(?string $countryCode): int
    {
        if (!isset($countryCode)) {
            return $this->getTax();
        }
        foreach ($this->tax->getOverlay() as $overlay) {
            if ($overlay->getCountryCode() === $countryCode) {
                return $overlay->getValue();
            }
        }
        return $this->getTax();
    }

    /**
     * @param string|null $countryCode
     * @return bool
     */
    public function hasTaxCountryOverlay(?string $countryCode): bool
    {
        if (!isset($countryCode)) {
            return false;
        }
        foreach ($this->tax->getOverlay() as $overlay) {
            if ($overlay->getCountryCode() === $countryCode) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param ProductTax $tax
     */
    public function setTax(ProductTax $tax): void
    {
        $this->tax = $tax;
    }
}
