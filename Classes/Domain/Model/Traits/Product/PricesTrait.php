<?php

namespace CodingMs\Shop\Domain\Model\Traits\Product;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Product tax is a tex-relation-record, but getTax returns only the tax-value
 * in order to stay backward compatible!
 */
trait PricesTrait
{
    protected int $price = 0;
    protected string $priceType = 'net';

    /**
     * @return string
     */
    public function getPriceType(): string
    {
        return $this->priceType;
    }

    /**
     * @param string $priceType
     */
    public function setPriceType(string $priceType): void
    {
        $this->priceType = $priceType;
    }

    public function getPrice(): int
    {
        if ($this->getPriceType() === 'gross') {
            if ($this->getTax() === 0) {
                return $this->price;
            }
            return (int)round($this->price / (1 + ($this->getTax() / 10000)));
        }
        // else if -> net price!
        return $this->price;
    }
    public function getPriceAsFloat(): float
    {
        return round($this->getPrice() / 100, 2);
    }
    public function getPriceAsString(): string
    {
        return number_format($this->getPriceAsFloat(), 2, ',', '.');
    }
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int $price
     */
    public function getPriceWithTax(): int
    {
        if ($this->getPriceType() === 'net') {
            return (int)round($this->price * (10000 + $this->getTax()) / 10000);
        }
        return $this->price;
    }

    /**
     * @return int $price
     */
    public function getPriceWithTaxForCountry(?string $countryCode): int
    {
        if ($this->getPriceType() === 'net') {
            return (int)round($this->price * (10000 + $this->getTaxForCountry($countryCode)) / 10000);
        }
        return $this->price;
    }

    /**
     * @return float $price
     */
    public function getPriceWithTaxAsFloat(): float
    {
        return round($this->getPriceWithTax() / 100, 2);
    }
}
