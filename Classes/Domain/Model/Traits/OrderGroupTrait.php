<?php

namespace CodingMs\Shop\Domain\Model\Traits;

use CodingMs\Shop\Domain\Model\FrontendUserGroup;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

trait OrderGroupTrait
{
    /**
     * @var ?FrontendUserGroup
     */
    protected ?FrontendUserGroup $orderGroup;

    /**
     * @var bool
     */
    protected bool $orderGroupOrdersVisible = false;

    /**
     * @return ?FrontendUserGroup
     */
    public function getOrderGroup(): ?FrontendUserGroup
    {
        return $this->orderGroup;
    }
    /**
     * @return int
     */
    public function getOrderGroupUid(): int
    {
        $uid = 0;
        if ($this->orderGroup !== null) {
            $uid = (int)$this->orderGroup->getUid();
        }

        return $uid;
    }

    /**
     * @param FrontendUserGroup $orderGroup
     */
    public function setOrderGroup(FrontendUserGroup $orderGroup): void
    {
        $this->orderGroup = $orderGroup;
    }

    /**
     * @return bool
     */
    public function getOrderGroupOrdersVisible(): bool
    {
        return $this->orderGroupOrdersVisible;
    }
    /**
     * @return bool
     */
    public function isOrderGroupOrdersVisible(): bool
    {
        return $this->orderGroupOrdersVisible;
    }

    /**
     * @param bool $orderGroupOrdersVisible
     */
    public function setOrderGroupOrdersVisible(bool $orderGroupOrdersVisible): void
    {
        $this->orderGroupOrdersVisible = $orderGroupOrdersVisible;
    }
}
