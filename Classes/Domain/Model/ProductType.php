<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\ImageTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\ImportIdStringTrait;

/**
 * Shop producttype
 *
 * @noinspection PhpUnused
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ProductType extends Base
{
    public const TABLE = 'tx_shop_domain_model_producttype';

    use ImageTrait;
    use ImportIdStringTrait;

    protected string $title = '';
    protected string $filter = '';

    protected bool $titleEnabled = false;
    protected bool $titleEnabledVariant = false;
    protected bool $slugEnabled = false;
    protected bool $slugEnabledVariant = false;
    protected bool $subtitleEnabled = false;
    protected bool $subtitleEnabledVariant = false;
    protected bool $productNoEnabled = false;
    protected bool $productNoEnabledVariant = false;
    protected bool $productTypeEnabled = false;
    protected bool $productTypeEnabledVariant = false;
    protected bool $teaserEnabled = false;
    protected bool $teaserEnabledVariant = false;
    protected bool $descriptionEnabled = false;
    protected bool $descriptionEnabledVariant = false;
    protected bool $pdfDescriptionEnabled = false;
    protected bool $pdfDescriptionEnabledVariant = false;
    protected bool $descriptionFileEnabled = false;
    protected bool $descriptionFileEnabledVariant = false;
    protected bool $wwwEnabled = false;
    protected bool $wwwEnabledVariant = false;
    protected bool $weightEnabled = false;
    protected bool $weightEnabledVariant = false;
    protected bool $highlightEnabled = false;
    protected bool $highlightEnabledVariant = false;
    protected bool $firstInListEnabled = false;
    protected bool $firstInListEnabledVariant = false;
    protected bool $colorEnabled = false;
    protected bool $colorEnabledVariant= false;
    protected bool $sizeEnabled = false;
    protected bool $sizeEnabledVariant = false;

    // Tab: SEO
    protected bool $canonicalLinkEnabled = false;
    protected bool $canonicalLinkEnabledVariant = false;
    protected bool $htmlTitleEnabled = false;
    protected bool $htmlTitleEnabledVariant = false;
    protected bool $metaAbstractEnabled = false;
    protected bool $metaAbstractEnabledVariant = false;
    protected bool $metaDescriptionEnabled = false;
    protected bool $metaDescriptionEnabledVariant = false;
    protected bool $metaKeywordsEnabled = false;
    protected bool $metaKeywordsEnabledVariant = false;

    // Tab: Prices
    protected bool $priceEnabled = false;
    protected bool $priceEnabledVariant = false;
    protected bool $priceTypeEnabled = false;
    protected bool $priceTypeEnabledVariant = false;
    protected bool $taxEnabled = false;
    protected bool $taxEnabledVariant = false;
    protected bool $offerEnabled = false;
    protected bool $offerEnabledVariant = false;
    protected bool $offerRebateValueEnabled = false;
    protected bool $offerRebateValueEnabledVariant = false;
    protected bool $inStockEnabled = false;
    protected bool $inStockEnabledVariant = false;
    protected bool $unitEnabled = false;
    protected bool $unitEnabledVariant = false;
    protected bool $discountEnabled = false;
    protected bool $discountEnabledVariant = false;
    protected bool $graduatedPricesEnabled = false;
    protected bool $graduatedPricesEnabledVariant = false;

    // Tab: Relations
    protected bool $categoriesEnabled = false;
    protected bool $categoriesEnabledVariant = false;
    protected bool $tagsEnabled = false;
    protected bool $tagsEnabledVariant = false;
    protected bool $relatedProductsEnabled = false;
    protected bool $relatedProductsEnabledVariant = false;
    protected bool $fileCollectionsEnabled = false;
    protected bool $fileCollectionsEnabledVariant = false;
    protected bool $filesEnabled = false;
    protected bool $filesEnabledVariant = false;
    protected bool $questionCategoriesEnabled = false;
    protected bool $questionCategoriesEnabledVariant = false;
    protected bool $attachFrontendUserGroupsEnabled = false;
    protected bool $attachFrontendUserGroupsEnabledVariant = false;

    // Tab: Images
    protected bool $imagesEnabled = false;
    protected bool $imagesEnabledVariant = false;
    protected bool $featureIconsEnabled = false;
    protected bool $featureIconsEnabledVariant = false;
    protected bool $otherImagesEnabled = false;
    protected bool $otherImagesEnabledVariant = false;

    // Tab: Videos
    protected bool $videosEnabled = false;
    protected bool $videosEnabledVariant = false;

    // Tab: Attributes
    protected bool $attributesEnabled = false;
    protected bool $attributesEnabledVariant = false;

    // Tab: Custom
    protected bool $customInformationEnabled = false;
    protected bool $customInformationRequired = false;
    protected string $customInformationLabel = 'Custom information';
    protected string $customInformationType = 'string';

    protected bool $attribute1Enabled = false;
    protected bool $attribute1EnabledVariant = false;
    protected string $attribute1Type = '';
    protected string $attribute1Label = '';
    protected string $attribute1Description = '';
    protected bool $attribute2Enabled = false;
    protected bool $attribute2EnabledVariant = false;
    protected string $attribute2Type = '';
    protected string $attribute2Label = '';
    protected string $attribute2Description = '';
    protected bool $attribute3Enabled = false;
    protected bool $attribute3EnabledVariant = false;
    protected string $attribute3Type = '';
    protected string $attribute3Label = '';
    protected string $attribute3Description = '';
    protected bool $attribute4Enabled = false;
    protected bool $attribute4EnabledVariant = false;
    protected string $attribute4Type = '';
    protected string $attribute4Label = '';
    protected string $attribute4Description = '';
    protected bool $attribute5Enabled = false;
    protected bool $attribute5EnabledVariant = false;
    protected string $attribute5Type = '';
    protected string $attribute5Label = '';
    protected string $attribute5Description = '';
    protected bool $attribute6Enabled = false;
    protected bool $attribute6EnabledVariant = false;
    protected string $attribute6Type = '';
    protected string $attribute6Label = '';
    protected string $attribute6Description = '';
    protected bool $attribute7Enabled = false;
    protected bool $attribute7EnabledVariant = false;
    protected string $attribute7Type = '';
    protected string $attribute7Label = '';
    protected string $attribute7Description = '';
    protected bool $attribute8Enabled = false;
    protected bool $attribute8EnabledVariant = false;
    protected string $attribute8Type = '';
    protected string $attribute8Label = '';
    protected string $attribute8Description = '';

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (str_starts_with($name, 'get')) {
            $propertyName = lcfirst(substr($name, 3));
            if (property_exists($this, $propertyName)) {
                return $this->$propertyName;
            }
        }
        if (str_starts_with($name, 'is')) {
            $propertyName = lcfirst(substr($name, 2));
            if (property_exists($this, $propertyName)) {
                return $this->$propertyName;
            }
        }
        return null;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getFilter(): string
    {
        return $this->filter;
    }

    public function isCustomInformationEnabled(): bool
    {
        return $this->customInformationEnabled;
    }

    public function isCustomInformationRequired(): bool
    {
        return $this->customInformationRequired;
    }

    public function isCustomInformationRequiredAsInteger(): int
    {
        return $this->customInformationRequired ? 1 : 0;
    }

    public function getCustomInformationLabel(): string
    {
        return $this->customInformationLabel;
    }

    public function getCustomInformationType(): string
    {
        return $this->customInformationType;
    }
}
