<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\QuantityTrait;
use CodingMs\Shop\Domain\Repository\ProductRepository;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;

/**
 * Item of a shopping basket.
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class BasketItem extends AbstractEntity
{
    public const TABLE = 'tx_shop_domain_model_basketitem';

    use QuantityTrait;

    /**
     * !!! ATTENTION !!!
     * We don't set a type on the property,
     * because otherwise we get a conflict with lazy loading,
     * because union-types are not supported yet!
     *
     * @var Product
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $product;

    protected int $price = 0;
    protected int $priceWithTax = 0;
    protected int $tax = 0;
    protected bool $isGraduatedPrice = false;
    protected bool $isDiscounted = false;
    protected string $discountType = '';
    protected string $discountLabel = '';
    protected int $discount = 0;
    protected int $discountValue = 0;
    protected string $customInformation = '';
    protected string $customInformationType = '';
    protected string $customInformationLabel = '';
    protected bool $customInformationRequired = false;

    /**
     * @return Product $product
     */
    public function getProduct(): Product
    {
        /** @phpstan-ignore-next-line */
        if ($this->product instanceof LazyLoadingProxy) {
            $this->product->_loadRealInstance();
        }
        //
        // Project might be disabled in backend!?
        if (!($this->product instanceof Product)) {
            $thisArray = BackendUtility::getRecord(BasketItem::TABLE, (int)$this->getUid());
            if (isset($thisArray) && is_array($thisArray)) {
                $productUid = (int)$thisArray['product'];
                if ($productUid > 0) {
                    /** @var ProductRepository $productRepository */
                    $productRepository = GeneralUtility::makeInstance(ProductRepository::class);
                    $this->product = $productRepository->findByIdentifierIgnoreEnableFieldsAndStorage($productUid);
                }
            }
        }
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return int $price
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return float $price
     */
    public function getPriceAsFloat(): float
    {
        return round($this->getPrice() / 100, 2);
    }

    /**
     * @return int $price
     */
    public function getPriceWithTax(): int
    {
        // compatibility with shop versions before price with tax was persisted in the db
        if (!isset($this->priceWithTax) || $this->priceWithTax === 0) {
            return (int)round($this->price * (10000 + $this->tax) / 10000);
        }
        return $this->priceWithTax;
    }

    /**
     * @return float $price
     */
    public function getPriceWithTaxAsFloat(): float
    {
        // compatibility with shop versions before price with tax was persisted in the db
        if (!isset($this->priceWithTax) || $this->priceWithTax === 0) {
            return round($this->getPriceWithTax() / 100, 2);
        }
        return round($this->priceWithTax / 100, 2);
    }

    /**
     * @param int $priceWithTax
     */
    public function setPriceWithTax(int $priceWithTax): void
    {
        $this->priceWithTax = $priceWithTax;
    }

    /**
     * @return int $tax
     */
    public function getTax(): int
    {
        return $this->tax;
    }

    /**
     * @return float $tax
     */
    public function getTaxAsFloat(): float
    {
        return round($this->tax / 100, 2);
    }

    /**
     * @param int $tax
     */
    public function setTax(int $tax): void
    {
        $this->tax = $tax;
    }

    /**
     * @return int $tax
     */
    public function getTaxPrice(): int
    {
        return $this->getPriceWithTax() - $this->getPrice();
    }

    /**
     * @return float $tax
     */
    public function getTaxPriceAsFloat(): float
    {
        return round($this->getTaxPrice() / 100, 2);
    }

    /**
     * @return int $price
     */
    public function getTotalPrice(): int
    {
        return $this->price * $this->quantity;
    }

    /**
     * @return float $price
     */
    public function getTotalPriceAsFloat(): float
    {
        return round($this->getTotalPrice() / 100, 2);
    }

    /**
     * @return int
     */
    public function getTotalPriceWithTax(): int
    {
        return $this->getPriceWithTax() * $this->quantity;
    }

    /**
     * @return float
     */
    public function getTotalPriceWithTaxAsFloat(): float
    {
        return round($this->getTotalPriceWithTax() / 100, 2);
    }

    /**
     * @return bool
     */
    public function getIsGraduatedPrice(): bool
    {
        return $this->isGraduatedPrice;
    }

    /**
     * @param bool $isGraduatedPrice
     */
    public function setIsGraduatedPrice(bool $isGraduatedPrice): void
    {
        $this->isGraduatedPrice = $isGraduatedPrice;
    }

    public function getIsDiscounted(): bool
    {
        return $this->isDiscounted;
    }

    public function setIsDiscounted(bool $isDiscounted): void
    {
        $this->isDiscounted = $isDiscounted;
    }

    public function getDiscountType(): string
    {
        return $this->discountType;
    }

    public function setDiscountType(string $discountType): void
    {
        $this->discountType = $discountType;
    }

    public function getDiscountLabel(): string
    {
        return $this->discountLabel;
    }

    public function setDiscountLabel(string $discountLabel): void
    {
        if ($this->discountLabel === '') {
            $this->discountLabel = $discountLabel;
        } else {
            $this->discountLabel .= ', ' . $discountLabel;
        }
    }

    public function getDiscount(): int
    {
        return $this->discount;
    }

    public function getDiscountAsFloat(): float
    {
        return round($this->getDiscount() / 100, 2);
    }

    public function setDiscount(int $discount): void
    {
        $this->discount = $discount;
        //
        // Subtract the discount from price
        $this->setPrice($this->getPrice() - $this->discount);
        $taxValueInCent = (int)round($this->getPrice() / 100) * ($this->product->getTax() / 100);
        $this->setPriceWithTax($this->getPrice() + $taxValueInCent);
    }

    public function getDiscountValue(): int
    {
        return $this->discountValue;
    }

    public function getDiscountValueAsFloat(): float
    {
        return round($this->getDiscountValue() / 100, 2);
    }

    public function setDiscountValue(int $discountValue): void
    {
        $this->discountValue = $discountValue;
    }

    public function getRegularPrice(): int
    {
        return $this->getPrice() + $this->getDiscount();
    }

    public function getRegularPriceAsFloat(): float
    {
        return $this->getPriceAsFloat() + $this->getDiscountAsFloat();
    }

    public function getCustomInformation(): string
    {
        return $this->customInformation;
    }

    public function setCustomInformation(string $customInformation): void
    {
        $this->customInformation = $customInformation;
    }

    public function getCustomInformationType(): string
    {
        return $this->customInformationType;
    }

    public function setCustomInformationType(string $customInformationType): void
    {
        $this->customInformationType = $customInformationType;
    }

    public function getCustomInformationLabel(): string
    {
        return $this->customInformationLabel;
    }

    public function setCustomInformationLabel(string $customInformationLabel): void
    {
        $this->customInformationLabel = $customInformationLabel;
    }

    public function getCustomInformationRequired(): bool
    {
        return $this->customInformationRequired;
    }

    public function setCustomInformationRequired(bool $customInformationRequired): void
    {
        $this->customInformationRequired = $customInformationRequired;
    }
}
