<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\LabelTrait;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Product shipping cost
 */
class ProductShippingCost extends Base
{
    use LabelTrait;

    protected string $labelPublic = '';
    protected string $labelBulkyPublic = '';
    protected int $weight = 0;
    protected int $price = 0;
    protected int $bulky = 0;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Shop\Domain\Model\ProductShippingCostCountryOverlay>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $overlay;

    public function getLabelPublic(): string
    {
        return $this->labelPublic;
    }
    public function setLabelPublic(string $labelPublic): void
    {
        $this->labelPublic = $labelPublic;
    }

    public function getLabelBulkyPublic(): string
    {
        return $this->labelBulkyPublic;
    }
    public function setLabelBulkyPublic(string $labelBulkyPublic): void
    {
        $this->labelBulkyPublic = $labelBulkyPublic;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }
    public function getWeightAsFloat(): float
    {
        return round($this->getWeight() / 100, 2);
    }
    public function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }

    public function getPrice(?string $countryCode=null): int
    {
        if (!isset($countryCode)) {
            return $this->price;
        }
        foreach ($this->getOverlay() as $overlay) {
            if ($overlay->getCountryCode() === $countryCode) {
                return $overlay->getPrice();
            }
        }
        return $this->price;
    }
    public function getPriceAsFloat(?string $countryCode=null): float
    {
        return round($this->getPrice($countryCode) / 100, 2);
    }
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    public function getBulky(?string $countryCode=null): int
    {
        if (!isset($countryCode)) {
            return $this->bulky;
        }
        foreach ($this->getOverlay() as $overlay) {
            if ($overlay->getCountryCode() === $countryCode) {
                return $overlay->getBulky();
            }
        }
        return $this->bulky;
    }
    public function getBulkyAsFloat(?string $countryCode=null): float
    {
        return round($this->getBulky($countryCode) / 100, 2);
    }
    public function setBulky(int $bulky): void
    {
        $this->bulky = $bulky;
    }

    /**
     * @return ObjectStorage<ProductShippingCostCountryOverlay>
     */
    public function getOverlay(): ObjectStorage
    {
        return $this->overlay;
    }

    /**
     * @param ObjectStorage<ProductShippingCostCountryOverlay> $overlay
     */
    public function setOverlay(ObjectStorage $overlay): void
    {
        $this->overlay = $overlay;
    }
}
