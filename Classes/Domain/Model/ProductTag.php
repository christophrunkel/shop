<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\DescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TitleTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\VariantStringTrait;
use CodingMs\Shop\Domain\Model\Traits\ProductTagCategoryTrait;

/**
 * Product tag
 */
class ProductTag extends Base
{
    use TitleTrait;
    use DescriptionTrait;
    use VariantStringTrait;
    use ProductTagCategoryTrait;

    public function getTooltipText(): string
    {
        $strippedDescription = strip_tags($this->getDescription());
        if (trim($strippedDescription) !== '') {
            return $strippedDescription;
        }
        return $this->getTitle();
    }
}
