<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Shopping basket.
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Basket extends AbstractEntity
{
    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Shop\Domain\Model\BasketItem>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $basketItems;

    /**
     * Complete data as JSON string
     * @var string
     */
    protected $data = '';

    /**
     * @var string Basket type: b2c or b2b
     */
    protected string $type = 'b2c';
    protected int $price = 0;

    /**
     * This is the tax value!
     * Not in percent!
     *
     * @var int
     */
    protected int $tax = 0;

    /**
     * Array with all different taxes and amount,
     * in case of products with different tax
     * @var array
     */
    protected $taxes = [];

    protected int $priceWithTax = 0;
    protected int $finalPrice = 0;
    protected int $minimumOrderValue;

    /**
     * This prices relates to the gross/total price
     * @var array
     */
    protected $additionalPrices = [];

    /**
     * This prices relates to the net price
     * @var array
     */
    protected $discountPrices = [];

    public function __construct()
    {
        $this->basketItems = new ObjectStorage();
    }

    /**
     * @param \CodingMs\Shop\Domain\Model\BasketItem $basketItem
     */
    public function addBasketItem(BasketItem $basketItem): void
    {
        $this->basketItems->attach($basketItem);
    }

    /**
     * @param \CodingMs\Shop\Domain\Model\BasketItem $basketItemToRemove The Item to be removed
     * @noinspection PhpUnused
     */
    public function removeBasketItem(BasketItem $basketItemToRemove): void
    {
        $this->basketItems->detach($basketItemToRemove);
    }

    public function removeAllBasketItems(): void
    {
        $this->basketItems = new ObjectStorage();
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Shop\Domain\Model\BasketItem> $basketItems
     */
    public function getBasketItems()
    {
        return $this->basketItems;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Shop\Domain\Model\BasketItem> $basketItems
     */
    public function setBasketItems(ObjectStorage $basketItems): void
    {
        $this->basketItems = $basketItems;
    }

    public function getBasketItemCount(): int
    {
        return count($this->getBasketItems());
    }

    /**
     * Returns the summary of weight of all products in basket
     * @return int
     */
    public function getWeight(): int
    {
        $weight = 0;
        foreach ($this->getBasketItems() as $basketItem) {
            $weight += ($basketItem->getProduct()->getWeight() * $basketItem->getQuantity());
        }
        return $weight;
    }

    /**
     * Returns true if at least one bulky product is in basket
     * @return bool
     */
    public function getBulky(): bool
    {
        foreach ($this->getBasketItems() as $basketItem) {
            if ($basketItem->getProduct()->getBulky()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return json_decode($this->data, true);
    }

    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }

    public function getType(): string
    {
        return $this->type;
    }
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getPriceAsFloat(): float
    {
        return round($this->price / 100, 2);
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * This is the tax value!
     * Not in percent!
     *
     * @return int
     */
    public function getTax(): int
    {
        return $this->tax;
    }

    /**
     * This is the tax value!
     * Not in percent!
     *
     * @return float
     */
    public function getTaxAsFloat(): float
    {
        return round($this->tax / 100, 2);
    }

    /**
     * @param int $tax
     */
    public function setTax(int $tax): void
    {
        $this->tax = $tax;
    }

    /**
     * @return array
     */
    public function getTaxes(): array
    {
        return $this->taxes;
    }

    /**
     * @param array $taxes
     */
    public function setTaxes(array $taxes): void
    {
        $this->taxes = $taxes;
    }

    /**
     * @return int
     */
    public function getPriceWithTax(): int
    {
        return $this->priceWithTax;
    }

    /**
     * @return float
     */
    public function getPriceWithTaxAsFloat(): float
    {
        return round($this->priceWithTax / 100, 2);
    }

    /**
     * @param int $priceWithTax
     */
    public function setPriceWithTax(int $priceWithTax): void
    {
        $this->priceWithTax = $priceWithTax;
    }

    public function getFinalPrice(): int
    {
        return $this->finalPrice;
    }
    public function getFinalPriceAsFloat(): float
    {
        return round($this->finalPrice / 100, 2);
    }
    public function setFinalPrice(int $finalPrice): void
    {
        $this->finalPrice = $finalPrice;
    }

    public function getMinimumOrderValue(): int
    {
        return $this->minimumOrderValue;
    }
    public function getMinimumOrderValueAsFloat(): float
    {
        return round($this->minimumOrderValue / 100, 2);
    }
    public function setMinimumOrderValue(int $minimumOrderValue): void
    {
        $this->minimumOrderValue = $minimumOrderValue;
    }
    public function getIsMinimumOrderValueReached(): bool
    {
        $reached = ($this->minimumOrderValue <= $this->priceWithTax);
        if ($this->getType() === 'b2b') {
            $reached = ($this->minimumOrderValue <= $this->price);
        }
        return $reached;
    }

    /**
     * @return array
     */
    public function getAdditionalPrices(): array
    {
        return $this->additionalPrices;
    }

    /**
     * @param array $additionalPrice
     */
    public function setAdditionalPrice(array $additionalPrice): void
    {
        $this->additionalPrices[] = $additionalPrice;
    }

    /**
     * @param array $additionalPrices
     */
    public function setAdditionalPrices(array $additionalPrices): void
    {
        $this->additionalPrices = $additionalPrices;
    }

    /**
     * @return array
     */
    public function getDiscountPrices(): array
    {
        return $this->discountPrices;
    }

    /**
     * @param array $discountPrice
     */
    public function setDiscountPrice(array $discountPrice): void
    {
        $this->discountPrices[] = $discountPrice;
    }

    /**
     * @param array $discountPrices
     */
    public function setDiscountPrices(array $discountPrices): void
    {
        $this->discountPrices = $discountPrices;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = [
            'price' => $this->getPrice(),
            'priceAsFloat' => $this->getPriceAsFloat(),
            'tax' => $this->getTax(),
            'taxAsFloat' => $this->getTaxAsFloat(),
            'taxes' => $this->getTaxes(),
            'priceWithTax' => $this->getPriceWithTax(),
            'priceWithTaxAsFloat' => $this->getPriceWithTaxAsFloat(),
            'finalPrice' => $this->getFinalPrice(),
            'finalPriceAsFloat' => $this->getFinalPriceAsFloat(),
            'quantity' => $this->getBasketItemCount(),
            'minimumOrderValue' => $this->getMinimumOrderValue(),
            'minimumOrderValueAsFloat' => $this->getMinimumOrderValueAsFloat(),
            'minimumOrderValueReached' => $this->getIsMinimumOrderValueReached(),
            'additionalPrices' => $this->getAdditionalPrices(),
            'discountPrices' => $this->getDiscountPrices(),
            'weight' => $this->getWeight(),
            'bulky' => $this->getBulky(),
        ];
        /** @var \CodingMs\Shop\Domain\Model\BasketItem $basketItem */
        foreach ($this->getBasketItems() as $basketItem) {
            /** @var Product $product */
            $product = $basketItem->getProduct();
            $productArray = [
                'uid' => $product->getUid(),
                'title' => $product->getTitle(),
                'subtitle' => $product->getSubtitle(),
                'pdfDescription' => $product->getPdfDescription(),
                'productNo' => $product->getProductNo(),
                'price' => $product->getPrice(),
                'priceAsFloat' => $product->getPriceAsFloat(),
                'priceWithTax' => $product->getPriceWithTax(),
                'priceWithTaxAsFloat' => $product->getPriceWithTaxAsFloat(),
                'tax' => $product->getTax(),
                'taxAsFloat' => $product->getTaxAsFloat(),
                'unit' => $product->getUnit(),
                'unitFactor' => $product->getUnitFactor(),
            ];
            $array['items'][] = [
                'quantity' => $basketItem->getQuantity(),
                // Single price
                'price' => $basketItem->getPrice(),
                'priceAsFloat' => $basketItem->getPriceAsFloat(),
                // Single price * quantity
                'totalPrice' => $basketItem->getTotalPrice(),
                'totalPriceAsFloat' => $basketItem->getTotalPriceAsFloat(),
                // (Single price * 1.19) - Single price
                'tax' => $basketItem->getTax(),
                'taxAsFloat' => $basketItem->getTaxAsFloat(),
                //
                'priceWithTax' => $basketItem->getPriceWithTax(),
                'priceWithTaxAsFloat' => $basketItem->getPriceWithTaxAsFloat(),
                //
                'totalPriceWithTax' => $basketItem->getTotalPriceWithTax(),
                'totalPriceWithTaxAsFloat' => $basketItem->getTotalPriceWithTaxAsFloat(),
                //
                'isGraduatedPrice' => $basketItem->getIsGraduatedPrice(),
                //
                'isDiscounted' => $basketItem->getIsDiscounted(),
                'discountType' => $basketItem->getIsDiscounted() ? $basketItem->getDiscountType() : '',
                'discount' => $basketItem->getIsDiscounted() ? $basketItem->getDiscount() : 0,
                'discountAsFloat' => $basketItem->getIsDiscounted() ? $basketItem->getDiscountAsFloat() : 0.0,
                'discountValue' => $basketItem->getIsDiscounted() ? $basketItem->getDiscountValue() : 0,
                'discountValueAsFloat' => $basketItem->getIsDiscounted() ? $basketItem->getDiscountValueAsFloat() : 0.0,
                'regularPrice' => $basketItem->getIsDiscounted() ? $basketItem->getRegularPrice() : $basketItem->getPrice(),
                'regularPriceAsFloat' => $basketItem->getIsDiscounted() ? $basketItem->getRegularPriceAsFloat() : $basketItem->getPriceAsFloat(),
                //
                'customInformation' => $basketItem->getCustomInformation(),
                'customInformationType' => $basketItem->getCustomInformationType(),
                'customInformationLabel' => $basketItem->getCustomInformationLabel(),
                'customInformationRequired' => $basketItem->getCustomInformationRequired(),
                //
                'product' => $productArray,
            ];
        }
        return $array;
    }
}
