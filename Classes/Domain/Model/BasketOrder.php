<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\EmailTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\NameTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\StatusStringTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TypeStringTrait;
use CodingMs\Shop\Domain\Model\Traits\FrontendUserTrait;
use DateTime;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class BasketOrder extends Base
{
    // State of the basket order: prepared, ordered, paid, canceled, requested
    use StatusStringTrait;

    // Type of order: request, onInvoice, payPal, prePayment
    use TypeStringTrait;

    use EmailTrait;
    use NameTrait;
    use FrontendUserTrait;

    protected int $invoiceNumber = 0;
    protected int $timeForPayment = 0;
    protected bool $processed = false;
    protected string $vatId = '';

    protected string $vatZone = '';
    protected bool $vatable = false;
    protected int $price = 0;

    /**
     * @var int
     */
    protected int $priceWithTax = 0;

    /**
     * This is the tax value!
     * Not in percent!
     *
     * @var int
     */
    protected int $tax = 0;
    protected bool $ordered = false;
    protected ?DateTime $orderDate = null;
    protected bool $paid = false;
    protected ?DateTime $paidDate = null;

    protected int $paidPrice = 0;
    protected bool $send = false;
    protected ?DateTime $sendDate = null;
    protected string $emailCopy = '';

    /**
     * Complete configuration and field/form data
     * @var string
     */
    protected string $params = '';

    /**
     * Callback parameter, for example received from PayPal
     * @var string
     */
    protected string $callbackParameter = '';

    /**
     * Stripe payment intent id
     * @var string
     */
    protected string $stripePaymentIntent = '';

    /**
     * Shopping basket
     * @var Basket
     */
    protected Basket $basket;

    public function getInvoiceNumber(): int
    {
        return $this->invoiceNumber;
    }

    public function setInvoiceNumber(int $invoiceNumber): void
    {
        $this->invoiceNumber = $invoiceNumber;
    }

    public function isTypeRequest(): bool
    {
        return $this->getType() === 'request';
    }

    public function getTimeForPayment(): int
    {
        return $this->timeForPayment;
    }

    public function getTimeForPaymentDate(): ?DateTime
    {
        $orderedDate = $this->getOrderDate();
        if ($orderedDate !== null) {
            $orderedTimestamp = $orderedDate->getTimestamp();
            $timeForPaymentSeconds = $this->timeForPayment * 60 * 60 * 24;
            $timeForPaymentDate = new DateTime();
            $timeForPaymentDate->setTimestamp($orderedTimestamp + $timeForPaymentSeconds);
            return $timeForPaymentDate;
        }
        return null;
    }

    public function setTimeForPayment(int $timeForPayment): void
    {
        $this->timeForPayment = $timeForPayment;
    }

    public function getProcessed(): bool
    {
        return $this->processed;
    }

    public function setProcessed(bool $processed): void
    {
        $this->processed = $processed;
    }

    /**
     * @return string
     */
    public function getVatId(): string
    {
        return $this->vatId;
    }

    public function setVatId(string $vatId): void
    {
        $this->vatId = $vatId;
    }

    public function getVatZone(): string
    {
        return $this->vatZone;
    }

    public function setVatZone(string $vatZone): void
    {
        $this->vatZone = $vatZone;
    }

    /**
     * @return bool
     */
    public function isVatable(): bool
    {
        return $this->vatable;
    }

    /**
     * @param bool $vatable
     */
    public function setVatable(bool $vatable): void
    {
        $this->vatable = $vatable;
    }

    /**
     * Returns the price
     *
     * @return int $price
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * Returns the price as float
     *
     * @return float $price
     */
    public function getPriceAsFloat(): float
    {
        return round($this->price / 100, 2);
    }

    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int $price
     */
    public function getPriceWithTax(): int
    {
        // compatibility with shop versions before price with tax was persisted in the db
        if (!isset($this->priceWithTax) || $this->priceWithTax === 0) {
            return (int)round($this->price * (10000 + $this->tax) / 10000);
        }
        return $this->priceWithTax;
    }

    /**
     * @return float $price
     */
    public function getPriceWithTaxAsFloat(): float
    {
        // compatibility with shop versions before price with tax was persisted in the db
        if (!isset($this->priceWithTax) || $this->priceWithTax === 0) {
            return round($this->getPriceWithTax() / 100, 2);
        }
        return round($this->priceWithTax / 100, 2);
    }

    public function setPriceWithTax(int $priceWithTax): void
    {
        $this->priceWithTax = $priceWithTax;
    }

    /**
     * This is the tax value!
     * Not in percent!
     *
     * @return int $tax
     */
    public function getTax(): int
    {
        return $this->tax;
    }

    /**
     * This is the tax value!
     * Not in percent!
     *
     * @return float $tax
     */
    public function getTaxAsFloat(): float
    {
        return round($this->tax / 100, 2);
    }

    /**
     * This is the tax value!
     * Not in percent!
     *
     * @param int $tax
     */
    public function setTax(int $tax): void
    {
        $this->tax = $tax;
    }

    public function isOrdered(): bool
    {
        return $this->ordered;
    }

    public function setOrdered(bool $ordered): void
    {
        $this->ordered = $ordered;
    }

    public function getOrderDate(): ?DateTime
    {
        return $this->orderDate;
    }

    /**
     * @param DateTime $orderDate
     */
    public function setOrderDate(DateTime $orderDate): void
    {
        $this->orderDate = $orderDate;
    }

    public function isPaid(): bool
    {
        return $this->paid;
    }

    public function getPaid(): bool
    {
        return $this->paid;
    }

    public function setPaid(bool $paid): void
    {
        $this->paid = $paid;
    }

    /**
     * @return DateTime
     */
    public function getPaidDate(): ?DateTime
    {
        return $this->paidDate;
    }

    /**
     * @param DateTime $paidDate
     */
    public function setPaidDate(DateTime $paidDate): void
    {
        $this->paidDate = $paidDate;
    }

    /**
     * Returns the paidPrice
     *
     * @return int $paidPrice
     */
    public function getPaidPrice(): int
    {
        return $this->paidPrice;
    }

    /**
     * Returns the paid price as float
     *
     * @return float $paidPrice
     */
    public function getPaidPriceAsFloat(): float
    {
        return round($this->paidPrice / 100, 2);
    }

    public function setPaidPrice(int $paidPrice): void
    {
        $this->paidPrice = $paidPrice;
    }

    /**
     * @return bool
     */
    public function isSend(): bool
    {
        return $this->send;
    }

    /**
     * @param bool $send
     */
    public function setSend(bool $send): void
    {
        $this->send = $send;
    }

    /**
     * @return DateTime
     */
    public function getSendDate(): ?DateTime
    {
        return $this->sendDate;
    }

    /**
     * @param DateTime $sendDate
     */
    public function setSendDate(DateTime $sendDate): void
    {
        $this->sendDate = $sendDate;
    }

    public function getEmailCopy(): string
    {
        return $this->emailCopy;
    }

    public function setEmailCopy(string $emailCopy): void
    {
        $this->emailCopy = $emailCopy;
    }

    public function getParams(): string
    {
        return $this->params;
    }

    /**
     * Returns the params
     *
     * @return array $params
     */
    public function getParamsArray(): array
    {
        return json_decode($this->params, true);
    }

    /**
     * Sets the params
     *
     * @param string $params
     */
    public function setParams(string $params): void
    {
        $this->params = $params;
    }

    public function getCallbackParameter(): string
    {
        return $this->callbackParameter;
    }

    public function setCallbackParameter(string $callbackParameter): void
    {
        $this->callbackParameter = $callbackParameter;
    }

    public function getBasket(): Basket
    {
        return $this->basket;
    }

    public function setBasket(Basket $basket): void
    {
        $this->basket = $basket;
    }

    /**
     * @return string
     */
    public function getStripePaymentIntent(): string
    {
        return $this->stripePaymentIntent;
    }

    /**
     * @param string $stripePaymentIntent
     */
    public function setStripePaymentIntent(string $stripePaymentIntent): void
    {
        $this->stripePaymentIntent = $stripePaymentIntent;
    }

    /**
     * @return string
     * @throws \TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException
     */
    public function getBackendInvoicePdfLink(): string
    {
        if ((int)VersionNumberUtility::getCurrentTypo3Version() >= 12) {
            $parameter = [
                'id' => $this->pid,
                'M' => 'shoppro_shoppro',
                'controller' => 'Backend',
                'basketOrder' => $this->uid,
            ];
        } else {
            $parameter = [
                'id' => $this->pid,
                'M' => 'web_ShopProShop',
                'tx_shoppro_web_shopproshop' => [
                    'controller' => 'Backend',
                    'basketOrder' => $this->uid,
                ]
            ];
        }
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        return (string)$uriBuilder->buildUriFromRoute('shop_create_invoice_pdf', $parameter);
    }

    public function isInvoicePdfPossible(): bool
    {
        return !$this->isTypeRequest() && $this->isOrdered();
    }

    /**
     * @return string
     * @throws \TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException
     */
    public function getBackendDeliveryNotePdfLink(): string
    {
        if ((int)VersionNumberUtility::getCurrentTypo3Version() >= 12) {
            $parameter = [
                'id' => $this->pid,
                'M' => 'shoppro_shoppro',
                'controller' => 'Backend',
                'basketOrder' => $this->uid,
            ];
        } else {
            $parameter = [
                'id' => $this->pid,
                'M' => 'web_ShopProShop',
                'tx_shoppro_web_shopproshop' => [
                    'controller' => 'Backend',
                    'basketOrder' => $this->uid,
                ]
            ];
        }
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        return (string)$uriBuilder->buildUriFromRoute('shop_create_delivery_note_pdf', $parameter);
    }

    public function isDeliveryNotePdfPossible(): bool
    {
        return !$this->isTypeRequest() && $this->isPaid();
    }

}
