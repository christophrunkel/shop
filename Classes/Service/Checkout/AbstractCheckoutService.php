<?php

declare(strict_types=1);

namespace CodingMs\Shop\Service\Checkout;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\BasketOrder;
use CodingMs\Shop\Domain\Model\FrontendUser;
use CodingMs\Shop\Domain\Repository\BasketOrderRepository;
use CodingMs\Shop\Service\InvoiceService;
use CodingMs\Shop\Service\TypoScriptService;
use CodingMs\Shop\Utility\ExtensionUtility;
use CodingMs\Shop\Utility\FrontendUserUtility;
use Exception;
use Psr\Log\LogLevel;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator;

/**
 * Abstract checkout service
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
abstract class AbstractCheckoutService
{
    protected BasketOrderRepository $basketOrderRepository;
    protected PersistenceManager $persistenceManager;
    protected InvoiceService $invoiceService;
    protected FrontendUserUtility $frontendUserUtility;

    const CUSTOMER_CONFIRMATION_EMAIL_TEMPLATE = '';
    const ORDER_EMAIL_TEMPLATE = '';

    /**
     * @var array
     */
    protected array $log = [];

    protected ?Logger $logger = null;

    public function __construct(
        BasketOrderRepository $basketOrderRepository,
        PersistenceManager $persistenceManager,
        InvoiceService $invoiceService,
        FrontendUserUtility $frontendUserUtility
    ) {
        $this->basketOrderRepository = $basketOrderRepository;
        $this->persistenceManager = $persistenceManager;
        $this->invoiceService = $invoiceService;
        $this->frontendUserUtility = $frontendUserUtility;
    }

    /**
     * Aggregates the form configuration
     *
     * @param array $settings Plugin TypoScrip settings
     * @param string $type
     * @return array
     * @throws Exception
     */
    public function getConfiguration(array $settings, string $type): array
    {
        /** @var LogManager $logManager */
        $logManager = GeneralUtility::makeInstance(LogManager::class);
        $this->logger = $logManager->getLogger(__CLASS__);
        //
        $configuration = $settings['basketOrder']['orderOptions'][$type];
        $available = GeneralUtility::trimExplode(',', $configuration['fields']['available'], true);
        $required = GeneralUtility::trimExplode(',', $configuration['fields']['required'], true);
        if (!in_array('email', $available) && !in_array('email', $required)) {
            throw new Exception('E-Mail must be available and mandatory!');
        }
        if (!in_array('lastname', $available) && !in_array('lastname', $required)) {
            throw new Exception('Lastname must be available and mandatory!');
        }
        unset($configuration['fields']['available']);
        unset($configuration['fields']['required']);
        //
        // Check if there is a logged-in frontend user
        $configuration['profilePid'] = 0;
        $configuration['frontendUser'] = null;
        $frontendUser = $this->frontendUserUtility->getCurrentFrontendUser();
        //
        // Insert frontend user data into configuration
        if ($frontendUser instanceof FrontendUser) {
            $this->log(
                'AbstractCheckoutService->getConfiguration with frontendUser: '
                . $frontendUser->getUsername() . ' [' . $frontendUser->getUid() . ']'
            );
            $typoScript = TypoScriptService::getTypoScript((int)$GLOBALS['TSFE']->id);
            $configuration['profilePid'] = $typoScript['plugin']['tx_modules']['settings']['pages']['profile'] ?? 0;
            $configuration['frontendUser'] = $frontendUser;
            //
            // Replace street+houseNumber by address, so that it fits to the fe_users record
            foreach ($available as $key => $field) {
                if ($field === 'street') {
                    $available[$key] = 'address';
                }
                if ($field === 'houseNumber') {
                    unset($available[$key]);
                }
            }
            foreach ($required as $key => $field) {
                if ($field === 'street') {
                    $required[$key] = 'address';
                }
                if ($field === 'houseNumber') {
                    unset($required[$key]);
                }
            }
        } else {
            $this->log('AbstractCheckoutService->getConfiguration without frontendUser');
        }
        //
        foreach ($available as $field) {
            if (isset($settings['basketOrder']['fieldDefinition'][$field])) {
                $configuration['fields'][$field] = $settings['basketOrder']['fieldDefinition'][$field];
                $configuration['fields'][$field]['required'] = in_array($field, $required);
                $configuration['fields'][$field]['readonly'] = false;
                //
                // Insert default value
                if ($frontendUser instanceof FrontendUser) {
                    if ($configuration['fields'][$field]['emailCompare']['active'] ?? false) {
                        $configuration['fields'][$field]['emailCompare']['active'] = false;
                    }
                    $configuration['fields'][$field]['value'] = $frontendUser->getValueForCheckout($field);
                    $configuration['fields'][$field]['readonly'] = $frontendUser->isValueForCheckout($field);
                } elseif (isset($configuration['fields'][$field]['default']) && !isset($configuration['fields'][$field]['value'])) {
                    $configuration['fields'][$field]['value'] = $configuration['fields'][$field]['default'];
                }
                if (!$configuration['fields'][$field]['readonly']) {
                    // Migrate false to null, because otherwise the ViewHelper attribute gets wrong!
                    $configuration['fields'][$field]['readonly'] = null;
                }
            }
        }
        return $this->prepareConfiguration($configuration);
    }

    /**
     * Override this method in order to prepare-payment related configurations.
     *
     * @param array $configuration
     * @return array
     */
    public function prepareConfiguration(array $configuration): array
    {
        return $configuration;
    }

    public function addReturnLinks(array $configuration, int $basketOrderUid = 0): array
    {
        return $configuration;
    }

    /**
     * @param array $configuration
     * @param array $request
     * @return array
     */
    public function validateFormData(array $configuration, array $request): array
    {
        // Divergent delivery address is selected by the user in checkout form at all?!
        $deliveryAddressEnabled = boolval($request['deliveryAddressEnabled'] ?? false);
        $errorsFound = false;
        foreach ($configuration['fields'] as $field => $fieldData) {
            $value = '';
            $valid = false;
            $evaluate = GeneralUtility::trimExplode(',', $fieldData['eval'] ?? '', true);
            $evaluate = array_combine($evaluate, $evaluate);
            //
            // Grab the value
            if (isset($request[$field])) {
                $value = $request[$field];
                if (in_array('trim', $evaluate)) {
                    $value = trim($value);
                    unset($evaluate['trim']);
                }
            }
            //
            // Ensure the frontend user fields are in use, if a frontend user is logged-in!
            if ($configuration['frontendUser'] instanceof FrontendUser
                && $configuration['frontendUser']->isValueForCheckout($field)) {
                $value = $configuration['frontendUser']->getValueForCheckout($field);
            }
            //
            // Required and other evaluations
            //
            // In addition: enable evaluation for divergent delivery address, if enabled!
            $fieldIsDeliveryAddressField = boolval($fieldData['deliveryAddress'] ?? false);
            if ($fieldData['required'] && (!$fieldIsDeliveryAddressField || $deliveryAddressEnabled)) {
                switch ($fieldData['type']) {
                    case 'input':
                        $valid = !empty($value);
                        if (!$valid) {
                            $errorMessage = $configuration['fields'][$field]['errors']['isEmpty'];
                            $configuration['fields'][$field]['displayErrors'][md5($errorMessage)] = $errorMessage;
                        }
                        //
                        // Check for a valid mail address
                        if (in_array('email', $evaluate) && $valid) {
                            /** @var EmailAddressValidator $emailValidator */
                            $emailValidator = GeneralUtility::makeInstance(EmailAddressValidator::class);
                            $result = $emailValidator->validate($value);
                            $valid = !$result->hasErrors();
                            if (!$valid) {
                                $errorMessage = $configuration['fields'][$field]['errors']['isInvalidEmail'];
                                $configuration['fields'][$field]['displayErrors'][md5($errorMessage)] = $errorMessage;
                            }
                        }
                        if (in_array('phone', $evaluate) && $valid) {
                            $valid = boolval(preg_match('/^(?!(?:\d*-){5,})(?!(?:\d* ){5,})\+?([\d\- ]){5,20}$/', $value));
                            if (!$valid) {
                                $errorMessage = $configuration['fields'][$field]['errors']['isInvalidPhone'];
                                $configuration['fields'][$field]['displayErrors'][md5($errorMessage)] = $errorMessage;
                            }
                        }
                        //
                        // In case of enabled double mail address fields
                        // compare if entered mail addresses are equal!
                        if (isset($fieldData['emailCompare']['active']) && $fieldData['emailCompare']['active']) {
                            if ($request[$field] !== $request[$field . '_compare']) {
                                $errorMessage = $configuration['fields'][$field]['errors']['unequalMails'];
                                $configuration['fields'][$field]['displayErrors'][md5($errorMessage)] = $errorMessage;
                                $valid = false;
                            } else {
                                $configuration['fields'][$field]['emailCompare']['value'] = $request[$field . '_compare'];
                            }
                        }
                        break;
                    case 'textarea':
                        $valid = !empty($value);
                        if (!$valid) {
                            $errorMessage = $configuration['fields'][$field]['errors']['isEmpty'];
                            $configuration['fields'][$field]['displayErrors'][md5($errorMessage)] = $errorMessage;
                        }
                        break;
                    case 'country':
                        $valid = !empty($value);
                        /**
                         * @todo validate by country database
                         */
                        if (!$valid) {
                            $errorMessage = $configuration['fields'][$field]['errors']['isEmpty'];
                            $configuration['fields'][$field]['displayErrors'][md5($errorMessage)] = $errorMessage;
                        }
                        break;
                    case 'checkbox':
                        $valid = ($value === '1');
                        if (!$valid) {
                            $errorMessage = $configuration['fields'][$field]['errors']['isEmpty'];
                            $configuration['fields'][$field]['displayErrors'][md5($errorMessage)] = $errorMessage;
                        }
                        break;
                }
            } else {
                $valid = true;
            }
            if (!$valid) {
                $errorsFound = true;
            }
            //
            $configuration['fields'][$field]['value'] = $value;
            $configuration['fields'][$field]['valid'] = $valid;
            $configuration['fields'][$field]['invalid'] = !$valid;
        }
        $configuration['valid'] = !$errorsFound;
        $configuration['invalid'] = $errorsFound;
        //
        // Concatenate full name
        $nameParts = [
            $configuration['fields']['firstname']['value'] ?? '',
            $configuration['fields']['lastname']['value'] ?? '',
        ];
        $fullName = trim(implode(' ', $nameParts));
        //
        // Prepare customer mail receiver
        $configuration['email']['customerConfirmation']['to']['email'] = $configuration['fields']['email']['value'];
        $configuration['email']['customerConfirmation']['to']['name'] = $fullName;
        //
        return $configuration;
    }

    /**
     * @param string $fromEmail
     * @param string $fromName
     * @param string $toEmail
     * @param string $toName
     * @param string $subject
     * @param string $template
     * @param array $assign
     * @param string $format
     * @param array $attachments,
     * @param ?string $ccEmail
     * @param ?string $ccName
     * @return bool
     */
    public function sendMail(
        string $fromEmail,
        string $fromName,
        string $toEmail,
        string $toName,
        string $subject,
        string $template,
        array $assign,
        string $format,
        array $attachments = [],
        string $ccEmail = '',
        string $ccName = ''
    ): bool {
        try {
            $from = new Address($fromEmail, $fromName);
            $to = new Address($toEmail, $toName);
            $cc = null;
            if ($ccEmail && $ccName) {
                $cc = new Address($ccEmail, $ccName);
            }
        } catch (Exception $e) {
            $this->logText('Failed to send E-Mail:');
            $this->logText('from: ' . $fromName . '<' . $fromEmail . '>');
            $this->logText('to: ' . $toName . '<' . $toEmail . '>');
            if ($ccEmail !== '' && $ccName !== '') {
                $this->logText('cc: ' . $ccName . '<' . $ccEmail . '>');
            }
            $this->logText('subject: ' . $subject);
            return false;
        }

        //
        // Render email content
        /** @var FluidEmail $mail */
        $mail = GeneralUtility::makeInstance(FluidEmail::class);
        $mail->format($format)
            ->from($from)
            ->to($to)
            ->subject($subject)
            ->setTemplate($template)
            ->assignMultiple($assign);
        //
        //
        if ($cc) {
            $mail->cc($cc);
        }
        //
        $configuration = ExtensionUtility::getExtensionConfiguration('shop');
        if (!empty($configuration)) {
            if (trim($configuration['emailBcc']) !== '') {
                $mail->bcc(new Address($configuration['emailBcc']));
                $this->logText('bcc: ' . $configuration['emailBcc'] . '<' . $configuration['emailBcc'] . '>');
            }
        }
        //
        // Attach invoice PDF
        foreach ($attachments as $attachment) {
            $contentType = $attachment['mimeType'] ?? 'application/pdf';
            $filename = $attachment['fileName'];
            $mail->attach($attachment['file'], $filename, $contentType);
        }
        //
        /** @var Mailer $mailer */
        $mailer = GeneralUtility::makeInstance(Mailer::class);
        try {
            $mailer->send($mail);
            $success = true;
            $this->logText('E-Mail sent:');
            $this->logText('from: ' . $fromName . '<' . $fromEmail . '>');
            $this->logText('to: ' . $toName . '<' . $toEmail . '>');
            $this->logText('subject: ' . $subject);
        } catch (Exception|TransportExceptionInterface $exception) {
            $success = false;
            $this->logText('Failed to send E-Mail:');
            $this->logText('from: ' . $fromName . '<' . $fromEmail . '>');
            $this->logText('to: ' . $toName . '<' . $toEmail . '>');
            $this->logText('subject: ' . $subject);
        }
        return $success;
    }

    /**
     * @param array $configuration
     * @param BasketOrder $basketOrder
     * @param array $settings
     * @return bool
     */
    abstract public function finishOrder(array $configuration, BasketOrder $basketOrder, array $settings): bool;

    /**
     * @param string $text
     */
    protected function logText(string $text): void
    {
        $this->log[] = $text;
    }

    /**
     * @param string $message
     * @param array $data
     */
    protected function log(string $message = '', array $data = []): void
    {
        /** @var LogManager $logManager */
        $logManager = GeneralUtility::makeInstance(LogManager::class);
        $logger = $logManager->getLogger(__CLASS__);
        $logger->log(LogLevel::DEBUG, $message, $data);
    }

    /**
     * @param string $uri
     * @return string
     */
    protected function uriWithoutCHash(string $uri): string
    {
        $uriArray = explode('?', $uri);
        if (count($uriArray) < 2) {
            return $uri;
        }
        $uriBase = $uriArray[0];
        $uriParams = $uriArray[1];
        $uriParamsArray = explode('&', $uriParams);
        $outputParamsArray = [];
        foreach ($uriParamsArray as $uriParam) {
            $uriParamPair = explode('=', $uriParam);
            if ($uriParamPair[0] !== 'cHash') {
                $outputParamsArray[] = implode('=', $uriParamPair);
            }
        }
        $outputUriParams = implode('&', $outputParamsArray);
        return $uriBase . '?' . $outputUriParams;
    }

    /**
     * @param string $url
     * @param array $post
     * @param string $postEncode json => json_encode or http => http_build_query
     * @param array $header
     * @param string $credentials
     * @return false|array
     */
    protected function curl(string $url, array $post, string $postEncode='json', array $header=[], string $credentials='')
    {
        $postFields = '';
        if ($postEncode === 'json') {
            $postFields = json_encode($post);
        }
        if ($postEncode === 'http') {
            $postFields = http_build_query($post);
        }
        if (!is_string($postFields)) {
            return false;
        }
        $ch = curl_init($url);
        if (!is_bool($ch)) {
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            if ($postFields !== '') {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
            }
            curl_setopt($ch, CURLOPT_SSLVERSION, 6);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            if ($credentials !== '') {
                curl_setopt($ch, CURLOPT_USERPWD, $credentials);
            }
            if (isset($GLOBALS['TYPO3_CONF_VARS']['HTTP']['proxy']) && trim($GLOBALS['TYPO3_CONF_VARS']['HTTP']['proxy']) !== '') {
                curl_setopt($ch, CURLOPT_PROXY, $GLOBALS['TYPO3_CONF_VARS']['HTTP']['proxy']);
            }
            $result = curl_exec($ch);
            if (is_string($result)) {
                if ($result === '') {
                    return false;
                }
                return json_decode($result, true);
            }
        }
        $this->log(
            'CheckoutService::curl: curl_init failed',
            ['url' => $url]
        );
        return false;
    }
    /**
     * @return string
     */
    public function getCustomerConfirmationEmailTemplate(): string
    {
        return static::CUSTOMER_CONFIRMATION_EMAIL_TEMPLATE;
    }

    /**
     * @return string
     */
    public function getOrderEmailTemplate(): string
    {
        return static::ORDER_EMAIL_TEMPLATE;
    }
}
