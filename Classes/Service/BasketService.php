<?php

declare(strict_types=1);

namespace CodingMs\Shop\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Basket;
use CodingMs\Shop\Domain\Model\BasketItem;
use CodingMs\Shop\Domain\Model\Product;
use CodingMs\Shop\Domain\Model\ProductDiscount;
use CodingMs\Shop\Domain\Model\ProductShippingCost;
use CodingMs\Shop\Domain\Repository\ProductDiscountRepository;
use CodingMs\Shop\Domain\Repository\ProductRepository;
use CodingMs\Shop\Domain\Repository\ProductShippingCostRepository;
use Exception;
use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Services on basket
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class BasketService
{
    protected int $price = 0;
    protected int $tax = 0;

    /**
     * @var array
     */
    protected array $taxes = [];
    protected int $priceWithTax = 0;
    protected int $finalPrice = 0;
    protected int $productQuantity = 0;
    protected ProductRepository $productRepository;
    protected ProductShippingCostRepository $productShippingCostRepository;
    protected ProductDiscountRepository $productDiscountRepository;

    public function __construct(
        ProductRepository $productRepository,
        ProductShippingCostRepository $productShippingCostRepository,
        ProductDiscountRepository $productDiscountRepository
    ) {
        $this->productRepository = $productRepository;
        $this->productShippingCostRepository = $productShippingCostRepository;
        $this->productDiscountRepository = $productDiscountRepository;
    }

    public function reset(): void
    {
        $this->price = 0;
        $this->tax = 0;
        $this->taxes = [];
        $this->priceWithTax = 0;
        $this->finalPrice = 0;
        $this->productQuantity = 0;
    }

    /**
     * Converts the session array into an object
     *
     * @param array $basketArray Session array
     * @param array $settings Settings
     * @param string $checkoutType
     * @param bool $vatable
     * @param string|null $countryCode
     * @return Basket
     */
    public function getBasketObject(array $basketArray, array $settings, string $checkoutType = '', bool $vatable = true, ?string $countryCode = null): Basket
    {
        try {
            // Reset the price and quantities
            $this->reset();
            //
            // Create a basket object
            /** @var Basket $basket */
            $basket = GeneralUtility::makeInstance(Basket::class);
            $basket->setPid((int)$settings['storagePid']);
            $basket->setType($settings['basket']['displayType']);
            $basket->setMinimumOrderValue((int)$settings['basketOrder']['minimumOrderValue']);
            //
            $discountsByType = $this->productDiscountRepository->findByOrderOption($checkoutType);
            $discountsForAll = $this->productDiscountRepository->findByOrderOption('all');
            //
            // Process basket items
            if (!empty($basketArray)) {
                //
                // For each product in basket
                foreach ($basketArray as $productId => $productValues) {
                    // Validate the quantity

                    if (is_int($productValues)) {
                        // "is_int"-condition is for backward compatibility for old basket sessions
                        $productValues = [
                            'quantity' => $productValues,
                            'customInformation' => '',
                        ];
                    }
                    $quantity = (int)$productValues['quantity'];
                    if ($quantity > 0) {
                        //
                        // Find the product in storage
                        $product = $this->productRepository->findByUid($productId);
                        if ($product instanceof Product) {
                            $product->fillFieldsFromParent();
                            //
                            // Create a new basket item
                            /** @var BasketItem $basketItem */
                            $basketItem = GeneralUtility::makeInstance(BasketItem::class);
                            $basketItem->setPid((int)$settings['storagePid']);
                            $basketItem->setProduct($product);
                            $basketItem->setQuantity($quantity);
                            $basketItem->setCustomInformation($productValues['customInformation']);
                            $basketItem->setCustomInformationType($product->getProductType()->getCustomInformationType());
                            $basketItem->setCustomInformationLabel($product->getProductType()->getCustomInformationLabel());
                            $basketItem->setCustomInformationRequired($product->getProductType()->getCustomInformationRequired());
                            $basketItem->setPrice($product->getPrice());
                            $basketItem->setPriceWithTax($product->getPriceWithTaxForCountry($countryCode));
                            $basketItem->setTax($product->getTaxForCountry($countryCode));
                            //
                            // Is graduated price?
                            if ($quantity > 1) {
                                $graduatedPrice = $product->getGraduatedPrice($quantity, $countryCode);
                                if ($graduatedPrice['price'] !== $product->getPrice()) {
                                    //
                                    // If yes, update the price in BasketItem
                                    $basketItem->setIsGraduatedPrice(true);
                                    $basketItem->setPrice($graduatedPrice['price']);
                                    $basketItem->setPriceWithTax($graduatedPrice['priceWithTax']);
                                    $basketItem->setTax($graduatedPrice['tax']);
                                }
                            }
                            //
                            // Process the discount on product
                            $discount = $product->getDiscount();
                            if ($discount instanceof ProductDiscount) {
                                if (!$basketItem->getIsDiscounted()) {
                                    $basketItem->setIsDiscounted(true);
                                    $basketItem->setDiscountType($discount->getType());
                                    $basketItem->setDiscountLabel($discount->getLabelPublic());
                                    $basketItem->setDiscountValue($discount->getValue());
                                    if ($discount->getType() === 'percent') {
                                        $subtract = intval(($basketItem->getPrice() / 100) * ($discount->getValuePercent() / 100));
                                        $basketItem->setDiscount($subtract);
                                    } else {
                                        $basketItem->setDiscount($discount->getValueFixed());
                                    }
                                }
                            } else {
                                //
                                // Process the discount for selected order option
                                if (count($discountsByType) > 0) {
                                    foreach ($discountsByType as $discount) {
                                        if (!$basketItem->getIsDiscounted()) {
                                            $basketItem->setIsDiscounted(true);
                                            $basketItem->setDiscountType($discount->getType());
                                            $basketItem->setDiscountLabel($discount->getLabelPublic());
                                            $basketItem->setDiscountValue($discount->getValue());
                                            if ($discount->getType() === 'percent') {
                                                $subtract = intval(($basketItem->getPrice() / 100) * ($discount->getValuePercent() / 100));
                                                $basketItem->setDiscount($subtract);
                                            } else {
                                                $basketItem->setDiscount($discount->getValueFixed());
                                            }
                                        }
                                    }
                                }
                                //
                                // Process the discount for all order options
                                if (count($discountsForAll) > 0) {
                                    foreach ($discountsForAll as $discount) {
                                        $basketItem->setIsDiscounted(true);
                                        $basketItem->setDiscountType($discount->getType());
                                        $basketItem->setDiscountLabel($discount->getLabelPublic());
                                        $basketItem->setDiscountValue($discount->getValue());
                                        if ($discount->getType() === 'percent') {
                                            $subtract = intval(($basketItem->getPrice() / 100) * ($discount->getValuePercent() / 100));
                                            $basketItem->setDiscount($subtract);
                                        } else {
                                            $basketItem->setDiscount($discount->getValueFixed());
                                        }
                                    }
                                }
                            }
                            //
                            //
                            // Isn't vatable?
                            if (!$vatable) {
                                // Reset price/tax
                                $basketItem->setPriceWithTax($basketItem->getPrice());
                                $basketItem->setTax(0);
                            }
                            $basket->addBasketItem($basketItem);
                            //
                            // Calculate the current amount
                            $this->price += $basketItem->getTotalPrice();
                            //
                            // Taxes summarized by tax percent
                            $taxValue = $basketItem->getTotalPriceWithTax() - $basketItem->getTotalPrice();
                            $taxPercent = $basketItem->getProduct()->getTaxForCountry($countryCode);
                            if (!isset($this->taxes[$taxPercent])) {
                                $this->taxes[$taxPercent] = [
                                    'percent' => $taxPercent,
                                    'percentAsFloat' => round($taxPercent / 100, 2),
                                    'value' => $taxValue,
                                    'valueAsFloat' => round($taxValue / 100, 2),
                                ];
                            } else {
                                $this->taxes[$taxPercent]['value'] += $taxValue;
                                $this->taxes[$taxPercent]['valueAsFloat'] = round($this->taxes[$taxPercent]['value'] / 100, 2);
                            }
                            // Complete tax summary
                            $this->tax += $taxValue;
                            //
                            $this->priceWithTax += $basketItem->getTotalPriceWithTax();
                            //
                            // Increment product quantity
                            $this->productQuantity++;
                        }
                    }
                    //
                }
                //
                // Check order options and additional costs
                //
                // 1. Find the closest shipping costs by weight
                $productShippingCost = $this->productShippingCostRepository->findClosestByWeight(
                    $basket->getWeight()
                );
                //
                // 2. Add shipping costs
                if ($productShippingCost instanceof ProductShippingCost) {
                    $basket->setAdditionalPrice(
                        [
                          'type' => 'fixed',
                          'value' => $productShippingCost->getPrice($countryCode),
                          'label' => $productShippingCost->getLabelPublic(),
                          'subtract' => false,
                      ]
                    );
                    //
                    // 3. check if it's bulky - this will be displayed in a separated row
                    if ($basket->getBulky()) {
                        $basket->setAdditionalPrice(
                            [
                                'type' => 'fixed',
                                'value' => $productShippingCost->getBulky($countryCode),
                                'label' => $productShippingCost->getLabelBulkyPublic(),
                                'subtract' => false,
                            ]
                        );
                    }
                }
                //
                // Additional costs calculation in basket values
                $basket = $this->calculateAdditionalPrices($basket, $vatable);
                $basket->setPrice($this->price);
                if ($vatable) {
                    $basket->setTax($this->tax);
                    $basket->setTaxes($this->taxes);
                    $basket->setPriceWithTax($this->priceWithTax);
                    $basket->setFinalPrice($this->finalPrice);
                } else {
                    // Use net price
                    $basket->setTax(0);
                    $basket->setTaxes($this->taxes);
                    $basket->setPriceWithTax($this->price);
                    $basket->setFinalPrice($this->finalPrice);
                }
            }
        } catch (Exception $exception) {
            $from = new Address(
                $settings['email']['from']['email'],
                $settings['email']['from']['name']
            );
            $to = new Address(
                $settings['email']['to']['email'],
                $settings['email']['to']['name']
            );
            /** @var FluidEmail $mail */
            $mail = GeneralUtility::makeInstance(FluidEmail::class);
            $mail->format(FluidEmail::FORMAT_PLAIN)
                ->from($from)
                ->to($to)
                ->subject($settings['siteName'] . ': Error while getting basket object ')
                ->setTemplate('Error')
                ->assign('exception', $exception);
            /** @var Mailer $mailer */
            $mailer = GeneralUtility::makeInstance(Mailer::class);
            $mailer->send($mail);
            die();
        }
        return $basket;
    }

    /**
     * Working on gross price!
     *
     * @param Basket $basket
     * @param bool $vatable
     * @return Basket
     */
    protected function calculateAdditionalPrices(Basket $basket, bool $vatable = true): Basket
    {
        $this->finalPrice = $this->priceWithTax;
        if (!$vatable) {
            // Use net price
            $this->finalPrice = $this->price;
        }
        $processAdditionalPrices = $basket->getAdditionalPrices();
        foreach ($processAdditionalPrices as $additionPriceKey => $additionPrice) {
            if ($additionPrice['type'] === 'percent') {
                //
                // Get percentage value
                $onePercent = $this->priceWithTax / 100;
                $additionalPricePercent = round($additionPrice['value'] * $onePercent / 100);
                $processAdditionalPrices[$additionPriceKey]['onePercent'] = $onePercent;
                //
                $processAdditionalPrices[$additionPriceKey]['price'] = $additionalPricePercent;
                $processAdditionalPrices[$additionPriceKey]['priceAsFloat'] = $additionalPricePercent / 100;
                // needs to be subtracted?
                if ($additionPrice['subtract']) {
                    $this->finalPrice = (int)round($this->finalPrice - $additionalPricePercent);
                } else {
                    $this->finalPrice = (int)round($this->finalPrice + $additionalPricePercent);
                }
            } else {
                $processAdditionalPrices[$additionPriceKey]['price'] = (int)$additionPrice['value'];
                $processAdditionalPrices[$additionPriceKey]['priceAsFloat'] = (int)$additionPrice['value'] / 100;
                // needs to be subtracted?
                if ($additionPrice['subtract']) {
                    $this->finalPrice = (int)round($this->finalPrice - $processAdditionalPrices[$additionPriceKey]['price']);
                } else {
                    $this->finalPrice = (int)round($this->finalPrice + $processAdditionalPrices[$additionPriceKey]['price']);
                }
            }
        }
        $basket->setAdditionalPrices($processAdditionalPrices);
        return $basket;
    }
}
