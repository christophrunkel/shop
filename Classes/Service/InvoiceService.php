<?php

namespace CodingMs\Shop\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\Result;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Invoice service
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class InvoiceService
{
    public function getNextInvoiceNumber(int $storagePid): int
    {
        // If there are no basket orders yet, the next number must be 1
        $invoiceNumber = 1;
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $connection = $connectionPool->getConnectionForTable('tx_shop_domain_model_product');
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder->resetRestrictions();
        $queryBuilder->select('invoice_number')
            ->from('tx_shop_domain_model_basketorder')
            ->where('pid=' . $storagePid)
            ->addOrderBy('invoice_number', 'DESC')
            ->setMaxResults(1);
        $result = $queryBuilder->execute();
        if ($result instanceof Result) {
            $row = $result->fetchFirstColumn();
            if (isset($row[0])) {
                $invoiceNumber = (int)$row[0] + 1;
            }
        }
        return $invoiceNumber;
    }
}
