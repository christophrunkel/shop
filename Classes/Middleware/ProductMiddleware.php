<?php

namespace CodingMs\Shop\Middleware;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2022 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Service\ViewCountService;
use Doctrine\DBAL\Result;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LogLevel;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ProductMiddleware
 */
class ProductMiddleware implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (isset($GLOBALS['BE_USER'])) {
            return $handler->handle($request);
        }
        $queryParams = $request->getQueryParams();
        if (isset($queryParams['tx_shop_products'])) {
            $action = $queryParams['tx_shop_products']['action'] ?? 'list';
            $controller = $queryParams['tx_shop_products']['controller'] ?? 'Product';
            $product = intval($queryParams['tx_shop_products']['product'] ?? 0);
            $routing = $request->getAttribute('routing');
            if ($routing instanceof PageArguments) {
                $pid = $routing->getPageId();
            } else {
                $pid = 0;
            }
            //
            if ($action === 'show' && $controller === 'Product' && $product > 0 && $pid > 0) {
                /** @var ConnectionPool $connectionPool */
                $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
                $ttContentQueryBuilder = $connectionPool->getConnectionForTable('tt_content')->createQueryBuilder();
                $pluginCount = 0;
                $result = $ttContentQueryBuilder
                    ->count('uid')
                    ->from('tt_content')
                    ->where($ttContentQueryBuilder->expr()->eq(
                        'pid',
                        $ttContentQueryBuilder->createNamedParameter($pid)
                    ))
                    ->andWhere($ttContentQueryBuilder->expr()->eq(
                        'CType',
                        $ttContentQueryBuilder->createNamedParameter('list')
                    ))
                    ->andWhere($ttContentQueryBuilder->expr()->eq(
                        'list_type',
                        $ttContentQueryBuilder->createNamedParameter('shop_products')
                    ))
                    ->execute();
                if ($result instanceof Result) {
                    $pluginCount = (int)$result->fetchOne();
                }
                if ($pluginCount > 0) {
                    //
                    // Check if the product is a variant!?
                    // In that case, count on parent product!
                    $productQueryBuilder = $connectionPool->getConnectionForTable('tx_shop_domain_model_product')
                        ->createQueryBuilder();
                    $resultProduct = $productQueryBuilder
                        ->select('uid', 'parent')
                        ->from('tx_shop_domain_model_product')
                        ->where($productQueryBuilder->expr()->eq(
                            'uid',
                            $productQueryBuilder->createNamedParameter($product)
                        ))
                        ->execute();
                    if ($resultProduct instanceof Result) {
                        $productArray = $resultProduct->fetchAssociative();
                        if ($productArray['parent'] > 0) {
                            $product = $productArray['parent'];
                        }
                    }
                    //
                    // Count view data
                    $shopCounterQueryBuilder = $connectionPool->getConnectionForTable('tx_shop_domain_model_productclickcount')->createQueryBuilder();
                    $shopCounterQueryBuilder
                        ->insert('tx_shop_domain_model_productclickcount')
                        ->values([
                            'product' => $product,
                            'click_timestamp' => time(),
                        ])
                        ->execute();
                    ViewCountService::updateViewerCountForProduct($product);
                }
            }
        }
        return $handler->handle($request);
    }

    /**
     * @param string $message
     * @param array $data
     */
    protected function log(string $message='', array $data=[]): void
    {
        /** @var LogManager $logManager */
        $logManager = GeneralUtility::makeInstance(LogManager::class);
        $logger = $logManager->getLogger(__CLASS__);
        $logger->log(LogLevel::DEBUG, $message, $data);
    }
}
