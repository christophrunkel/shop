<?php

namespace CodingMs\Shop\Form\Container;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Service\TypoScriptService;
use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class TaxInformation extends AbstractFormElement
{
    /**
     * @return array
     * @throws SiteNotFoundException
     */
    public function render(): array
    {
        $prefix = 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_basketorder.';
        // Get configured display type for current page
        /** @var ?Site $site */
        $site = $this->data['site'];
        if ($site instanceof Site) {
            $pageUid = $this->data['databaseRow']['pid'];
            $rootline = $this->data['rootline'];
            $configuration = TypoScriptService::getTypoScript($pageUid, 0, $rootline, $site);
            $vatConfiguration = $configuration['plugin']['tx_shop']['settings']['basketOrder']['vat'];
            $vatId = trim($this->data['databaseRow']['vat_id']);
            $vatType = ($vatId !== '') ? 'b2b' : 'b2c';
            $hasVatId = ($vatId !== '');
            $html = [];
            $lines = [];
            /**
             * @todo translate
             */
            $html[] = '<div style="margin: -10px 0 0;" class="alert alert-info shop-basket-order-tax-information">';
            // VAT_ID
            if ($hasVatId) {
                $lines[] = 'The basket order contains a VAT-ID. This indicates that the buyer is a company in european union or inland. ';
            } else {
                $lines[] = 'The basket order doesn\'t contain a VAT-ID. This indicates that the buyer is a private person or order from a third country. ';
            }
            // VAT zone information
            $vatZone = $this->data['databaseRow']['vat_zone'][0] ?? '';
            if ($vatZone === 'inland') {
                $lines[] = 'You\'re selling within inland. ';
            } elseif ($vatZone === 'europeanUnion') {
                $lines[] = 'You\'re selling within european union. ';
            } else {
                $lines[] = 'You\'re selling to a third country. ';
            }
            // Use VAT!?
            if ($vatZone === 'inland' || !$hasVatId && $vatZone === 'europeanUnion') {
                $lines[] = 'VAT is in use in this basket order! ';
                $vatable = true;
            } else {
                $lines[] = 'VAT is not in use in this basket order! ';
                $vatable = false;
            }
            if ($vatZone !== '' && trim($vatConfiguration[$vatType][$vatZone]['notice']) !== '') {
                $vatNotice = sprintf($vatConfiguration[$vatType][$vatZone]['notice'], $vatId);
                $lines[] = '<b>Invoice notice:</b><br /><i>' . $vatNotice . '</i>';
            }
            $html[] = '<ul><li>' . implode('</li><li>', $lines) . '</li></ul>';
            $html[] = '</div>';
            //
            // Configuration check!
            if ((bool)$this->data['databaseRow']['vatable'] !== $vatable) {
                $html[] = '<div style="margin: 10px 0 0;" class="alert alert-danger">';
                $html[] = 'Potential misconfiguration! Vatable checkbox doesn\'t met your VAT-ID and VAT zone!';
                $html[] = '</div>';
            }
            //
            $result = $this->initializeResultArray();
        } else {
            $html = [];
            $html[] = '<div class="alert alert-danger">';
            $html[] = LocalizationUtility::translate($prefix . 'tax_information_outside_of_webroot', 'Shop');
            $html[] = '</div>';
        }
        $result['html'] = implode(PHP_EOL, $html);
        return $result;
    }
}
