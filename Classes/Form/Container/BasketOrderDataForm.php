<?php

declare(strict_types=1);

namespace CodingMs\Shop\Form\Container;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Service\TypoScriptService;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Result;
use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class BasketOrderDataForm
 */
class BasketOrderDataForm extends AbstractFormElement
{
    /**
     * @return array
     * @throws SiteNotFoundException
     */
    public function render()
    {
        // Get configured display type for current page
        /** @var Site $site */
        $site = $this->data['site'];
        $pageUid = $this->data['databaseRow']['pid'];
        $rootline = $this->data['rootline'];
        $configuration = TypoScriptService::getTypoScript($pageUid, 0, $rootline, $site);
        $fields = GeneralUtility::trimExplode(',', $configuration['plugin']['tx_shop']['settings']['basketOrder']['orderOptions']['manual']['fields']['available']);
        $fieldDefinitions = $configuration['plugin']['tx_shop']['settings']['basketOrder']['fieldDefinition'];

        $usedFieldDefinitions = ['fields' => []];
        if ($this->data['command'] === 'edit') {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_shop_domain_model_basketorder');
            $queryBuilder
                ->select('params')
                ->from('tx_shop_domain_model_basketorder')
                ->where($queryBuilder->expr()->eq('uid', $this->data['vanillaUid']));
            $paramsJson = '';
            $result = $queryBuilder->execute();
            if ($result instanceof Result) {
                $paramsJson = $result->fetchOne();
            }
            if (isset($paramsJson) && $paramsJson !== null && $paramsJson !== '') {
                $usedFieldDefinitions = json_decode($paramsJson);
            } else {
                foreach ($fields as $field) {
                    $usedFieldDefinitions['fields'][$field] = $fieldDefinitions[$field];
                    $usedFieldDefinitions['fields'][$field]['required'] = false;
                    $usedFieldDefinitions['fields'][$field]['valid'] = true;
                    $usedFieldDefinitions['fields'][$field]['invalid'] = false;
                }
            }
        } else {
            foreach ($fields as $field) {
                $usedFieldDefinitions['fields'][$field] = $fieldDefinitions[$field];
                $usedFieldDefinitions['fields'][$field]['required'] = false;
                $usedFieldDefinitions['fields'][$field]['valid'] = true;
                $usedFieldDefinitions['fields'][$field]['invalid'] = false;
            }
        }

        $mainFieldHtml = [];
        $mainFieldHtml[] = '<div style="display:none;" id="field-definition-json-object">';
        $mainFieldHtml[] = strip_tags((string)json_encode($usedFieldDefinitions));
        $mainFieldHtml[] = '</div>';
        $mainFieldHtml[] = '<div id="params-form-wrap" class="form-control-wrap">';

        $mainFieldHtml[] = '<input type="hidden" name="data' . $this->data['elementBaseName'] . '">';

        foreach ($fields as $field) {
            $type = $fieldDefinitions[$field]['type'];
            if ($type === 'input') {
                $mainFieldHtml[] = $this->createInputField(
                    $fieldDefinitions[$field]['label'],
                    $field,
                    isset($fieldDefinitions[$field]['deliveryAddress']) && boolval($fieldDefinitions[$field]['deliveryAddress'])
                );
            } elseif ($type === 'textarea') {
                $mainFieldHtml[] = $this->createInputTextarea(
                    $fieldDefinitions[$field]['label'],
                    $field,
                    isset($fieldDefinitions[$field]['deliveryAddress']) && boolval($fieldDefinitions[$field]['deliveryAddress'])
                );
            } elseif ($type === 'checkbox') {
                $mainFieldHtml[] = $this->createCheckbox(
                    $fieldDefinitions[$field]['label'],
                    $field,
                    isset($fieldDefinitions[$field]['deliveryAddress']) && boolval($fieldDefinitions[$field]['deliveryAddress']),
                    isset($fieldDefinitions[$field]['deliveryAddressToggle']) && boolval($fieldDefinitions[$field]['deliveryAddressToggle'])
                );
            }
        }

        $mainFieldHtml[] = '</div>';

        $result = $this->initializeResultArray();
        $result['requireJsModules'] = ['TYPO3/CMS/Shop/Backend/BasketOrderParamsSerializer'];
        $result['html'] = implode(PHP_EOL, $mainFieldHtml);
        return $result;
    }

    /**
     * @param string $fieldLabel
     * @param string $fieldName
     * @param bool $isDeliveryAddressField
     * @return string
     */
    private function createInputField(string $fieldLabel, string $fieldName, bool $isDeliveryAddressField): string
    {
        $html = [];
        $html[] = '<div data-delivery="' . $isDeliveryAddressField . '">';
        $html[] = '<div style="margin-top:10px;">' . strip_tags($fieldLabel) . '</div>';
        $html[] = '<input class="form-control" type="text" id="' . $fieldName . '" data-fieldname="' . $fieldName . '"></input>';
        $html[] = '</div>';
        return implode(PHP_EOL, $html);
    }

    /**
     * @param string $fieldLabel
     * @param string $fieldName
     * @param bool $isDeliveryAddressField
     * @return string
     */
    private function createInputTextarea(string $fieldLabel, string $fieldName, bool $isDeliveryAddressField): string
    {
        $html = [];
        $html[] = '<div data-delivery="' . $isDeliveryAddressField . '">';
        $html[] = '<div style="margin-top:10px;" >' . strip_tags($fieldLabel) . '</div>';
        $html[] = '<textarea class="form-control  t3js-formengine-textarea formengine-textarea" id="' . $fieldName . '" data-fieldname="' . $fieldName . '"></textarea>';
        $html[] = '</div>';
        return implode(PHP_EOL, $html);
    }

    /**
     * @param string $fieldLabel
     * @param string $fieldName
     * @param bool $isDeliveryAddressField
     * @param bool $isDeliveryAddressToggle
     * @return string
     */
    private function createCheckbox(string $fieldLabel, string $fieldName, bool $isDeliveryAddressField, bool $isDeliveryAddressToggle): string
    {
        $html = [];
        $html[] = '<div data-delivery="' . $isDeliveryAddressField . '">';
        $html[] = '<div style="margin-top:10px;">';
        $html[] = '<div><input class="" type="checkbox" id="' . $fieldName . '" data-fieldname="' . $fieldName . '" data-delivery-toggle="' . $isDeliveryAddressToggle . '"></input>&nbsp;&nbsp;&nbsp;&nbsp;' . strip_tags($fieldLabel) . '</div>';
        $html[] = '</div>';
        $html[] = '</div>';
        return implode(PHP_EOL, $html);
    }
}
