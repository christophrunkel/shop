<?php

declare(strict_types=1);

namespace CodingMs\Shop\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Generate label for TCA records
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class LabelService
{
    protected LanguageService $lang;
    protected static array $productRecords = [];

    public function __construct()
    {
        if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
            $this->lang = GeneralUtility::makeInstance(LanguageService::class);
            $this->lang->init($GLOBALS['BE_USER']->uc['lang']);
        } else {
            $this->lang = GeneralUtility::makeInstance(LanguageServiceFactory::class)
                ->createFromUserPreferences($GLOBALS['BE_USER']);
        }
    }

    /**
     * @param array $params
     * @param array $pObj
     * @return array
     */
    public function getProductGraduatedPriceLabel(&$params, &$pObj): array
    {
        $arguments = [
            0 => $params['row']['quantity'],
            1 => number_format($params['row']['price'] / 100, 2, ',', '.') . ' €',
        ];
        $params['title'] = $this->translate('tx_shop_domain_model_productgraduatedprice.custom_title', $arguments);
        return $params;
    }

    protected function getProductRecord(int $uid): array
    {
        if (!isset(self::$productRecords[$uid])) {
            self::$productRecords[$uid] = BackendUtility::getRecord('tx_shop_domain_model_product', $uid);
        }
        return self::$productRecords[$uid];
    }

    /**
     * @param array $params
     * @param array $pObj
     * @return array
     */
    public function getProductLabel(&$params, &$pObj): array
    {
        $params['title'] = $params['row']['title'];
        if (isset($params['row']['parent']) && (int)$params['row']['parent'] > 0) {
            if (!is_int($params['row']['parent'])) {
                $parentUid = $params['row']['parent'][0]['uid'];
            } else {
                $parentUid = $params['row']['parent'];
            }
            $parent = $this->getProductRecord($parentUid);
            if (trim($parent['title']) === '') {
                $title = '[!product:' . $params['row']['parent'] . ' without title!]';
            } else {
                $title = $parent['title'];
            }
            $params['title'] = $title . ', Variant: ' . $params['title'] . ' (' . $params['title'] . ')';
        }
        return $params;
    }

    /**
     * @param array $params
     * @param array $pObj
     * @return array
     */
    public function getProductTaxLabel(&$params, &$pObj): array
    {
        if (!isset($params['row']['value']) || !is_string($params['row']['value'])) {
            $params['title'] = number_format($params['row']['value'] / 100, 2, ',') . ' %';
        }
        return $params;
    }

    /**
     * @param array $params
     * @param array $pObj
     * @return array
     */
    public function getProductShippingCostLabel(&$params, &$pObj): array
    {
        //
        // First of all:
        // Check if all necessary values are available!
        if (!isset($params['row']['order_option']) || !is_string($params['row']['order_option'])) {
            $params['title'] = $params['row']['label'];
        } else {
            $price = number_format($params['row']['price'] / 100, 2, ',', '.') . ' €';
            $weight = number_format($params['row']['weight'] / 100, 2, ',', '.') . ' kg';
            $bulky = number_format($params['row']['bulky'] / 100, 2, ',', '.') . ' €';
            $arguments = [
                0 => $params['row']['label'],
                1 => $price,
                2 => $weight,
                3 => $bulky,
            ];
            $params['title'] = $this->translate('tx_shop_domain_model_productshippingcost.custom_title', $arguments);
        }
        return $params;
    }

    /**
     * @param array $params
     * @param array $pObj
     * @return array
     */
    public function getProductDiscountLabel(&$params, &$pObj): array
    {
        //
        // First of all:
        // Check if all necessary values are available!
        if (!isset($params['row']['order_option']) || !is_string($params['row']['order_option'])) {
            $params['title'] = $params['row']['label'];
        } else {
            if ($params['row']['type'] === 'percent') {
                $value = number_format($params['row']['value_percent'] / 100, 2, ',') . ' %';
            } else {
                $value = number_format($params['row']['value_fixed'] / 100, 2, ',', '.') . ' €';
            }
            $orderOption = GeneralUtility::camelCaseToLowerCaseUnderscored($params['row']['order_option']);
            $orderOptionTranslationKey = 'tx_shop_domain_model_productdiscount.order_option_' . $orderOption;
            $arguments = [
                0 => $params['row']['label'],
                1 => $value,
                2 => $this->translate($orderOptionTranslationKey),
            ];
            $params['title'] = $this->translate('tx_shop_domain_model_productdiscount.custom_title', $arguments);
        }
        return $params;
    }

    /**
     * @param string $key
     * @param array $arguments
     * @return string
     */
    protected function translate(string $key, array $arguments = []): string
    {
        $lllFile = 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:';
        $translation = $this->lang->sL($lllFile . $key);
        if (count($arguments) > 0) {
            $translation = vsprintf($translation, $arguments);
        }
        return $translation;
    }
}
