<?php

declare(strict_types=1);

namespace CodingMs\Shop\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Tca\Configuration as ConfigurationDefaults;
use CodingMs\Shop\Domain\Model\ProductType;
use CodingMs\Shop\Utility\ExtensionUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Configuration presets for TCA fields.
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * @version 1.2.0
 */
class Configuration extends ConfigurationDefaults
{

    protected static array $extensionConfiguration = [];

    /**
     * @param string $type
     * @param bool $required
     * @param bool $readonly
     * @param string $label
     * @param array $options
     * @return array
     */
    public static function get($type, $required = false, $readonly = false, $label = '', array $options = []): array
    {
        $extKey = 'shop';
        $table = 'tx_shop_domain_model_producttype';
        $lllProductType = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;
        //
        if (count(self::$extensionConfiguration) === 0) {
            self::$extensionConfiguration = ExtensionUtility::getExtensionConfiguration($extKey);
        }
        //
        switch ($type) {
            case 'slug':
                $config = [
                    'type' => 'slug',
                    'size' => 50,
                    'prependSlash' => true,
                    'generatorOptions' => [
                        'fields' => ['title', 'product_no'],
                        'prefixParentPageSlug' => true,
                        'replacements' => [
                            '/' => '-'
                        ],
                    ],
                    'fallbackCharacter' => '-',
                    'eval' => 'lower,unique',
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'attributeType':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'items' => [
                        ['label' => $lllProductType . '.checkbox', 'value' => 'checkbox'],
                        ['label' => $lllProductType . '.int', 'value' => 'int'],
                        ['label' => $lllProductType . '.int_required', 'value' => 'int_required'],
                        ['label' => $lllProductType . '.string', 'value' => 'string'],
                        ['label' => $lllProductType . '.string_required', 'value' => 'string_required'],
                        ['label' => $lllProductType . '.textarea', 'value' => 'textarea'],
                        ['label' => $lllProductType . '.textarea_required', 'value' => 'textarea_required'],
                        ['label' => $lllProductType . '.rte', 'value' => 'rte'],
                        ['label' => $lllProductType . '.rte_required', 'value' => 'rte_required'],
                    ],
                    'default' => 'checkbox',
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        [$lllProductType . '.checkbox', 'checkbox'],
                        [$lllProductType . '.int', 'int'],
                        [$lllProductType . '.int_required', 'int_required'],
                        [$lllProductType . '.string', 'string'],
                        [$lllProductType . '.string_required', 'string_required'],
                        [$lllProductType . '.textarea', 'textarea'],
                        [$lllProductType . '.textarea_required', 'textarea_required'],
                        [$lllProductType . '.rte', 'rte'],
                        [$lllProductType . '.rte_required', 'rte_required'],
                    ];
                }
                break;
            case 'attributeCategoryTemplate':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'items' => [
                        ['label' => $lllProductType . '.display_as_list', 'value' => 'List'],
                        ['label' => $lllProductType . '.display_as_link_list', 'value' => 'Link']
                    ],
                    'default' => 'List',
                    'eval' => 'trim',
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        [$lllProductType . '.display_as_list', 'List'],
                        [$lllProductType . '.display_as_link_list', 'Link']
                    ];
                }
                $config = self::insertRequired($config, $required);
                break;
            case 'tagCategorySingle':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'tx_shop_domain_model_producttagcategory',
                    'foreign_table_where' => 'AND tx_shop_domain_model_producttagcategory.pid=###CURRENT_PID### ORDER BY title',
                    // Insert empty value
                    'items' => [
                        ['label' => 'none', 'value' => 0],
                    ],
                    'size' => 1,
                    'maxitems' => 1,
                    'minitems' => 0,
                    'wizards' => [
                        'suggest' => [
                            'type' => 'suggest',
                        ],
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        ['none', 0],
                    ];
                }
                break;
            case 'discountSingle':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'tx_shop_domain_model_productdiscount',
                    'foreign_table_where' => 'AND tx_shop_domain_model_productdiscount.pid=###CURRENT_PID### AND tx_shop_domain_model_productdiscount.order_option=\'product\' ORDER BY label',
                    // Insert empty value
                    'items' => [
                        ['label' => 'none', 'value' => 0],
                    ],
                    'size' => 1,
                    'maxitems' => 1,
                    'minitems' => 0,
                    'wizards' => [
                        'suggest' => [
                            'type' => 'suggest',
                        ],
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                    'default' => 0
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        ['none', 0],
                    ];
                }
                break;
            case 'taxSingle':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'tx_shop_domain_model_producttax',
                    'foreign_table_where' => 'AND tx_shop_domain_model_producttax.pid=###CURRENT_PID###',
                    // Insert empty value
                    'items' => [],
                    'size' => 1,
                    'required' => true,
                    'maxitems' => 1,
                    'minitems' => 0,
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'tagVariant':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'default' => 'default',
                    'items' => [
                        ['label' => 'default', 'value' => 'default'],
                        ['label' => 'primary', 'value' => 'primary'],
                        ['label' => 'secondary', 'value' => 'secondary'],
                        ['label' => 'success', 'value' => 'success'],
                        ['label' => 'info', 'value' => 'info'],
                        ['label' => 'warning', 'value' => 'warning'],
                        ['label' => 'danger', 'value' => 'danger'],
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        ['default', 'default'],
                        ['primary', 'primary'],
                        ['secondary', 'secondary'],
                        ['success', 'success'],
                        ['info', 'info'],
                        ['warning', 'warning'],
                        ['danger', 'danger'],
                    ];
                }
                break;
            case 'productTypeFilter':
                $lll = 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:' . ProductType::TABLE;
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'default' => 'Title',
                    'items' => [
                        ['label' => $lll . '.filter_title', 'value' => 'Title'],
                        ['label' => $lll . '.filter_size', 'value' => 'Size'],
                        ['label' => $lll . '.filter_color', 'value' => 'Color'],
                        ['label' => $lll . '.filter_color_size', 'value' => 'ColorSize'],
                        ['label' => $lll . '.filter_color_size_matrix', 'value' => 'ColorSizeMatrix'],
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        [$lll . '.filter_title', 'Title'],
                        [$lll . '.filter_size', 'Size'],
                        [$lll . '.filter_color', 'Color'],
                        [$lll . '.filter_color_size', 'ColorSize'],
                        [$lll . '.filter_color_size_matrix', 'ColorSizeMatrix'],
                    ];
                }
                break;
            case 'startOrEndTime':
                $config = [
                    'type' => 'input',
                    'size' => 13,
                    'max' => 20,
                    'eval' => 'datetime',
                    'checkbox' => 0,
                    'default' => 0,
                    'range' => [
                        'lower' => mktime(0, 0, 0, (int)date('m'), (int)date('d'), (int)date('Y'))
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                break;
            case 'productSingle':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'tx_shop_domain_model_product',
                    'foreign_table_where' => ' AND tx_shop_domain_model_product.pid = ###CURRENT_PID### ORDER BY tx_shop_domain_model_product.title ASC ',
                    'size' => 1,
                    'autoSizeMax' => 1,
                    'minitems' => 0,
                    'maxitems' => 1,
                    'multiple' => 0,
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'orderGroupSingle':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'fe_groups',
                    'foreign_table_where' => ' AND fe_groups.pid = ###CURRENT_PID### ORDER BY fe_groups.title ASC ',
                    'size' => 1,
                    'autoSizeMax' => 1,
                    'minitems' => 0,
                    'maxitems' => 1,
                    'multiple' => 0,
                    'items' => [
                        ['label' => '', 'value' => '0'],
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        ['', '0'],
                    ];
                }
                break;
            case 'productColorSingle':
                $order = self::$extensionConfiguration['backend']['orderingForColor'] ?? 'title';
                $order = 'ORDER BY tx_shop_domain_model_productcolor.' . $order;
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'tx_shop_domain_model_productcolor',
                    'foreign_table_where' => 'AND tx_shop_domain_model_productcolor.pid=###CURRENT_PID### ' . $order,
                    'size' => 1,
                    'maxitems' => 1,
                    'minitems' => 0,
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'productSizeSingle':
                $order = self::$extensionConfiguration['backend']['orderingForSize'] ?? 'title';
                $order = 'ORDER BY tx_shop_domain_model_productsize.' . $order;
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'tx_shop_domain_model_productsize',
                    'foreign_table_where' => 'AND tx_shop_domain_model_productsize.pid=###CURRENT_PID### ' . $order,
                    'size' => 1,
                    'maxitems' => 1,
                    'minitems' => 0,
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        ['', '0'],
                    ];
                }
                break;
            case 'attributeCategorySingle':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'tx_shop_domain_model_productattributecategory',
                    'foreign_table_where' => ' AND tx_shop_domain_model_productattributecategory.pid = ###CURRENT_PID### ',
                    'size' => 1,
                    'autoSizeMax' => 1,
                    'minitems' => 0,
                    'maxitems' => 1,
                    'multiple' => 0,
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            default:
                $config = parent::get($type, $required, $readonly, $label, $options);
                break;
        }
        if ($readonly) {
            $config['readOnly'] = true;
        }
        return $config;
    }
}
