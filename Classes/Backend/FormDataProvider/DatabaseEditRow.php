<?php

declare(strict_types=1);

namespace CodingMs\Shop\Backend\FormDataProvider;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2021 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Form\FormDataProviderInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Configuration\Richtext;
use TYPO3\CMS\Core\Html\RteHtmlParser;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Dynamic actions on products TCA
 */
class DatabaseEditRow implements FormDataProviderInterface
{
    /**
     * @param array<string, mixed> $result
     * @return array<string, mixed>
     */
    public function addData(array $result): array
    {
        if ($result['tableName'] === 'tx_shop_domain_model_product') {
            if ($result['command'] === 'new') {
                //
                // If no parent product is set, a main product is in creation
                $parentProductTypeUid = (int)($result['databaseRow']['parent'][0]['row']['product_type'] ?? 0);
                if ($parentProductTypeUid > 0) {
                    $type = BackendUtility::getRecord('tx_shop_domain_model_producttype', $parentProductTypeUid);
                    if (is_array($type)) {
                        $result = $this->removeField($result, 'product_type');
                        foreach ($type as $fieldKey => $enabled) {
                            if (str_ends_with($fieldKey, '_enabled_variant') && !$enabled) {
                                $result = $this->removeField(
                                    $result,
                                    str_replace('_enabled_variant', '', $fieldKey)
                                );
                            }
                        }
                    }
                    foreach (['information', 'views', 'views_day', 'views_week', 'views_month', 'views_year', 'views_last_update'] as $field) {
                        $result = $this->removeField($result, $field);
                    }
                } else {
                    foreach ($result['processedTca']['columns'] as $key => $config) {
                        if ($key !== 'product_type' && $key !== 'tax') {
                            $result = $this->removeField($result, $key);
                        }
                    }
                }
            } else {
                //
                // If a parent product is set, a product variant is in modification
                $parentProductTypeUid = (int)($result['databaseRow']['parent'][0]['row']['product_type'] ?? 0);
                if ($parentProductTypeUid > 0) {
                    $type = BackendUtility::getRecord('tx_shop_domain_model_producttype', $parentProductTypeUid);
                    if (is_array($type)) {
                        $result = $this->removeField($result, 'product_type');
                        foreach ($type as $fieldKey => $enabled) {
                            if (str_ends_with($fieldKey, '_enabled_variant') && !$enabled) {
                                $result = $this->removeField(
                                    $result,
                                    str_replace('_enabled_variant', '', $fieldKey)
                                );
                            }
                        }
                    }
                    foreach (['information', 'views', 'views_day', 'views_week', 'views_month', 'views_year', 'views_last_update'] as $field) {
                        $result = $this->removeField($result, $field);
                    }
                } else {
                    //
                    // If no parent product is set, a base product is in modification
                    $productTypeUid = (int)($result['databaseRow']['product_type'][0] ?? 0);
                    if ($productTypeUid > 0) {
                        $type = BackendUtility::getRecord('tx_shop_domain_model_producttype', $productTypeUid);
                        if (is_array($type)) {
                            foreach ($type as $fieldKey => $enabled) {
                                if (str_ends_with($fieldKey, '_enabled') && !$enabled) {
                                    //
                                    // If it's enabled in variant, it must be enabled in base product anyway
                                    if (isset($type[$fieldKey . '_variant']) && $type[$fieldKey . '_variant']) {
                                        continue;
                                    }
                                    //
                                    $result = $this->removeField(
                                        $result,
                                        str_replace('_enabled', '', $fieldKey)
                                    );
                                }
                            }
                        }
                        $result = $this->addCustomAttributeFields($result, $type);
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param array<string, mixed> $result
     * @param string $fieldKey
     * @return array<string, mixed>
     */
    protected function removeField(array $result, string $fieldKey): array
    {
        if ($fieldKey === 'variants') {
            return $result;
        }
        if ($fieldKey === 'unit') {
            $result = $this->removeField($result, 'unit_factor');
        }
        if ($fieldKey === 'stock') {
            $result = $this->removeField($result, 'in_stock');
            return $this->removeField($result, 'stock_amount');
        }
        //
        // Remove from palettes
        foreach ($result['processedTca']['palettes'] as $paletteKey => $palette) {
            $paletteFields = GeneralUtility::trimExplode(',', $palette['showitem'], true);
            if (($settingFound = array_search($fieldKey, $paletteFields)) !== false) {
                unset($paletteFields[$settingFound]);
            }
            $result['processedTca']['palettes'][$paletteKey]['showitem'] = implode(',', $paletteFields);
            unset($palette, $paletteKey, $paletteFields, $settingFound);
        }
        //
        // Remove from types
        foreach ($result['processedTca']['types'] as $typesKey => $types) {
            $typesFields = GeneralUtility::trimExplode(',', $types['showitem'], true);
            if (($settingFound = array_search($fieldKey, $typesFields)) !== false) {
                unset($typesFields[$settingFound]);
            }
            $result['processedTca']['palettes'][$typesKey]['showitem'] = implode(',', $typesFields);
            unset($types, $typesKey, $typesFields, $settingFound);
        }
        //
        // Remove from columnsToProcess
        if (($settingFound = array_search($fieldKey, $result['columnsToProcess'])) !== false) {
            unset($result['columnsToProcess'][$settingFound]);
        }
        unset($settingFound);
        //
        // Remove from columnsToProcess
        unset($result['processedTca']['columns'][$fieldKey]);
        return $result;
    }

    /**
     * @param array<string, mixed> $result
     * @param array $type
     * @return array<string, mixed>
     */
    protected function addCustomAttributeFields(array $result, array $type): array
    {
        for ($i = 1; $i <= 8; $i++) {
            $fieldName = 'attribute' . $i;
            if (isset($result['processedTca']['columns'][$fieldName])) {
                $result['processedTca']['columns'][$fieldName]['label'] = $type[$fieldName . '_label'];
                if (trim($result['processedTca']['columns'][$fieldName]['label']) === '') {
                    $result['processedTca']['columns'][$fieldName]['label'] = ucfirst($fieldName);
                }
                $result['processedTca']['columns'][$fieldName]['description'] = $type[$fieldName . '_description'];
                switch ($type[$fieldName . '_type']) {
                    case 'checkbox':
                        $result['processedTca']['columns'][$fieldName]['config'] = [
                            'type' => 'check',
                            'items' => [
                                ['label' => '', 'value' => '1'],
                            ],
                            'default' => 0,
                        ];
                        if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                            $result['processedTca']['columns'][$fieldName]['config']['items'] = [
                                ['', '1'],
                            ];
                        }
                        break;
                    case 'string':
                        $result['processedTca']['columns'][$fieldName]['config'] = [
                            'type' => 'input',
                            'size' => 30,
                            'eval' => 'trim',
                            'default' => '',
                        ];
                        break;
                    case 'string_required':
                        $result['processedTca']['columns'][$fieldName]['config'] = [
                            'type' => 'input',
                            'size' => 30,
                            'eval' => 'trim,required',
                            'default' => '',
                        ];
                        break;
                    case 'int':
                        $result['processedTca']['columns'][$fieldName]['config'] = [
                            'type' => 'input',
                            'size' => 4,
                            'eval' => 'int',
                            'default' => 0,
                        ];
                        break;
                    case 'int_required':
                        $result['processedTca']['columns'][$fieldName]['config'] = [
                            'type' => 'input',
                            'size' => 4,
                            'eval' => 'int, required',
                            'default' => 0,
                        ];
                        break;
                    case 'textarea':
                        $result['processedTca']['columns'][$fieldName]['config'] = [
                            'type' => 'text',
                            'cols' => 40,
                            'rows' => 5,
                            'eval' => 'trim',
                            'default' => '',
                        ];
                        break;
                    case 'textarea_required':
                        $result['processedTca']['columns'][$fieldName]['config'] = [
                            'type' => 'text',
                            'cols' => 40,
                            'rows' => 5,
                            'eval' => 'trim, required',
                            'default' => '',
                        ];
                        break;
                    case 'rte':
                        $result['processedTca']['columns'][$fieldName]['defaultExtras'] = 'richtext:rte_transform[flag=rte_enabled|mode=ts]';
                        $result['processedTca']['columns'][$fieldName]['config'] = [
                            'type' => 'text',
                            'cols' => 40,
                            'rows' => 15,
                            'eval' => 'trim',
                            'enableRichtext' => 1,
                            'richtextConfiguration' => 'default',
                            'default' => '',
                        ];
                        //
                        // Bind RTE library
                        $richtextConfigurationProvider = GeneralUtility::makeInstance(Richtext::class);
                        $richtextConfiguration = $richtextConfigurationProvider->getConfiguration(
                            $result['tableName'],
                            $fieldName,
                            $result['effectivePid'],
                            '0',
                            $result['processedTca']['columns'][$fieldName]['config']
                        );
                        // Transform if richtext is not disabled in configuration
                        if (!($richtextConfiguration['disabled'] ?? false)) {
                            // remember RTE preset name
                            $result['processedTca']['columns'][$fieldName]['config']['richtextConfigurationName'] = $fieldConfig['config']['richtextConfiguration'] ?? '';
                            // Add final resolved configuration to TCA array
                            $result['processedTca']['columns'][$fieldName]['config']['richtextConfiguration'] = $richtextConfiguration;
                            // If eval=null is set for field, value might be null ... don't transform anything in this case.
                            if ($result['databaseRow'][$fieldName] !== null) {
                                // Process "from-db-to-rte" on current value
                                $richTextParser = GeneralUtility::makeInstance(RteHtmlParser::class);
                                $result['databaseRow'][$fieldName] = $richTextParser->transformTextForRichTextEditor($result['databaseRow'][$fieldName], $richtextConfiguration['proc.'] ?? []);
                            }
                        }
                        break;
                    case 'rte_required':
                        $result['processedTca']['columns'][$fieldName]['defaultExtras'] = 'richtext:rte_transform[flag=rte_enabled|mode=ts]';
                        $result['processedTca']['columns'][$fieldName]['config'] = [
                            'type' => 'text',
                            'cols' => 40,
                            'rows' => 15,
                            'eval' => 'trim, required',
                            'enableRichtext' => 1,
                            'richtextConfiguration' => 'default',
                            'default' => '',
                        ];
                        //
                        // Bind RTE library
                        $richtextConfigurationProvider = GeneralUtility::makeInstance(Richtext::class);
                        $richtextConfiguration = $richtextConfigurationProvider->getConfiguration(
                            $result['tableName'],
                            $fieldName,
                            $result['effectivePid'],
                            '0',
                            $result['processedTca']['columns'][$fieldName]['config']
                        );
                        // Transform if richtext is not disabled in configuration
                        if (!($richtextConfiguration['disabled'] ?? false)) {
                            // remember RTE preset name
                            $result['processedTca']['columns'][$fieldName]['config']['richtextConfigurationName'] = $fieldConfig['config']['richtextConfiguration'] ?? '';
                            // Add final resolved configuration to TCA array
                            $result['processedTca']['columns'][$fieldName]['config']['richtextConfiguration'] = $richtextConfiguration;
                            // If eval=null is set for field, value might be null ... don't transform anything in this case.
                            if ($result['databaseRow'][$fieldName] !== null) {
                                // Process "from-db-to-rte" on current value
                                $richTextParser = GeneralUtility::makeInstance(RteHtmlParser::class);
                                $result['databaseRow'][$fieldName] = $richTextParser->transformTextForRichTextEditor($result['databaseRow'][$fieldName], $richtextConfiguration['proc.'] ?? []);
                            }
                        }
                        break;
                }
            }
        }
        return $result;
    }

    protected function getBackendUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }

}
