<?php

$EM_CONF['shop'] = [
    'title' => 'Shop',
    'description' => 'Simple Shop for TYPO3 incl. PayPal, PayPal-Plus, Klarna, Stripe, Bookmarks, UPS-API, Invoice generation, backend module, compare feature, graduated prices and many more - note there is a Pro version as well!',
    'category' => 'plugin',
    'author' => 'Thomas Deuling',
    'author_email' => 'typo3@coding.ms',
    'author_company' => 'coding.ms GmbH',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '4.1.3',
    'constraints' => [
        'depends' => [
            'php' => '7.4.0-8.2.99',
            'typo3' => '11.5.0-12.4.99',
            'additional_tca' => '1.14.8-1.99.99',
            'modules' => '6.2.0-6.99.99',
            'static_info_tables' => '11.5.2-12.99.99',
        ],
        'conflicts' => [],
        'suggests' => [
            'questions' => '3.0.0',
        ],
    ],
];
