# Quick Start

## 1. Installation der Extension

Installiere die Extension via Composer oder alternativ über den Extension Manager und füge die statischen TypoScript Templates zu Deinem Root-Template hinzu.

## 2. Anlegen der Seitenstruktur

Folgende Seiten-Struktur kann empfohlen werden. Die jeweiligen Seiten können über den TypoScript Konstanten-Editor konfiguriert werden.

```text
🌎 Website root
├─ 🗀 Data
│  └─ 🛒 Products
├─ 📄 Products list
│  ├─ 📄 Products detail
│  ├─ 📄 Basket
├─ 📄 Checkout
│  ├─ 📄 Error
│  ├─ 📄 Callback
│  ├─ 📄 Cancel
│  └─ 📄 Success
└─ 🗀 Footer Menu
   ├─ 📄 Terms
   ├─ 📄 Disclaimer
   └─ 📄 Privacy
```

## 3. TypoScript Konfiguration (minimal)

Nach dem alle Seiten angelegt sind, kann die minimale TypoScript Konfiguration hinzugefügt werden.
Empfohlen wird eine einfache Textdatei in Deinem Projekt-Verzeichnis / Template-Ordner oder im Dateisystem abzulegen. Die Namensgebung der Datei sollte einen Hinweis auf den Inhalt liefern. In unserem Beispiel nennen wir die Datei "shop.constants.txt"
Die ID's der entsprechenden Seiten ersetzt Du mit den ID's aus Deiner Installation. Wenn Du mit der Maus über das Icon der Seite im Seitenbaum gehst, wird Dir die ID angezeigt.
Emailadressen, Shopnamen und Pfade sind ebenfalls entsprechend Deiner Instanz anzupassen.

```text
themes.resourcesPrivatePath = fileadmin/
module.tx_shoppro.persistence.storagePid = 88
themes.configuration.extension.shop.basket.minimumOrderValue = 100
themes.configuration.extension.shop.ups.pid = 0
themes.configuration.container.shop = 88
themes.configuration.extension.shop.basket.displayType = b2c
themes.configuration.extension.shop.detail.image.width = 450
themes.configuration.extension.shop.detail.image.height = 450
themes.configuration.extension.shop.email.to.name = Mein Shop Name
themes.configuration.extension.shop.email.from.email = shop@beispiel.de
themes.configuration.extension.shop.email.from.name =  Mein Shop Name
themes.configuration.extension.shop.email.to.email = shop@beispiel.de
themes.configuration.extension.shop.basket.minimumOrderValue = 100
themes.configuration.pages.shop.detail = 81
themes.configuration.pages.shop.checkoutCancel = 87
themes.configuration.pages.shop.notFound = 68
themes.configuration.pages.shop.basket = 82
themes.configuration.pages.shop.list = 80
themes.configuration.pages.shop.checkout = 83
themes.configuration.pages.shop.checkoutError = 84
themes.configuration.pages.shop.checkoutSuccess = 86
themes.configuration.pages.shop.checkoutCallback = 85
themes.configuration.pages.terms = 63
themes.configuration.pages.disclaimer = 63
themes.configuration.pages.shop.payPalPlusSuccessRedirectPid = 86
themes.configuration.pages.privacy = 63
themes.configuration.extension.shop.checkout.payPalPlus.webhookID = XXXX
themes.configuration.extension.shop.checkout.payPalPlus.payPalPlusSecret = XXX
themes.configuration.extension.shop.checkout.payPalPlus.payPalPlusClientId = XXX
themes.configuration.extension.shop.checkout.payPalPlus.sandbox = 1
themes.configuration.extension.shop.checkout.payPalPlus.paymentTitle = Bestellung Mein Shop
```

Binde die Datei in Deinem Root-Template gemäß Deiner Projektstruktur ein.

```text
<INCLUDE_TypoScript: source="EXT:myPageSetup/templates/ts/shop.constants.txt">
```

## 4. Plugins für Seiten anlegen

- **Data-Products** Diese Seite ist eine Seite vom Typ Ordner und sollte nicht in den Suchindex/Sitemap aufgenommen werden. In den Einstellungen der Seite, stellst Du unter "Einstellungen - Enthält Plugin" das Plugin "Produkte" ein. Nach dem Speichern wird das Icon der Seite im Seitenbaum von einem Ordner zu einem Einkaufswagen wechseln.
- **Products-List** Diese Seite enthält die Liste Deiner Produkte. Sie ist eine Seite vom Typ "Standard". Als Inhaltselement wird hier ein das Plugin _Shop product list and single view_ angelegt.
- **Products-detail** Diese Seite enthält die Detaiansicht Deiner Produkte. Sie ist eine Seite vom Typ "Standard". Als Inhaltselement wird hier ein das Plugin _Shop product list and single view_ angelegt.
- **Checkout-Seite** Diese Seite ist im Menü unsichtbar und sollte nicht in den Suchindex/Sitemap aufgenommen werden. Auf dieser Seite muss ein Plugin vom Typ _Shop checkout_ platziert werden, sodass hier der Bestellprozess durchgeführt werden kann.
- **Error-Seite** Diese Seite ist im Menü unsichtbar und sollte nicht in den Suchindex/Sitemap aufgenommen werden. Auf dieser Seite muss ein Plugin vom Typ _Shop checkout_ platziert werden, damit Fehler einer Bestellung entsprechend verarbeitet werden kann.
- **Callback-Seite** Diese Seite ist im Menü unsichtbar und sollte nicht in den Suchindex/Sitemap aufgenommen werden. Auf dieser Seite muss ein Plugin vom Typ _Shop checkout_ platziert werden, damit das ein Callback-Aufruf (von bspw. PayPal) einer Bestellung verarbeitet werden kann.
- **Cancel-Seite** Diese Seite ist im Menü unsichtbar und sollte nicht in den Suchindex/Sitemap aufgenommen werden. Auf dieser Seite muss ein Plugin vom Typ _Shop checkout_ platziert werden, damit das Abbrechen einer Bestellung entsprechend verarbeitet werden kann.
- **Success-Seite** Diese Seite ist im Menü unsichtbar und sollte nicht in den Suchindex/Sitemap aufgenommen werden. Auf dieser Seite muss ein Plugin vom Typ _Shop checkout_ platziert werden, damit der Abschluss einer Bestellung entsprechend verarbeitet werden kann.
- **Terms/Disclaimer/Privac-Seiten** Diese Seiten sind Seiten vom Typ "Standard" und enthalten reine Textinhalte, mit den entsprechenden Passagen.

### Einstellungen Plugin Product list and single view (minimal)

Im Plugin im Tab "General" wählst Du einen der Darstellungsmodi - Columns, Group oder Deck (in der Pro Version steht noch der Modus Isotope zur Verfügung). Die Darstellungsmodi definieren das Rendering der Produkte im Frontend angelehnt an die Darstellungsmodi in [Boostrap 4 / Cards](https://getbootstrap.com/docs/5.0/components/card).

![Display Mode](https://www.coding.ms/fileadmin/extensions/shop/current/Documentation/Images/Display_Mode.png) {.inline-image rel=enlarge-image}

Weiterhin wählst Du hier bitte die sichtbaren Elemente die ausgegeben werden sollen.

Im Kartenreiter "Sorting" muss mindestens ein Element ausgewählt sein, damit das Plugin gespeichert werden kann.

![Sorting](https://www.coding.ms/fileadmin/extensions/shop/current/Documentation/Images/Sorting.png) {.inline-image rel=enlarge-image}

## 5. Produkte anlegen (minimal)

Die Produktdatensätze müssen auf der Seite **Data-Products** eingefügt werden. Wähle die Seite im Seitenbaum aus und wechsel in das Listen-Modul. Füge einen neuen Datensatz vom Typ "Products" hinzu. Fülle mindestens die Felder Titel, Teaser, Description, Price, In Stock, In Stock Amount, Unit Factor, Unit und Images. Lege mindestens 3 Produkte an.

## 6. Warenkorb Button hinzufügen

Es ist notwendig, einen globalen Warenkorb-Button in Deine Seite einzubinden – anderenfalls wird es nicht möglich sein, Produkte in den Warenkorb zu legen!

Der _Warenkorb-Button_ kann entweder als ein Content-Element oder als ein TypoScript Marker eingebunden werden.

1. Hinzufügen des vordefinierten _Warenkorb-Button_ Markers im Fluid-Template:

	```xml
	<f:cObject typoscriptObjectPath="lib.shop.basketButton" />
	```

2. Hinzufügen des vordefinierten _Warenkorb-Buttons_ im TypoScript:

	```typo3_TypoScript
	page = PAGE
	page {
	  10 = USER
	  10 < lib.shop.basketButton
	  # ...
	}
	```

Alternativ kannst Du den _Warenkorb-Button_ als Plugin hinzufügen.

## 7. HTML Templates für weitere Bearbeitung verfügbar machen

Dein Shop sollte nun bereits Produkte auf Deiner Webseite anzeigen und funktional sein. Um das Aussehen Deines Shops weiter bearbeiten zu können, kannst Du die HTML Templates in Dein Projektverzeichnis kopieren. Kopiere alle Dateien aus der Extension unter Ressources/Private gemäß der unten aufgeführten Darstellung.

```text
📄 Extensions
└─ 🗀 Shop
   ├─ 📄 Layout
   ├─ 📄 Partials
   └─ 📄 Templates
```

Passe den Pfad zu den Dateien in den Konstanten Datei shop.constants.txt an.

```typo3_TypoScript
themes.resourcesPrivatePath = EXT:myPageSetup/Extensions/Shop/
```

### Fertig :-)
