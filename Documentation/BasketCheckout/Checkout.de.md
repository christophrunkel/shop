# Warenkorb Checkout

Die Shop-Erweiterung kommt mit 7 vordefinierten Checkout-Varianten. Diese sind:

- `request` Anfrage
- `prePayment` Vorkasse
- `onInvoice` Auf Rechnung
- `payPal` (erfordert Pro-Version der Shop-Erweiterung)
- `payPalPlus` (erfordert Pro-Version der Shop-Erweiterung)
- `klarna` (erfordert Pro-Version der Shop-Erweiterung)
- `stripe` (erfordert Pro-Version der Shop-Erweiterung)

## Allgemeine Konfiguration

- `checkoutPid` Hier muss die Page-Uid für die Checkout-Seite angegeben werden.
- `minimummOrderValue` Hier kann der Mindestbestellwert (in Cents) angegeben werden.
- `timeForPayment` Hier können die verfügbaren Tage für die Zahlung angegeben werden.
- `deliveryTime` Hier kann ein optionaler Text für die Lieferzeit angegeben werden.
- `orderOptions` Hier werden die Checkout-Varianten definiert. Auf diese gehen wir in separaten Abschnitten genauer ein.
- `fieldDefinition` Hier werden Formular-Felder für den Checkout konfiguriert. Diese werden erst in den jeweiligen TypoScript-Knoten vorkonfiguriert und können dann in den verschiedenen Checkouts verwendet werden.

Weitere Checkout spezifische Konfigurationen findest Du auf den jeweiligen Dokumentationsseiten.

## TypoScript Konfiguration Checkout Basis

```typo3_TypoScript
plugin.tx_shop.settings.basketOrder {
	# Page-ID des Checkout
	checkoutPid = {$themes.configuration.pages.shop.checkout}
	# Minimum order value in cents
	minimumOrderValue = {$themes.configuration.extension.shop.basket.minimumOrderValue}
	# Time for payment in days default value
	timeForPayment = {$themes.configuration.extension.shop.basketOrder.timeForPayment}
	# Default string for delivery time notice in basket above the additional costs
	deliveryTime =
	# VAT information depending on country and b2b/b2c
	vat {
		b2b {
			inland {
				notice = Es handelt sich um eine Inland-Lieferung an ein Unternehmen.<br />Ihre USt.-ID: %1$s
			}
			europeanUnion {
				# Parameter is VAT-ID in basket order
				notice = Es handelt sich um eine steuerbefreite innergemeinschaftliche Lieferung gem. §4 Nr. 1b UstG.<br />Unsere USt.-ID: DE 123 456 789<br />Ihre USt.-ID: %1$s
			}
			thirdCountry {
				notice = Es handelt sich um eine steuerbefreite Ausfuhrlieferung gem. §4 Nr. 1a UstG.
			}
		}
		b2c {
			inland {
				notice = Es handelt sich um eine Inland-Lieferung an eine Privatperson.
			}
			europeanUnion {
				notice = Es handelt sich um eine innergemeinschaftliche Lieferung an eine Privatperson.
			}
			thirdCountry {
				notice = Es handelt sich um eine steuerbefreite Ausfuhrlieferung gem. §4 Nr. 1a UstG.
			}
		}
	}
	orderOptions {
		# Different order options
	}
}
```
