# PayPal Checkout konfigurieren

>	#### Achtung: {.alert .alert-danger}
>
>	Der PayPal Checkout erfordert der die Pro-Version der Shop-Erweiterung!

>	#### Achtung: {.alert .alert-danger}
>
>	Der PayPal Checkout erfordert der den Eintrag der vollen URL in der Site-Konfiguration Deiner TYPO3-Instanz!



## PayPal-Account

Zuerst benötigst Du einen PayPal-Account. Logge Dich damit ein und öffne `https://developer.paypal.com`. Hier musst Du nun unter dem Menüpunkt *My Apps & Credentials* eine neue App erstellen. Dabei musst Du im ersten Schritt darauf achten, ob Du einen *Live* oder *Sandbox* Account erstellen möchtest. Nach dem Erstellen solltest Du folgende Informationen erhalten haben:

*   **(Sandbox) Account:** Dies ist eine E-Mail-Adresse.
*   **Client ID:** Dies ist ein Hashwert, der den Client identifiziert.
*   **Secret:** Dies ist ebenfalls ein Hashwert. Diesen sollten auf jeden Fall geheim halten!

Für die Einrichtung des PayPal-Checkouts benötigst Du nur den (Sandbox) Account. (Siehe Felder **sandbox** und **payPalId** im Abschnitt ***Checkout Konfiguration***)

Zum Testen benötigst Du einen weiteren Account, welchen Du unter *https://developer.paypal.com/developer/accounts/* anlegen kannst.


>	#### Achtung: {.alert .alert-danger}
>
>	Wenn Deine Umgebung durch ein Passwort geschützt ist, kann der Webhook nicht aufgerufen werden. Bitte stelle sicher, dass der Webhook direkt aufrufbar ist.

## Checkout Konfiguration

*   **active** Hier kann der Check-out aktiviert/deaktiviert werden
*   **checkoutPid** Hier muss die Page-Uid der Checkout-Seite angegeben werden.
*   **successPid** Hier muss die Page-Uid der Erfolgs-Seite angegeben werden. Dies ist die Seite, auf die man weitergeleitet wird, wenn die Zahlung erfolgreich war.
*   **cancelPid** Hier muss die Page-Uid der Cancel-Seite angegeben werden. Dies ist die Seite, auf die man weitergeleitet wird, wenn man den Zahlungsprozess abgebrochen hat.
*   **callbackPid** Hier muss die Page-Uid der Callback-Seite angegeben werden. Die Callback-Seite ist eine Seite, die von PayPal asynchron aufgerufen wird, um die Bezahlung zu bestätigen.
*   **service** Hier wird die Checkout-Service PHP-Klasse zugewiesen. Diese kann angepasst werden, wenn etwas am Prozess verändert werden muss.
*   **sandbox** Hier kann die Sandbox zum Testen aktiviert/deaktiviert werden.
*   **payPalId** Hier muss der (Sandbox) Account Name eingetragen werden, welche bei PayPal erstellt wurde


## Slot
>	#### Achtung: {.alert .alert-danger}
>
>	Dieser Slot ist veraltet und wird bald entfernt.

Beim erfolgreichen Ausführen des Callbacks wird ein Signal dispatched und es kann ein Slot registriert werden. Dieser Einrichtungsschritt ist optional.

Mithilfe des Slots können, nachdem ein Shop-Produkt erfolgreich bezahlt wurde, zusätzliche Aktionen ausgeführt werden. z.B. Versenden weiterer E-Mails,
Erstellung von Accounts, Freischaltung digitaler Produkte, etc.


1.	Dazu muss ein Slot erstellt werden:
	```php
	<?php
	namespace YourVendor\YourExtension\Slot;
	use CodingMs\Shop\Domain\Model\BasketOrder;
	class DoSomeThingSlot
	{
		public function doSomeThingFunction(BasketOrder $basketOrder)
		{
			/* Do something */
		}
	}
	```
2.	Dieser Slot muss anschließend in der `ext_localconf.php` registriert werden:
	```php
	$signalSlotDispatcher = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
	$signalSlotDispatcher->connect(
		\CodingMs\Shop\Controller\BasketOrderController::class,
		'afterPayPalCallback',
		\YourVendor\YourExtension\Slot\DoSomeThingSlot::class,
		'doSomeThingFunction'
	);
	```

## Aktionen nach erfolgreicher Bezahlung

Wenn Du nach der erfolgreichen Bezahlung weitere Aktionen durchführen möchten, kannst Du dazu den `PayPalPaidPaid` Event Listener nutzen.

1.	Dazu muss ein Event Listener erstellt werden:
	```php
	<?php
	namespace YourVendor\YourExtension\EventListener;
	use CodingMs\ShopPro\Event\BasketOrder\PayPalPaidPaidEvent;
	class DoSomeThingEventListener
	{
		public function __invoke(PayPalPaidPaidEvent $event): void
		{
			/* Do something */
		}
	}
	```
2.	Dieser Event Listener muss anschließend in der `Configuration/Services.yaml` registriert werden:
	```yaml
	services:
		YourVendor\YourExtension\EventListener\DoSomeThingEventListener:
			tags:
				- name: event.listener
				  identifier: 'myListener'
				  event: CodingMs\ShopPro\Event\BasketOrder\PayPalPaidPaidEvent
	```

## Debugging

Für den Fall, dass der Callback-Aufruf nicht funktioniert, kannst Du ein Debugging aktivieren. Wie dies funktioniert, kannst Du im Abschnitt *HowTo/Debugging* lesen.



## Testing

Zum Testen der PayPal Callbacks am besten im https://developer.paypal.com einloggen (hier musst Du Deine echten Zugangsdaten verwenden). Hier musst Du unter *SANDBOX* den *IPN Simulator* aufrufen.

Im Feld *IPN handler URL* fügst Du Deine Callback-URL ein und wählst dann bei *Transaction type* den Wert *Cart checkout* aus. Nun erscheinen unter dem Formular eine vielzahl an Formularfeldern. Hier musst Du in dem Feld *item_number* die Uid Deine Basket-Order eintragen, damit der Callback die entsprechende Order wiederfindet.
