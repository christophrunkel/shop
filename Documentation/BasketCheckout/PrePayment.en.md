# TypoScript configuration order onPrePayment

```typo3_TypoScript
plugin.tx_shop.settings.basketOrder {
	orderOptions {
		prePayment {
			active = {$themes.configuration.extension.shop.checkout.prePayment.active}
			attachProductFilesToAdminEmail = {$themes.configuration.extension.shop.checkout.prePayment.attachProductFilesToAdminEmail}
			attachProductFilesToCustomerEmail = {$themes.configuration.extension.shop.checkout.prePayment.attachProductFilesToCustomerEmail}
			attachInvoiceToAdminEmail = {$themes.configuration.extension.shop.checkout.prePayment.attachInvoiceToAdminEmail}
			attachInvoiceToCustomerEmail = {$themes.configuration.extension.shop.checkout.prePayment.attachInvoiceToCustomerEmail}
			attachDeliveryNoteToAdminEmail = {$themes.configuration.extension.shop.checkout.prePayment.attachDeliveryNoteToAdminEmail}
			attachDeliveryNoteToCustomerEmail = {$themes.configuration.extension.shop.checkout.prePayment.attachDeliveryNoteToCustomerEmail}
			attachOrderXmlToAdminEmail = {$themes.configuration.extension.shop.checkout.prePayment.attachOrderXmlToAdminEmail}
			storeOrderXmlInFolder = {$themes.configuration.extension.shop.checkout.prePayment.storeOrderXmlInFolder}
			storeOrderXmlInFolderName = {$themes.configuration.extension.shop.checkout.prePayment.storeOrderXmlInFolderName}
			type = prePayment
			checkoutPid = {$themes.configuration.pages.shop.checkout}
			successPid = {$themes.configuration.pages.shop.checkoutSuccess}
			errorPid = {$themes.configuration.pages.shop.checkoutError}
			service = CodingMs\Shop\Service\Checkout\PrePaymentCheckoutService
			button {
				title = tx_shop_label.pre_payment_button_title
				label = tx_shop_label.pre_payment_button_label
				icon = fa fa-shopping-cart
			}
			fields {
				available = {$themes.configuration.extension.shop.checkout.prePayment.fields.available}
				required = {$themes.configuration.extension.shop.checkout.prePayment.fields.required}
			}
			email {
				customerConfirmation {
					active = {$themes.configuration.extension.shop.checkout.prePayment.email.customerConfirmation.active}
					from {
						name = {$themes.configuration.extension.shop.email.from.name}
						email = {$themes.configuration.extension.shop.email.from.email}
					}
					to {
						# Customer address data
					}
					template = {$themes.configuration.extension.shop.email.templates.prePayment.customerConfirmation}
				}
				order {
					active =  {$themes.configuration.extension.shop.checkout.prePayment.email.order.active}
					from {
						name = {$themes.configuration.extension.shop.email.from.name}
						email = {$themes.configuration.extension.shop.email.from.email}
					}
					to {
						name = {$themes.configuration.extension.shop.email.to.name}
						email = {$themes.configuration.extension.shop.email.to.email}
					}
					template = {$themes.configuration.extension.shop.email.templates.prePayment.order}
				}
			}
		}
	}
}
```
