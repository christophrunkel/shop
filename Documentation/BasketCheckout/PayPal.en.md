# PayPal Checkout configuration

>	#### Attention: {.alert .alert-danger}
>
>	PayPal Checkout requires the Pro version of the shop extension!

>	#### Attention: {.alert .alert-danger}
>
>	The PayPal Checkout requires the entry of the full URL in the site configuration of your TYPO3 instance!



## PayPal Account

You will need a PayPal account. Log-in to PayPal and open `https://developer.paypal.com`. Go to *My Apps & Credentials* in the menu and create a new app. Take note of whether you want to create a  *Live* or a *Sandbox* account. After creating it, you should have the following information

*   **(Sandbox) Account:** This is an email address.
*   **Client ID:** This is a hash value that identifies the client.
*   **Secret:** This is also a hash value. Make sure you keep it secret!

To set up the PayPal checkout, you only need the (sandbox) account. (See the **sandbox** and **payPalId** fields in the ***Checkout Configuration*** section).

For testing purposes you will need an extra account which you can set up under *https://developer.paypal.com/developer/accounts/*.


>	#### Warning: {.alert .alert-danger}
>
>	If your environment is password protected the webhook can't be executed. Please make sure that the webhook can be accessed.



## Checkout configuration

*   **active** Activate/deactivate checkout
*   **checkoutPid** Enter the page Uid of the checkout page.
*   **successPid** Enter the page Uid of the success page. This is the page you will be redirected to after successful payment.
*   **cancelPid** Enter the page Uid of the cancel page. This is the page you will be redirected to if you cancel the payment process.
*   **callbackPid** Enter the page Uid of the callback page. The callback page is accessed asynchronously by Paypal to confirm payment.
*   **service** The checkout service PHP class is assigned here. This can be modified if the process needs to be changed.
*   **sandbox** Activate/deactivate the sandbox.
*   **payPalId** Enter the (sandbox) account name created in PayPal.


## Add additional actions after a successful payment

If you need actions to be carried out after successful payment, use the `PayPalPaid` event listener.

1.	Create a new slot:
	```php
	<?php
	namespace YourVendor\YourExtension\EventListener;
	use CodingMs\ShopPro\Event\BasketOrder\PayPalPaidEvent;
	class DoSomeThingEventListener
	{
		public function __invoke(PayPalPaid $event): void
		{
			/* Do something */
		}
	}
	```
2.	Then register the event listener in `Configuration/Services.yaml`:
	```yaml
	services:
		YourVendor\YourExtension\EventListener\DoSomeThingEventListener:
			tags:
				- name: event.listener
				  identifier: 'myListener'
				  event: CodingMs\ShopPro\Event\BasketOrder\PayPalPaid
	```

## Additional actions after a successful payment

If you want to carry out further actions after successful payment use the `payPalPaid` signal/slot.

1.	Create a new slot:
	```php
	<?php
	namespace YourVendor\YourExtension\Slot;
	use CodingMs\Shop\Domain\Model\BasketOrder;
	class DoSomeThingSlot
	{
		public function doSomeThingFunction(BasketOrder $basketOrder)
		{
			/* Do something */
		}
	}
	```
2.	Then register the slot in `ext_localconf.php`:
	```php
	$signalSlotDispatcher = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
	$signalSlotDispatcher->connect(
		\CodingMs\ShopPro\Service\Checkout\PayPalCheckoutService::class,
		'payPalPaid',
		\YourVendor\YourExtension\Slot\DoSomeThingSlot::class,
		'doSomeThingFunction'
	);
	```

## Debugging

If a callback does not work, you can activate debugging. Read how this works in the *How To/Debugging* section.



## Testing

PayPal callbacks are best tested by logging in to https://developer.paypal.com (using your real login data). Access the *IPN Simulator* under *SANDBOX*.

Enter your callback URL in the *IPN handler URL* then under *Transaction type* select *Cart checkout*. A large number of form fields will appear below the form. In the *item_number* field enter the Uid of your basket order so that the callback can find the right order.
