# TypoScript Konfiguration onInvoice

```typo3_TypoScript
plugin.tx_shop.settings.basketOrder {
	orderOptions {
		onInvoice {
			active = {$themes.configuration.extension.shop.checkout.onInvoice.active}
			attachProductFilesToAdminEmail = {$themes.configuration.extension.shop.checkout.onInvoice.attachProductFilesToAdminEmail}
			attachProductFilesToCustomerEmail = {$themes.configuration.extension.shop.checkout.onInvoice.attachProductFilesToCustomerEmail}
			attachInvoiceToAdminEmail = {$themes.configuration.extension.shop.checkout.onInvoice.attachInvoiceToAdminEmail}
			attachInvoiceToCustomerEmail = {$themes.configuration.extension.shop.checkout.onInvoice.attachInvoiceToCustomerEmail}
			attachDeliveryNoteToAdminEmail = {$themes.configuration.extension.shop.checkout.onInvoice.attachDeliveryNoteToAdminEmail}
			attachDeliveryNoteToCustomerEmail = {$themes.configuration.extension.shop.checkout.onInvoice.attachDeliveryNoteToCustomerEmail}
			attachOrderXmlToAdminEmail = {$themes.configuration.extension.shop.checkout.onInvoice.attachOrderXmlToAdminEmail}
			storeOrderXmlInFolder = {$themes.configuration.extension.shop.checkout.onInvoice.storeOrderXmlInFolder}
			storeOrderXmlInFolderName = {$themes.configuration.extension.shop.checkout.onInvoice.storeOrderXmlInFolderName}
			type = onInvoice
			checkoutPid = {$themes.configuration.pages.shop.checkout}
			successPid = {$themes.configuration.pages.shop.checkoutSuccess}
			errorPid = {$themes.configuration.pages.shop.checkoutError}
			service = CodingMs\Shop\Service\Checkout\OnInvoiceCheckoutService
			button {
				title = tx_shop_label.order_on_invoice_button_title
				label = tx_shop_label.order_on_invoice_button_label
				icon = fa fa-shopping-cart
			}
			fields {
				available = {$themes.configuration.extension.shop.checkout.onInvoice.fields.available}
				required = {$themes.configuration.extension.shop.checkout.onInvoice.fields.required}
			}
			email {
				customerConfirmation {
					active = {$themes.configuration.extension.shop.checkout.onInvoice.email.customerConfirmation.active}
					from {
						name = {$themes.configuration.extension.shop.email.from.name}
						email = {$themes.configuration.extension.shop.email.from.email}
					}
					to {
						# Customer address data
					}
					template = {$themes.configuration.extension.shop.email.templates.onInvoice.customerConfirmation}
				}
				order {
					active = {$themes.configuration.extension.shop.checkout.onInvoice.email.order.active}
					from {
						name = {$themes.configuration.extension.shop.email.from.name}
						email = {$themes.configuration.extension.shop.email.from.email}
					}
					to {
						name = {$themes.configuration.extension.shop.email.to.name}
						email = {$themes.configuration.extension.shop.email.to.email}
					}
					template = {$themes.configuration.extension.shop.email.templates.onInvoice.order}
				}
			}
		}
	}
}
```
