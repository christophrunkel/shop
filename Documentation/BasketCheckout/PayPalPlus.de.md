# PayPal Plus Checkout konfigurieren

>	#### Achtung: {.alert .alert-danger}
>
>	Der PayPalPlus Checkout erfordert der die Pro-Version der Shop-Erweiterung!

>	#### Achtung: {.alert .alert-danger}
>
>	Der PayPalPlus Checkout erfordert der den Eintrag der vollen URL in der Site-Konfiguration Deiner TYPO3-Instanz!

>	#### Achtung: {.alert .alert-danger}
>
>	Die Bezahloptionen des PayPalPlus Checkout sind nicht konfigurierbar. Abhängig vom Käuferland werden die verfügbaren Bezahloptionen angezeigt!



## PayPal Plus-Account

Zuerst benötigst Du einen PayPal-Account. Logge Dich damit ein und öffne `https://developer.paypal.com`. Hier musst Du nun unter dem Menüpunkt *My Apps & Credentials* eine neue App erstellen. Dabei musst Du im ersten Schritt darauf achten, ob Du einen *Live* oder *Sandbox* Account erstellen möchtest. Nach dem Erstellen solltest Du folgende Informationen erhalten haben:

*   **(Sandbox) Account:** Dies ist eine E-Mail-Adresse.
*   **Client ID:** Dies ist ein Hashwert, der den Client identifiziert.
*   **Secret:** Dies ist ebenfalls ein Hashwert. Diesen solltest Du auf jeden Fall geheim halten!

Zum Testen benötigst Du einen weiteren Account, welchen Du unter *https://developer.paypal.com/developer/accounts/* anlegen kannst.



## Callback konfigurieren

Auf developer.paypal.com muss innerhalb der App & Credentials Ansicht ein neuer Webhook hinzugefügt werden. Dieser reagiert auf das Event *Payment sale completed* und bekommt die Callback-URL aus Deinem TYPO3. Eine Callback-URL könnte wie folgt aussehen.

Um die CallBack-URL zu erzeugen, musst Du zunächst eine zusätzliche Seite vom Typ "Im Menü verborgen" an. Platziere auf der Seite ein Plugin vom Typ "Shop Checkout & Order by mail".

![Plugin Mode](https://www.coding.ms/fileadmin/extensions/shop/current/Documentation/Images/PayPalPlus_PluginMode.png "Plugin Mode") {.inline-image rel=enlarge-image}

Rufe nun diese Seite im Frontend auf und kopiere Dir die URL. An diese URL hängst Du nun alle Parameter die Du im unten genannten Beispiel siehst, ab dem Vorkommen des Fragezeichens an.

```text
Parameter: ?tx_shop_basketorder%5Baction%5D=payPalPlusCallback&tx_shop_basketorder%5Bcontroller%5D=BasketOrder

Komplette URL als Beispiel: https://test9.t3co.de/checkout?tx_shop_basketorder%5Baction%5D=payPalPlusCallback&tx_shop_basketorder%5Bcontroller%5D=BasketOrder
```
Diese URL trägst Du nun in Deinem PayPalPlus Account unter "Webhook URL" ein und erhälst dort nach dem Speichern die Webhook-ID. Die ID muss im TypoScript Konstanten Template eingetragen werden. Die ID ersetzt die Zeichenfolge "XXXx" im nachstehenden Beispiel.

```typo3_TypoScript
themes.configuration.extension.shop.checkout.payPalPlus.webhookID = XXXX
```


Mit human readable urls:
```yaml
routeEnhancers:
  ShopCheckoutPlugin:
    type: Extbase
    limitToPages:
      - 4
    extension: Shop
    plugin: BasketOrder
    routes:
      - routePath: '/pay-pal-plus-callback'
        _controller: 'BasketOrder::payPalPlusCallback'
```
```https://shop.typo3-demos.de/checkout/pay-pal-plus-callback```

>	#### Achtung: {.alert .alert-danger}
>
>	Wenn Deine Umgebung durch ein Passwort geschützt ist, kann der Webhook nicht aufgerufen werden. Bitte stelle sicher, dass der Webhook direkt aufrufbar ist.

Nach der Erstellung des Webhooks wird die Webhook ID angezeigt. Diese muss bei der Konfiguration des Checkouts berücksichtigt werden. (Siehe Abschnitt *Checkout Konfiguration*)

## Checkout Konfiguration

*   **active** Hier kann der Checkout aktiviert/deaktiviert werden
*   **checkoutPid** Hier muss die Page-Uid der Checkout-Seite angegeben werden.
*   **successPid** Hier muss die Page-Uid der Erfolgs-Seite angegeben werden. Dies ist die Seite, auf die man weitergeleitet wird, wenn die Zahlung erfolgreich war.
*   **cancelPid** Hier muss die Page-Uid der Cancel-Seite angegeben werden. Dies ist die Seite, auf die man weitergeleitet wird, wenn man den Zahlungsprozess abgebrochen hat.
*   **service** Hier wird die Checkout-Service PHP-Klasse zugewiesen. Diese kann angepasst werden, wenn etwas am Prozess verändert werden muss.
*   **sandbox** Hier kann die Sandbox zum Testen aktiviert/deaktiviert werden.
*   **payPalPlusSuccessRedirectPid** Hier muss die Page-Uid der Erfolgs-Seite angegeben werden, auf die der Nutzer nach einer erfolgreichen PayPal Authentifizierung weitergeleitet wird.
*   **webhookID** Hier muss die Webhook ID des Webhooks angegeben werden, mit dem das `Payment sale completed` Event verfolgt wird. (Siehe Abschnitt *Callback konfigurieren*)
*   **payPalPlusClientId** Hier muss die Client ID des (Sandbox) Accounts eingetragen werden, welche bei PayPal erstellt wurde
*   **payPalPlusSecret** Hier muss das Secret des (Sandbox) Accounts eingetragen werden, welche bei PayPal erstellt wurde


>	#### Achtung: {.alert .alert-danger}
>
>	Wenn die Initialisierung des PayPal-Widgets im Frontend den Fehler invalid approvalurl anzeigt, stelle sicher das eine gültige Seite für die payPalPlusSuccessRedirectPid hinterlegt wurde.



## Aktionen nach erfolgreicher Bezahlung

Wenn Du nach der erfolgreichen Bezahlung weitere Aktionen durchführen möchtest, kannst Du dazu den `PayPalPlusPaid` Event Listener nutzen.

1.	Dazu muss ein Event Listener erstellt werden:
	```php
	<?php
	namespace YourVendor\YourExtension\EventListener;
	use CodingMs\ShopPro\Event\BasketOrder\PayPalPlusPaidEvent;
	class DoSomeThingEventListener
	{
		public function __invoke(PayPalPlusPaidEvent $event): void
		{
			/* Do something */
		}
	}
	```
2.	Dieser Event Listener muss anschließend in der `Configuration/Services.yaml` registriert werden:
	```yaml
	services:
		YourVendor\YourExtension\EventListener\DoSomeThingEventListener:
			tags:
				- name: event.listener
				  identifier: 'myListener'
				  event: CodingMs\ShopPro\Event\BasketOrder\PayPalPlusPaidEvent
	```

## Debugging

Für den Fall, dass der Callback-Aufruf nicht funktioniert, kannst Du ein Debugging aktivieren. Wie dies funktioniert, kannst Du im Abschnitt *HowTo/Debugging* lesen.

