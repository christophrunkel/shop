# Configuring Stripe Checkout

>	#### Attention: {.alert .alert-danger}
>
>	Stripe Checkout requires the Pro version of the shop extension!



## Preparation

In order to use Stripe Checkout you need the following:

*   an active Stripe account
*   a composer-based installation of TYPO3



## Stripe API access credentials

1. On the Stripe login page, switch to the *Developer* -> *API key* area (https://dashboard.stripe.com/developers)
2. Create new API credentials
3. A public and a secret key will be displayed
4. These keys must be stored in TYPO3 using TypoScript constants
5. Create an endpoint secret to be able to listen to stripe events (https://dashboard.stripe.com/webhooks/create) (see chapter "Events" for more information)
6. Save the endpoint secret in the `singlePaymentIntentSucceededEndpointSecret` TypoScript constant

## Events
### Payment Intent Succeeded Event

You need to configure an event listener for the `payment_intent.succeeded` event so that your Stripe checkout knows when a payment has arrived via Stripe. The setup is done in 2 steps.
1. setup of the webhook page
2. configuration of the webhook in the Stripe dashboard

#### Setup of the webhook page

The stripe callback should call the stripeCallbackAction in the BackendOrderController. For the simplicity of the setup a short url can be configured
```yaml
  ShopCheckoutPlugin:
    type: Extbase
    limitToPages:
      - {uid of the page with the basket order plugin}
    extension: Shop
    plugin: BasketOrder
    routes:
      - routePath: '/stripe-callback'
        _controller: 'BasketOrder::stripeCallback'
```

#### Configuration of the webhook in the Stripe dashboard

1. Go to the https://dashboard.stripe.com/webhooks page and click Add Endpoint.
2. Enter the URL of the page that was set up in step "Setup of the webhook page".
3. Select "Select Events" and select `payment_intent.succeeded`.
4. click "Add Endpoint".
5. click on the newly created webhook entry and copy the endpoint secret for signature.
6. paste the key into the `singlePaymentIntentSucceededEndpointSecret` TypoScript constant

![Stripe Webhook Creation](https://www.coding.ms/fileadmin/extensions/shop/current/Documentation/Images/StripeWebhookConfiguration.png)

To test the webhook in a local development environment, the Stripe cli tool can be used. You can find more information here: https://stripe.com/docs/webhooks/quickstart#download

## Add additional actions after a successful payment

If you need actions to be carried out after successful payment, use the `StripePaid` event listener.

1.	Create a new slot:
	```php
	<?php
	namespace YourVendor\YourExtension\EventListener;
	use CodingMs\ShopPro\Event\BasketOrder\StripePaidEvent;
	class DoSomeThingEventListener
	{
		public function __invoke(StripePaidEvent $event): void
		{
			/* Do something */
		}
	}
	```
2.	Then register the event listener in `Configuration/Services.yaml`:
	```yaml
	services:
		YourVendor\YourExtension\EventListener\DoSomeThingEventListener:
			tags:
				- name: event.listener
				  identifier: 'myListener'
				  event: CodingMs\ShopPro\Event\BasketOrder\StripePaidEvent
	```



## Testing

See: https://stripe.com/docs/testing


###  Test credit card

In order to simulate/test the card payment flow from end user perspective use the following card details:

Credit card number: 4242 4242 4242 4242
CVV: 123
Exp: 12/25 (Or any other valid date in the future)



## Error handling and messages

The Stripe checkout uses the `FlashMessage.push()` JavaScript method for pushing messages. For this service you need to define a div container as wrapper for the messages, for example `<div id="shop-flash-messages"></div>`.

