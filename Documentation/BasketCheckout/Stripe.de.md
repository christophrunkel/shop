# Stripe Checkout konfigurieren

>	#### Achtung: {.alert .alert-danger}
>
>	Der Stripe Checkout erfordert der die Pro-Version der Shop-Erweiterung!



## Vorbereitungen

Um den Stripe Checkout nutzen zu können, müssen folgende Voraussetzungen erfüllt sein:

*   Du musst über ein aktiviertes Stripe-Konto verfügen
*   Du musst TYPO3 auf composer Basis verwenden



## Stripe API-Zugangsdaten

1.  Wechsel im Stripe-Login in den Bereich *Entwickler* -> *API-Schlüssel* (https://dashboard.stripe.com/developers)
2.  Erstelle hier neue API-Zugangsdaten
3.  Hier bekommst Du nun einen öffentlichen und einen Geheimschlüssel angezeigt
4.  Diese Schlüssel müssen via TypoScript-Konstanten im TYPO3 hinterlegt werden
5.  Erstellen Sie ein Endpoint-Secret, um Stripe-Events empfangen zu können (https://dashboard.stripe.com/webhooks/create) (siehe Kapitel "Events" für weitere Informationen)
6.  Kopieren Sie das Endpoint Secret in die `singlePaymentIntentSucceededEndpointSecret` TypoScript Konstante

## Events
### Payment Intent Succeeded Event

Sie müssen einen Event-Listener für das "payment_intent.succeeded" Event konfigurieren, damit das Stripe Checkout weiß, wenn eine Zahlung über Stripe eingegangen ist. Die Einrichtung erfolgt in 2 Schritten.
1. Einrichtung der Webhook-Seite
2. Konfiguration des Webhooks im Stripe Dashboard

#### Setup der Webhook-Seite

Der Stripe-Callback sollte die stripeCallbackAction im BackendOrderController aufrufen. Zur Vereinfachung der Einrichtung kann eine kurze Url konfiguriert werden
```yaml
  ShopCheckoutPlugin:
    type: Extbase
    limitToPages:
      - {uid der Seite mit dem Warenkorb-Bestell-Plugin}
    extension: Shop
    plugin: BasketOrder
    routes:
      - routePath: '/stripe-callback'
        _controller: 'BasketOrder::stripeCallback'
```

#### Konfiguration des Webhooks im Stripe Dashboard

1. Gehen Sie auf die Seite https://dashboard.stripe.com/webhooks und klicken Sie auf Endpunkt hinzufügen.
2. Geben Sie die URL der Seite ein, die im Schritt "Einrichten der Webhook-Seite" eingerichtet wurde.
3. Wählen Sie "Select Events" und wählen Sie `payment_intent.succeeded`.
4. klicken Sie auf "Endpunkt hinzufügen".
5. Klicken Sie auf den neu erstellten Webhook-Eintrag und kopieren Sie das Endpoint Secret.
6. Fügen Sie das Endpoint Secret in die `singlePaymentIntentSucceededEndpointSecret` TypoScript Konstante ein.

![Stripe Webhook Creation](https://www.coding.ms/fileadmin/extensions/shop/current/Documentation/Images/StripeWebhookConfiguration.png)

Zum Testen des Webhooks in einer lokalen Entwicklungsumgebung kann das Stripe cli Tool verwendet werden. Mehr Informationen dazu finden Sie hier: https://stripe.com/docs/webhooks/quickstart#download

## Aktionen nach erfolgreicher Bezahlung

Wenn Du nach der erfolgreichen Bezahlung weitere Aktionen durchführen möchtest, kannst Du dazu den `StripePaid` Event Listener nutzen.

1.	Dazu muss ein Event Listener erstellt werden:
	```php
	<?php
	namespace YourVendor\YourExtension\EventListener;
	use CodingMs\ShopPro\Event\BasketOrder\StripePaidEvent;
	class DoSomeThingEventListener
	{
		public function __invoke(StripePaidEvent $event): void
		{
			/* Do something */
		}
	}
	```
2.	Dieser Event Listener muss anschließend in der `Configuration/Services.yaml` registriert werden:
	```yaml
	services:
		YourVendor\YourExtension\EventListener\DoSomeThingEventListener:
			tags:
				- name: event.listener
				  identifier: 'myListener'
				  event: CodingMs\ShopPro\Event\BasketOrder\StripePaidEvent
	```



## Testing

Siehe: https://stripe.com/docs/testing


###  Test Credit Card

Um den Kartenzahlungsfluss aus der Sicht des Endbenutzers zu simulieren/testen, verwendest Du die folgenden Kartendetails im geladenen Klarna-Widget:

Credit card number: 4242 4242 4242 4242
CVV: 123
Exp: 12/25 (Oder ein anderes gültiges Datum in der Zukunft)



## Fehlerbehandlung und Meldungen

Der Stripe-Checkout verwendet die JavaScript-Methode `FlashMessage.push()` zum Pushen von Nachrichten. Für diesen Dienst musst Du einen div-Container als Wrapper für die Nachrichten definieren, zum Beispiel `<div id="shop-flash-messages"></div>`.
