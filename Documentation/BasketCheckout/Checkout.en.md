# Basket Checkout

The Shop extension comes with 7 predefined checkout types. These are:

- `request`
- `prePayment`
- `onInvoice`
- `payPal` (requires Pro Version of the Shop extension)
- `payPalPlus` (requires Pro Version of the Shop extension)
- `klarna` (requires Pro Version of the Shop extension)
- `stripe` (requires Pro Version of the Shop extension)

## General configuration

- `checkoutPid` Enter the page UID of the checkout page.
- `minimummOrderValue` Enter the minimum order value (in cents).
- `timeForPayment` Enter the giving amount of days for payment.
- `deliveryTime` Enter an optional text for describing the delivery time for payment.
- `orderOptions` Define the checkout types here. We will look at this in more detail in another section.
- `fieldDefinition` Configure the form fields for checkout. These are configured in the respective TypoScript node to make them available in the various checkouts.

Further checkout-specific configurations can be found on the relevant documentation page.

## TypoScript configuration checkout basis

```typo3_TypoScript
plugin.tx_shop.settings.basketOrder {
	# Page-ID des Checkout
	checkoutPid = {$themes.configuration.pages.shop.checkout}
	# Minimum order value in cents
	minimumOrderValue = {$themes.configuration.extension.shop.basket.minimumOrderValue}
	# Time for payment in days default value
	timeForPayment = {$themes.configuration.extension.shop.basketOrder.timeForPayment}
	# Default string for delivery time notice in basket above the additional costs
	deliveryTime =
	# VAT information depending on country and b2b/b2c
	vat {
		b2b {
			inland {
				notice = Es handelt sich um eine Inland-Lieferung an ein Unternehmen.<br />Ihre USt.-ID: %1$s
			}
			europeanUnion {
				# Parameter is VAT-ID in basket order
				notice = Es handelt sich um eine steuerbefreite innergemeinschaftliche Lieferung gem. §4 Nr. 1b UstG.<br />Unsere USt.-ID: DE 123 456 789<br />Ihre USt.-ID: %1$s
			}
			thirdCountry {
				notice = Es handelt sich um eine steuerbefreite Ausfuhrlieferung gem. §4 Nr. 1a UstG.
			}
		}
		b2c {
			inland {
				notice = Es handelt sich um eine Inland-Lieferung an eine Privatperson.
			}
			europeanUnion {
				notice = Es handelt sich um eine innergemeinschaftliche Lieferung an eine Privatperson.
			}
			thirdCountry {
				notice = Es handelt sich um eine steuerbefreite Ausfuhrlieferung gem. §4 Nr. 1a UstG.
			}
		}
	}
	orderOptions {
		# Different order options
	}
}
```
