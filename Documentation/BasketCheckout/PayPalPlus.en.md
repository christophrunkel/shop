# Configure PayPal Plus Checkout

>	#### Attention: {.alert .alert-danger}
>
>	PayPalPlus Checkout requires the Pro version of the shop extension!

>	#### Attention: {.alert .alert-danger}
>
>	The PayPalPlus Checkout requires the entry of the full URL in the site configuration of your TYPO3 instance!

>	#### Attention: {.alert .alert-danger}
>
>	The payment options of the PayPalPlus Checkout are not configurable. Depending on the country and the possible payment options are displayed.!


## PayPal Plus Account

You will need a PayPal account. Log-in to PayPal and open `https://developer.paypal.com`. Go to *My Apps & Credentials* in the menu and create a new app. Take note of whether you want to create a  *Live* or a *Sandbox* account. After creating it, you should have the following information

*   **(Sandbox) Account:** This is an email address.
*   **Client ID:** This is a hash value that identifies the client.
*   **Secret:** This is also a hash value. Make sure you keep it secret!

For testing purposes you will need an extra account which you can set up under *https://developer.paypal.com/developer/accounts/*.



## Configure callback

Create a new webhook in the App & Credentials view on developer.paypal.com. The webhook is triggered by a *Payment sale completed* event and will receive the callback URL from your TYPO3.

To create the CallBack URL, you must first create an additional page of type "Hidden in menu". Place a plugin of type "Shop Checkout & Order by mail" on the page.

![Plugin Mode](https://www.coding.ms/fileadmin/extensions/shop/current/Documentation/Images/PayPalPlus_PluginMode.png "Plugin Mode") {.inline-image rel=enlarge-image}

Now call this page in the frontend and copy the URL. Now append to this URL all the parameters you see in the example below, starting with the occurrence of the question mark.

```text
Parameter: ?tx_shop_basketorder%5Baction%5D=payPalPlusCallback&tx_shop_basketorder%5Bcontroller%5D=BasketOrder

Full URL as example: https://test9.t3co.de/checkout?tx_shop_basketorder%5Baction%5D=payPalPlusCallback&tx_shop_basketorder%5Bcontroller%5D=BasketOrder
```
Now enter this URL in your PayPalPlus account under "Webhook URL" and get the Webhook ID there after saving. The ID must be entered in the TypoScript constants template. The ID replaces the string "XXXx" in the example below.

```typo3_TypoScript
themes.configuration.extension.shop.checkout.payPalPlus.webhookID = XXXX
```

Using human readable urls:
```yaml
routeEnhancers:
  ShopCheckoutPlugin:
    type: Extbase
    limitToPages:
      - 4
    extension: Shop
    plugin: BasketOrder
    routes:
      - routePath: '/pay-pal-plus-callback'
        _controller: 'BasketOrder::payPalPlusCallback'
```
```https://shop.typo3-demos.de/checkout/pay-pal-plus-callback```

>	#### Warning: {.alert .alert-danger}
>
>	If your environment is password protected the webhook can't be executed. Please make sure that the webhook can be accessed.

When a webhook is created the webhook ID is displayed. This ID is important for configuring checkout. (See *Checkout configuration* section)

## Checkout configuration

*   **active** Activate/deactivate checkout
*   **checkoutPid** Enter the page Uid of the checkout page.
*   **successPid** Enter the page Uid of the success page. This is the page you will be redirected to after successful payment.
*   **cancelPid** Enter the page Uid of the cancel page. This is the page you will be redirected to if you cancel the payment process.
*   **service** The checkout service PHP class is assigned here. This can be modified if the process needs to be changed.
*   **sandbox** Activate/deactivate the sandbox.
*   **payPalPlusSuccessRedirectPid** Enter the page Uid of the success page to which the user will be redirected after successful PayPal authentication.
*   **webhookID** Enter the webhook ID of the webhook triggered by the `Payment sale completed` event. (See section *Configure callback*)
*   **payPalPlusClientId** Enter the client ID of the (sandbox) account created in PayPal.
*   **payPalPlusSecret** Enter the Secret of the (sandbox) account that was created in PayPal.

>	#### Warning: {.alert .alert-danger}
>
>	If the initialization of the PayPal widget in the frontend shows the error invalid approvalurl, make sure that a valid page was stored for the payPalPlusSuccessRedirectPid.



## Add additional actions after a successful payment

If you need actions to be carried out after successful payment, use the `PayPalPlusPaid` event listener.

1.	Create a new slot:
	```php
	<?php
	namespace YourVendor\YourExtension\EventListener;
	use CodingMs\ShopPro\Event\BasketOrder\PayPalPlusPaidEvent;
	class DoSomeThingEventListener
	{
		public function __invoke(PayPalPlusPaidEvent $event): void
		{
			/* Do something */
		}
	}
	```
2.   Then register the event listener in `Configuration/Services.yaml`:
	```yaml
	services:
		YourVendor\YourExtension\EventListener\DoSomeThingEventListener:
			tags:
				- name: event.listener
				  identifier: 'myListener'
				  event: CodingMs\ShopPro\Event\BasketOrder\PayPalPlusPaidEvent
	```

## Debugging

If a callback does not work, you can activate debugging. Read how this works in the *How To/Debugging* section.

