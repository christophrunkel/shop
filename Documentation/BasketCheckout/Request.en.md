# TypoScript configuration order onRequest

```typo3_TypoScript
plugin.tx_shop.settings.basketOrder {
	orderOptions {
		request {
			active = {$themes.configuration.extension.shop.checkout.request.active}
			attachProductFilesToAdminEmail = {$themes.configuration.extension.shop.checkout.request.attachProductFilesToAdminEmail}
			attachProductFilesToCustomerEmail = {$themes.configuration.extension.shop.checkout.request.attachProductFilesToCustomerEmail}
			attachInvoiceToAdminEmail = {$themes.configuration.extension.shop.checkout.request.attachInvoiceToAdminEmail}
			attachInvoiceToCustomerEmail = {$themes.configuration.extension.shop.checkout.request.attachInvoiceToCustomerEmail}
			attachDeliveryNoteToAdminEmail = {$themes.configuration.extension.shop.checkout.request.attachDeliveryNoteToAdminEmail}
			attachDeliveryNoteToCustomerEmail = {$themes.configuration.extension.shop.checkout.request.attachDeliveryNoteToCustomerEmail}
			attachOrderXmlToAdminEmail = {$themes.configuration.extension.shop.checkout.request.attachOrderXmlToAdminEmail}
			storeOrderXmlInFolder = {$themes.configuration.extension.shop.checkout.request.storeOrderXmlInFolder}
			storeOrderXmlInFolderName = {$themes.configuration.extension.shop.checkout.request.storeOrderXmlInFolderName}
			type = request
			checkoutPid = {$themes.configuration.pages.shop.checkout}
			successPid = {$themes.configuration.pages.shop.checkoutSuccess}
			errorPid = {$themes.configuration.pages.shop.checkoutError}
			service = CodingMs\Shop\Service\Checkout\RequestCheckoutService
			button {
				title = tx_shop_label.request_button_title
				label = tx_shop_label.request_button_label
				icon = fa fa-shopping-cart
			}
			fields {
				available = {$themes.configuration.extension.shop.checkout.request.fields.available}
				required = {$themes.configuration.extension.shop.checkout.request.fields.required}
			}
			email {
				customerConfirmation {
					active = {$themes.configuration.extension.shop.checkout.request.email.customerConfirmation.active}
					from {
						name = {$themes.configuration.extension.shop.email.from.name}
						email = {$themes.configuration.extension.shop.email.from.email}
					}
					to {
						# Customer address data
					}
					template = {$themes.configuration.extension.shop.email.templates.request.customerConfirmation}
				}
				order {
					active = {$themes.configuration.extension.shop.checkout.request.email.order.active}
					from {
						name = {$themes.configuration.extension.shop.email.from.name}
						email = {$themes.configuration.extension.shop.email.from.email}
					}
					to {
						name = {$themes.configuration.extension.shop.email.to.name}
						email = {$themes.configuration.extension.shop.email.to.email}
					}
					template = {$themes.configuration.extension.shop.email.templates.request.request}
				}
			}
		}
	}
}
```
