# Field definitions

Fields for checkout can be defined like described on this page. Basically you define a set of fields and assign it afterwards to your different checkouts. In that way you're able to have different fields on each checkout type.

All field definitions must be inserted in `plugin.tx_shop.settings.basketOrder.fieldDefinition` in which the key acts as the internal field name.

| Attribute             | Description                                                                                                                                                                                                                                                                                                                      |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| label                 | Label for the field. It supports `a` and `span` HTML tags for style manipulation and checkbox links.                                                                                                                                                                                                                             |
| placeholder           | Placeholder text for input fields.                                                                                                                                                                                                                                                                                               |
| type                  | Possible types: `input` as a simple text input, `country` as a country select-box, `textarea` as a multiline textarea, `checkbox` as a simple checkbox, `select` as a select box.                                                                                                                                                |
| eval                  | For example `trim` on type=input for removing spaces before/after, `email` on type=input for validating an email, `phone` on type=input for validating a phone number.                                                                                                                                                           |
| default               | Defines a default value which will be inserted on first load of the form. This is useful for example for selecting the `DE` country in the country selection by default.                                                                                                                                                         |
| options               | Defines the options for the select box, like: `options.0.value = male` for the select box option value of the first item and `options.0.label = Male` for the label - see example below.                                                                                                                                         |
| linkPid               | Only for checkboxes. Can contain a page uid for inserting a link for example to terms or privacy. For this the label must contain a `%s` - for example `I've read the <a href="%s" target="_blank">privacy protection and accept them</a>`. A value could be a TypoScript constant as well `{$themes.configuration.pages.terms}` |
| errors                | Contains one or more validations.                                                                                                                                                                                                                                                                                                |
| errors.isEmpty        | Validator which requires text in an input. The value must contain the error message.                                                                                                                                                                                                                                             |
| errors.isInvalidEmail | Validator which requires a valid email address in an input. The value must contain the error message.                                                                                                                                                                                                                            |
| deliveryAddressToggle | This attribute defines the divergent delivery address checkbox. It must be simply set to `deliveryAddressToggle = 1`.                                                                                                                                                                                                            |
| deliveryAddress       | This attribute defines that a field belongs to the divergent delivery address. All fields with `deliveryAddress = 1` will be toggled by the `deliveryAddressToggle` checkbox.                                                                                                                                                    |

The predefined fields can be added to the checkout like this:

```typo3_typoscript
plugin.tx_shop.settings.basketOrder {
	orderOptions {
		onInvoice {
			fields {
				available = firstname, lastname, street, houseNumber, postalCode, city, country, phone, email, termsConfirmed
				required = firstname, lastname, country, phone, email, termsConfirmed
			}
		}
	}
}
```

`avaiable` defines the fields which are visible and `required` the fields which are mandatory.

>	#### Attention: {.alert .alert-danger}
>
>	Depending on Payment type different fields must be required - for example the country.




## Already predefined fields

```typo3_TypoScript
plugin.tx_shop.settings.basketOrder {
	fieldDefinition {

		company {
			label = Company
			placeholder = Your company
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your company name
			}
		}
		vatId {
			label = VAT-ID
			placeholder = Your VAT-ID
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your VAT-ID
			}
		}
		gender {
			label = Gender
			type = select
			eval = trim
			options {
				0 {
					value = male
					label = Male
				}
				1 {
					value = female
					label = Female
				}
				2 {
					value = other
					label = Other
				}
			}
		}
		firstname {
			label = Firstname
			placeholder = Your firstname
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your firstname
			}
		}
		lastname {
			label = Lastname
			placeholder = Your lastname
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your lastname
			}
		}
		street {
			label = Street
			placeholder = Your street
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your street
			}
		}
		houseNumber {
			label = Housenumber
			type = input
			eval = trim
			placeholder = Your housenumber
			errors {
				isEmpty = Please enter your housenumber
			}
		}
		postalCode {
			label = Postal code
			placeholder = Your postal code
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your postal code
			}
		}
		city {
			label = City
			placeholder = Your city
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your city
			}
		}
		country {
			label = Land
			type = country
			eval = trim
			default = DE
			errors {
				isEmpty = Please choose your country
			}
		}
		phone {
			label = Phone
			placeholder = Your phone number
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your phone number
			}
		}
		email {
			label = Email
			placeholder = Your email
			type = input
			eval = trim,email
			errors {
				isEmpty = Please enter your email address
				isInvalidEmail = Please enter a valid mail address
			}
		}
		message {
			label = Message
			placeholder = Your message
			type = textarea
			eval = trim
			errors {
				isEmpty = Please enter a message
			}
		}

		privacyProtectionConfirmed {
			label = I've read the <a href="%s" target="_blank">privacy protection and accept them</a>
			type = checkbox
			eval = trim
			linkPid = {$themes.configuration.pages.privacy}
			errors {
				isEmpty = Please accept our privacy protection
			}
		}
		termsConfirmed {
			label = I've read the <a href="%s" target="_blank">terms</a> and accept them
			type = checkbox
			eval = trim
			linkPid = {$themes.configuration.pages.terms}
			errors {
				isEmpty = Please accept our terms
			}
		}
		disclaimerConfirmed {
			label = I've read the <a href="%s" target="_blank">disclaimer</a> and accept them
			type = checkbox
			eval = trim
			linkPid = {$themes.configuration.pages.disclaimer}
			errors {
				isEmpty = Please accept the disclaimer
			}
		}

		deliveryAddressEnabled {
			type = checkbox
			deliveryAddressToggle = 1
			label = Deviating delivery address
		}
		deliveryAddressCompany {
			label = Company
			type = input
			eval = trim
			deliveryAddress = 1
			placeholder = Your company
			errors {
				isEmpty = Please enter your company name
			}
		}
		deliveryAddressFirstname {
			label = Firstname
			type = input
			eval = trim
			deliveryAddress = 1
			placeholder = Your firstname
			errors {
				isEmpty = Please enter your firstname
			}
		}
		deliveryAddressLastname {
			label = Lastname
			placeholder = Your lastname
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your lastname
			}
		}
		deliveryAddressStreet {
			label = Street
			placeholder = Your street
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your street
			}
		}
		deliveryAddressHouseNumber {
			label = Housenumber
			type = input
			eval = trim
			deliveryAddress = 1
			placeholder = Your housenumber
			errors {
				isEmpty = Please enter your housenumber
			}
		}
		deliveryAddressPostalCode {
			label = Postal code
			placeholder = Your postal code
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your postal code
			}
		}
		deliveryAddressCity {
			label = City
			placeholder = Your city
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your city
			}
		}
		deliveryAddressCountry {
			label = Country
			type = country
			eval = trim
			default = DE
			deliveryAddress = 1
			errors {
				isEmpty = Please choose your country
			}
		}
		deliveryAddressPhone {
			label = Phone
			placeholder = Your phone number
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your phone number
			}
		}

	}
}
```


## Translation of field definitions

Translations for defined fields can be provided like this:

```typo3_typoscript
[siteLanguage("locale") == "de_DE.UTF-8"]
	plugin.tx_shop.settings.basketOrder.fieldDefinition {
		company {
			label = Firma
			placeholder = Ihre Firma
			errors {
				isEmpty = Bitte geben Sie Ihre Firma ein
			}
		}
	}
[END]
```
