# Feld Definitionen

Felder für den Checkout können wie auf dieser Seite beschrieben definiert werden. Grundsätzlich definieren Sie eine Reihe von Feldern und weisen diese anschließend Ihren verschiedenen Checkouts zu. Auf diese Weise können Sie für jeden Checkout-Typ unterschiedliche Felder haben.

Alle Felddefinitionen müssen in `plugin.tx_shop.settings.basketOrder.fieldDefinition` eingefügt werden, wobei der Schlüssel als interner Feldname fungiert.

| Attribut             | Bweschreibung                                                                                                                                                                                                                                                                                                                                                                      |
|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| label                | Label für das Feld. Es unterstützt `a`  und `span` HTML-Tags zur Stilmanipulation und Checkbox-Links.                                                                                                                                                                                                                                                                              |
| placeholder          | Platzhaltertext für Eingabefelder.                                                                                                                                                                                                                                                                                                                                                 |
| type                 | Mögliche Typen:  `input` als einfache Texteingabe, `country` als Länderauswahlfeld, `textarea` als mehrzeiliges Textfeld, `checkbox` als ein einfaches checkbox, `select` als Auswahlfeld.                                                                                                                                                                                         |
| eval                 | Zum Beispiel `trim` bei type=input entfernt Leerzeichen davor/danach, `email` bei type=input validiert die eingegebene email, `phone` bei type=input validiert die Teleonnummer.                                                                                                                                                                                                   |
| default              | Definiert einen Standardwert, der beim ersten Laden des Formulars eingefügt wird. Dies ist beispielsweise sinnvoll, um in der Länderauswahl standardmäßig das Land `DE` auszuwählen.                                                                                                                                                                                               |
| options              | Definiert die Optionen für das Auswahlfeld, wie : `options.0.value = male` für den Optionswert des Auswahlfelds des ersten Elements und `options.0.label = Male` für das Label - siehe Beispiel unten.                                                                                                                                                                             |
| linkPid              | Nur für checkbox. Kann eine Seiten-UID zum Einfügen eines Links enthalten, beispielsweise zu Bedingungen oder zum Datenschutz. Dazu muss das Label ein `%s` enthalten - zum Beispiel `Ich habe die <a href="%s" target="_blank">Datenschutzbestimmungen gelesen und akzeptiere diese</a>`. Ein Wert kann auch eine TypoScript-Konstante sein `{$themes.configuration.pages.terms}` |
| errors               | Enthält eine oder mehrere Validierungen inkl. Fehlermeldungen.                                                                                                                                                                                                                                                                                                                     |
| errors.isEmpty       | Validator, der Text in einer Eingabe erfordert. Der Wert muss die Fehlermeldung enthalten.                                                                                                                                                                                                                                                                                         |
| errors.isInvalidEmail | Validator, der eine gültige E-Mail-Adresse in einer Eingabe erfordert. Der Wert muss die Fehlermeldung enthalten.                                                                                                                                                                                                                                                                  |
| deliveryAddressToggle | Dieses Attribut definiert das Checkbox abweichende Lieferadresse. Es muss einfach auf `deliveryAddressToggle = 1` gesetzt werden.                                                                                                                                                                                                                                                  |
| deliveryAddress      | Dieses Attribut definiert, dass ein Feld zur abweichenden Lieferadresse gehört. Alle Felder mit `deliveryAddress = 1` werden durch das Checkbox `deliveryAddressToggle` umgeschaltet.                                                                                                                                                                                              |

Vordefinierte Felder können wir folgt zu den unterschiedlichen Checkouts hinzugefügt werden:

```typo3_typoscript
plugin.tx_shop.settings.basketOrder {
	orderOptions {
		onInvoice {
			fields {
				available = firstname, lastname, street, houseNumber, postalCode, city, country, phone, email, termsConfirmed
				required = firstname, lastname, country, phone, email, termsConfirmed
			}
		}
	}
}
```

`avaiable` definiert sichtbare Felder und `required` die Pflichtfelder.

>	#### Achtung: {.alert .alert-danger}
>
>	Abhängig von der Bezahlart sind verschiedene Felder erforderlich - bspw. die Landes-Auswahl.



## Bereits vordefinierte Felder

```typo3_TypoScript
plugin.tx_shop.settings.basketOrder {
	fieldDefinition {

		company {
			label = Company
			placeholder = Your company
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your company name
			}
		}
		vatId {
			label = VAT-ID
			placeholder = Your VAT-ID
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your VAT-ID
			}
		}
		gender {
			label = Gender
			type = select
			eval = trim
			options {
				0 {
					value = male
					label = Male
				}
				1 {
					value = female
					label = Female
				}
				2 {
					value = other
					label = Other
				}
			}
		}
		firstname {
			label = Firstname
			placeholder = Your firstname
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your firstname
			}
		}
		lastname {
			label = Lastname
			placeholder = Your lastname
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your lastname
			}
		}
		street {
			label = Street
			placeholder = Your street
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your street
			}
		}
		houseNumber {
			label = Housenumber
			type = input
			eval = trim
			placeholder = Your housenumber
			errors {
				isEmpty = Please enter your housenumber
			}
		}
		postalCode {
			label = Postal code
			placeholder = Your postal code
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your postal code
			}
		}
		city {
			label = City
			placeholder = Your city
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your city
			}
		}
		country {
			label = Land
			type = country
			eval = trim
			default = DE
			errors {
				isEmpty = Please choose your country
			}
		}
		phone {
			label = Phone
			placeholder = Your phone number
			type = input
			eval = trim
			errors {
				isEmpty = Please enter your phone number
			}
		}
		email {
			label = Email
			placeholder = Your email
			type = input
			eval = trim,email
			errors {
				isEmpty = Please enter your email address
				isInvalidEmail = Please enter a valid mail address
			}
		}
		message {
			label = Message
			placeholder = Your message
			type = textarea
			eval = trim
			errors {
				isEmpty = Please enter a message
			}
		}

		privacyProtectionConfirmed {
			label = I've read the <a href="%s" target="_blank">privacy protection and accept them</a>
			type = checkbox
			eval = trim
			linkPid = {$themes.configuration.pages.privacy}
			errors {
				isEmpty = Please accept our privacy protection
			}
		}
		termsConfirmed {
			label = I've read the <a href="%s" target="_blank">terms</a> and accept them
			type = checkbox
			eval = trim
			linkPid = {$themes.configuration.pages.terms}
			errors {
				isEmpty = Please accept our terms
			}
		}
		disclaimerConfirmed {
			label = I've read the <a href="%s" target="_blank">disclaimer</a> and accept them
			type = checkbox
			eval = trim
			linkPid = {$themes.configuration.pages.disclaimer}
			errors {
				isEmpty = Please accept the disclaimer
			}
		}

		deliveryAddressEnabled {
			type = checkbox
			deliveryAddressToggle = 1
			label = Deviating delivery address
		}
		deliveryAddressCompany {
			label = Company
			type = input
			eval = trim
			deliveryAddress = 1
			placeholder = Your company
			errors {
				isEmpty = Please enter your company name
			}
		}
		deliveryAddressFirstname {
			label = Firstname
			type = input
			eval = trim
			deliveryAddress = 1
			placeholder = Your firstname
			errors {
				isEmpty = Please enter your firstname
			}
		}
		deliveryAddressLastname {
			label = Lastname
			placeholder = Your lastname
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your lastname
			}
		}
		deliveryAddressStreet {
			label = Street
			placeholder = Your street
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your street
			}
		}
		deliveryAddressHouseNumber {
			label = Housenumber
			type = input
			eval = trim
			deliveryAddress = 1
			placeholder = Your housenumber
			errors {
				isEmpty = Please enter your housenumber
			}
		}
		deliveryAddressPostalCode {
			label = Postal code
			placeholder = Your postal code
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your postal code
			}
		}
		deliveryAddressCity {
			label = City
			placeholder = Your city
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your city
			}
		}
		deliveryAddressCountry {
			label = Country
			type = country
			eval = trim
			default = DE
			deliveryAddress = 1
			errors {
				isEmpty = Please choose your country
			}
		}
		deliveryAddressPhone {
			label = Phone
			placeholder = Your phone number
			type = input
			eval = trim
			deliveryAddress = 1
			errors {
				isEmpty = Please enter your phone number
			}
		}

	}
}
```


## Übersetzungen von Feld Definitionen

Übersetzungen für Feld Definitionen können wie folgt bereitgestellt werden:

```typo3_typoscript
[siteLanguage("locale") == "de_DE.UTF-8"]
	plugin.tx_shop.settings.basketOrder.fieldDefinition {
		company {
			label = Firma
			placeholder = Ihre Firma
			errors {
				isEmpty = Bitte geben Sie Ihre Firma ein
			}
		}
	}
[END]
```
