# Automatische Rechnungsgenerierung

*   Produkte bekommen je Container eine fortlaufende Nummer
*   Die neue Rechnungsnummer wird via Service generiert. Mithilfe von Dependency-Injection kann dieser Service ersetzt und somit die Rechnungsnummer-Generierung manipuliert werden.
*   Das Rechnungslayout kann via Fluid (EXT:fluid_fpdf) erstellt werden und über TypoScript je Seite/Zweig konfiguriert werden


## Ausweisung der Mehrwertsteuer/Umsatzsteuer

| Ziel-Land | B2B (Firma)           | B2C (Privat)          |
|:----------|:----------------------|:----------------------|
| Inland    | MwSt. ausweisen       | MwSt. ausweisen       |
| EU        | Keine MwSt. ausweisen | MwSt. ausweisen       |
| Drittland | Keine MwSt. ausweisen | Keine MwSt. ausweisen |


##  Hinweise zur Mehrwertsteuer/Umsatzsteuer

| Ziel-Land | B2B (Firma)                                                                                                                              | B2C (Privat)                                                                 |
|:----------|:-----------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------|
| Inland    | -                                                                                                                                        | -                                                                            |
| EU        | Es handelt sich um eine steuerbefreite innergemeinschaftliche Lieferung gem. §4 Nr. 1b UstG. Hier muss die USt.-ID mit angegeben werden! | -                                                                            |
| Drittland | Es handelt sich um eine steuerbefreite Ausfuhrlieferung gem. §4 Nr. 1a UstG.                                                             | Es handelt sich um eine steuerbefreite Ausfuhrlieferung gem. §4 Nr. 1a UstG. |
