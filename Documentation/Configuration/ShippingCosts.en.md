# Shipping

With the help of the shipping cost records, you are able to define shipping costs based on weight and delivery country.

The logic in detail:

- Each product has a field in which its weight can be specified and a checkbox that can be used to indicate whether a product is bulky or not (i.e. additional costs may apply for delivery).
- The weights of all products contained in the basket order are now added up - this is the total weight.
- If at least one product in the shopping cart is marked as _bulky_, the _bulky_ flat rate will be added to the shipping costs.
- Now several shipping cost data sets can be created, for example _up to 2 kg -> 2 €_, _up to 10 kg -> 10 €_, etc.
- The shipping cost data set that comes closest to the weight of the shopping cart (next higher!) is now determined in the checkout.
- If you need different shipping costs in a specific delivery country, simply create an overlay with the country code. Your main data set is therefore the fallback if you have not created a different overlay.
