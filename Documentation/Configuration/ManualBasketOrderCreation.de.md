# Manuelle Erstellung von Bestellung im Shop-Backend-Modul

Im Shop-Backend-Modul können unter *Manage basket orders* > *Create a new basket order*
neue Bestellungen manuell erstellt werden. Das Formular zur Erfassung der Kundendaten kann
konfiguriert werden, indem die Folgende TypoScript Variable überschrieben wird:

```typo3_typoscript
plugin.tx_shop.settings.basketOrder.orderOptions.manual.fields.available = company, firstname, lastname, phone, email, message, privacyProtectionConfirmed, termsConfirmed, deliveryAddressEnabled, deliveryAddressCompany, deliveryAddressFirstname, deliveryAddressLastname
```
