# Versandkosten

Mit Hilfe der Versandkosten-Datensätze bist du in der Lage, Versandkosten gestaffelt nach Gewicht und nach Lieferland abweichend zu definieren.

Die Logik hierbei ist wie folgt:

- Jedes Produkt hat ein Feld in dem sein Gewicht angegeben werden kann und eine Checkbox mit der angegebenw werden kann, ob ein Produkt sperrig ist (d.h. eventuell zusätzliche Kosten bei der Lieferung anfallen).
- Im Warenkorb werden nun die Gewichte aller enthaltenen Produkte summiert - dies ist das Gesamt-Gewicht.
- Wenn mindestens ein Produkt im Warenkorb als _sperrig_ markiert ist, wird die _sperrig_-Pauschale zu den Versandkosten addiert.
- Nun können mehrere Versandkosten-Datensätze erstellt werden, bspw. _bis 2 kg -> 2 €_, _bis 10 Kg -> 10 €_, etc.
- Im Checkout wird nun der Versandkosten-Datensatz ermittelt, welcher dem Gewicht des Warenkorb am nächsten kommt (nächst höher!).
- Wenn Du in einem speziellen Lieferland abweichende Versandkosten benötigst, erstellst du einfach ein Overlay mit der Länderkennung. Dein Haupt-Datensatz ist somit der Fallback, wenn du kein abweichendes Overlay erstellt hast.
