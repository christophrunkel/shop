# Tax rates

Since version 4 of this shop taxes wil be defined with tax-rate records. Here, each tax percentage represents a record. It is also possible to create a different tax rates for each delivery country (as an overlay in a tax rate record).

>	#### Attention: {.alert .alert-danger}
>
>	When updating the shop between version 3 and 4 - don't forget to run the upgrade wizards!

If you're using Stripe with subscriptions, you need to define a related tax-record in the Stripe Platform. For this you need to log-in on dashboard.stripe.com and click on _Products_ -> Tax rates - here you need to create a record for each tax percentage. If you're selling in different countries, it might make sense to create different records each country and tax percentage. Important is, that you need to insert the related Stripe tax id into your TYPO3 Shop tax record, so that they are connected. A Stripe tax id usually starts with `txr_…`.
