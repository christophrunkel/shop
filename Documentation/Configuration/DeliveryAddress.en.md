# Alternative delivery address

From EXT: shop version 2.2.0 upwards a different delivery address can be entered in the checkout form.

## Configuration

To activate this function, the following TypoScript variables need to be overwritten:

```typo3_typoscript
plugin.tx_shop.settings.basketOrder.onInvoice.fields {
    available = deliveryAddressEnabled, deliveryAddressCompany, deliveryAddressFirstname, deliveryAddressLastname, deliveryAddressStreet, deliveryAddressPostalCode, deliveryAddressCity
    required = deliveryAddressCompany, deliveryAddressFirstname, deliveryAddressLastname, deliveryAddressStreet, deliveryAddressPostalCode, deliveryAddressCity
}
```

The TypoScript predefined field `deliveryAddressEnabled` is a checkbox. If this checkbox is set, the fields listed in the `plugin.tx_shop.settings.basketOrder.onInvoice.fields.required` variable will be validated when the form is submitted. If not, the fields are treated as optional fields.

## Predefined fields and field definitions

The `deliveryAddressCompany`, `deliveryAddressFirstname`, `deliveryAddressLastname`, `deliveryAddressStreet`, `deliveryAddressPostalCode` and `deliveryAddressCity` fields are predefined.

You can also define your own fields by adding TypoScript variables as follows:

```typo3_typoscript
plugin.tx_shop.settings.basketOrder.fieldDefinition {
    deliveryAddressHouseNumber {
    	label = house number
    	type = input
    	eval = trim
    	deliveryAddress = 1
    	placeholder = house number
    	errors {
    		isEmpty = Please enter your house number
    	}
    }
}
```

The `deliveryAddress = 1` flag ensures that the field is treated as an alternative delivery address and is only validated if needed.
