# Graduated pricing

Display graduated prices as follows:

*   The unit price (per the stored packaging unit type) is displayed in the product data record,
    i.e. `{product.price}` or `{product.priceAsFloat}` returns the price of only 1 unit of the product.
*   The data record can also display the price of a specific quantity X of the product.
*   The basket/basket service then calculates the price of the current basket. It also checks the amount of units bought and determines any price reductions.
*   Note: Graduated prices cannot be used together with digital products, as the number of digital products in the shopping cart is limited to 1.


