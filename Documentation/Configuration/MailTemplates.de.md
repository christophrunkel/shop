# Email Konfiguration

Hier findest Du eine Übersicht über die Konfigurationsmöglichkeiten für Emails.

## Email Einstellungen

Die Basiseinstellungen für Empfänger- und Absenderemailadresse wird im TypoScript Setup festgelegt. Du kannst die Einstellungen über eine individuelle Konstanten Datei überschreiben.

Mögliche Einstellungen sind:
_Email Empfänger Name_ Gib hier den Namen des Empfängers/ Empfangsstelle an.
_Email Empfänger E-Mail Adresse_ Gib hier die E-Mail Adresse des Empfängers / Empfangsstelle an.
_Email Absender Name_ Gib hier den Namen des Absenders / Absendestelle an.
_Email Absender E-Mail Adresse_ Gib hier die E-Mail Adresse des Absenders / Absendestelle an.
_Email Typ_ Gib hier an, ob die Email als Plaintext (plain) oder HTML (html) ausgeliefert werden soll.
_Email Footer Text_ Hier hast Du die Möglichkeit einen optionalen Text im Footer der E-Mail anzugeben.
_Email-Betreff_ Hier hast Du die Möglichkeit, den Betreff für verschiedene Checkout-Typen festzulegen.

### Einträge in einer individuellen Konstanten Datei

```typo3_TypoScript
themes.configuration.extension.shop.email.to.name = Mein Shop Name
themes.configuration.extension.shop.email.from.email = shop@beispiel.de
themes.configuration.extension.shop.email.from.name =  Mein Shop Name
themes.configuration.extension.shop.email.to.email = shop@beispiel.de
themes.configuration.extension.shop.email.format = html
themes.configuration.extension.shop.email.footer = Wir bedanken uns für Ihr Vertrauen.

themes.configuration.extension.shop.email.templates.onInvoice.subjectCustomerConfirmation = Shop Product OnInvoice [Templates/Email/Checkout/OnInvoice/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.onInvoice.subjectOrder = Shop Product onInvoice order [Templates/Email/Checkout/OnInvoice/Order.html]
themes.configuration.extension.shop.email.templates.prePayment.subjectCustomerConfirmation = Shop Product PrePayment [Templates/Email/Checkout/PrePayment/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.prePayment.subjectOrder = Shop Product PrePayment [Templates/Email/Checkout/PrePayment/Order.html]
themes.configuration.extension.shop.email.templates.request.subjectCustomerConfirmation = Shop Product Request [Templates/Email/Checkout/Request/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.request.subjectRequest = Shop Product Request [Templates/Email/Checkout/Request/Order.html]

themes.configuration.extension.shop.email.templates.payPal.subjectCustomerConfirmation = Shop Product PayPal [Templates/Email/Checkout/PayPal/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.payPal.subjectOrder = Shop Product PayPal [Templates/Email/Checkout/PayPal/Order.html]
themes.configuration.extension.shop.email.templates.payPalPlus.subjectCustomerConfirmation = Shop Product PayPalPlus [Templates/Email/Checkout/PayPalPlus/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.payPalPlus.subjectOrder = Shop Product PayPalPlus [Templates/Email/Checkout/PayPalPlus/Order.html]
themes.configuration.extension.shop.email.templates.klarna.subjectCustomerConfirmation = Shop Product Klarna [Templates/Email/Checkout/Klarna/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.klarna.subjectOrder = Shop Product Klarna [Templates/Email/Checkout/Klarna/Order.html]
themes.configuration.extension.shop.email.templates.stripe.subjectCustomerConfirmation = Shop Product Stripe [Templates/Email/Checkout/Stripe/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.stripe.subjectOrder = Shop Product Stripe [Templates/Email/Checkout/Stripe/Order.html]
```

### Entries in an individual setup file

```typo3_TypoScript
plugin.tx_shop {
    settings{
        email {
            from {
                name = {$themes.configuration.extension.shop.email.from.name}
                email = {$themes.configuration.extension.shop.email.from.email}
            }

            to {
                name = {$themes.configuration.extension.shop.email.to.name}
                email = {$themes.configuration.extension.shop.email.to.email}
            }

            format = {$themes.configuration.extension.shop.email.format}
            footer (
TYPO3  Shop Extension by coding.ms
www:   www.coding.ms
email: typo3@coding.ms
            )
        }
        basketOrder {
            orderOptions {
                onInvoice {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.onInvoice.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.onInvoice.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.onInvoice.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.onInvoice.subjectOrder}
                        }
                    }
                }
                prePayment {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.prePayment.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.prePayment.subjectCustomerConfirmation}
                        }
                        order {
                            active =  {$themes.configuration.extension.shop.checkout.prePayment.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.prePayment.subjectOrder}
                        }
                    }
                }
                request {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.request.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }

                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.request.subjectCustomerConfirmation}
                        }
                        request {
                            active = {$themes.configuration.extension.shop.checkout.request.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.request.subjectRequest}
                        }
                    }
                }

                payPal {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.payPal.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.payPal.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.payPal.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.payPal.subjectOrder}
                        }
                    }
                }
                payPalPlus {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.payPalPlus.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.payPalPlus.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.payPalPlus.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.payPalPlus.subjectOrder}
                        }
                    }
                }
                klarna {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.klarna.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.klarna.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.klarna.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.klarna.subjectOrder}
                        }
                    }
                }
                stripe {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.stripe.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.stripe.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.stripe.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.stripe.subjectOrder}
                        }
                    }
                }
            }
        }
    }
}
```

### Override Email Templates

Du kannst die E-Mail-Templates überschreiben, indem Du folgendes in der `ext_localconf.php` deines Site-Packages hinzufügst:

```php
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['layoutRootPaths'][1707217385] = 'EXT:your_extension/Resources/Private/Templates/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][1707217385] = 'EXT:your_extension/Resources/Private/Templates/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['partialRootPaths'][1707217385] = 'EXT:your_extension/Resources/Private/Partials/Email/';
```
Verwende als Array-Index einfach den aktuellen Timestamp, um sicher zu gehen das dein Eintrag ganz oben liegt und somit als erstes genommen wird.

Eine Datei/Verzeicnis Struktur könnte wie folgt aussehen:

```text
📄 your_extension/Resources/Private
├─ 📂 Partials
│  ├─ 📂 Email
│  │  ├─ 📄 OrderSummary.html
│  │  └─ 📄 OrderSummaryPlain.txt
└─ 📂 Templates
   └─ 📂 Email
      └─ 📂 Checkout
         ├─ 📂 OnInvoice
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 PrePayment
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 Request
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 Klarna
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 PayPal
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 PayPalPlus
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         └─ 📂 Stripe
            ├─ 📄 CustomerConfirmation.html
            ├─ 📄 CustomerConfirmation.txt
            ├─ 📄 Order.html
            └─ 📄 Order.txt
```
