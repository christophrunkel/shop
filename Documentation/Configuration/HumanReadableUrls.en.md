# Human readable URLs

## Slug configuration (TYPO3 9.5 upwards)

```yaml
routeEnhancers:
  PageTypeSuffix:
    type: PageType
    map:
      shop.json: 1496773586
  ShopProductsPlugin:
    type: Extbase
    limitToPages:
      - 50
    extension: Shop
    plugin: Products
    routes:
      - routePath: '/{product_slug}'
        _controller: 'Product::show'
        _arguments:
          product_slug: product
    defaultController: 'Product::list'
    aspects:
      product_slug:
        type: PersistedAliasMapper
        tableName: tx_shop_domain_model_product
        routeFieldName: slug
        routeValuePrefix: /
  ShopCheckoutPlugin:
    type: Extbase
    limitToPages:
      - 54
    extension: Shop
    plugin: BasketOrder
    routes:
      - routePath: '/{checkout_type}'
        _controller: 'BasketOrder::checkout'
        _arguments:
          checkout_type: type
      - routePath: '/{checkout_type}/confirm'
        _controller: 'BasketOrder::confirmOrder'
        _arguments:
          checkout_type: type
      - routePath: '/{checkout_type}/cancel/{basket_order}'
        _controller: 'BasketOrder::cancel'
        _arguments:
          checkout_type: type
          basket_order: basketOrder
      - routePath: '/{checkout_type}/callback'
        _controller: 'BasketOrder::callback'
        _arguments:
          checkout_type: type
      - routePath: '/{checkout_type}/success/{basket_order}'
        _controller: 'BasketOrder::success'
        _arguments:
          checkout_type: type
          basket_order: basketOrder
      - routePath: '/{checkout_type}/error/{basket_order}'
        _controller: 'BasketOrder::error'
        _arguments:
          checkout_type: type
          basket_order: basketOrder
      - routePath: '/success'
        _controller: 'BasketOrder::success'
      - routePath: '/stripe-callback'
        _controller: 'BasketOrder::stripeCallback'
    aspects:
      checkout_type:
        type: StaticValueMapper
        map:
          on-invoice: onInvoice
          pre-payment: prePayment
          pay-pal: payPal
          pay-pal-plus: payPalPlus
          klarna: klarna
          stripe: stripe
          request: request
      basket_order:
        type: PersistedAliasMapper
        tableName: tx_shop_domain_model_basketorder
        routeFieldName: uid
  ShopSubscriptionsPlugin:
    type: Extbase
    limitToPages:
      - 52
    extension: ShopPro
    plugin: Subscriptions
    routes:
      - routePath: '/stripe-subscription-callback'
        _controller: 'Subscription::stripeSubscriptionCallback'
    defaultController: 'Subscription::list'
  ShopOrderListPlugin:
    type: Extbase
    limitToPages:
      - 48
    extension: ShopPro
    plugin: BasketOrderList
    routes:
      - routePath: '/{username}'
        _controller: 'BasketOrder::list'
        _arguments:
          username: frontendUser
    defaultController: 'BasketOrder::list'
    aspects:
      username:
        type: PersistedAliasMapper
        tableName: fe_users
        routeFieldName: username
```

## Realurl configuration

```php
$configuration = [

    'fixedPostVars' => [
        'shopConfiguration' => [
                0 => [
                    'GETvar' => 'tx_shop_products[action]',
                    'valueMap' => [
                        'show' => '',
                    ],
                    'noMatch' => 'bypass',
                ],
                1 => [
                    'GETvar' => 'tx_shop_products[controller]',
                    'valueMap' => [],
                    'noMatch' => 'bypass',
                ],
                2 => [
                    'GETvar' => 'tx_shop_products[product]',
                    'lookUpTable' => [
                        'table' => 'tx_shop_domain_model_product',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        //'alias_field' => "CONCAT(title, '-', sub_title)",
                        'addWhereClause' => ' AND NOT deleted',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                        'autoUpdate' => 1,
                        'expireDays' => 180,
                    ],
                ],
            ],
            17 => 'shopConfiguration',
    ],

];
```
