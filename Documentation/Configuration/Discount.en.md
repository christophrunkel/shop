# Discounts

Discounts and other costs to be deducted can be configured via *discounts*. These are data records that you create in the product container and which are applied to the price (net).

*   **Label:** This is the label for the discount. This label is also used in the shopping basket, the email and the invoice.
*   **Type:** Type controls whether the value should be a fixed value (*Fixed price*) or a percentage (*In percent*).
*   **Only on order option:** Specify the type of orders where this discount are added.
    *   **all:** Applies to every order.
    *   **on product:** This discount can be specified for an individual product. For example, if a product is outdated, a discount can be selected here.
    *   **Request:** Applies to all checkouts of type *request*
    *   **Pre-payment:** Applies to all checkouts of type  *Pre-payment*
    *   **On-invoice:** Applies to all checkouts of *On-invoice*
    *   **PayPal:** Applies to all checkouts of type *PayPal*
    *   **PayPal-Plus:** Applies to all checkouts of type *PayPal-Plus*
    *   **Klarna:** Applies to all checkouts of type *Klarna*
    *   **Stripe:** Applies to all checkouts of type *Stripe*
*   **Fixed value:** This field is only visible if the *Fixed price* type is selected. This is the fixed value to be used.
*   **Value in percent:** This field is only visible if the *In percent* type is selected. This is the percentage that should be used.

Currently only one discount per product possible. The first matching discount wins.

The order is:
1. Is a discount in product selected
2. Is a discount for checkout type available
3. Is a discount for all order types available



## Examples

A 2% discount for prepayment:

```
label = Discount:
order_option = prePayment
type = percent
value_fixed = 2,00
```

Special product discount:

```
label = Special discount:
order_option = product
type = fixed
value = 2,50
```
