# Sitemap.xml Konfiguration für Produkte

Mit der folgenden Konfiguration kannst Du eine separate Sitemap.xml für die Produkte einrichten:

```typo3_typoscript
plugin.tx_seo {
	config {
		xmlSitemap {
			sitemaps {
				shop {
					provider = CodingMs\Shop\XmlSitemap\RecordsXmlSitemapDataProvider
					config {
						table = tx_shop_domain_model_product
						sortField = sorting
						lastModifiedField = tstamp
						recursive = 1
						pid = 2
						url {
							pageId = 6
							fieldToParameterMap {
								uid = tx_shop_products[product]
							}
							additionalGetParameters {
								tx_shop_products.controller = Product
								tx_shop_products.action = show
							}
							useCacheHash = 1
						}
					}
				}
			}
		}
	}
}
```

Oft ist es so, dass die unterschiedlichen Produkte an verschiedenen Stellen einer Website angezeigt werden. Um nun *Duplicate-Content* auf der Seite oder auch via Sitemap zu vermeiden, hast Du die Möglichkeit eine Canonical-URL in den Produkten anzugeben. Diese URL wird dann auch für die Sitemap.xml verwendet. Hier ist es aktuell nur möglich, URLs in Form von `t3://page?uid=54` zu verwenden.
