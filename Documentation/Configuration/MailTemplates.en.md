# Email configuration

Here you will find an overview of email configuration options.

## Email settings

The basic settings for recipient and sender email address is defined in the TypoScript setup. You can override the settings via an individual constants file.

Possible settings are:
_Email Recipient Name_ Enter the name of the recipient/receiving site here.
_Email Recipient Email Address_ Enter the email address of the recipient / receiving station here.
_Email Sender Name_ Enter the name of the sender / sending point here.
_Email Sender Email Address_ Enter the email address of the sender / sending point here.
_Email Type_ Specify here whether the email should be delivered as plain text (plain) or HTML (html).
_Email Footer Text_ Here you have the possibility to specify an optional text in the footer of the email.
_Email Subject_ Here you have the possibility to specify the subject for different checkout types.

### Entries in an individual constant file

```typo3_TypoScript
themes.configuration.extension.shop.email.to.name = Mein Shop Name
themes.configuration.extension.shop.email.from.email = shop@beispiel.de
themes.configuration.extension.shop.email.from.name =  Mein Shop Name
themes.configuration.extension.shop.email.to.email = shop@beispiel.de
themes.configuration.extension.shop.email.format = html
themes.configuration.extension.shop.email.footer = Wir bedanken uns für Ihr Vertrauen.

themes.configuration.extension.shop.email.templates.onInvoice.subjectCustomerConfirmation = Shop Product OnInvoice [Templates/Email/Checkout/OnInvoice/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.onInvoice.subjectOrder = Shop Product onInvoice order [Templates/Email/Checkout/OnInvoice/Order.html]
themes.configuration.extension.shop.email.templates.prePayment.subjectCustomerConfirmation = Shop Product PrePayment [Templates/Email/Checkout/PrePayment/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.prePayment.subjectOrder = Shop Product PrePayment [Templates/Email/Checkout/PrePayment/Order.html]
themes.configuration.extension.shop.email.templates.request.subjectCustomerConfirmation = Shop Product Request [Templates/Email/Checkout/Request/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.request.subjectRequest = Shop Product Request [Templates/Email/Checkout/Request/Order.html]

themes.configuration.extension.shop.email.templates.payPal.subjectCustomerConfirmation = Shop Product PayPal [Templates/Email/Checkout/PayPal/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.payPal.subjectOrder = Shop Product PayPal [Templates/Email/Checkout/PayPal/Order.html]
themes.configuration.extension.shop.email.templates.payPalPlus.subjectCustomerConfirmation = Shop Product PayPalPlus [Templates/Email/Checkout/PayPalPlus/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.payPalPlus.subjectOrder = Shop Product PayPalPlus [Templates/Email/Checkout/PayPalPlus/Order.html]
themes.configuration.extension.shop.email.templates.klarna.subjectCustomerConfirmation = Shop Product Klarna [Templates/Email/Checkout/Klarna/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.klarna.subjectOrder = Shop Product Klarna [Templates/Email/Checkout/Klarna/Order.html]
themes.configuration.extension.shop.email.templates.stripe.subjectCustomerConfirmation = Shop Product Stripe [Templates/Email/Checkout/Stripe/CustomerConfirmation.html]
themes.configuration.extension.shop.email.templates.stripe.subjectOrder = Shop Product Stripe [Templates/Email/Checkout/Stripe/Order.html]
```

### Entries in an individual setup file

```typo3_TypoScript
plugin.tx_shop {
	settings{
		email {
			from {
				name = {$themes.configuration.extension.shop.email.from.name}
				email = {$themes.configuration.extension.shop.email.from.email}
			}

			to {
				name = {$themes.configuration.extension.shop.email.to.name}
				email = {$themes.configuration.extension.shop.email.to.email}
			}

            format = {$themes.configuration.extension.shop.email.format}
			footer (
TYPO3  Shop Extension by coding.ms
www:   www.coding.ms
email: typo3@coding.ms
			)
		}
        basketOrder {
            orderOptions {
                onInvoice {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.onInvoice.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.onInvoice.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.onInvoice.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.onInvoice.subjectOrder}
                        }
					}
				}
                prePayment {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.prePayment.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.prePayment.subjectCustomerConfirmation}
                        }
                        order {
                            active =  {$themes.configuration.extension.shop.checkout.prePayment.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.prePayment.subjectOrder}
                        }
					}
				}
                request {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.request.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }

                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.request.subjectCustomerConfirmation}
                        }
                        request {
                            active = {$themes.configuration.extension.shop.checkout.request.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.request.subjectRequest}
                        }
					}
				}

                payPal {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.payPal.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.payPal.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.payPal.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.payPal.subjectOrder}
                        }
					}
				}
                payPalPlus {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.payPalPlus.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.payPalPlus.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.payPalPlus.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.payPalPlus.subjectOrder}
                        }
					}
				}
                klarna {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.klarna.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.klarna.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.klarna.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.klarna.subjectOrder}
                        }
					}
				}
                stripe {
                    email {
                        customerConfirmation {
                            active = {$themes.configuration.extension.shop.checkout.stripe.email.customerConfirmation.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                # Customer address data
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.stripe.subjectCustomerConfirmation}
                        }
                        order {
                            active = {$themes.configuration.extension.shop.checkout.stripe.email.order.active}
                            from {
                                name = {$themes.configuration.extension.shop.email.from.name}
                                email = {$themes.configuration.extension.shop.email.from.email}
                            }
                            to {
                                name = {$themes.configuration.extension.shop.email.to.name}
                                email = {$themes.configuration.extension.shop.email.to.email}
                            }
                            subject = {$themes.configuration.extension.shop.email.templates.stripe.subjectOrder}
                        }
					}
				}
			}
		}
	}
}
```

### Override Email Templates

You can override email templates by adding the following to your `ext_localconf.php` in your site-package:

```php
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['layoutRootPaths'][1707217385] = 'EXT:your_extension/Resources/Private/Templates/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][1707217385] = 'EXT:your_extension/Resources/Private/Templates/Email/';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['partialRootPaths'][1707217385] = 'EXT:your_extension/Resources/Private/Partials/Email/';
```

Simply use the current timestamp as the array index to ensure that your entry is at the top and is therefore taken first.

A file/folder structure could look like this:

```text
📄 your_extension/Resources/Private
├─ 📂 Partials
│  ├─ 📂 Email
│  │  ├─ 📄 OrderSummary.html
│  │  └─ 📄 OrderSummaryPlain.txt
└─ 📂 Templates
   └─ 📂 Email
      └─ 📂 Checkout
         ├─ 📂 OnInvoice
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 PrePayment
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 Request
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 Klarna
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 PayPal
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         ├─ 📂 PayPalPlus
         │  ├─ 📄 CustomerConfirmation.html
         │  ├─ 📄 CustomerConfirmation.txt
         │  ├─ 📄 Order.html
         │  └─ 📄 Order.txt
         └─ 📂 Stripe
            ├─ 📄 CustomerConfirmation.html
            ├─ 📄 CustomerConfirmation.txt
            ├─ 📄 Order.html
            └─ 📄 Order.txt
```

