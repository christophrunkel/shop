# Templates

## Overview

The extension is developed according to the coding guidelines of TYPO3 based on Extbase. After installing and adding the static TypoScript templates to your root template, the store extension already brings the necessary TypoScript markup to use individualized templates for the store.

The supplied TypoScript expects you to specify a path under which your own templates can be found via the constant configuration. If a file is not available, the default file that is delivered with the extension will be used.

## Create path to the files

Please put the following TypoScript path in your constants file. The specified path, here "fileadmin", has to be adapted according to your project structure. An overview of how to create a constants file can be found here in the documentation in section [Quickstart](https://www.coding.ms/documentation/typo3-shop/installation/quick-start).

```typo3_TypoScript
themes.resourcesPrivatePath = fileadmin/
```

## Create directories

Starting from the base directory for templates in your project structure, it is _mandatory_ that the folders "Templates", "Partials" and "Layouts" exist. All three folders must be on the same level. The following TypoScript configuration gives an overview of a possible structure. The positions after the 300 can be extended or overwritten as you like. This can be helpful if you want to manage several TYPO3 websites in one system.

```typo3_TypoScript
plugin.tx_shop {
	view {
		templateRootPaths {
			300 = {$plugin.tx_shop.view.templateRootPath}
			400 = {$themes.resourcesPrivatePath}Extensions/Shop/Templates/
		}

		partialRootPaths {
			300 = {$plugin.tx_shop.view.partialRootPath}
			400 = {$themes.resourcesPrivatePath}Extensions/Shop/Partials/
		}

		layoutRootPaths {
			300 = {$plugin.tx_shop.view.layoutRootPath}
			400 = {$themes.resourcesPrivatePath}Extensions/Shop/Layouts/
		}
	}
}
```

## File overview

Below you will find an overview of all template files of the store system, to illustrate the possibilities of the level of customization.

```text
📄 Shop
├─ 📂 Layouts
│  ├─ 📄 Default.html
│  └─ 📄 Widget.html
├─ 📂 Partials
│  ├─ 📂 Basket
│  │  ├─ 📄 BasketContent.html
│  │  ├─ 📄 BasketFooterRow.html
│  │  ├─ 📄 BasketItemRow.html
│  │  └─ 📄 EmptyBasket.html
│  ├─ 📂 Email
│  │  ├─ 📄 OrderSummary.html
│  │  └─ 📄 OrderSummaryPlain.txt
│  ├─ 📂 Product
│  │  ├─ 📂 Buttons
│  │  │  ├─ 📄 AddToBasket.html
│  │  │  ├─ 📄 Compare.html
│  │  │  ├─ 📄 CreatePdf.html
│  │  │  └─ 📄 Details.html
│  │  ├─ 📂 Detail
│  │  │  ├─ 📄 Attributes.html
│  │  │  ├─ 📄 Categories.html
│  │  │  ├─ 📄 Details.html
│  │  │  ├─ 📄 FeatureIcons.html
│  │  │  ├─ 📄 FileCollections.html
│  │  │  ├─ 📄 Headline.html
│  │  │  ├─ 📄 Images.html
│  │  │  ├─ 📄 OtherImages.html
│  │  │  ├─ 📄 QuestionCategories.html
│  │  │  ├─ 📄 RelatedProduct.html
│  │  │  └─ 📄 Tags.html
│  │  └─ 📂 List
│  │  │  ├─ 📂 Filter
│  │  │  │  ├─ 📄 Category.html
│  │  │  │  ├─ 📄 Submit.html
│  │  │  │  ├─ 📄 TagCategorized.html
│  │  │  │  ├─ 📄 TagMultiple.html
│  │  │  │  ├─ 📄 TagSingle.html
│  │  │  │  └─ 📄 Word.html
│  │  │  ├─ 📂 Item
│  │  │  │  ├─ 📄 Default.html
│  │  │  │  └─ 📄 Teaser.html
│  │  │  ├─ 📄 Filter.html
│  │  │  └─ 📄 Sorting.html
│  │  └─ 📄 SingleView.html
│  ├─ 📄 Percent.html
│  └─ 📄 Price.html
└─ 📂 Templates
   ├─ 📂 Basket
   │  ├─ 📄 Show.html
   │  └─ 📄 ShowBasketButton.html
   ├─ 📂 BasketOrder
   │  ├─ 📄 Cancel.html
   │  ├─ 📄 Checkout.html
   │  ├─ 📄 ConfirmOrder.html
   │  ├─ 📄 Error.html
   │  ├─ 📄 Order.html
   │  └─ 📄 Success.html
   ├─ 📂 Email
   │  └─ 📂 Checkout
   │     ├─ 📂 OnInvoice
   │     │  ├─ 📄 CustomerConfirmation.html
   │     │  ├─ 📄 CustomerConfirmation.txt
   │     │  ├─ 📄 Order.html
   │     │  └─ 📄 Order.txt
   │     ├─ 📂 PrePayment
   │     │  ├─ 📄 CustomerConfirmation.html
   │     │  ├─ 📄 CustomerConfirmation.txt
   │     │  ├─ 📄 Order.html
   │     │  └─ 📄 Order.txt
   │     └─ 📂 Request
   │     │  ├─ 📄 CustomerConfirmation.html
   │     │  ├─ 📄 CustomerConfirmation.txt
   │     │  ├─ 📄 Order.html
   │     │  └─ 📄 Order.txt
   └─ 📂 Product
      ├─ 📄 List.html
      ├─ 📄 Show.html
      └─ 📄 ShowQuickSearch.html
```
