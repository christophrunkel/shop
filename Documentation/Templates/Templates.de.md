# Templates

## Allgemeines

Die Extension ist nach den Coding Guidelines von TYPO3 auf Basis von Extbase entwickelt. Nach der Installation und dem Hinzufügen der statischen TypoScript Templates zu Deinem Root-Template, bringt die Shop Extension bereits das nötige TypoScript Markup mit um individualisierte Templates für den Shop nutzen zu können.

Das mitgelieferte TypoScript erwartet, dass Du über die Konstanten-Konfiguration einen Pfad angibst, unter dem Deine eigenen Templates zu finden sind. Sollte eine Datei nicht vorhanden sein, wird die Standard Datei die mit der Extension ausgeliefert wird genutzt.

## Pfad zu den Dateien anlegen

Bitte lege folgenden TypoScript-Pfad in Deiner Konstanten-Datei an. Der angegebene Pfad, hier "fileadmin", ist gemäß Deiner Projektstruktur anzupassen. Einen Überblick, wie Du eine Konstanten-Datei anlegst findest Du hier in der Dokumentation im Abschnitt [Quickstart](https://www.coding.ms/de/dokumentation/typo3-shop/installation/quick-start).

```typo3_TypoScript
themes.resourcesPrivatePath = fileadmin/
```

## Verzeichnisse anlegen

Ausgehend vom Basisverzeichnis für Templates in Deiner Projektstruktur müssen _zwingend_ die Ordner "Templates", "Partials" und "Layouts" vorhanden sein. Alle drei Ordner müssen auf gleicher Ebene liegen. Die nachstehende TypoScript Konfiguration gibt einen Überblick einer Möglichen Struktur. Die Postionen nach der 300 können beliebig erweitert oder Überschrieben werden. Dies kann z.B hilfreich sein, wenn Du mehrere TYPO3-Webseiten in einem System verwalten möchtest.

```typo3_TypoScript
plugin.tx_shop {
	view {
		templateRootPaths {
			300 = {$plugin.tx_shop.view.templateRootPath}
			400 = {$themes.resourcesPrivatePath}Extensions/Shop/Templates/
		}

		partialRootPaths {
			300 = {$plugin.tx_shop.view.partialRootPath}
			400 = {$themes.resourcesPrivatePath}Extensions/Shop/Partials/
		}

		layoutRootPaths {
			300 = {$plugin.tx_shop.view.layoutRootPath}
			400 = {$themes.resourcesPrivatePath}Extensions/Shop/Layouts/
		}
	}
}
```

## Datei Übersicht

Nachstehend findest Du eine Übersicht über alle Template-Dateien des Shop-Systems, zur Veranschaulichung der Möglichkeiten des Individualisierungsgrades.

```text
📄 Shop
├─ 📂 Layouts
│  ├─ 📄 Default.html
│  └─ 📄 Widget.html
├─ 📂 Partials
│  ├─ 📂 Basket
│  │  ├─ 📄 BasketContent.html
│  │  ├─ 📄 BasketFooterRow.html
│  │  ├─ 📄 BasketItemRow.html
│  │  └─ 📄 EmptyBasket.html
│  ├─ 📂 Email
│  │  ├─ 📄 OrderSummary.html
│  │  └─ 📄 OrderSummaryPlain.txt
│  ├─ 📂 Product
│  │  ├─ 📂 Buttons
│  │  │  ├─ 📄 AddToBasket.html
│  │  │  ├─ 📄 Compare.html
│  │  │  ├─ 📄 CreatePdf.html
│  │  │  └─ 📄 Details.html
│  │  ├─ 📂 Detail
│  │  │  ├─ 📄 Attributes.html
│  │  │  ├─ 📄 Categories.html
│  │  │  ├─ 📄 Details.html
│  │  │  ├─ 📄 FeatureIcons.html
│  │  │  ├─ 📄 FileCollections.html
│  │  │  ├─ 📄 Headline.html
│  │  │  ├─ 📄 Images.html
│  │  │  ├─ 📄 OtherImages.html
│  │  │  ├─ 📄 QuestionCategories.html
│  │  │  ├─ 📄 RelatedProduct.html
│  │  │  └─ 📄 Tags.html
│  │  └─ 📂 List
│  │  │  ├─ 📂 Filter
│  │  │  │  ├─ 📄 Category.html
│  │  │  │  ├─ 📄 Submit.html
│  │  │  │  ├─ 📄 TagCategorized.html
│  │  │  │  ├─ 📄 TagMultiple.html
│  │  │  │  ├─ 📄 TagSingle.html
│  │  │  │  └─ 📄 Word.html
│  │  │  ├─ 📂 Item
│  │  │  │  ├─ 📄 Default.html
│  │  │  │  └─ 📄 Teaser.html
│  │  │  ├─ 📄 Filter.html
│  │  │  └─ 📄 Sorting.html
│  │  └─ 📄 SingleView.html
│  ├─ 📄 Percent.html
│  └─ 📄 Price.html
└─ 📂 Templates
   ├─ 📂 Basket
   │  ├─ 📄 Show.html
   │  └─ 📄 ShowBasketButton.html
   ├─ 📂 BasketOrder
   │  ├─ 📄 Cancel.html
   │  ├─ 📄 Checkout.html
   │  ├─ 📄 ConfirmOrder.html
   │  ├─ 📄 Error.html
   │  ├─ 📄 Order.html
   │  └─ 📄 Success.html
   ├─ 📂 Email
   │  └─ 📂 Checkout
   │     ├─ 📂 OnInvoice
   │     │  ├─ 📄 CustomerConfirmation.html
   │     │  ├─ 📄 CustomerConfirmation.txt
   │     │  ├─ 📄 Order.html
   │     │  └─ 📄 Order.txt
   │     ├─ 📂 PrePayment
   │     │  ├─ 📄 CustomerConfirmation.html
   │     │  ├─ 📄 CustomerConfirmation.txt
   │     │  ├─ 📄 Order.html
   │     │  └─ 📄 Order.txt
   │     └─ 📂 Request
   │     │  ├─ 📄 CustomerConfirmation.html
   │     │  ├─ 📄 CustomerConfirmation.txt
   │     │  ├─ 📄 Order.html
   │     │  └─ 📄 Order.txt
   └─ 📂 Product
      ├─ 📄 List.html
      ├─ 📄 Show.html
      └─ 📄 ShowQuickSearch.html
```
