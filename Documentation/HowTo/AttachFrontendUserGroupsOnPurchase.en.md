# Attach frontend user groups on purchase

In order to give a frontend user some groups on a successful purchase, you just need to select the desired frontend user groups in the product.

>	#### Warning: {.alert .alert-danger}
>
>	Please note that this feature is only implemented for checkout types with immediate valuation like PayPal, PayPal-Plus, Klarna and Stripe!
