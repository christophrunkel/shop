# Arbeiten mit Vorschau-Bildern

Die Produkt-Bilder können mit der Checkbox "In Vorschau darstellen" (`showinpreview`) markiert werden, um zu entscheiden ob diese bspw. in der Liste oder auch in Teasern verwendet werden sollen. Dies können ein oder mehrere Bilder sein. Für die Verwendung im Fluid können dann folgende Variablen genutzt werden:

- `{product.imagesPreviewOnly}` - Gibt alle Bilder zurück, welche mit "In Vorschau darstellen" (`showinpreview`) markiert sind
- `{product.imagesNonPreview}` - Gibt alle Bilder zurück, welche nicht mit "In Vorschau darstellen" (`showinpreview`) markiert sind

Verwendung:

```xml
<f:for each="{product.imagesPreviewOnly}" as="image" iteration="imageIteration">
	<f:if condition="{imageIteration.isFirst}">
		<f:then>
			<f:image src="{image.uid}" treatIdAsReference="1" class="first"
					 width="{settings.list.image.width}" height="{settings.list.image.height}"
					 alt="{product.title}" title="{product.title}" />

		</f:then>
		<f:else>
			<f:image src="{image.uid}" treatIdAsReference="1" class="non-first"
					 width="{settings.list.image.width}" height="{settings.list.image.height}"
					 alt="{product.title}" title="{product.title}" />
		</f:else>
	</f:if>
</f:for >
```

## Feature-Icons

Die gleiche Funktion gibt es für die Feature-Icons:

- `{product.featureIconsPreviewOnly}` - Gibt alle Bilder zurück, welche mit "In Vorschau darstellen" (`showinpreview`) markiert sind
- `{product.featureIconsNonPreview}` - Gibt alle Bilder zurück, welche nicht mit "In Vorschau darstellen" (`showinpreview`) markiert sind
