# Products cannot be added to shopping basket

The route enhancers have probably not been configured. If page type `shop.json: 1496773586` is not defined, AJAX requests cannot be routed.

If the shopping basket button still doesn't appear, you may have the static templates in the wrong order. Static templates must follow the order in the static template which defines content elements.
